// Based on Muaz Khan's RecordRTC Repository
//
// https://github.com/muaz-khan/RecordRTC
//
// www.MuazKhan.com

var Storage = {};
var AudioContext = window.AudioContext || window.webkitAudioContext;
var recorder = new AudioRecorder();
var startButton = document.getElementById('btn-start-recording');
var stopButton = document.getElementById('btn-stop-recording');
var download = document.getElementById('btn-download');
var upload = document.getElementById('btn-upload');


startButton.onclick = recorder.start;
stopButton.onclick = recorder.stop;
download.onclick = recorder.download;
upload.onclick = recorder.upload;

function AudioRecorder(config) {

    config = config || {};

    var self = this;
    var mediaStream;
    var audioInput;
    var jsAudioNode;
    var bufferSize = config.bufferSize || 4096;
    var sampleRate = config.sampleRate || 44100;
    var numberOfAudioChannels = config.numberOfAudioChannels || 2;
    var leftChannel = [];
    var rightChannel = [];
    var recording = false;
    var recordingLength = 0;
    var isPaused = false;
    var isAudioProcessStarted = false;
    var final_audio = '';

    this.start = function() {
        setupStorage();


        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(onMicrophoneCaptured)
            .catch(onMicrophoneCaptureError);
    };

    this.stop = function() {

        stopRecording(function(blob) {
            startButton.disabled = false;
            stopButton.disabled = true;
            upload.disabled = false;
            download.disabled = false;


            var url = URL.createObjectURL(blob);
            var audio = document.querySelector("audio");
            audio.src = url;

        });

    };
    this.download = function() {
        var url = URL.createObjectURL(final_audio);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        console.log(a)
        var d = new Date();
        a.download = d.getTime() + 'profile.webm';
        document.body.appendChild(a);
        a.click();
    }
    this.upload = function() {
        console.log('qqqqqqqqqqqqqqqqqqqqq', final_audio);
        uploadBlob(final_audio)
        return false
        var url = URL.createObjectURL(final_audio);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        console.log(a)
        var d = new Date();
        a.download = d.getTime() + 'profile.webm';
        document.body.appendChild(a);
        a.click();
    }

    function uploadBlob(blob) {
        var form = $('#audioForm')[0];
        console.log('formmmmmmmmmm', form);
        var formdata = new FormData(form);
        formdata.append('audio-blob', blob);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let base_url = document.getElementById('base_url').value

        var title = $('.title').val();
        var course_id = $('.course_id').val();
        var reference_number = $('.reference_number').val();
        var grading = $('.grading').val();
        var duration = $('.duration').val();
        var link = $('.link').val();

        $.ajax({
            type: 'POST',
            url: base_url + '/professional/test-quiz/audioFile',
            data: formdata,
            processData: false,
            contentType: false,

            success: function(response) {
                console.log("-------rrrrrrrrrrr", response);
                $(".btn-save").hide()
                $('.btn-upload').show();
                notif({
                    msg: "Your Audio file saved successfully",
                    type: "success"
                });
                upload.disabled = true;
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //error message
                notif({
                    msg: "Server Issue",
                    type: "error"
                });
            }
        });
    }


    function stopRecording(callback) {

        // stop recording
        recording = false;

        // to make sure onaudioprocess stops firing
        audioInput.disconnect();
        jsAudioNode.disconnect();

        mergeLeftRightBuffers({
            sampleRate: sampleRate,
            numberOfAudioChannels: numberOfAudioChannels,
            internalInterleavedLength: recordingLength,
            leftBuffers: leftChannel,
            rightBuffers: numberOfAudioChannels === 1 ? [] : rightChannel

        }, function(buffer, view) {

            self.blob = new Blob([view], {
                type: 'audio/wav'
            });

            self.buffer = new ArrayBuffer(view.buffer.byteLength);
            self.view = view;
            self.sampleRate = sampleRate;
            self.bufferSize = bufferSize;
            self.length = recordingLength;
            final_audio = self.blob;
            callback && callback(self.blob);

            // clearRecordedData();

            isAudioProcessStarted = false;
        });
    }

    function clearRecordedData() {
        leftChannel = rightChannel = [];
        recordingLength = 0;
        isAudioProcessStarted = false;
        recording = false;
        isPaused = false;
    }

    function setupStorage() {
        Storage.ctx = new AudioContext();

        if (Storage.ctx.createJavaScriptNode) {
            jsAudioNode = Storage.ctx.createJavaScriptNode(bufferSize, numberOfAudioChannels, numberOfAudioChannels);
        } else if (Storage.ctx.createScriptProcessor) {
            jsAudioNode = Storage.ctx.createScriptProcessor(bufferSize, numberOfAudioChannels, numberOfAudioChannels);
        } else {
            throw 'WebAudio API has no support on this browser.';
        }

        jsAudioNode.connect(Storage.ctx.destination);
    }

    function onMicrophoneCaptured(microphone) {
        startButton.disabled = true;
        stopButton.disabled = false;

        mediaStream = microphone;

        audioInput = Storage.ctx.createMediaStreamSource(microphone);
        audioInput.connect(jsAudioNode);

        jsAudioNode.onaudioprocess = onAudioProcess;

        recording = true;
    }

    function onMicrophoneCaptureError() {
        console.log("There was an error accessing the microphone. You may need to allow the browser access");
    }

    function onAudioProcess(e) {

        if (isPaused) {
            return;
        }

        if (isMediaStreamActive() === false) {
            if (!config.disableLogs) {
                console.log('MediaStream seems stopped.');
            }
        }

        if (!recording) {
            return;
        }

        if (!isAudioProcessStarted) {
            isAudioProcessStarted = true;
            if (config.onAudioProcessStarted) {
                config.onAudioProcessStarted();
            }

            if (config.initCallback) {
                config.initCallback();
            }
        }

        var left = e.inputBuffer.getChannelData(0);

        // we clone the samples
        leftChannel.push(new Float32Array(left));

        if (numberOfAudioChannels === 2) {
            var right = e.inputBuffer.getChannelData(1);
            rightChannel.push(new Float32Array(right));
        }

        recordingLength += bufferSize;

        // export raw PCM
        self.recordingLength = recordingLength;
    }

    function isMediaStreamActive() {
        if (config.checkForInactiveTracks === false) {
            // always return "true"
            return true;
        }

        if ('active' in mediaStream) {
            if (!mediaStream.active) {
                return false;
            }
        } else if ('ended' in mediaStream) { // old hack
            if (mediaStream.ended) {
                return false;
            }
        }
        return true;
    }

    function mergeLeftRightBuffers(config, callback) {
        function mergeAudioBuffers(config, cb) {
            var numberOfAudioChannels = config.numberOfAudioChannels;

            // todo: "slice(0)" --- is it causes loop? Should be removed?
            var leftBuffers = config.leftBuffers.slice(0);
            var rightBuffers = config.rightBuffers.slice(0);
            var sampleRate = config.sampleRate;
            var internalInterleavedLength = config.internalInterleavedLength;
            var desiredSampRate = config.desiredSampRate;

            if (numberOfAudioChannels === 2) {
                leftBuffers = mergeBuffers(leftBuffers, internalInterleavedLength);
                rightBuffers = mergeBuffers(rightBuffers, internalInterleavedLength);
                if (desiredSampRate) {
                    leftBuffers = interpolateArray(leftBuffers, desiredSampRate, sampleRate);
                    rightBuffers = interpolateArray(rightBuffers, desiredSampRate, sampleRate);
                }
            }

            if (numberOfAudioChannels === 1) {
                leftBuffers = mergeBuffers(leftBuffers, internalInterleavedLength);
                if (desiredSampRate) {
                    leftBuffers = interpolateArray(leftBuffers, desiredSampRate, sampleRate);
                }
            }

            // set sample rate as desired sample rate
            if (desiredSampRate) {
                sampleRate = desiredSampRate;
            }

            // for changing the sampling rate, reference:
            // http://stackoverflow.com/a/28977136/552182
            function interpolateArray(data, newSampleRate, oldSampleRate) {
                var fitCount = Math.round(data.length * (newSampleRate / oldSampleRate));
                //var newData = new Array();
                var newData = [];
                //var springFactor = new Number((data.length - 1) / (fitCount - 1));
                var springFactor = Number((data.length - 1) / (fitCount - 1));
                newData[0] = data[0]; // for new allocation
                for (var i = 1; i < fitCount - 1; i++) {
                    var tmp = i * springFactor;
                    //var before = new Number(Math.floor(tmp)).toFixed();
                    //var after = new Number(Math.ceil(tmp)).toFixed();
                    var before = Number(Math.floor(tmp)).toFixed();
                    var after = Number(Math.ceil(tmp)).toFixed();
                    var atPoint = tmp - before;
                    newData[i] = linearInterpolate(data[before], data[after], atPoint);
                }
                newData[fitCount - 1] = data[data.length - 1]; // for new allocation
                return newData;
            }

            function linearInterpolate(before, after, atPoint) {
                return before + (after - before) * atPoint;
            }

            function mergeBuffers(channelBuffer, rLength) {
                var result = new Float64Array(rLength);

                var offset = 0;
                var lng = channelBuffer.length;

                for (var i = 0; i < lng; i++) {
                    var buffer = channelBuffer[i];
                    result.set(buffer, offset);
                    offset += buffer.length;
                }

                return result;
            }

            function interleave(leftChannel, rightChannel) {
                var length = leftChannel.length + rightChannel.length;

                var result = new Float64Array(length);

                var inputIndex = 0;

                for (var index = 0; index < length;) {
                    result[index++] = leftChannel[inputIndex];
                    result[index++] = rightChannel[inputIndex];
                    inputIndex++;
                }
                return result;
            }

            function writeUTFBytes(view, offset, string) {
                var lng = string.length;
                for (var i = 0; i < lng; i++) {
                    view.setUint8(offset + i, string.charCodeAt(i));
                }
            }

            // interleave both channels together
            var interleaved;

            if (numberOfAudioChannels === 2) {
                interleaved = interleave(leftBuffers, rightBuffers);
            }

            if (numberOfAudioChannels === 1) {
                interleaved = leftBuffers;
            }

            var interleavedLength = interleaved.length;

            // create wav file
            var resultingBufferLength = 44 + interleavedLength * 2;

            var buffer = new ArrayBuffer(resultingBufferLength);

            var view = new DataView(buffer);

            // RIFF chunk descriptor/identifier
            writeUTFBytes(view, 0, 'RIFF');

            // RIFF chunk length
            view.setUint32(4, 44 + interleavedLength * 2, true);

            // RIFF type
            writeUTFBytes(view, 8, 'WAVE');

            // format chunk identifier
            // FMT sub-chunk
            writeUTFBytes(view, 12, 'fmt ');

            // format chunk length
            view.setUint32(16, 16, true);

            // sample format (raw)
            view.setUint16(20, 1, true);

            // stereo (2 channels)
            view.setUint16(22, numberOfAudioChannels, true);

            // sample rate
            view.setUint32(24, sampleRate, true);

            // byte rate (sample rate * block align)
            view.setUint32(28, sampleRate * 2, true);

            // block align (channel count * bytes per sample)
            view.setUint16(32, numberOfAudioChannels * 2, true);

            // bits per sample
            view.setUint16(34, 16, true);

            // data sub-chunk
            // data chunk identifier
            writeUTFBytes(view, 36, 'data');

            // data chunk length
            view.setUint32(40, interleavedLength * 2, true);

            // write the PCM samples
            var lng = interleavedLength;
            var index = 44;
            var volume = 1;
            for (var i = 0; i < lng; i++) {
                view.setInt16(index, interleaved[i] * (0x7FFF * volume), true);
                index += 2;
            }

            if (cb) {
                return cb({
                    buffer: buffer,
                    view: view
                });
            }

            postMessage({
                buffer: buffer,
                view: view
            });
        }

        var webWorker = processInWebWorker(mergeAudioBuffers);

        webWorker.onmessage = function(event) {
            callback(event.data.buffer, event.data.view);

            // release memory
            URL.revokeObjectURL(webWorker.workerURL);
        };

        webWorker.postMessage(config);
    }

    function processInWebWorker(_function) {
        var workerURL = URL.createObjectURL(new Blob([_function.toString(),
            ';this.onmessage =  function (e) {' + _function.name + '(e.data);}'
        ], {
            type: 'application/javascript'
        }));

        var worker = new Worker(workerURL);
        worker.workerURL = workerURL;
        return worker;
    }
}
