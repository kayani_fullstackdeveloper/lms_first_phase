<?php

use Illuminate\Support\Facades\Route;

// install
Route::get('/install', function () {

  // Artisan::call('migrate');
  // Artisan::call('key:generate');
  Artisan::call('config:clear');
  Artisan::call('route:clear');
  Artisan::call('cache:clear');
//   Artisan::call('storage:link');
  //  Artisan::call('db:seed --class=RoleSeeder');
  //   Artisan::call('db:seed --class=AdminSeeder');



});


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;

Route::get('/', function () {
    // echo"lms Develop";
    return view('comming_soon.index');
    // Auth::guard('teacher')->logout();
});

Route::get('/chatshow', function () {
    // echo"lms Develop";
    return view('comming_soon.chat');
    // Auth::guard('teacher')->logout();
});

Route::group(
  [
      'domain' => config('app.short_url'),
      // 'middleware' => ['web']
  ],
  function () {
      Route::get('/', function () {
        // dd(config('app.short_url'));
      Auth::guard('teacher')->logout();
      })->name('start');
  }
);




// teacher route

Route::group(['middleware' => 'auth:teacher'], function () {


Route::group(
    [
        'domain' => '{subdomain}.' . config('app.short_url'),
        'prefix'=>'professional'

    ], function ($subdomain) {

   Route::get('/','TeacherController\DashboardController@index');
   Route::get('/dashboard','TeacherController\DashboardController@dashboard');
   Route::get('/profile','TeacherController\DashboardController@profile');
   Route::get('/profile/view','TeacherController\DashboardController@profile_complete');

   Route::post('/profile/add_profile','TeacherController\DashboardController@add_profile');
   Route::get('/profile/approve-go-live','TeacherController\DashboardController@approveGolive');

  //  media links
   Route::get('/profile/audio','TeacherController\DashboardController@audio_view');
   Route::get('/profile/video','TeacherController\DashboardController@video_view');
   Route::post('/profile/profilevideo','TeacherController\DashboardController@profilevideo');
   Route::post('/profile/profileaudio','TeacherController\DashboardController@profileaudio');
   Route::get('/profile/image','TeacherController\DashboardController@profile_image');
   Route::post('/profile/upload_profile_image','TeacherController\DashboardController@upload_profile_image');
   Route::get('/profile/weblink','TeacherController\DashboardController@web_link');
   Route::post('/profile/weblink','TeacherController\DashboardController@upload_weblink');
   Route::get('/profile/bio','TeacherController\DashboardController@bio_view');
   Route::post('/profile/bioupdate','TeacherController\DashboardController@bioupdate');
   Route::post('/profile/uploadAudio','TeacherController\DashboardController@uploadAudio');
   Route::post('/profile/uploadVideo','TeacherController\DashboardController@uploadVideo');
   Route::post('/profile/webimageupload','TeacherController\DashboardController@webimageupload');

   Route::post('/profile/contact-form','TeacherController\DashboardController@contactForm');

  //  course management

   Route::resource('/course','TeacherController\CourseControler');
   Route::get('courselist','TeacherController\CourseControler@courseList')->name('course.list');
   Route::delete('coursedestroy/{id}','TeacherController\CourseControler@destroy');
   Route::get('coursedetail/{id}','TeacherController\CourseControler@coursedetail')->name('course.coursedetail');
   Route::get('courseoption/{id}','TeacherController\CourseControler@courseoption')->name('course.courseoption');
   Route::get('syllabusoption/{id}','TeacherController\CourseControler@Syllabusoption');
   Route::get('upload_course_material/{id}','TeacherController\CourseControler@upload_course_material');
   Route::get('create_course_material/{id}','TeacherController\CourseControler@create_course_material');
   Route::get('weblink_course_material/{id}','TeacherController\CourseControler@weblink_course_material');
   Route::get('create_weblink_course_material/{id}','TeacherController\CourseControler@create_weblink_course_material');
   Route::post('save_course_material','TeacherController\CourseControler@save_course_material');
   Route::post('save_weblinlk_course_material','TeacherController\CourseControler@save_weblinlk_course_material');
   Route::post('getCourseList','TeacherController\CourseControler@getCourseList');
   Route::post('del_course_material','TeacherController\CourseControler@del_course_material');//del_course_material
   //
  //  course cluster
  Route::get('course/{id}/cluster', 'TeacherController\CourseClusterController@cluster');
  Route::get('course/{id}/cluster/create', 'TeacherController\CourseClusterController@clusterCreate');
  Route::post('course/{id}/cluster/store', 'TeacherController\CourseClusterController@clusterStore');
   Route::get('course/{id}/cluster/edit/{edit}', 'TeacherController\CourseClusterController@clusterEdit');
   Route::post('course/cluster/update/{id}/{edit}', 'TeacherController\CourseClusterController@clusterUpdate');
  Route::get('cluster/delete/{id}', 'TeacherController\CourseClusterController@clusterDelete');

   //self libraby
   Route::get('self-library/{id}','TeacherController\library\SelfstudyController@index');
   Route::get('self-library/online_digital/{id}','TeacherController\library\SelfstudyController@online_digital');
   Route::post('self-library/online_digital','TeacherController\library\SelfstudyController@online_digital');
   Route::post('self-library/addcourselibrary','TeacherController\library\SelfstudyController@addcourselibrary');

   Route::get('self-library/online_digital/create/{id}', 'TeacherController\library\SelfstudyController@create');
   Route::post('self-library/online_digital/store/{id}', 'TeacherController\library\SelfstudyController@store');

   Route::get('self-library/online_digital/edit/{id}/{edit}', 'TeacherController\library\SelfstudyController@edit');
   Route::get('self-library/online_digital/delete/{id}', 'TeacherController\library\SelfstudyController@destory');
   Route::post('self-library/online_digital/update/{id}/{edit}', 'TeacherController\library\SelfstudyController@update');

  Route::post('self-library/online_digital/multiple-delete', 'TeacherController\library\SelfstudyController@deleteMultiple');
// podcast
   Route::get('self-library/podcast/{id}','TeacherController\library\PodcastController@index');
   Route::post('self-library/podcast','TeacherController\library\PodcastController@index');
   Route::post('self-library/podcast/addcoursepodcast','TeacherController\library\PodcastController@addcoursepodcast');
  //  custom podcast
  Route::get('custompodcast/remove/{id}', 'TeacherController\library\Custompodcast@remove');
  // Route::resource('/custompodcast', 'TeacherController\library\Custompodcast');
  Route::get('/custompodcast/{id}', 'TeacherController\library\Custompodcast@index');
  Route::get('/custompodcast/create/{id}', 'TeacherController\library\Custompodcast@create');
  Route::get('/custompodcast/edit/{edit}/{id}', 'TeacherController\library\Custompodcast@edit');
  Route::post('/custompodcast/store/{id}', 'TeacherController\library\Custompodcast@store');
  Route::post('/custompodcast/update/{edit}/{id}', 'TeacherController\library\Custompodcast@update');

  // social media
  Route::get('self-library/social-media/{id}', 'TeacherController\library\SocialMediaController@index');
  Route::get('self-library/social-media/list/{id}/{social}', 'TeacherController\library\SocialMediaController@list');
  Route::get('self-library/social-media/add/{id}/{social}', 'TeacherController\library\SocialMediaController@add');
  Route::post('self-library/social-media/save/{id}/{social}', 'TeacherController\library\SocialMediaController@save');
  Route::get('self-library/social-media/edit/{id}/{social}/{edit}', 'TeacherController\library\SocialMediaController@edit');
  Route::post('self-library/social-media/update/{id}/{social}/{edit}', 'TeacherController\library\SocialMediaController@update');
  Route::post('self-library/social-media/addcoursesocial', 'TeacherController\library\SocialMediaController@addcoursesocial');
// study notes
  Route::get('self-library/study-notes/{id}', 'TeacherController\library\StudynotesController@index');
  Route::post('self-library/study-notes/addcoursenotes', 'TeacherController\library\StudynotesController@addcoursenotes');
  Route::get('self-library/study-notes/create/{id}', 'TeacherController\library\StudynotesController@create');
  Route::post('self-library/study-notes/save/{id}', 'TeacherController\library\StudynotesController@save');
  Route::get('self-library/study-notes/edit/{id}/{edit}', 'TeacherController\library\StudynotesController@edit');
  Route::post('self-library/study-notes/update/{id}/{edit}', 'TeacherController\library\StudynotesController@update');
  //Route::delete('self-library/study-notes/multiple-delete-record', 'TeacherController\library\StudynotesController@multipleDeleteRecord');
  Route::get('self-library/study-notes/delete/{id}', 'TeacherController\library\StudynotesController@destroy');
  //Route::delete('myproductsDeleteAll', [TeacherController\library\StudynotesController::class, 'multipleDeleteRecord']);
  //Route::post('self-library/study-notes/multipleusersdelete', 'TeacherController\library\StudynotesController@multipleusersdelete');
  Route::post('self-library/study-notes/multiple-delete', 'TeacherController\library\StudynotesController@deleteMultiple');
  // study books
  Route::get('self-library/study-book/{id}', 'TeacherController\library\Booklibrary@index');
  Route::get('self-library/study-book/create/{id}', 'TeacherController\library\Booklibrary@create');
  Route::post('self-library/study-book/save/{id}', 'TeacherController\library\Booklibrary@save');
  Route::get('self-library/study-book/edit/{id}/{edit}', 'TeacherController\library\Booklibrary@edit');
  Route::post('self-library/study-book/update/{id}/{edit}', 'TeacherController\library\Booklibrary@update');
  Route::post('self-library/study-book/addcoursebook', 'TeacherController\library\Booklibrary@addcoursebook');

  Route::get('self-library/study-book/delete/{id}', 'TeacherController\library\Booklibrary@destroy');
  Route::post('self-library/study-book/multiple-delete', 'TeacherController\library\Booklibrary@deleteMultiple');

  // self-study-test
  Route::get('self-library/task/list/{id}', 'TeacherController\library\SelflibraryTest@list');
  Route::get('self-library/task/{id}/{type}', 'TeacherController\library\SelflibraryTest@index');

  Route::post('self-library/task/addcoursepractise', 'TeacherController\library\SelflibraryTest@addcoursepractise');
  Route::get('self-library/task/create/{id}/{type}', 'TeacherController\library\SelflibraryTest@create');
  Route::post('self-library/task/save/{id}', 'TeacherController\library\SelflibraryTest@save');
  Route::get('self-library/task/edit/{id}/{edit}', 'TeacherController\library\SelflibraryTest@edit');
  Route::post('self-library/task/update/{id}/{edit}', 'TeacherController\library\SelflibraryTest@update');
  Route::get('self-library/task/delete/{id}/{edit}', 'TeacherController\library\SelflibraryTest@destroy');

 // Route::post('self-library/task/multiple-delete', 'TeacherController\library\SelflibraryTest@deleteMultiple');

  // Assessments list
  Route::get('assessments/section/{id}', 'TeacherController\AssessmentsController@section');
  Route::get('assessments/{id}/{section}', 'TeacherController\AssessmentsController@index');
  Route::get('assessments/type/{id}/{type}', 'TeacherController\AssessmentsController@type');
  Route::post('assessments/get_assessments', 'TeacherController\AssessmentsController@get_assessments');
  Route::post('assessments/save_assessments', 'TeacherController\AssessmentsController@save_assessments');
  Route::post('assessments/update_assessments', 'TeacherController\AssessmentsController@update_assessments');
  Route::post('assessments/delete', 'TeacherController\AssessmentsController@delete');
  Route::get('assessments/generate', 'TeacherController\AssessmentsController@generate_accessment');

  Route::get('assessments-formative/{id}', 'TeacherController\AssessmentsController@Formative');
  Route::get('assessments-weblink/{id}', 'TeacherController\AssessmentsController@weblink');

  Route::get('assessments/course/{id}/type/{type}/section/{section}/register-student', 'TeacherController\AssessmentsController@registerStudentList');

  Route::get('assessments/{id}/{section}/toolkit', 'TeacherController\AssessmentsController@toolkit');
  //Assessment Guidelines
  Route::get('assessments/type/{id}/{type}/guidelines', 'TeacherController\AssessmentsController@CourseSectionGuideline');
  Route::post('assessments/type/{type}/guidelines-create', 'TeacherController\AssessmentsController@GuidelineCreate');

  Route::get('assessments/guideline/{id}/delete', 'TeacherController\AssessmentsController@destroy');

  //attendance
  Route::get('attendance/{id}', 'TeacherController\AttendanceController@section');
  Route::get('attendance_mark/{course_id}/{section_id}', 'TeacherController\AttendanceController@attendance_mark');
   Route::get('attendance/type/{id}/{section_id}/{type}', 'TeacherController\AttendanceController@type');
      Route::get('attendance/form/{id}/{section_id}/{type}', 'TeacherController\AttendanceController@form');
  Route::post('attendance/form', 'TeacherController\AttendanceController@attendanceForm');
  Route::get('attendance/view/{course_id}/{section_id}', 'TeacherController\AttendanceController@attendanceView');

  //  AV
  Route::get('av-library', 'TeacherController\AvLibarayController@index');
  Route::get('av-library/view-all', 'TeacherController\AvLibarayController@viewAllFile');
  Route::get('av-library/view-audio-file', 'TeacherController\AvLibarayController@viewAudioFile');
  Route::get('av-library/view-video-file', 'TeacherController\AvLibarayController@viewVideoFile');

  Route::get('av-library/create-audio-file/{course_id?}', 'TeacherController\AvLibarayController@createAudioFile');
  Route::post('av-library/audio','TeacherController\AvLibarayController@audioFile');
  Route::post('av-library/uploadAudio','TeacherController\AvLibarayController@uploadAudio');

  Route::get('av-library/audio/{id}/edit','TeacherController\AvLibarayController@editAudio');
  Route::post('av-library/update-audio/{id}','TeacherController\AvLibarayController@updateAudio');

  Route::get('av-library/delete-audio/{id}','TeacherController\AvLibarayController@deleteAudio');

  Route::get('av-library/create-video-file/{course_id?}', 'TeacherController\AvLibarayController@createVideoFile');
  Route::post('av-library/video','TeacherController\AvLibarayController@videoFile');
  Route::post('av-library/videodVideo','TeacherController\AvLibarayController@uploadVideo');

  Route::get('av-library/video/{id}/edit','TeacherController\AvLibarayController@editVideo');
  Route::post('av-library/update-video/{id}','TeacherController\AvLibarayController@updateVideo');

  Route::get('av-library/delete-video/{id}','TeacherController\AvLibarayController@deleteVideo');
  Route::get('av-library/delete-audio-video/{id}','TeacherController\AvLibarayController@deleteAudioVideo');
  Route::get('av-library/edit-audio-video/{id}','TeacherController\AvLibarayController@EditAudioVideo');
  Route::post('av-library/update-audio-video/{id}','TeacherController\AvLibarayController@updateAudioVideo');

  //live class
  Route::get('live-class-session/{id}', 'TeacherController\LiveClassController@index');
  Route::get('live-class-session/{id}/room-create', 'TeacherController\LiveClassController@roomCreate');
  Route::post('live-class-session/{id}/room-save', 'TeacherController\LiveClassController@roomSave');
  Route::get('live-class-session/{id}/room-list', 'TeacherController\LiveClassController@getRoomList');
  Route::get('live-class-session/{id}/room-edit/{room}', 'TeacherController\LiveClassController@edit');
  Route::post('live-class-session/{id}/room-update/{room}', 'TeacherController\LiveClassController@update');
  Route::get('live-class-session/{id}/room-delete/{room}', 'TeacherController\LiveClassController@destory');



    //Lesson Lecture
    Route::get('lesson-lecture/{id}/lesson-lecture-list', 'TeacherController\LessonLectureController@index');
    Route::get('lesson-lecture/{id}/lesson-lecture-create', 'TeacherController\LessonLectureController@create');
    Route::post('lesson-lecture/{id}/lesson-lecture-store', 'TeacherController\LessonLectureController@store');
    Route::get('lesson-lecture/{id}/lesson-lecture-edit/{lesson}', 'TeacherController\LessonLectureController@edit');
    Route::post('lesson-lecture/{id}/lesson-lecture-update/{lesson}', 'TeacherController\LessonLectureController@update');
    Route::get('lesson-lecture/{id}/lesson-lecture-delete/{lesson}', 'TeacherController\LessonLectureController@destory');

    //Discussion Boards
    Route::get('discussion-borad/{id}/discussion-borad-list', 'TeacherController\DiscussionBoardController@index');
    Route::get('discussion-borad/{id}/discussion-borad-create', 'TeacherController\DiscussionBoardController@create');
    Route::post('discussion-borad/{id}/discussion-borad-store', 'TeacherController\DiscussionBoardController@store');
    Route::get('discussion-borad/{id}/discussion-borad-edit/{discussion}', 'TeacherController\DiscussionBoardController@edit');
    Route::post('discussion-borad/{id}/discussion-borad-update/{discussion}', 'TeacherController\DiscussionBoardController@update');
    Route::get('discussion-borad/{id}/discussion-borad-delete/{discussion}', 'TeacherController\DiscussionBoardController@destory');

    //Assignment
    Route::get('assignment/{id}/assignment-list', 'TeacherController\AssignmentController@index');
    Route::get('assignment/{id}/assignment-create', 'TeacherController\AssignmentController@create');
    Route::post('assignment/{id}/assignment-store', 'TeacherController\AssignmentController@store');
    Route::get('assignment/{id}/assignment-edit/{assignment}', 'TeacherController\AssignmentController@edit');
    Route::post('assignment/{id}/assignment-update/{assignment}', 'TeacherController\AssignmentController@update');
    Route::get('assignment/{id}/assignment-delete/{assignment}', 'TeacherController\AssignmentController@destory');

    // Assignment audio
    Route::get('assignment/{id}/create-audio', 'TeacherController\AssignmentController@createAudio');
    Route::post('assignment/{id}/create-audio-store', 'TeacherController\AssignmentController@uploadAudio');
    Route::post('assignment/{id}/audio-update/{assignment}', 'TeacherController\AssignmentController@updateAudio');
    Route::post('assignment/audioFile','TeacherController\AssignmentController@audioFile');

    // Assignment audio
    Route::get('assignment/{id}/create-video', 'TeacherController\AssignmentController@createVideo');
    Route::post('assignment/{id}/create-video-store', 'TeacherController\AssignmentController@uploadVideo');
    Route::post('assignment/{id}/video-update/{assignment}', 'TeacherController\AssignmentController@updateVideo');
    Route::post('assignment/videoFile','TeacherController\AssignmentController@videoFile');


    //Test&Quiz
    Route::get('test-quiz/{id}/test-quiz-list', 'TeacherController\TestQuizController@index');
    Route::get('test-quiz/{id}/test-quiz-create', 'TeacherController\TestQuizController@create');
    Route::post('test-quiz/{id}/test-quiz-store', 'TeacherController\TestQuizController@store');
    Route::get('test-quiz/{id}/test-quiz-edit/{testquiz}', 'TeacherController\TestQuizController@edit');
    Route::post('test-quiz/{id}/test-quiz-update/{testquiz}', 'TeacherController\TestQuizController@update');
    Route::get('test-quiz/{id}/test-quiz-delete/{testquiz}', 'TeacherController\TestQuizController@destory');

    // Test&Quiz audio
    Route::get('test-quiz/{id}/test-quiz-create-audio', 'TeacherController\TestQuizController@createAudio');
    Route::post('test-quiz/{id}/create-audio-store', 'TeacherController\TestQuizController@uploadAudio');
    Route::post('test-quiz/{id}/audio-update/{testquiz}', 'TeacherController\TestQuizController@updateAudio');
    Route::post('test-quiz/audioFile','TeacherController\TestQuizController@audioFile');
    // Test&Quiz video
    Route::get('test-quiz/{id}/create-video', 'TeacherController\TestQuizController@createVideo');
    Route::post('test-quiz/{id}/video-update/{testquiz}', 'TeacherController\TestQuizController@updateVideo');
    Route::post('test-quiz/videoFile','TeacherController\TestQuizController@videoFile');

  // logout route
  Route::get('logout/teacher','TeacherController\DashboardController@logout')->name('logout.teacher');

  }
  );
});

Route::group(['middleware' => 'auth:teacher'], function () {


Route::group(
    [
        'domain' => '{subdomain}.' . config('app.short_url'),
        'prefix'=>'student'

    ], function ($subdomain) {

      Route::get('/','Student\DashboardController@index');
      Route::get('/profile','Student\DashboardController@profile');

      Route::get('/profile/weblink','Student\DashboardController@webLink');
      Route::post('/profile/weblink-store','Student\DashboardController@webLinkstore');

      Route::get('/profile/audio','Student\DashboardController@audio');
      Route::post('/profile/audio-store','Student\DashboardController@audioStore');
      Route::post('/profile/record-audio','Student\DashboardController@recordAudio');


      Route::get('/profile/video','Student\DashboardController@video');
      Route::post('/profile/video-store','Student\DashboardController@videoStore');
      Route::post('/profile/record-video','Student\DashboardController@recordVideo');


      Route::get('/profile/image','Student\DashboardController@image');
      Route::post('/profile/upload-image','Student\DashboardController@uploadImage');
      Route::post('/profile/webimageupload','Student\DashboardController@webimageupload');

      Route::get('/profile/pdf','Student\DashboardController@pdf');
      Route::get('profile/pdf/type/{type}', 'Student\DashboardController@pdfType');
      Route::post('profile/pdf/type/store', 'Student\DashboardController@pdfTypestore');



      Route::get('logout/student','Student\DashboardController@logout')->name('logout.student');


    }
    );
});


Route::group(['middleware' => 'auth:teacher'], function () {
    Route::get('/teacher', function () {
  echo "hi t";
});
});

Auth::routes();

Route::get('/login/student', [LoginController::class, 'showStudentLoginForm'])->name('login.student');
Route::get('/register/student', [RegisterController::class, 'showStudentRegisterForm']);
Route::post('/login/student', [LoginController::class,'StudentLogin'])->name('student.login');
Route::post('/register/student', [RegisterController::class,'createStudent'])->name('student.register');

// <-------------- teacher --------------------->

Route::get('/login/teacher', [LoginController::class, 'showTeacherLoginForm'])->name('teacher.login');
Route::get('/register/teacher', [RegisterController::class, 'showTeacherRegisterForm']);
Route::post('/login/teacher', [LoginController::class,'TeacherLogin'])->name('teacher.login');
Route::get('/reg-success/teacher', [RegisterController::class,'reg_success'])->name('teacher.reg-success');
Route::get('/reg-success/student', [RegisterController::class,'studentRegistersuccess'])->name('student.reg-success');
Route::post('/register/teacher', [RegisterController::class,'createteacher'])->name('teacher.register');
Route::get('/forget-password/teacher', 'TeacherController\ForgotPasswordController@getEmail')->name('teacher.reset-password');
Route::post('/forget-password/teacher', 'TeacherController\ForgotPasswordController@postEmail')->name('forget-password.sendlink');

Route::get('/reset-password/teacher/{token}', 'TeacherController\ResetPasswordController@getPassword')->name('reset-password.teacher');
Route::post('/reset-password', 'TeacherController\ResetPasswordController@updatePassword');
//end teacher route <---------------------------------------->
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//<-------------------------Admin route-------------------------->

Route::get('admin/login','Admin\AdminLoginController@login')->name('admin.login');
// Route::get('admin/login','Admin\AdminLoginController@login')->name('admin.login');
Route::get('admin/logout','Admin\AdminLoginController@logout')->name('admin.logout');
Route::post('admin/login', [LoginController::class,'adminLogin'])->name('admin.login');
Route::group(['name'=>'admin','middleware' => 'AdminValidate','prefix' => 'admin'], function() {

Route::get('/','Admin\AdminDashboardController@index');
Route::get('/dashboard','Admin\AdminDashboardController@index')->name('admin.index');
Route::resource('/users','Admin\AdminUser');
Route::get('/get-courses','Admin\AdminDashboardController@getCourses');
Route::get('/professional/get-courses','Admin\AdminProfessional@getCoursedatatable')->name('professional.courses.datatable');

// professionals
Route::resource('/professionals','Admin\AdminProfessional');
Route::get('/professional/datatable','Admin\AdminProfessional@datatable')->name('professional.datatable');

Route::get('/podcastcategory/datatable','Admin\PostCategoiesController@datatable')->name('podcastcategory.datatable');
Route::resource('podcastcategory','Admin\PostCategoiesController');
Route::get('/podcast/datatable','Admin\PodcastController@datatable')->name('podcast.datatable');
Route::resource('podcast','Admin\PodcastController');
Route::get('/library/datatable','Admin\LibraryConroller@datatable')->name('library.datatable');
Route::resource('library','Admin\LibraryConroller');
//professional end

// admin datatables
Route::get('/user/datatable','Admin\AdminUser@datatable')->name('user.datatable');
});
