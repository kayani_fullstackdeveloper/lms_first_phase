<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEduAndProfToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {

            $table->unsignedBigInteger('edu_qualification')->nullable();
            $table->unsignedBigInteger('professional_edu')->nullable();
            $table->foreign('edu_qualification')->references('id')->on('educational_qualifications')->onDelete('cascade');
            $table->foreign('professional_edu')->references('id')->on('professional_qualifications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            //
        });
    }
}
