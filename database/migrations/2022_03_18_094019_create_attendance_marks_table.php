<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_marks', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger('attendance_id');
          $table->unsignedBigInteger('std_id');
          $table->string('attendance')->nullable();

          $table->foreign('std_id')->references('id')->on('students')->onDelete('cascade');
          $table->foreign('attendance_id')->references('id')->on('attendances')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_marks');
    }
}
