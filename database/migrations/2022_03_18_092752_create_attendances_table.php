<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger('course_id');
          $table->unsignedBigInteger('professional_id');
          $table->unsignedBigInteger('section_id');
          $table->string('type');
          $table->string('reference_number');
          $table->date('date');
          $table->time('time');
          $table->string('duration');

          $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
          $table->foreign('professional_id')->references('id')->on('teachers')->onDelete('cascade');
          $table->foreign('section_id')->references('id')->on('course_publish_models')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
