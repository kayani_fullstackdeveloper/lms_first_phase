<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger('course_id');
          $table->unsignedBigInteger('p_id');
          $table->string('reference_number');
          $table->string('title');
          $table->string('grading');
          $table->string('date_open')->nullable();
          $table->string('date_close')->nullable();
          $table->string('formate');
          $table->string('link');
          $table->string('description')->nullable();
          $table->string('file')->nullable();
          $table->timestamps();

          $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
          $table->foreign('p_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
