<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooklibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booklibraries', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('writer_name');
            $table->string('link');
            $table->string('isbn');
            $table->integer('user_id');
            $table->string('user_guard');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booklibraries');
    }
}
