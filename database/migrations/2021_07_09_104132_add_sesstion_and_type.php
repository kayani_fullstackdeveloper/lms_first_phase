<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSesstionAndType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessments_models', function (Blueprint $table) {
            //
          
            $table->enum('type',['formative','assessments','summative']);
            $table->unsignedBigInteger('section_id');
            $table->foreign('section_id')->references('id')->on('course_publish_models')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessments_models', function (Blueprint $table) {
            //
        });
    }
}
