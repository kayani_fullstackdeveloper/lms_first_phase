<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_boards', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger('course_id');
          $table->unsignedBigInteger('p_id');
          $table->string('session_number');
          $table->string('title');
          $table->string('date');
          $table->string('start_time');
          $table->string('duration');
          $table->string('password');
          $table->timestamps();

          $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
          $table->foreign('p_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_boards');
    }
}
