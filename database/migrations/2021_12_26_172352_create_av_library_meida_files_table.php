<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvLibraryMeidaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('av_library_meida_files', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('category');
            $table->string('reference_number');
            $table->text('file');
            $table->string('type');
            $table->string('guard_name');
            $table->unsignedBigInteger('f_id');
            $table->unsignedBigInteger('course_id');
            $table->timestamps();

            $table->foreign('f_id')->references('id')->on('teachers')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('av_library_meida_files');
    }
}
