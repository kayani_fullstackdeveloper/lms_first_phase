<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSocialToTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->string('LinkedIn')->nullable();
            $table->string('Pinterest')->nullable();
            $table->string('Reddit')->nullable();
            $table->string('Snapchat')->nullable();
            $table->string('Tik_tok')->nullable();
            $table->string('Quora')->nullable();
            $table->string('Website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            //
        });
    }
}
