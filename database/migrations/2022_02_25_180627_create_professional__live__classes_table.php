<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalLiveClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional__live__classes', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger('course_id');
          $table->unsignedBigInteger('p_id');
          $table->string('session_number');
          $table->string('title');
          $table->string('professionalUrl');
          $table->string('visiterUrl');
          $table->string('duration');
          $table->string('type');
          $table->string('date');
          $table->string('start_time');
          $table->string('password');
          $table->timestamps();

          $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
          $table->foreign('p_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional__live__classes');
    }
}
