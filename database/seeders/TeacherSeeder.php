<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert([
            'name' => 'ahmad',
            'user_name' => 'ahmad12',
            'email' => 'ahmad@ahmad.com',
            'domain_name' => 'ahmad12',
            'profile_live'=>0,
            'password' => bcrypt('Software12@'),
        ]);
    }
}
