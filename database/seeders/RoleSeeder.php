<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
 
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        //  teacher role and permission
        Permission::create(['name' => 'Primary','guard_name'=>'teacher']);
        Permission::create(['name' => 'Secondary','guard_name'=>'teacher']);
        Permission::create(['name' => 'Post_secondary','guard_name'=>'teacher']);
        $role = Role::create(['name' => 'Teacher','guard_name'=>'teacher']);
        //other
        $role = Role::create(['name' => 'Trainer','guard_name'=>'teacher']);
        $role = Role::create(['name' => 'Coach','guard_name'=>'teacher']);
        $role = Role::create(['name' => 'Consultant','guard_name'=>'teacher']);
        $role = Role::create(['name' => 'Admin','guard_name'=>'admin']);
            // ->givePermissionTo(['Primary', 'Secondary','Post_secondary']);

    }
}
