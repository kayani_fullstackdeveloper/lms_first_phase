<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'coure_title' => 'CMD12',
            'date_start' => '2022-02-02',
            'end_date' => '2022-03-03',
            'price' => '1000',
            'f_id'=>1,
            'guard' => 'teacher',
            'course_description' => '<p>html</p>',
            'currency' => 'PKR',
        ]);
    }
}
