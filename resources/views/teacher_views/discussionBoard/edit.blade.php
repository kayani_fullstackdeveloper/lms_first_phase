@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                 <div class="col-md-4 " >
                    <div class=" form-control     color-bar-green  text-white " >
                        <a target="blank" href="https://viizard.com/chat/agent.php" class="font-weight-bold text-decoration-none"> Live class Session</a>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Attendance Register</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Access Clearance</p>
                      </div>
                      <div class=" form-control  color-bar-gray  text-white" >
                        <p class="font-weight-bold">Email Invitation</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Timetable / Schedule</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Assessment</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Test/Quizzes/ Assignments/ MCQ</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Notifications</p>
                      </div>
                </div>
                 <div class="col-md-8">
                   <form method="POST" action="{{url('professional/discussion-borad/'.$course->id.'/discussion-borad-update',$discussion)}}" enctype="multipart/form-data">
                     @csrf

                     <input type="hidden"  name="course_id" value="{{$course->id}}"/>
                     <input type="hidden"  name="type" value="Lesson Lecture"/>
                   <div class="row">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Session Number</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <div class="form-group @error('session_number') has-danger @enderror" >
                         <input type="text" name="session_number" class="form-control" value="{{$discussion->session_number}}">
                         @error('session_number')
                           <label class="error mt-2 text-danger">{{ $message }}</label></br>
                         @enderror
                       </div>
                     </div>
                   </div>
                   <div class="row mt-2">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Date</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <div class="form-group @error('date') has-danger @enderror" >
                         <input type="date" name="date" class="form-control" value="{{$discussion->date}}">
                         @error('date')
                           <label class="error mt-2 text-danger">{{ $message }}</label></br>
                         @enderror
                       </div>
                     </div>
                   </div>
                   <div class="row mt-2">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Title</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <div class="form-group @error('title') has-danger @enderror" >
                         <input type="text" name="title" class="form-control" value="{{$discussion->title}}">
                         @error('title')
                           <label class="error mt-2 text-danger">{{ $message }}</label></br>
                         @enderror
                       </div>
                     </div>
                   </div>
                   <div class="row mt-2">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Start Time</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <div class="form-group @error('start_time') has-danger @enderror" >
                         <input type="time" name="start_time" class="form-control" value="{{$discussion->start_time}}">
                         @error('start_time')
                           <label class="error mt-2 text-danger">{{ $message }}</label></br>
                         @enderror
                       </div>
                     </div>
                   </div>
                   <div class="row mt-2">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Duration</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <select class="form-control @error('duration') has-danger @enderror" name="duration" id="duration">
                         <option value="15">15</option>
                         <option value="30">30</option>
                         <option value="45">45</option>
                       </select>
                       @error('duration')
                         <label class="error mt-2 text-danger">{{ $message }}</label></br>
                       @enderror
                     </div>
                   </div>
                   <div class="row mt-2">
                     <div class="col-md-4">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">password</p>
                       </div>
                     </div>
                     <div class="col-md-6">
                       <div class="form-group @error('password') has-danger @enderror" >
                         <input type="password" name="password" class="form-control" value="{{$discussion->password}}">
                         @error('password')
                           <label class="error mt-2 text-danger">{{ $message }}</label></br>
                         @enderror
                       </div>
                     </div>
                   </div>
                   <div class="row mt-2 d-flex justify-content-center">
                     <div class="col-md-4">
                       <button type="submit" class="text-decoration-none w-100  border-0 text-white">
                         <div class=" form-control text-white font-weight-bold text-center" style="background:#5638FF;">
                           Create
                         </div>
                       </button>
                     </div>
                   </div>
                 </form>
                </div>

             </div>


        </div>
    </div>
</section>
@endsection
