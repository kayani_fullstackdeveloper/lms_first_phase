@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                 <div class="col-md-4 " >
                    <div class=" form-control     color-bar-green  text-white " >
                        <a target="blank" href="https://viizard.com/chat/agent.php" class="font-weight-bold text-decoration-none"> Live class Session</a>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Attendance Register</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Access Clearance</p>
                      </div>
                      <div class=" form-control  color-bar-gray  text-white" >
                        <p class="font-weight-bold">Email Invitation</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Timetable / Schedule</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Assessment</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Test/Quizzes/ Assignments/ MCQ</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Notifications</p>
                      </div>
                </div>
                 <div class="col-md-8">
                   <div class="row">
                     <div class="col-md-8">
                       <div class=" form-control color-bar-gray text-white" >
                         <p class="font-weight-bold">Discussion Board Live Session Listing</p>
                       </div>
                     </div>
                     <div class="col-md-3">
                         <a href="{{url('professional/discussion-borad/'.$course->id.'/discussion-borad-create')}}" class="text-decoration-none w-100  border-0 text-white">
                           <div class=" form-control text-white font-weight-bold text-center" style="background:#5638FF;">
                             Create
                           </div>
                         </a>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <table class="table table-striped table-bordered  mt-2" style="border-collapse: separate;">
                        <thead class="" style="background:black;">
                          <tr class="mt-2">
                            <th class="text-white">Session Number</th>
                            <th class="text-white">Date</th>
                            <th class="text-white">Title</th>
                            <th class="text-white">Start Time </th>
                            <th class="text-white">Duration </th>
                            <th class="text-white">Action </th>
                          </tr>
                        </thead>
                        <tbody class="mt-3">
                          @foreach($discussions as $discussion)
                          <tr>
                            <td>{{$discussion->session_number}}</td>
                            <td>{{$discussion->date}}</td>
                            <td>{{$discussion->title}}</td>
                            <td>{{$discussion->start_time}}</td>
                            <td>{{$discussion->duration}}</td>
                            <td class="">
                              <a href="{{url('professional/discussion-borad/'.$course->id.'/discussion-borad-edit',$discussion)}}" class="font-weight-bold text-decoration-none   btn btn-sm  bg-primary border-0 text-white">
                                  <span>Edit</span>
                              </a>
                              <a href="{{url('professional/discussion-borad/'.$course->id.'/discussion-borad-delete',$discussion)}}" class="font-weight-bold text-decoration-none  btn btn-sm  color-bar-red border-0 text-white">
                                  <span>Delete</span>
                              </a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                     </div>
                   </div>
                </div>

             </div>


        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
