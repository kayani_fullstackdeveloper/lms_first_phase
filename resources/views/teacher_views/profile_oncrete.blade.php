@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('custom_css')
<style type="text/css">
    #checkbox-large{
      bottom: .2rem;
      left: .2rem;
  width: 1.25rem;
  height: 1.25rem;
    }
  </style>
@endsection
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-4 alert " style="background:#FCFF33"  role="alert">
        <div class="row ">
       <div class="col-md-6 "><h3 class="">Step 1</h3></div>
       <div class="col-md-6 text-right">
           <h3>
                <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">

               Profile
            </h3>
        </div>
       </div>
    </div>

</div>

</section>
<section class="mt-4" >
    <form action="{{ url('professional/profile/add_profile') }}" method="post">
        @csrf
    <div class="row  ">
        <div class="col-sm-11 offset-md-1">
            @include('common.flash-message')
        </div>
    <div class="col-sm-11 offset-md-1 alert  " style="border-top: solid gray 1px; border-left: solid gray 1px;border-bottom: solid black;border-right: solid black;">
        <div class="row ">
            <div class="col-sm-12 ">
                <p class="text-secondary">Select  from the following, are you a: (you can select more than one) </p>
            </div>

        </div>

            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " name="teacher" type="checkbox" name="" value="true" @if(old('teacher'))
                                    checked
                                @endif @if (Auth::user()->hasRole('Teacher'))
                                    checked
                                @endif >
                                <label for="my-input" class="form-check-label">Teacher </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="teacher_type[]" value="Primary"@if (old('teacher_tyoe')=='Primary')
                                checked
                                @endif
                                @if (Auth::user()->can('Primary'))
                                    checked
                                @endif
                                >
                                <label for="my-input" class="form-check-label">Primary  </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="teacher_type[]" value="Secondary"@if (old('teacher_tyoe')=='Secondary')
                                checked
                                @endif
                                @if (Auth::user()->can('Secondary'))
                                    checked
                                @endif
                                >
                                <label for="my-input" class="form-check-label">Secondary  </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="teacher_type[]" value="Post_secondary"@if (old('teacher_tyoe')=='Post_secondary')
                                checked
                                @endif
                                @if (Auth::user()->can('Post_secondary'))
                                    checked
                                @endif
                                >
                                <label for="my-input" class="form-check-label">Post-secondary </label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            @if ($errors->has('teacher_type'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('teacher_type') }}</strong>
                                  </small>
                                @endif
                        </div>


                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Trainer  </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label"> L&D </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">T&D  </label>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-1 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Other  </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 ">

                            <div class="form-check">
                             <input class="form-control" type="text" name="">
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Coach   </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label"> Career  </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Life   </label>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-2 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Leadership   </label>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-1 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Other  </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 ">

                            <div class="form-check">
                             <input class="form-control" type="text" name="">
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Mentor </label>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 ">

                            <div class="form-check">
                             <input class="form-control" type="text" name="">
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Consultant    </label>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 ">

                            <div class="form-check">
                             <input class="form-control" type="text" name="" placeholder="Please Specify">
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-11 offset-sm-1">
                    <div class="row text-secondary">

                        <div class="col-md-2 col-sm-4 ">

                            <div class="form-check">
                                <input id="checkbox-large" class="form-check-input " type="checkbox" name="" value="true">
                                <label for="my-input" class="form-check-label">Other     </label>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 ">

                            <div class="form-check">
                             <input class="form-control" type="text" name="" placeholder="Please Specify">
                            </div>
                        </div>


                    </div>

                </div>
            </div>

    </div>

</div>
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-2 mt-4">
            <a href="{{ url('professional') }}" class="text-decoration-none   text-secondary ">
                <p class="btn  btn-outline-dark" type="">Go Back &nbsp;  <span class="fa fa-undo fa-3x"></span></p>
                </a>

    </div>
    <div class="col-sm-2 mt-4">
        <button class="btn  btn-outline-dark" type="submit">Save And Continue   <span class="fa fa-redo fa-3x"></span></button>

    </div>
</div>
    </form>
</section>

@endsection
