@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control color-bar-green" >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <label  style=" " class="form-control text-center bg-secondary text-white">Practise {{ucfirst(Request()->type)}}</label>
                    <img src="{{ asset('css/site_assets/images/common/floder.jpg') }}" class=" " alt="">
                    
                </div>
                <div class="col-md-9   ">
                    
                    <div class="card">
                        <div class="card-header">
                            Add Practise  {{ucfirst(Request()->type)}}
                        </div>
                        <div class="card-body">
                            <form method="POST"  action="{{ url('professional/self-library/task/save',[$course->id]) }}" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <input type="hidden" name="type" value="{{Request()->type}}">
                            <div class="row">
                                <div class="col-sm-12">
                                     <div class="form-group">
                                         <label for="my-select">Title</label>
                                         <input class="form-control" type="text" name="title" value="{{old('title')}}">
                                         @if ($errors->has('title'))
                                             <small class="text-danger">Please Add title</small>
                                         @endif
                                     </div>   
                                 </div>
                                 
                                
                                <div class="row mb-3">
                                    
                                    <div class="col-sm-12 media files " >
                                        <div class="form-group">
                                            <label for="my-input">File</label>
                                            <input id="my-input" class="" type="file" name="media" >
                                            @if ($errors->has('media'))
                                             <small class="text-danger">Please Add doc</small>
                                         @endif
                                        </div>    
                                    </div>
                                   
                                       
                                </div> 
                                 <div class="col-sm-12">
                                    <input type="submit" value="save" class="btn btn-primary float-right">     
                                </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    
                </div>
             </div>
             
        
        </div>
    </div>
    
</section>
@endsection
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush