@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control color-bar-green" >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <a class="text-decoration-none " href="{{url('professional/courseoption',$course->id)}}">
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                        </a>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row ">
                
                <div class="col-md-12">
                    <div  class="card tab-box"  style=" border-color: green;border-style: dashed">
                        
                        <div  class="card-body">
                            <h3 class="card-title  text-center">Practise</h3>
                            
                        </div>
                        
                    </div>
                    
                    
                </div>
                
             </div>
             
        
        </div>
        <div class="col-md-10">
            <div class="row ">
                
                <div class="col-md-3">
                    <div  class="card tab-box syllabus-color"  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/task',[$course->id,'test'])}}">
                         <div  class="card-body">
                            <h3 class="card-title  text-center">Tests</h3>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div  class="card tab-box syllabus-color"  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/task',[$course->id,'quiz'])}}">
                            <div  class="card-body">
                            <h3 class="card-title  text-center">Quizzes</h3>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div  class="card tab-box syllabus-color"  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/task',[$course->id,'mcq'])}}">
                            <div  class="card-body">
                            <h3 class="card-title  text-center">MCQ's</h3>
                        </div>
                        </a>
                    </div>
                </div>

             </div>
             
        
        </div>
    </div>
    @csrf
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')
    <script>
        $('.add_notes').click(function (e) { 
    e.preventDefault();
    var _token = $("input[name=_token]").val();
    let course_id=$('.courde_id').data('course')
    let studynotes_id=$(this).data('notes')
    $.ajax({
     url:"{{ url('professional/self-library/test/addcoursenotes') }}",
     method:"POST",
     data:{_token:_token, course_id,studynotes_id},
     success:function(data)
     {
        if(data==1)
          {
            notif({
                msg: " Already Linked ",
                type: "warning"
                });

          }else
          {
            notif({
                msg: " Successfully Linked",
                type: "success"
                });
          }
     }
   });


    
});
    </script>
    
@endpush
