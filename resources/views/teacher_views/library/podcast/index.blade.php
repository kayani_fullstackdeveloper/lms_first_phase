@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.rightMenu')
@push('custom_cs')
<style>
label:before {
  /* content: url("https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/unchecked_checkbox.png"); */
  position: absolute;
  z-index: 100;
}
:checked+label:before {
  /* content: url("https://cdn0.iconfinder.com/data/icons/social-messaging-ui-color-shapes/128/check-square-green-50.png"); */
    content: url("{{asset('css/site_assets/images/trueicon.png')}}")
}
input[type=checkbox] {
  display: none;
}

label {
  margin: 10px;
}
</style>
    
@endpush
<section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green course" data-course_id="{{$course->id}}" >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <span  style=" " class="form-control text-center bg-secondary text-white">Podcast</span>   
                </div>
                 
                 <div class="col-md-2 " >
                    <span  style=" " class="form-control text-center bg-secondary text-white">Search Tearm</span>
                 </div>
                 <div class="col-md-7 " >
                   <input  style=" border-color: green;border-style: dashed" class="form-control search_bar" name="" >
                 </div>
             </div>
             
        
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"> 
            <img class="mt-5" style="width:100px;" src="{{ asset('css/site_assets/images/social/podcast.jpg') }}">
            <div class="row">
                <div class="col-sm-12 mt-3">
                    <button class="btn btn-primary btn-sm course_link">link to Course</button>
                </div>
            </div>
            <a href="{{ url("professional/custompodcast",[request()->id]) }}">
            <img data-toggle="tooltip" title="Add new Podcast!" class="mt-5" style="width:80px;" src="{{ asset('css/site_assets/images/microphone.png') }}">
            </a>
        </div>
       
        <div class="col-md-9   " >
            <div class="" id="table_data">
            <div class="row">
                @foreach ($podcast as $item)
                    @if ($item->type=='link')
                        @php
                        $url=$item->audio_video_link;
                            
                        @endphp
                    @else
                    @php
                    $url=asset("storage/podcast/$item->audio_video_link");
                     @endphp                    
                        
                    @endif
                <div class="col-md-3 col-sm-3 col-xs-3 mt-4">
                    <input type="checkbox" value="{{$item->id}}" style="" id="myCheckbox{{$item->id}}" />
                    <label for="myCheckbox{{$item->id}}">
                    <img src="{{ url("storage/podcast/thambnail/$item->image") }}" class="img-fluid " alt="">
                    </label>
                    <a href="{{$url}}" class="text-decoration-none" target="blank">
                    <p class="text-center">{{$item->title}}</p>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {{$podcast->links('pagination::bootstrap-4')}}
                </div>
            </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12">
                    <h3>Your Podcast List</h3>
                </div>
            </div>
            <div class="row">
                @foreach ($user_podcast as $item)
                    @if ($item->type=='link')
                        @php
                        $url=$item->audio_video_link;
                            
                        @endphp
                    @else
                    @php
                    $url=asset("storage/podcast/$item->audio_video_link");
                     @endphp                    
                        
                    @endif
                <div class="col-md-3 col-sm-3 col-xs-3 mt-4">
                    <input type="checkbox" value="{{$item->id}}" style="" id="myCheckbox{{$item->id}}" />
                    <label for="myCheckbox{{$item->id}}">
                    <img src="{{ url("storage/podcast/thambnail/$item->image") }}" class="img-fluid " alt="">
                    </label>
                    <a href="{{$url}}" class="text-decoration-none" target="blank">
                    <p class="text-center">{{$item->title}}</p>
                    </a>
                </div>
                @endforeach
            </div>
           
            
            
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10 mt-4  " >
            
        
        </div>
    </div>
</section>
@csrf
@endsection
@push('custom_js')
    <script>
        $(document).on('click', '.page-link', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
            });

            function fetch_data(page)
            {
            var _token = $("input[name=_token]").val();
            $.ajax({
                url:"{{ url('professional/self-library/podcast') }}",
                method:"POST",
                data:{_token:_token, page:page},
                success:function(data)
                {
                $('#table_data').html(data);
                }
            });
            }
            $('.search_bar').keyup(function (e) { 
                var _token = $("input[name=_token]").val();
                let filter=$(this).val()
            $.ajax({
                url:"{{ url('professional/self-library/podcast') }}",
                method:"POST",
                data:{_token:_token, filter},
                success:function(data)
                {
                $('#table_data').html(data);
                }
            });
            });


        $('.course_link').click(function (e) { 
            e.preventDefault();
            var list = $.map($('input:checkbox:checked'), function(c){return c.value; })
            if(list.length>0)
            {
                var _token = $("input[name=_token]").val();
                let course_id=$('.course').data('course_id')
               
            $.ajax({
                url:"{{ url('professional/self-library/podcast/addcoursepodcast') }}",
                method:"POST",
                data:{_token:_token, list,course_id},
                success:function(data)
                {   
                    $.each(data.list, function (indexInArray, valueOfElement) { 
                        console.log(valueOfElement)
                        
                        warning_noti(''+valueOfElement+' Already Linked')

                                
                    });
                    if(data.save>0)
                    {   
                        
                        success_noti(''+data.save+' Podcast linked')
                    }
                }
            });
            }else{
                warning_noti('Please Select Podcast') 
            }

            
        });
      



 </script>

@endpush
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
    
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
    
@endpush
