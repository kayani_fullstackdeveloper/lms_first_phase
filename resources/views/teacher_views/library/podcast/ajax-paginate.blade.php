<div class="row">
    @foreach ($podcast as $item)
        @if ($item->type=='link')
            @php
            $url=$item->audio_video_link;
                
            @endphp
        @else
        @php
        $url=asset("storage/podcast/$item->audio_video_link");
         @endphp                    
            
        @endif
    <div class="col-md-3 col-sm-3 col-xs-3 mt-4">
        <input type="checkbox" value="{{$item->id}}" style="" id="myCheckbox{{$item->id}}" />
        <label for="myCheckbox{{$item->id}}">
        <img src="{{ url("storage/podcast/thambnail/$item->image") }}" class="img-fluid " alt="">
        </label>
        <a href="{{$url}}" class="text-decoration-none" target="blank">
        <p class="text-center">{{$item->title}}</p>
        </a>
    </div>
    @endforeach
</div>
<div class="row">
    <div class="col-sm-12">
        {{$podcast->links('pagination::bootstrap-4')}}
    </div>
</div>
@if ($podcast->isEmpty())
<div class="col-md-12 mt-5">
    <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
        
        <div  class="card-body">
            <h3 class="card-title  text-center"> No Result Found</h3>
            
        </div>
        
    </div>
    
</div>    
@endif