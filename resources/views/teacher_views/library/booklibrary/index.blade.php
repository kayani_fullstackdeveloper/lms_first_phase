@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
{{-- <section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control color-bar-green" >
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<div class="container">
    <div class="row">
      <div class="col-md-2">
        <label  class="form-control form-control-sm text-center bg-secondary text-white">Books</label>
        <img src="{{ asset('css/site_assets/images/common/floder.jpg') }}">
        
      </div>
      <div class="col-md-10">
        <div class="row">
            <div class="col-md-2">
                <a href="#" class="btn btn-sm font-weight-bold text-decoration-none w-100  btn  color-bar-green border-0 text-white"> 
                    Weblink/ share link 
                  </a>
            </div>
            <div class="col-md-5">
                <input type="text" class="form-control form-control-sm text-center searchInput" placeholder="https://archive.org" aria-label="Username" aria-describedby="basic-addon1">
                
            </div>
            <div class="col-md-2">
                <a href="{{url('professional/self-library/study-book/create',[$course->id])}}" class="font-weight-bold text-decoration-none w-100  btn btn-sm color-bar-yellow border-0 text-dark">
                    Add New
                </a>
                
            </div>
            <div class="col-md-3">
                <button type="button" class="font-weight-bold text-decoration-none w-100  btn btn-sm color-bar-yellow border-0 text-dark"> 
                    Action 
                </button>
                
            </div>
        </div>
        <div id="searchList">
            @foreach ($books as $key => $item)
            <div class="row mt-2 list">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                        <strong>Title</strong>  
                                </div>
                                <div class="col-md-3">
                                    <strong>Author Name</strong>  
                                </div>
                                <div class="col-md-3 ">
                                    <strong>ISBN</strong>
                                </div>
                                <div class="col-md-3 ">
                                    <strong>Url</strong>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col">
                                    <hr class="mb-5">
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-3">
                                        {{$item->title}}
                                    
                                </div>
                                <div class="col-md-3">
                                    {{$item->writer_name}}
                                
                            </div>
                            <div class="col-md-3">
                                <strong>{{ $item->isbn }}</strong>
                            </div>
                                <div class="col-md-3">
                                    
                                    <a href="{{ asset('storage/booklibrary/'.$item->link) }}" class="" target="_BLANK">{{ asset('storage/booklibrary/'.$item->link) }}</a>
                                </div>
                               
                                
                            </div>
                        </div>


                    </div>
                    
                    {{-- <div class="card">
                        <div class="card-body" style="padding: 0.25rem;">
                        <small><a href="{{ $url }}" class="" target="_BLANK">{{ $url }}</a></small>
                        </div>
                    </div> --}}
                </div>
            
                <div class="col-md-3 text-center">
                    <a href="{{url('professional/self-library/study-book/edit',[$course->id,$item->id])}}" class="edit font-weight-bold text-decoration-none   btn btn-sm  color-bar-green border-0 text-dark color-bar-green" title="Edit" data-toggle="tooltip">Update</a>
                    <a href="{{url('professional/self-library/study-book/delete',$item->id)}}" class=" delete font-weight-bold text-decoration-none  btn btn-sm  color-bar-red border-0 text-dark" title="delete" data-toggle="tooltip">Delete</a>
                
                </div>
                
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-sm-12">
                {{$books->links('pagination::bootstrap-4')}}
            </div>
        </div> 
        @if($books->count()==0)
        <div class="col-md-12">
            <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                
                <div  class="card-body">
                    <h3 class="card-title  text-center">  List Empty</h3>
                    
                </div>
            </div>
        </div>
        @endif 
      </div>
    </div>
</div>


@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')
    <script>
        $('.add_books').click(function (e) { 
    e.preventDefault();
    var _token = $("input[name=_token]").val();
    let course_id=$('.courde_id').data('course')
    let book_id=$(this).data('books')
    $.ajax({
     url:"{{ url('professional/self-library/study-book/addcoursebook') }}",
     method:"POST",
     data:{_token:_token, course_id,book_id},
     success:function(data)
     {
        if(data==1)
          {
            notif({
                msg: " Already Linked ",
                type: "warning"
                });

          }else
          {
            notif({
                msg: " Successfully Linked",
                type: "success"
                });
          }
     }
   });


    
});
    </script>

<script language="javascript">
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush
