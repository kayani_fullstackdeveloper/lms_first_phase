@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.rightMenu')
<section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                       
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>

                        
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <label  style=" " class="form-control text-center bg-secondary text-white">Social Media</label>
                    <img src="assets/images/common/floder.jpg" class=" " alt="">

                </div>
                <div class="col-md-9   ">
                    <div class="row">
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'facebook']) }}">
                            <img width="" class="btn-blank" src="{{ asset('css/site_assets/images/social/fb.png') }} ">
                            </a>
                        </div> 
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'instagram']) }}">
                            <img width="" class="btn-blank" src="{{asset('css/site_assets/images/social/instagram.png')}}">
                            </a>
                        </div> 
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'linkedin']) }}">
                            <img width="" class="btn-blank" src="{{asset('css/site_assets/images/social/linkedin.png')}}">
                            </a>
                        </div> 
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'twitter']) }}">
                            <img width="" class="btn-blank" src="{{asset('css/site_assets/images/social/twitter.png')}}">
                            </a>
                        </div> 
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'google']) }}">
                            <img width="" class="btn-blank" src="{{asset('css/site_assets/images/social/google.png')}}">
                            </a>
                        </div> 
                        <div class="col-md-4 ">
                            <a href="{{ url('professional/self-library/social-media/list', [$course->id,'youtube']) }}">
                            <img width="" class="btn-blank" src="{{asset('css/site_assets/images/social/youtube.jpg')}}">
                            </a>
                        </div>    
                       </div>
                    
                    
                </div>
             </div>
             
        
        </div>
    </div>
    
</section>
@endsection
