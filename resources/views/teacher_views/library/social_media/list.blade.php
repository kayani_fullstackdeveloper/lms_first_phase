@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.rightMenu')
<section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                       
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>

                        
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <label  style=" " class="form-control text-center bg-secondary text-white">Social Media {{ucfirst(request()->social)}}</label>
                    <img src="{{ asset('css/site_assets/images/common/floder.jpg') }}" class=" " alt="">
                    <a href="{{url('professional/self-library/social-media/add',[$course->id,request()->social])}}" class=" radius-30 mt-2 mt-lg-0">
                        <span  style=" " class="form-control text-center bg-secondary text-white">Add New</span>
                    </a>
                     <a href="{{url('professional/self-library/social-media',[$course->id])}}" class=" radius-30 mt-4 ">
                        <span  style=" " class="form-control text-center bg-secondary text-white">Back</span>
                    </a>

                </div>
                <div class="col-md-9   ">
                    <div class="row">
                        @foreach ($social as $item)
                        
                        <div class="col-md-12">
                            <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                                
                                <div  class="card-body row">
                                    <div class="col-sm-6">
                                    <h3 class="card-title ">{{$item->title}}</h3>
                                    <small>{{$item->description}}</small>
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-4 float-right">
                                        
                                        <a target="blank" href="{{ $item->link }} " class="btn btn-sm btn-success">View</a>
                                        <button  data-social="{{$item->id}}" class="btn btn-sm btn-success add_social">Add in Course</button>
                                        <a target="blank" href="{{url('professional/self-library/social-media/edit',[$course->id,request()->social,$item->id])}}" class="btn btn-sm btn-success">Edit</a>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        @endforeach
                        @if($social->count()==0)
                        <div class="col-md-12">
                            <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                                
                                <div  class="card-body">
                                    <h3 class="card-title  text-center"> {{ucfirst(request()->social)}} List Empty</h3>
                                    
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        @endif 
                       </div>
                    
                    
                </div>
             </div>
             
        
        </div>
    </div>
    @csrf
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')
    <script>
        $('.add_social').click(function (e) { 
    e.preventDefault();
    var _token = $("input[name=_token]").val();
    let course_id=$('.courde_id').data('course')
    let social_id=$(this).data('social')
    $.ajax({
     url:"{{ url('professional/self-library/social-media/addcoursesocial') }}",
     method:"POST",
     data:{_token:_token, course_id,social_id},
     success:function(data)
     {
        if(data==1)
          {
            notif({
                msg: " Already Linked ",
                type: "warning"
                });

          }else
          {
            notif({
                msg: " Successfully Linked",
                type: "success"
                });
          }
     }
   });


    
});
    </script>
    
@endpush
