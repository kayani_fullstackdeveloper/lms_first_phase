@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control color-bar-green" >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <label  style=" " class="form-control text-center bg-secondary text-white">Study Notes</label>
                    <img src="{{ asset('css/site_assets/images/common/floder.jpg') }}" class=" " alt="">
                   

                </div>
                <div class="col-md-9   ">
                    
                    <div class="card">
                        <div class="card-header">
                            Add Social Media
                        </div>
                        <div class="card-body">
                            <form method="POST"  action="{{ url('professional/self-library/study-notes/update',[$course->id,$data->id]) }}" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                            <div class="row">
                                <div class="col-sm-12">
                                     <div class="form-group">
                                         <label for="my-select">Title</label>
                                         <input class="form-control" type="text" name="title" value="{{$data->title}}">
                                         @if ($errors->has('title'))
                                             <small class="text-danger">Please Add title</small>
                                         @endif
                                     </div>   
                                 </div>
                                 <div class="col-sm-12">
                                     <div class="form-group">
                                         <label for="my-select">Description</label>
                                         <textarea id="my-textarea"  class="form-control" name="description" rows="3">{{$data->description}}</textarea>
                                         @if ($errors->has('description'))
                                             <small class="text-danger">Please Enter Description</small>
                                         @endif
                                     </div>   
                                 </div>
                                 <div class="col-md-12">
                                    <label for="validationCustom01" class="form-label">Type</label><br>
                                    <div class="form-check form-check-inline">
                                        <input  class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="link"  @if ($data->type=='link') checked @endif>
                                        <label class="form-check-label" for="inlineRadio2">Link</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="files"  @if ($data->type=='files') checked @endif>
                                        <label class="form-check-label" for="inlineRadio2">File</label>
                                    </div>
                                    
                                    @if ($errors->has('type'))
                                    <div class="text-danger">Type Required</div>
                                    @else
                                    <div class="valid-feedback">Looks good!</div>
                                    @endif
                                    
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-12 media link " style="@if ($data->type=='link') checked @else display: none @endif">
                                        <div class="form-group">
                                            <label for="my-input">link</label>
                                            <input id="my-input" class="form-control" value="@if ($data->type=='link') {{$data->link}}  @endif" type="url" name="link" >
                                        </div>    
                                    </div>
                                    @if ($data->type=='link')
                                    @php
                                        $url=$data->link;
                                    @endphp
                                    @endif
                                    @if ($data->type=='files')
                                        @php
                                            $url=asset("storage/studynotes/$data->link");
                                        @endphp
                                    @endif
                                    <div class="col-sm-12 media files " style="@if ($data->type=='files') checked @else display: none @endif">
                                        <div class="form-group">
                                            <label for="my-input">File</label>
                                            <input id="my-input" class="form-control" type="file" value="@if ($data->type=='files') {{ asset('storage/studynotes/'.$data->link) }}  @endif" name="file" >
                                            <a href="{{ $url }}" class="" target="_BLANK">{{ Str::limit($url, 30) }}</a>

                                        </div>    
                                    </div>
                                   
                                       
                                </div> 
                                 <div class="col-sm-12">
                                    <input type="submit" value="save" class="btn btn-primary float-right">     
                                </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    
                </div>
             </div>
             
        
        </div>
    </div>
    
</section>
@endsection
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush