@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.rightMenu')
@push('custom_cs')
    <!-- Bootstrap CSS -->
	<link href="{{asset('admin_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet">
@endpush


<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <span  style=" " class="form-control text-center bg-secondary text-white">Podcast</span>   
                </div>
                 
                 <div class="col-md-2 " >
                    <span  style=" " class="form-control text-center bg-secondary text-white">Search Tearm</span>
                 </div>
                 <div class="col-md-7 " >
                   <input  style=" border-color: green;border-style: dashed" class="form-control search_bar" name="" >
                 </div>
             </div>
             
        
        </div>
    </div>
    <div class="row">
     <div class="col-md-12 " >
        <div class="card mt-5">
            <div class="card-body">
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">
                            <a href="{{ url('professional/custompodcast') }}" class="float-left btn btn-primary radius-30 mt-2 mt-lg-0"><i class="bx bxs-plus-square"></i>Back </a>
                            </div>   
                    </div>
                </div>
                <form  method="post" action="{{ url('professional/custompodcast/store',[request()->id]) }}" enctype="multipart/form-data" class="row g-3 needs-validation" novalidate="">
                    @csrf
                    @method('post')
                    <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="validationCustom01" class="form-label"> Title</label>
                        <input type="text" class="form-control @if($errors->has('title')) is-invalid @else in-valid  @endif" id="validationCustom01" name="title" value="{{ old('title') }}" >
                        @if ($errors->has('title'))
                        <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                        @else
                        <div class="valid-feedback">Looks good!</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                       <div class="form-group">
                           <label class="form-label" for="my-select">Category</label>
                           <select id="" class="form-select mb-3" name="cat_id">
                               @foreach ($podcats as $item)
                                   <option value="{{$item->id}}" >{{$item->title}}</option>
                               @endforeach
                           </select>
                           @if ($errors->has('cat_id'))
                           <div class="invalid-feedback">Please select Category</div>
                           @else
                           <div class="valid-feedback">Looks good!</div>
                           @endif
                       </div>
                       
                    </div>
                    <div class="col-md-4">
                        <label for="validationCustom01" class="form-label">Media Type</label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="link">
                            <label class="form-check-label" for="inlineRadio2">Link</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="audio">
                            <label class="form-check-label" for="inlineRadio2">Audio</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="video">
                            <label class="form-check-label" for="inlineRadio2">Video</label>
                        </div>
                        @if ($errors->has('type'))
                        <div class="text-danger">Media Required</div>
                        @else
                        <div class="valid-feedback">Looks good!</div>
                        @endif
                        
                    </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-4 media link " style="display: none">
                            <div class="form-group">
                                <label for="my-input">link</label>
                                <input id="my-input" class="form-control" type="url" name="link" required>
                            </div>    
                        </div>
                        <div class="col-sm-4 media audio " style="display: none">
                            <div class="form-group">
                                <label for="my-input">Audio</label>
                                <input id="my-input" class="form-control" type="file" name="audio" required>
                            </div>    
                        </div>
                        <div class="col-sm-4 media video " style="display: none">
                            <div class="form-group">
                                <label for="my-input">Video</label>
                                <input id="my-input" class="form-control" type="file" name="video" required>
                            </div>    
                        </div> 
                        <div class="col-sm-4  " >
                            <div class="form-group">
                                <label for="my-input">Thumbnail Image</label>
                                <input id="my-input" class="form-control" type="file" name="image">
                            </div>    
                        </div>    
                    </div> 
                    
                    
                   
                    
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
            
            
            
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10 mt-4  " >
            
        
        </div>
    </div>
</section>

@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
 
    
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
    
@endpush
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush
