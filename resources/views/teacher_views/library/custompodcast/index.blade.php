@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.rightMenu')


<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <a href="{{ url('professional/self-library/podcast',[request()->id]) }}" class=" radius-30 mt-2 mt-lg-0">

                    <span  style=" " class="form-control text-center bg-secondary text-white">Podcast</span>   
                    </a>
                </div>
                 
                 <div class="col-md-2 " >
                    <a href="{{ url('professional/custompodcast/create',[request()->id]) }}" class=" radius-30 mt-2 mt-lg-0">

                    <span  style=" " class="form-control text-center bg-secondary text-white">Add New</span></a>
                 </div>
                 <div class="col-md-7 " >
                   <input  style=" border-color: green;border-style: dashed" class="form-control search_bar" name="" >
                 </div>
             </div>
             
        
        </div>
    </div>
    <div class="row ">
    @foreach ($data as $item)
        @if ($item->type=='link')
                        @php
                        $url=$item->audio_video_link;
                            
                        @endphp
                    @else
                    @php
                    $url=asset("storage/podcast/$item->audio_video_link");
                     @endphp                    
                        
                    @endif
        <div class="col-md-10  mt-5">
            <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                
                <div  class="card-body row">
                    <div class="col-sm-6">
                    <h3 class="card-title ">{{$item->title}}</h3>
                    </div>
                    <div class="col-sm-6 float-right">
                        @if ($item->type=='link')
                            
                        <a target="blank" href="{{ $url }} " class="btn btn-sm btn-success">View</a>
                        @else
                        <a target="blank" href="{{$url }} " class="btn btn-sm btn-success">View</a>
                        @endif
                        {{-- <button  data-library="{{$item->id}}" class="btn btn-sm btn-success add_library">Add in Course</button> --}}
                        <a href="{{ url("professional/custompodcast/edit",['edit'=>$item->id,'id'=>request()->id])}}" class="btn btn-sm  btn-info">Edit</i></a>&nbsp;
                        <a class="btn btn-danger btn-sm  ms-3" href="{{ url("professional/custompodcast/remove?edit=$item->id",['id'=>request()->id]) }}">Delete</a>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        @endforeach
     
    </div>
</section>
 {{-- delete form  --}}
 <form style="display: none;" id="delete_form" method="POST" 
 action="" >
<input type="hidden" name="id" id="delete_key">
@method('DELETE')
@csrf  
</form>

@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
 <script>
      $(document).on("click", ".btn-delete", function() { 
        var $ele = $(this).parent().parent().parent();
        var url= $(this).data('remote');
        
        // var dltUrl = url+"/"+id;
        if (confirm("Are you sure to delete?"))
	{ 
		$.ajax({
			url: url,
			type: "DELETE",
			cache: false,
			data:{
				_token:'{{ csrf_token() }}'
			},
			success: function(dataResult){
                success_noti('Record Delete')
				$ele.fadeOut().remove();
			}
		});
    }
	});
 </script>
    
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
