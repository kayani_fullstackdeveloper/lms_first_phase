

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                @foreach ($library as $item)
                    
                <div class="col-md-12">
                    <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                        
                        <div  class="card-body row">
                            <div class="col-sm-6">
                            <h3 class="card-title ">{{$item->title}}</h3>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4 float-right">
                                @if ($item->type=='link')
                                    
                                <a target="blank" href="{{ $item->file_link }} " class="btn btn-sm btn-success">View</a>
                                @else
                                <a target="blank" href="{{ asset("storage/library/$item->file_link") }} " class="btn btn-sm btn-success">View</a>
                                @endif
                                <button  data-library="{{$item->id}}" class="btn btn-sm btn-success add_library">Add in Course</button>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                @endforeach
                @if($library->isEmpty())

                <div class="col-md-12">
                    <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                        
                        <div  class="card-body">
                            <h3 class="card-title  text-center"> No Result Found</h3>
                            
                        </div>
                        
                    </div>
                    
                </div>
                @endif
                {{$library->links('pagination::bootstrap-4')}}

                
        </div>
        
        </div>
    </div>
</section>

<script>
    $('.add_library').click(function (e) { 
    e.preventDefault();
    var _token = $("input[name=_token]").val();
    let course_id=$('.courde_id').data('course')
    let library_id=$(this).data('library')
    $.ajax({
     url:"{{ url('professional/self-library/addcourselibrary') }}",
     method:"POST",
     data:{_token:_token, course_id,library_id},
     success:function(data)
     {
        if(data==1)
          {
            notif({
                msg: "<b>Error:</b> Already Linked ",
                type: "error"
                });

          }else
          {
            notif({
                msg: "<b>Success:</b> Successfully Linked",
                type: "success"
                });
          }
     }
   });


    
});
</script>
    

