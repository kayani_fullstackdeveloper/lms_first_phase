@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu') 
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>  -->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                {{-- <div class="col-md-12">
                    <div  class="card tab-box   text-center "  style=" border-color: green;border-style: dashed">
                        
                        <div  class="card-body " style="cursor: pointer">
                            <h3 class="card-title ">Web Links
                            </h3>
                        </div>
                        <ul class="list-group" style="display: none">
                            <a class="list-group-item list-group-item-dark  text-uppercase" >Libraries</a>
                            <a class="list-group-item list-group-item-action " href="{{url('professional/self-library/online_digital',$course->id)}}">Online/Digital</a>
                            <a class="list-group-item list-group-item-action " href="{{url('professional/self-library/podcast',$course->id)}}">Podcast</a>
                            <a class="list-group-item list-group-item-action " href="{{url('professional/self-library/social-media',$course->id)}}">Social Media</a>
                        </ul>
                    </div>
                    
                </div> --}}
                <div class="col-md-12">
                    <div class="card tab-box  text-center "  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/online_digital',$course->id)}}">

                        <div class="card-body">
                            <h3 class="card-title ">Web Links
                            </h3>
                        </div>
                    </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card tab-box  text-center "  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/study-notes',$course->id)}}">

                        <div class="card-body">
                            <h3 class="card-title ">Study Notes
                            </h3>
                        </div>
                    </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card tab-box  text-center "  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/study-book',$course->id)}}">

                        <div class="card-body">
                            <h3 class="card-title ">Books
                            </h3>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card tab-box  text-center "  style=" border-color: green;border-style: dashed">
                        <a class="text-decoration-none " href="{{url('professional/self-library/task/list',$course->id)}}">

                        <div class="card-body">
                            <h3 class="card-title ">Practise
                            </h3>
                        </div>
                        </a>
                    </div>
                </div>
                
            
        </div>
        
        </div>
    </div>
</section>
@endsection
@push('custom_js')

<script>
    
    $('.tab-box').mouseup(function () {
         
        $(this).find('ul').toggle('300');
    });
    $('.tab-box').mouseleave(function () { 
        $(this).find('ul').hide('300');
        
    });
    
</script>
    
@endpush