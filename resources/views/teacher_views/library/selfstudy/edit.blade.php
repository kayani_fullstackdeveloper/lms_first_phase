@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control color-bar-green" >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>  -->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
        </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-3 " >
                    <label  style=" " class="form-control text-center bg-secondary text-white">Books</label>
                    <img src="{{ asset('css/site_assets/images/common/floder.jpg') }}" class=" " alt="">
                    {{-- <a href="{{url('professional/self-library/study-book',[$course->id])}}" class=" radius-30 mt-2 mt-lg-0">

                        <span  style=" " class="form-control text-center bg-secondary text-white">Back</span></a> --}}

                </div>
                <div class="col-md-9   ">
                    
                    <div class="card">
                        <div class="card-header">
                            Edit Book
                        </div>
                        <div class="card-body">
                            <form  method="POST" action="{{ url('professional/self-library/online_digital/update',[$course->id,$data->id]) }}" enctype="multipart/form-data" class="  needs-validation" novalidate="">
                                @csrf
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control @error('title')is-invalid @enderror"
                                                   name="title" value="{{$data->title}}">
                                            @error('title')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                
                                    {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="form-lable mb-2">Display</label>
                                            <select id="my-select" class="form-control" name="display">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="my-input" class="mt-2">Display Order</label>
                                            <input id="my-input" min="1" class="form-control" type="number" name="displayorder" value="{{old('displayorder')}}">
                                            @if ($errors->has('displayorder'))
                                        <div class="invalid-feedback">{{ $errors->first('displayorder') }}</div>
                                        @else
                                        <div class="valid-feedback">Looks good!</div>
                                        @endif
                                        </div>
                                    </div> --}}
                            
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Media Type</label>
                                            <br>
                                            <div class="form-check form-check-inline">
                                                <input  class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="link"  @if ($data->type=='link') checked @endif>

                                                <label class="form-check-label" for="inlineRadio2">Link</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input @if ($data->type=='file') checked @endif class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="file">
                                                <label class="form-check-label" for="inlineRadio2">File</label>
                                            </div>
                                            
                                            @if ($errors->has('type'))
                                            <div class="text-danger">Media Required</div>
                                            @else
                                            <div class="valid-feedback">Looks good!</div>
                                            @endif
                                        </div>
                                    </div>
                            
                                </div>
                                <div class="row">
                                    <div class="col-md-6 media link" style="@if ($data->type=='link') checked @else display: none @endif">
                                        <div class="form-group">
                                            <label>link</label>
                                            
                                                   <input id="my-input" name="link" class="form-control @error('link')is-invalid @enderror"  value="@if ($data->type=='link') {{$data->file_link}}  @endif">
                                                   
                                            @error('link')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                
                                    <div class="col-md-6 media file" style="@if ($data->type=='file') checked @else display: none @endif">
                                        <div class="form-group">
                                            <label>File</label>
                                            
                                                <input id="my-input" class="form-control" type="file" value="{{$data->file_link}}" name="file" required>
                                            @error('file')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                
                
                                  {{--   <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control @error('title')is-invalid @enderror"
                                                   name="title" value="{{ old('title') }}">
                                            @error('title')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    --}}
                            
                            
                                </div>
                            
                            
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                    
                    
                </div>
             </div>
             
        
        </div>
    </div>
    
</section>
@endsection
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush