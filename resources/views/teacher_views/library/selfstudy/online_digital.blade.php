@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
{{-- @include('teacher_views.inc.rightMenu') --}}
@include('teacher_views.inc.courseNavMenu') 

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center color-bar-green"  >
                        <div class="card-body">
                            <h3 class="card-title ">
                                Self Study library
                            </h3>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row mb-3 justify-content-center">
            <div class="col-md-4 ">
                <div class="card tab-box  text-center color-bar-yellow" style="height: 30px;" >
                    <div class="card-body">
                        <p class="card-title text-dark text-uppercase font-weight-bolder" style="margin-top: -16px;">
                            Multimedia Library
                        </p>
                    </div>
                </div>
            </div> 
        </div>
        
        </div>
        </div>
</section>
<div class="container">
    <div class="row">
     
      <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
                <a href="#" class="btn btn-sm font-weight-bold text-decoration-none w-100  btn  color-bar-green border-0 text-white"> 
                    Weblink/ share link 
                  </a>
            </div>
            <div class="col-md-5">
                <input type="text" class="form-control form-control-sm text-center searchInput" placeholder="https://archive.org" aria-label="Username" aria-describedby="basic-addon1">
                
            </div>
            <div class="col-md-2">
                
                <a href="{{url('professional/self-library/online_digital/create',[$course->id])}}"  class="font-weight-bold text-decoration-none w-100  btn btn-sm  color-bar-yellow border-0 text-dark "> 
                        Add 
                    </a>
                
            </div>
            <div class="col-md-3">
                <button type="button" class="font-weight-bold text-decoration-none w-100  btn btn-sm color-bar-yellow border-0 text-dark"> 
                    Action 
                </button>
                
            </div>
        </div>
        <div id="searchList">
            @foreach ($library as $key => $item)
            <div class="row mt-2 list">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                        <strong>Title</strong>  
                                </div>
                                <div class="col-md-4 ">
                                    <strong>Type</strong>
                                </div>
                                <div class="col-md-4 ">
                                    <strong>Url</strong>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col">
                                    <hr class="mb-5">
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-4">
                                        {{$item->title}}
                                    
                                </div>
                                <div class="col-md-4">
                                    {{$item->type}}
                                    {{-- <button  data-library="{{$item->id}}" class="btn btn-sm btn-success add_library">Add in Course</button> --}}
                                </div>
                                <div class="col-md-4">
                                    @if ($item->type=='link')
                                        
                                    <a target="blank" href="{{ $item->file_link }} " class="btn btn-sm btn-success">{{ Str::limit(asset("storage/library/$item->file_link"), 35) }}</a>
                                    @else
                                    <a target="blank" href="{{ asset("storage/library/$item->file_link") }} " class="btn btn-sm btn-success">{{ Str::limit(asset("storage/library/$item->file_link"), 35) }}</a>
                                    @endif
                                    {{-- <button  data-library="{{$item->id}}" class="btn btn-sm btn-success add_library">Add in Course</button> --}}
                                </div>
                               
                               
                                
                            </div>
                        </div>


                    </div>
                    
                </div>
            
                <div class="col-md-3  text-center">
                         
                            
                            <a href="{{url('professional/self-library/online_digital/edit',[$course->id,$item->id])}}" class="delete font-weight-bold text-decoration-none   btn btn-sm  color-bar-green border-0 text-dark color-bar-green" title="Edit" data-toggle="tooltip">Update</a>
                            <a href="{{url('professional/self-library/online_digital/delete',$item->id)}}" class="edit font-weight-bold text-decoration-none  btn btn-sm  color-bar-red border-0 text-dark" title="delete" data-toggle="tooltip">Delete</a>
                       
                    
                </div>
            </div>
            @endforeach
        </div> 
        <div class="row">
            <div class="col-sm-12">
                {{$library->links('pagination::bootstrap-4')}}
            </div>
        </div> 
        @if($library->count()==0)
        <div class="col-md-12">
            <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                
                <div  class="card-body">
                    <h3 class="card-title  text-center">  List Empty</h3>
                    
                </div>
            </div>
        </div>
        @endif 
      </div>
    </div>
</div>
@csrf
{{-- <div class="mb-5" id="table_data">
    <section class="">
        <div class="row">
            <div class="col-md-10">
                <div class="row mt-3">
                    @foreach ($library as $item)
                        
                    <div class="col-md-12">
                        <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                            
                            <div  class="card-body row">
                                <div class="col-sm-6">
                                <h3 class="card-title ">{{$item->title}}</h3>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-4 float-right">
                                    @if ($item->type=='link')
                                        
                                    <a target="blank" href="{{ $item->file_link }} " class="btn btn-sm btn-success">View</a>
                                    @else
                                    <a target="blank" href="{{ asset("storage/library/$item->file_link") }} " class="btn btn-sm btn-success">View</a>
                                    @endif
                                    <button  data-library="{{$item->id}}" class="btn btn-sm btn-success add_library">Add in Course</button>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    @endforeach
                    @if($library->count()==0)
                    <div class="col-md-12">
                        <div  class="card tab-box   "  style=" border-color: green;border-style: dashed">
                            
                            <div  class="card-body">
                                <h3 class="card-title  text-center"> Library Empty</h3>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    @endif
                    {{$library->links('pagination::bootstrap-4')}}
                    
            </div>
            
            </div>
        </div>
    </section>
   </div> --}}
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')
    <script>
        $(document).ready(function(){

$(document).on('click', '.page-link', function(event){
   event.preventDefault(); 
   var page = $(this).attr('href').split('page=')[1];
   fetch_data(page);
});

function fetch_data(page)
{
 var _token = $("input[name=_token]").val();
 $.ajax({
     url:"{{ url('professional/self-library/online_digital') }}",
     method:"POST",
     data:{_token:_token, page:page},
     success:function(data)
     {
      $('#table_data').html(data);
     }
   });
}
$('.search_bar').keyup(function (e) { 
    var _token = $("input[name=_token]").val();
    let filter=$(this).val()
 $.ajax({
     url:"{{ url('professional/self-library/online_digital') }}",
     method:"POST",
     data:{_token:_token, filter},
     success:function(data)
     {
      $('#table_data').html(data);
     }
   });
});

});

$('.add_library').click(function (e) { 
    e.preventDefault();
    var _token = $("input[name=_token]").val();
    let course_id=$('.courde_id').data('course')
    let library_id=$(this).data('library')
    $.ajax({
     url:"{{ url('professional/self-library/addcourselibrary') }}",
     method:"POST",
     data:{_token:_token, course_id,library_id},
     success:function(data)
     {
        if(data==1)
          {
            notif({
                msg: " Already Linked ",
                type: "warning"
                });

          }else
          {
            notif({
                msg: " Successfully Linked",
                type: "success"
                });
          }
     }
   });


    
});

    </script>

<script language="javascript">
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush
