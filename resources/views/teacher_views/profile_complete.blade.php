@section('title', __(Auth()->user()->name.' panel'))

@extends('teacher_views.profile_layout')
@section('profile_layout')
<div class="card tab-box ">
    <div class="card-body">
        <h3 class="card-title">About</h3>
        @if (Auth()->user()->weblink)
            {!!Auth()->user()->weblink->name!!}
        @endif
    </div>
</div>
@if (Auth()->user()->audio)
    
<div class="card tab-box ">
    <div class="card-body">
        <h3 class="card-title">Audio</h3>
        <audio controls>
            <source src="{{ asset('storage/audio/'.Auth()->user()->audio->name) }}" type="audio/mpeg">
          Your browser does not support the audio element.
          </audio>
    </div>
</div>
@endif
@if (Auth()->user()->video)
    
<div class="card tab-box ">
    <div class="card-body">
        <h3 class="card-title">Video</h3>
        <video controls width="500px">
            <source src="{{ asset('storage/video/'.Auth()->user()->video->name) }}" type="">
          Your browser does not support the audio element.
            </video>
    </div>
</div>
@endif
@endsection
