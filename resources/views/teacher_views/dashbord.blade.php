@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-sm-4 alert profile dashboard-tabs " style="background:#FCFF33" data-tab="1" data-bgcolor='#FCFF33' data-color='gray'  role="alert">
        <a  href="{{ url('professional/profile') }}" class="text-decoration-none">
        <div class="row ">
       <div class="col-md-3 "><h3 class="">Profile</h3></div> 
       <div class="col-md-9 text-right "><strong class="profile_title text-secondary">Click to view/create your profile</strong></div> 
       </div>
        </a>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-9">
                <p class=" text-muted ml-5 tab-text tab-text tab-text tab-text"></p>
                @php
                // $profile_count=$profile_count*2;
                $count=6;
                @endphp
                  @if ($profile_count==0)
                  <span  class="box alert  text-white bg-danger" ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                @php
                    $count=4;
                @endphp
                  @endif
                @for($i=1;$i<$profile_count;$i++)
                <span class="box alert  text-white " style="background: #72C21C  "></span>
                
                @endfor
                @if ($profile_count)
                <span  class="box alert  text-white " style="background: #72C21C  " ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
               
                @endif
               
                
                @for($i=$profile_count;$i<$count;$i++)
                <span class="box  alert text-white"></span>
                @endfor
              
                
                

            </div>
        </div>
        
    </div>
    <div class="col-sm-2 ">
        <ul class="nav navbar-nav flex-nowrap form-group float-right">




            <!-- Notifications dropdown -->
            <li class="nav-item dropdown dropdown-notifications dropdown-menu-sm-full">
                {{-- <button class="nav-link btn-flush dropdown-toggle " type="button" data-toggle="dropdown" data-dropdown-disable-document-scroll="" data-caret="false" aria-expanded="false">
                    <!-- <i class="material-icons">setting</i> -->
                    <span class="float-right">Settings <i class="fa fa-cog"></i></span>
                </button> --}}
                <div class="dropdown-menu dropdown-menu-right " style="min-width: 400px">
                    <div data-perfect-scrollbar="" class="position-relative ps" >
                      
                        
                        <div class="form-control side-menu text-center dashbord-info" >
                        
                        <div class="row">
                            
                            <div class="col-7">
                                <p>Administrator Control Panel</p>
                            </div>
                            <div class="col-5">
                            <span class="dot " style="background:#FCFF33"></span>
                            <span class="dot" style="background: #72C21C  "></span>
                            <span class="dot" style="background: #1C91C2"></span>
                            <span class="dot" style="background: #01090C"></span>
                            </div>
                        </div>
                      
                        </div>
                        <div class="form-control side-menu text-center slide-details "  style="height: auto">
                            <strong class="d-details"></strong>
                        </div>
                       </div>
                </div>
            </li>
            <!-- // END Notifications dropdown -->
           
        </ul>
    </div>
    
</div>
</section>
<section class="">

    <div class="row">
    <div class="col-md-5 alert dashboard-tabs" style="background: #72C21C  " data-tab="2" data-bgcolor='#72C21C' data-color='white'   role="alert">
        <a  href="{{ url('professional/courselist') }}" class="text-decoration-none">
        <div class="row">
       <div class="col-md-4"><h3 class="text-white">Course/s</h3></div> 
       <div class="col-md-8 text-right" ><strong class="profile_title text-white">Click to view/develop your courses</strong></div> 
       </div>
        </a>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-sm-8">
                <p class=" text-muted ml-5 tab-text tab-text" style="padding:0px"></p>

                <span class="box alert  text-white"></span>
                <span class="box alert  text-white"></span>
                <span  class="box alert bg-danger text-white"><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                <span class="box  alert text-white"></span>
                <span class="box alert text-white"></span>
            </div>
        </div>
        
    </div>
</div>
</section>
<section class="">
    <div class="row">
    <div class="col-md-6 alert dashboard-tabs " style="background: #1C91C2" data-tab="3" data-bgcolor='#1C91C2' data-color='white'  role="alert">
        <div class="row">
       <div class="col-md-4"><h3 class="text-white">Finances</h3></div> 
       <div class="col-md-8 text-right"><strong class="profile_title text-white">Click to view your finances</strong></div> 
       </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-sm-12">
                <p class=" text-muted ml-5 tab-text tab-text" style="padding:0px"></p>
                <span class="box alert  text-white"></span>
                <span class="box alert  text-white"></span>
                <span  class="box alert bg-danger text-white"><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                <span class="box  alert text-white"></span>
                <span class="box alert text-white"></span>
            </div>
        </div>
        
    </div>
</div>
</section>
<section class="">
    <div class="row">
    <div class="col-md-7 alert dashboard-tabs" style="background: #01090C" data-tab="4" data-bgcolor='#01090C' data-color='white'  role="alert">
        <div class="row">
       <div class="col-md-4"><h3 class="text-white">Dashboard</h3></div> 
       <div class="col-md-8 text-right"><strong class="profile_title text-white">Click to view your dashboard</strong></div> 
       </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-sm-12">
                <p class=" text-muted ml-5 tab-text tab-text" style="padding:0px"></p>
                <span class="box alert  text-white"></span>
                <span class="box alert  text-white"></span>
                <span  class="box alert bg-danger text-white"><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                <span class="box  alert text-white"></span>
                <span class="box alert text-white"></span>
            </div>
        </div>
        
    </div>
</div>
</section>
<section>
    <div class="row ">
        <div class="col-7 border-3 " >
            <div class="row p-2">
                <div class="col-sm-2 text-center">
                    <a href="">
                    <img src="{{asset('professional_assets/icons/faq.png')}}" alt="LOGO" class=" mt-2">
                    </a>
                </div>
                <div class="col-sm-2 text-center">
                    <a href="" class="remove_anchore_style text-secondary">
                    <img src="{{asset('professional_assets/icons/service.png')}}" alt="LOGO" class=" ">
                    
                    </a>
                    <p class="">Support</p>
                </div>
                <div class="col-sm-2 text-center">
                    <a href="{{url('professional/av-library')}}" class="remove_anchore_style text-secondary">
                    <img src="{{asset('professional_assets/icons/av_50.png')}}" alt="LOGO" class="img-fluid h-25" class=" ">
                    
                    </a>
                    <p class="">My AV Library</p>
                </div>
                <div class="col-sm-2 text-center mt-1">
                    <a href="" class="remove_anchore_style  text-secondary">
                    <img src="{{asset('professional_assets/icons/contact-us.png')}}" alt="LOGO" class=" ">
                </a>
                <p>Contact us</p>
                </div>
                <div class="col-sm-2 text-center ">
                    <a href="{{url('/')}}" class="remove_anchore_style text-secondary">
                    <img src="{{asset('professional_assets/icons/logo2.jpg')}}" alt="LOGO" class=" ">
                    <label>{{env('APP_DOMAIN')}}</label>
                    
                    </a>
                </div>
                <div class="col-sm-2 text-center mt-1">
                    <a href="{{url('professional/logout/teacher')}}" class="mt-5 remove_anchore_style text-secondary">
                    <img src="{{asset('professional_assets/icons/logout.png')}}" alt="LOGO" class="">
                </a>
                <p>Log out</p>
                </div>
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('custom_js')
<script>
    let data=[
        {details:"Create your profile for your target audience to view your background and experience.",tab:1},
        {details:"Develop your programme that you want your target audiences to view what your offer.",tab:2},
        {details:"Review your finances and know what you are making and what your expense are.",tab:3},
        {details:"View your dashboard to analyse analytics and to improve your programme offerings.",tab:4},
    ]
    $(".dashboard-tabs").mouseover(function(){
        // alert($(this).data('tab'));
        let obj = data.find(o => o.tab == $(this).data('tab'));
        $(".d-details").text(obj.details)
        $('.slide-details').css('background-color',$(this).data('bgcolor'));
        $('.slide-details').css('color',$(this).data('color'));

        $('.dropdown-menu-right').addClass('show')
    });
    $(".dashboard-tabs").mouseout(function(){
        // alert();
        $('.dropdown-menu-right').removeClass('show')
    });    
</script>    
@endpush