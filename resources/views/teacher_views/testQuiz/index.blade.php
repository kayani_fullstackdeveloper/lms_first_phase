@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
table {
  border-collapse: collapse;
}
tr ,td {
  border: 1px solid black;
}
tr > td:last-child {
  border-bottom-style:hidden;
  border-right-style:hidden;
}
tr :last-child {
  border-bottom-style:hidden;
  border-right-style:hidden;
}
#action{
  border-top-style: hidden;
    border-bottom-style: hidden;
}
</style>
<section>
    <div class="row">
        <div class="col-md-12">
              <div class="row mt-3">
                <div class="col-md-4 " >
                  <div class=" form-control color-bar-green text-white text-center" >
                    <span class="font-weight-bold text-decoration-none"> Tests / quizzes / Exams Timetable</span>
                  </div>
                </div>
              </div>
              <div class="row mt-3">
                  <div class="col-md-4 " >
                      <div class=" container form-control text-white h-100">
                          <div class="row">
                            <div class="col-sm">
                              <a href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-create')}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <img width="40" height="40" src="{{asset('word-pic.jpeg')}}"/>
                              </a>
                            </div>
                            <div class="col-sm">
                              <a href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-create-audio')}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-music-note-beamed" viewBox="0 0 16 16">
                                  <path d="M6 13c0 1.105-1.12 2-2.5 2S1 14.105 1 13c0-1.104 1.12-2 2.5-2s2.5.896 2.5 2zm9-2c0 1.105-1.12 2-2.5 2s-2.5-.895-2.5-2 1.12-2 2.5-2 2.5.895 2.5 2z"/>
                                  <path fill-rule="evenodd" d="M14 11V2h1v9h-1zM6 3v10H5V3h1z"/>
                                  <path d="M5 2.905a1 1 0 0 1 .9-.995l8-.8a1 1 0 0 1 1.1.995V3L5 4V2.905z"/>
                                </svg>
                              </a>
                            </div>
                            <div class="col-sm">
                              <a href="{{url('professional/test-quiz/'.$course->id.'/create-video')}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-film" viewBox="0 0 16 16">
                                  <path d="M0 1a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm4 0v6h8V1H4zm8 8H4v6h8V9zM1 1v2h2V1H1zm2 3H1v2h2V4zM1 7v2h2V7H1zm2 3H1v2h2v-2zm-2 3v2h2v-2H1zM15 1h-2v2h2V1zm-2 3v2h2V4h-2zm2 3h-2v2h2V7zm-2 3v2h2v-2h-2zm2 3h-2v2h2v-2z"/>
                                </svg>
                              </a>
                            </div>
                          </div>
                      </div>
                </div>
                <div class="col-md-4 " >
                  <a class="btn color-bar-green" href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-create')}}" role="button">Create</a>
                </div>
             </div>
             <div class="row mt-3">
               <div class="col-md-9 " >
                 <div class=" form-control color-bar-yellow text-dark text-center" >
                   <span class="font-weight-bold text-decoration-none"> Tests / quizzes / Exams Timetable</span>
                 </div>
               </div>
             </div>
             <div class="row">
                 <div class="col-md-12 " >
                   <table class="table mt-2" border="1" >
                      <thead>
                        <tr>
                          <th class="text-dark ">Ref. </th>
                          <th class="text-dark">Type</th>
                          <th class="text-dark">Title</th>
                          <th class="text-dark">Date Open</th>
                          <th class="text-dark">Date Closed</th>
                          <th class="text-dark">Duration </th>
                          <th class="text-dark">Format </th>
                          <th class="text-dark">Link </th>
                          <th class="text-dark" id="action">  </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($testquizes as $testquize)
                        <tr>
                          <td> {{$testquize->reference_number}} </td>
                          <td> {{$testquize->type}} </td>
                          <td> {{$testquize->title}} </td>
                          <td> {{$testquize->date_open}} </td>
                          <td> {{$testquize->date_close}} </td>
                          <td> {{$testquize->duration}} </td>
                          <td> {{$testquize->formate}} </td>
                          <td>{{Str::limit($testquize->link, 10)}}</td>
                          <td>
                            <!-- <a href="" class="font-weight-bold text-decoration-none  btn btn-sm  color-bar-gray border-0 text-white">
                                <span>Save</span>
                            </a> -->
                            <a href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-edit',$testquize)}}" class="font-weight-bold text-decoration-none  btn btn-sm  color-bar-gray border-0 text-white">
                                <span>Edit</span>
                            </a>
                            <a href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-delete',$testquize)}}" class="font-weight-bold text-decoration-none  btn btn-sm  color-bar-gray border-0 text-white">
                                <span>Delete</span>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                 </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
