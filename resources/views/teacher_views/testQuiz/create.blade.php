@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
  input ,select{
    border:2px solid !important;
  }
</style>
<section>
  <div class="container">
    <form method="POST" enctype="multipart/form-data" action="{{url('professional/test-quiz/'.$course->id.'/test-quiz-store')}}">
        @csrf
        <input type="hidden"  name="course_id" value="{{$course->id}}"/>

      <div class="row mt-2">
        <div class="col-sm">
            <div class=" form-control     color-bar-yellow  text-dark text-center " >
              <span class="font-weight-bold text-decoration-none"> Reference</a>
            </div>
              <input type="text" name="reference_number" value="{{old('reference_number')}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="DS - 001">
              @error('reference_number')
                <label class="error mt-2 text-danger">{{ $message }}</label></br>
              @enderror
        </div>
        <div class="col-sm">
          <div class=" form-control     color-bar-yellow  text-dark text-center " >
             <span class="font-weight-bold text-decoration-none"> Type</a>
          </div>
           <input type="text" name="type" value="{{old('type')}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Test">
           @error('type')
             <label class="error mt-2 text-danger">{{ $message }}</label></br>
           @enderror
        </div>
        <div class="col-sm">
            <div class=" form-control     color-bar-yellow  text-dark text-center " >
              <span class="font-weight-bold text-decoration-none"> Title</a>
            </div>
            <input type="text" name="title" value="{{old('title')}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Title">
            @error('title')
              <label class="error mt-2 text-danger">{{ $message }}</label></br>
            @enderror
        </div>
        <div class="col-sm">
            <div class=" form-control     color-bar-yellow  text-dark text-center " >
              <span class="font-weight-bold text-decoration-none"> Grading</a>
            </div>
            <input type="text" name="grading" value="{{old('grading')}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Grading">
            @error('grading')
              <label class="error mt-2 text-danger">{{ $message }}</label></br>
            @enderror
        </div>
        <div class="col-sm">
            <div class=" form-control     color-bar-yellow  text-dark text-center " >
              <span class="font-weight-bold text-decoration-none"> Duration</a>
            </div>
            <select class="mt-3 form-control color-bar-white text-dark " name="duration" >
              <option value="15">15</option>
              <option value="30">30</option>
              <option value="45">45</option>
              <option value="60">60</option>
            </select>
            @error('duration')
              <label class="error mt-2 text-danger">{{ $message }}</label></br>
            @enderror

        </div>
        <div class="col-sm">
            <div class=" form-control     color-bar-yellow  text-dark text-center " >
              <span class="font-weight-bold text-decoration-none"> Link</a>
            </div>
            <input type="text" name="link" value="{{old('link')}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Link">
            @error('link')
              <label class="error mt-2 text-danger">{{ $message }}</label></br>
            @enderror
        </div>
      </div>
      <div class="row mt-2">
            <div class="col-sm-12">
                <div class="form-group">
                    <textarea class="ckeditor form-control" name="description">{{old('description')}}</textarea>
                </div>
                @error('description')
                  <label class="error mt-2 text-danger">{{ $message }}</label></br>
                @enderror
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-2 mt-4">
                <a href="#" class="text-decoration-none   text-secondary ">
                    <p class="btn  btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                </a>
            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>
            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>
            </div>
      </div>
    </form>
  </div>
</section>
@endsection
