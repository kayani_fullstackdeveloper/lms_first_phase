@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
  input ,select{
    border:2px solid !important;
  }
</style>
<section>
  <div class="container">

          <div class="row mt-2">
            <div class="col-md-12">
              <form method="POST" enctype="multipart/form-data" action="{{url('professional/test-quiz/'.$course->id.'/audio-update',$testQuiz)}}">
                  @csrf
              <input type="hidden"  name="course_id" value="{{$course->id}}"/>
            <div class="row mt-2">
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Reference</a>
                  </div>
                    <input type="text" name="reference_number" value="{{$testQuiz->reference_number}}" id="reference_number" class="reference_number mt-3 form-control color-bar-white text-dark"  placeholder="DS - 001">
                    @error('reference_number')
                      <label class="error mt-2 text-danger">{{ $message }}</label></br>
                    @enderror
              </div>
              <div class="col-sm-4">
                <div class=" form-control     color-bar-yellow  text-dark text-center " >
                   <span class="font-weight-bold text-decoration-none"> Type</a>
                </div>
                 <input type="text" name="type" id="type" value="{{$testQuiz->type}}" class="type mt-3 form-control color-bar-white text-dark"  placeholder="Test">
                 @error('type')
                   <label class="error mt-2 text-danger">{{ $message }}</label></br>
                 @enderror
              </div>
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Title</a>
                  </div>
                  <input type="text" name="title" id="title" value="{{$testQuiz->title}}" class="title mt-3 form-control color-bar-white text-dark"  placeholder="Title">
                  @error('title')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Grading</a>
                  </div>
                  <input type="text" name="grading" value="{{$testQuiz->grading}}" id="grading" class="grading mt-3 form-control color-bar-white text-dark"  placeholder="Grading">
                  @error('grading')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Duration</a>
                  </div>
                  <select class="mt-3 form-control color-bar-white text-dark duration" name="duration" id="duration">
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                    <option value="60">60</option>
                  </select>
                  @error('duration')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror

              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Link</a>
                  </div>
                  <input type="text" name="link" id="link" value="{{$testQuiz->link}}" class="link mt-3 form-control color-bar-white text-dark"  placeholder="Link">
                  @error('link')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
                </div>
                <div class="col-sm-4 mt-4">
                    <div class=" form-control     color-bar-yellow  text-dark text-center " >
                      <span class="font-weight-bold text-decoration-none"> File</a>
                    </div>
                    <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                    @error('file')
                      <label class="error mt-2 text-danger">{{ $message }}</label></br>
                    @enderror
                    <audio controls class="mt-3">
                        <source src="{{ asset('storage/testQuiz/audio/').'/'.$testQuiz->file }}" type="audio/ogg">
                    </audio>
                  </div>
                  <div class="col-sm-4 mt-3">
                    <div class="mt-5" >

                    </div>
                      <button type="submit" class="btn btn-sm  btn-outline-dark " >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                  </div>
              </div>
            </form>
            </div>
            </div>
          </div>

</section>
@endsection
@section('custom_js')
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/testQuiz-audio.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
@endsection
@section('custom_css')
<style>

video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}


video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}

</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {

      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide();
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });

    $(document).on('change', '.reference_number', function() {
    var reference_number =  $(this).val();
    $('#hidereference_number').val(reference_number)
});

  $(document).on('change', '.type', function() {
  var type =  $(this).val();
  $('#hidetype').val(type)
});

  $(document).on('change', '.title', function() {
  var title_id =  $(this).val();
  $('#hidetitle').val(title_id)
});
    $(document).on('change', '.grading', function() {
    var grading =  $(this).val();
    $('#hidegrading').val(grading)
});

$(document).on('change', '.duration', function() {
    var duration =  $(this).val();
    $('#hideduration').val(duration)
});

$(document).on('change', '.link', function() {
    var link =  $(this).val();
    $('#hidelink').val(link)
});

$(document).on('change', '.course_id', function() {
    var course_id =  $(this).val();
    $('#course_id').val(course_id)
});
    </script>

@endpush
