@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-4 " >
                    <div class=" form-control     color-bar-green  text-white " >
                        <a target="blank" href="https://viizard.com/chat/agent.php" class="font-weight-bold text-decoration-none"> Live class Session</a>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Attendance Register</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Access Clearance</p>
                      </div>
                      <div class=" form-control  color-bar-gray  text-white" >
                        <p class="font-weight-bold">Email Invitation</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Timetable / Schedule</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Assessment</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Test/Quizzes/ Assignments/ MCQ</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Notifications</p>
                      </div>
                </div>
                 <div class="col-md-8  " >
                    <div class="card">
                        <div class="card-header">
                            Create Room
                        </div>
                        <div class="card-body">
                            <form method="POST" name="myForm" id="myForm"  onsubmit="event.preventDefault(); return createRoom()">
                                <input type="hidden" id="names"/>
                                <input type="hidden" id="shortagent"/>
                                <input type="hidden" id="shortvisitor"/>
                                <div class="form-group">
                                    <label for="">Date</label>
                                    <input required id="datetime"  type="datetime-local" class="form-control flatpickr-input" placeholder="Select Dat time"   >
                                </div>
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input required id="roomName"  type="text" class="form-control"  >
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1"> Select Duration</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                      <option value="15">15</option>
                                      <option value="30">30</option>
                                      <option value="45">45</option>
                                    </select>
                                  </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input  id="roomPass"  type="text" class="form-control"  >
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Create</button>
                              </form>
                        </div>
                    </div>
                    
                    
                </div>
               
             </div>
             
        
        </div>
    </div>
</section>
@endsection
@push('custom_js')
<script type="text/javascript">
    $(function () {
       flatpickr('input[type=datetime-local]',
       {enableTime: true,
      dateFormat: "Y-m-d H:i",
    })
    });
</script>
<script>
 var isAdmin = true;
            var roomId = false;
            var agentId = "{{Auth::user()->id}}";
            var agentUrl, visitorUrl, sessionId, shortAgentUrl, shortVisitorUrl, agentBroadcastUrl, viewerBroadcastLink;
    function createRoom() {
                    generateLink();
                    var datetime = ($('#datetime').val()) ? new Date($('#datetime').val()).toISOString() : '';
                  
                    $.ajax({
                        type: 'POST',
                        url: lsRepUrl + '/server/script.php',
                        data: {'type': 'scheduling', 'agentId': agentId, 'agent': $('#names').val(), 'agenturl': agentUrl, 'visitor': $('#visitorName').val(), 'visitorurl': visitorUrl,
                            'password': $('#roomPass').val(), 'session': sessionId, 'datetime': datetime, 'duration': $('#duration').val(), 'shortVisitorUrl': shortVisitorUrl, 'shortAgentUrl': shortAgentUrl}
                    })
                            .done(function (data) {
                                if (data == 200) {
                                     window.open(agentUrl);
                                } else {
                                    alert(data);
                                }
                            })
                            .fail(function () {
                                console.log('failed');
                            });
    
    }
</script>
@endpush
@push('custom_js')
<script src="{{url('chat/js/loader.v2.js')}}" data-source_path="{{url('/')}}/chat/" ></script>
@endpush