@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-4 " >
                    <div class=" form-control     color-bar-green  text-white " >
                        <a target="blank" href="https://viizard.com/chat/agent.php" class="font-weight-bold text-decoration-none"> Live class Session</a>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Attendance Register</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Access Clearance</p>
                      </div>
                      <div class=" form-control  color-bar-gray  text-white" >
                        <p class="font-weight-bold">Email Invitation</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Timetable / Schedule</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Assessment</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Test/Quizzes/ Assignments/ MCQ</p>
                      </div>
                      <div class=" form-control color-bar-gray text-white" >
                        <p class="font-weight-bold">Notifications</p>
                      </div>
                </div>
                 <div class="col-md-8  row" >
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>
                    <div class="col-md-3 " >
                        <div class="" >
                            <img src="{{asset('css/site_assets/images/live-class/student.png')}}" class="img-fluid" alt="">
                          </div>
                    </div>


                </div>

             </div>


        </div>
    </div>
</section>
@endsection
