@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                 <div class="col-md-12" >
                    <div class="row ">
                        <div class="col-md-4" >
                            <div class=" form-control  color-bar-gray text-white text-center" >

                                @if(Request()->type =='formative')
                                <a class="font-weight-bold text-decoration-none text-white">
                                    {{ucfirst(Request()->type)}} Assessment
                                </a>
                                @elseif(Request()->type =='summative')
                                <a class="font-weight-bold text-decoration-none text-white">
                                    {{ucfirst(Request()->type)}} Assessment
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" >
                            <div class=" form-control color-bar-green text-center">
                                <a  class="font-weight-bolder text-decoration-none text-dark">Guidelines & Suggestions</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12" >
                    <div class="card p-2 mb-2" style="border: 1px solid;">
                        Educational standards are the learning goals for what students should know and be able to do at the end of each
                        course. The objectives must be clear to students. They all must know what they are learning and why they are doing it.
                        Education standards (learning outcomes), are not a curriculum. Educators choose their own curriculum, which is a
                        detailed plan for day to day teaching.
                    </div>
                </div>
                @if (Request()->type =='formative')
                <div class="col-md-12" >
                    <div class="card p-2 mb-2" style="border: 1px solid;">
                        Formative assessment takes place on a day-to-day basis during teaching and learning, allowing teachers and pupils to
                        assess attainment and progress more frequently. It begins with diagnostic assessment, indicating what is already
                        known and what gaps may exist in skills or knowledge. If a teacher and pupil understand what has been achieved to
                        date, it is easier to plan the next steps. As the learning continues, further formative assessments indicate whether
                        teaching plans need to be amended to reinforce or extend learning.
                    </div>
                </div>
                @elseif(Request()->type =='summative')
                <div class="col-md-12" >
                    <div class="card p-2 mb-2" style="border: 1px solid;">
                        Summative assessment sums up what a student has achieved at the end of the term, relative to the learning aims and
                        the relevant standards. There may be an assessment at the end of a topic, at the end of a term or half-term.
                        A summative assessment may be a written test, an observation, a conversation or a task. It may be recorded through
                        writing, through photographs or other visual media, or through an audio recording. Whichever medium is used, the
                        assessment will show what has been achieved. It will summarise attainment at a particular point in time and may
                        provide individual and cohort data that will be useful for tracking progress and for informing stakeholders (e.g.
                        parents, governors, etc.).
                    </div>
                </div>
                @endif
                <div class="col-md-12" >
                    <div class="card p-2 mb-2" style="border: 1px solid;">
                        Information about student learning can be assessed through both direct and indirect measures. Direct measures may
                        include homework, quizzes, exams, reports, essays, research projects, case study analysis, and rubics for oral and other
                        performances.
                        Indirect measures include course evaluations, student surveys, course enrolment information, student retention,
                        student surveys, and placement statistics. It provides useful feedback to ascertain if required learning has taken place.
                        What can be improved upon to increase student learning potential. To provide the best support to student to increase
                        their learning.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
