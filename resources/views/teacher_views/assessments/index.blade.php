@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
{{--<section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green courde_id" data-course="{{$course->id}}" >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>
--}}
<section>
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                 <div class="col-md-12">
                    <div  class="card mt-1" style="border: 1px solid black;">
                        <h3 class="card-title text-center" style="color: black;">Create Your Assessment Toolkit</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" >
                    <div class=" form-control form-control-sm color-bar-gray text-white text-center" >
                        @if(Request()->type =='formative')
                        <a href="{{url('professional/assessments/type',[$course->id,'formative'])}}" class="font-weight-bold text-decoration-none text-white">
                            {{ucfirst(Request()->type)}} Assessment
                        </a>
                        @elseif(Request()->type =='summative')
                        <a href="{{url('professional/assessments/type',[$course->id,'summative'])}}" class="font-weight-bold text-decoration-none text-white">
                            {{ucfirst(Request()->type)}} Assessment
                        </a>
                        @endif
                    </div>
                </div>
                <div class="col-md-4" >
                    <div class=" form-control form-control-sm color-bar-yellow text-center">
                        <a href="{{url('professional/av-library/create-audio-file',$course->id)}}"  class="font-weight-bolder text-decoration-none text-dark">Create Audio File</a>
                    </div>
                </div>
                <div class="col-md-4" >
                    <a href="{{url('professional/av-library/create-video-file')}}" class=" text-decoration-none text-dark">
                        <div class="form-control form-control-sm color-bar-blue font-weight-bolder text-center">
                            Create Video File
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <form method="POST" action="{{url('professional/assessments/type/{type}/guidelines-create')}}" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                              <input class="form-control" type="hidden" name="course_id" value="{{$course->id}}">
                                @if(Request()->type =='formative')
                                    <input class="form-control" type="hidden" name="type" value="formative">
                                @elseif(Request()->type =='summative')
                                    <input class="form-control" type="hidden" name="type" value="summative">
                                @endif
                                <textarea class="ckeditor form-control" name="guideline">{{$guideline->guideline ?? ''}}</textarea>
                                </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-2 mt-4">
                                    <a href="javascript:history.back()" class="text-decoration-none   text-secondary ">
                                        <p class="btn btn-sm btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                                        </a>
                            </div>
                            <div class="col-sm-2 mt-4">
                                <button class="btn btn-sm btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>

                            </div>
                            <div class="col-sm-2 mt-4">
                                <button class="btn btn-sm btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>
                            </div>
                            </div>
                        </div>

                    </div>
                    </form>
               </div>
           </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                   <div  class="card mt-1 border border-dark" >
                       <h3 class="text-dark text-center font-weight-bold">
                            How to create your Toolkit
                       </h3>
                       <p class="p-2 font-weight-bold">
                        You have three options, word file,
                        audio record and or video record to
                        create your assessment criteria.
                        You can use one or all three for your
                        recipients. If you select the audio
                        and or video file, it will take you to
                        your AV library where you can record
                        your audio/video. The file created in
                        the AV library will also show in this
                        section.
                       </p>
                   </div>
               </div>
           </div>
        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
@push('custom_js')
    <script>
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $('.std_click').click(function (e) {
            e.preventDefault();
            $(".std_click").removeClass('active');
            $(this).addClass('active');
            $(".loader_list div").addClass('spinner-grow');
            var token = $("meta[name='csrf-token']").attr("content");
            var url="{{url('professional/assessments/get_assessments')}}";
            let std_id=$(this).data('std')
            let course_id=$(this).data('course')
            let section_id=$(this).data('section')
            let type='@php echo Request()->type; @endphp'

             $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {std_id,course_id,section_id,type},
                        success: function (data){
                            $('.loader_list div').removeClass('spinner-grow');
                           $('.student_list').html(data.list)
                        }
                    });

        });
    </script>
@endpush
