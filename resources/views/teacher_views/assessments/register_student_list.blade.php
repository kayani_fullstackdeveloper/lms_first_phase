@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu') 
{{-- <section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green courde_id" data-course="{{$course->id}}" >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section>
    <div class="row">
        <div class="col-md-10"> 
            <div class=" form-control  color-bar-gray mt-2" >
                <p class="font-weight-bold text-center text-white">Registered Students Cluster {{$section->id}}</p>
              </div>
        </div>
        <div class="col-md-10"> 
            <table class="table table-hover table-bordered mt-2">
                <thead class="bg-primary">
                  <tr>
                    <th scope="col" class="text-center text-white">Name </th>
                    <th scope="col" class="text-center text-white">Email</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                    <tr class="table-primary">
                        <td class="text-center syllabus-color font-weight-bold std_click" data-course="{{$course->id}}" data-std="{{$item->std_id}}" data-section="{{$item->section_id}}"  aria-disabled="true">{{ucfirst($item->student->name)}}</td>
                        <td class="text-center font-weight-bold std_click" data-course="{{$course->id}}" data-std="{{$item->std_id}}" data-section="{{$item->section_id}}"  aria-disabled="true">{{$item->student->email}}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
        <div class="col-md-10">
            <div class="d-flex justify-content-center loader_list">
                <div class=" text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-success" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-danger" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-warning" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-light" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class=" text-dark" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
              </div>
            <div class="row  student_list">
                
                
                
                
             </div>
             
        
        </div>
    </div>
</section>

@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')
    <script>
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $('.std_click').click(function (e) { 
            e.preventDefault();
            $(".std_click").removeClass('active');
            $(this).addClass('active');
            $(".loader_list div").addClass('spinner-grow');
            var token = $("meta[name='csrf-token']").attr("content");
            var url="{{url('professional/assessments/get_assessments')}}";
            let std_id=$(this).data('std')
            let course_id=$(this).data('course')
            let section_id=$(this).data('section')
            let type='@php echo Request()->type; @endphp'
            
             $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {std_id,course_id,section_id,type},
                        success: function (data){
                            $('.loader_list div').removeClass('spinner-grow');
                           $('.student_list').html(data.list)
                        }
                    });
            
        });
    </script>
@endpush