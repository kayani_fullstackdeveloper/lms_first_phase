@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">

        <div class="col-md-2">
          @foreach($clusters as $cluster)
            <a href="{{url('professional/assessments',[$course->id,$cluster->id])}}" class="mt-2 text-decoration-none w-100  btn color-bar-blue  border-0 text-white btn-outline-secondary">
              class cluster {{$cluster->title}}
            </a>
          @endforeach
        </div>

        <div class="col-md-10">
            <div class="row mt-2">
              <div class="col-md-8">
                      <h3 class="disabled text-decoration-none w-100  btn color-bar-gray border-0 text-white btn-outline-secondary">Create your Assessment Criteria to use the Assessment Toolkit</h3>
                  </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/type',[$course->id,'formative',$section->id])}}" class="disabled color-bar-gray text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary">
                        <span>Formative Assessments</span>
                    </a>
                    <a href="{{url('professional/assessments/type',[$course->id,'formative',$section->id,'guidelines'])}}" class="disabled color-bar-gray text-decoration-none font-weight-bold mt-1 w-100  btn color-bar-green border-0 text-dark btn-outline-dark">
                        <span>Guidelines & Suggestions</span>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/type',[$course->id,'summative',$section->id])}}" class="disabled color-bar-gray text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary">
                        <span>Summative Assessments</span>
                    </a>
                    <a href="{{url('professional/assessments/type',[$course->id,'summative',$section->id,'guidelines'])}}"
                        class="disabled color-bar-gray text-decoration-none mt-1 w-100 font-weight-bold btn
                        color-bar-green border-0  text-dark btn-outline-dark"
                        >
                        <span>Guidelines & Suggestions</span>
                    </a>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-8">
                    <a href="#" class="text-decoration-none w-100  btn  btn-danger border-0 text-white">
                        <span>Assessment Toolkit</span>
                    </a>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/course/'.$course->id.'/type/formative/section/'.$section->id.'/register-student')}}" class="text-decoration-none w-100  btn  btn-secondary border-0 text-white btn-outline-secondary">
                        <span>Formative Assessments</span>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/course/'.$course->id.'/type/formative/section/'.$section->id.'/register-student')}}" class="text-decoration-none w-100  btn  btn-secondary border-0 text-white btn-outline-secondary">
                        <span>Summative Assessments</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
    @csrf
</section>
@endsection
