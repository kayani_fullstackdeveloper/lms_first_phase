@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">

        <div class="col-md-2">
          @foreach($clusters as $cluster)
            <a href="{{url('professional/assessments',[$course->id,$cluster->id])}}" class="mt-2 text-decoration-none w-100  btn color-bar-blue  border-0 text-white btn-outline-secondary">
              class cluster {{$cluster->title}}
            </a>
          @endforeach
        </div>

        <div class="col-md-10">
            <div class="row mt-2">
              <div class="col-md-8">
                      <h3 class="text-decoration-none w-100  btn btn-danger border-0 text-white btn-outline-secondary">Create your Assessment Criteria to use the Assessment Toolkit</h3>
                  </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/type',[$course->id,'formative'])}}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary">
                        <span>Formative Assessments</span>
                    </a>
                    <a href="{{url('professional/assessments/type',[$course->id,'formative','guidelines'])}}" class="text-decoration-none font-weight-bold mt-1 w-100  btn color-bar-green border-0 text-dark btn-outline-dark">
                        <span>Guidelines & Suggestions</span>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{url('professional/assessments/type',[$course->id,'summative'])}}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary">
                        <span>Summative Assessments</span>
                    </a>
                    <a href="{{url('professional/assessments/type',[$course->id,'summative','guidelines'])}}"
                        class="text-decoration-none mt-1 w-100 font-weight-bold btn
                        color-bar-green border-0  text-dark btn-outline-dark"
                        >
                        <span>Guidelines & Suggestions</span>
                    </a>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-8">
                    <a href="#" class="text-decoration-none w-100  btn  color-bar-gray border-0 text-white btn-outline-secondary">
                        <span>Assessment Toolkit</span>
                    </a>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-4">
                    <a class="text-decoration-none w-100  btn  btn-secondary border-0 text-white btn-outline-danger">
                        <span>Formative Assessments</span>
                    </a>
                    <div class="row" >
                      @foreach($sectionGuidelines as $guideline)
                      @if($guideline->type == 'formative')
                      <input type="hidden" name="course_id" value="{{$course->id}}"/>
                      <div class="col-md-12">
                          <div class="row p-3" >
                              <div class="col-md-4">
                                <a href="{{url('professional/assessments/type',[$course->id,'formative'])}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                  <img width="50" height="50" src="{{asset('word-pic.jpeg')}}"/>
                                </a>
                              </div>
                            <div class="col-md-4">
                              <a href="{{url('professional/av-library/create-audio-file',$course->id)}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-music-note-beamed" viewBox="0 0 16 16">
                                  <path d="M6 13c0 1.105-1.12 2-2.5 2S1 14.105 1 13c0-1.104 1.12-2 2.5-2s2.5.896 2.5 2zm9-2c0 1.105-1.12 2-2.5 2s-2.5-.895-2.5-2 1.12-2 2.5-2 2.5.895 2.5 2z"/>
                                  <path fill-rule="evenodd" d="M14 11V2h1v9h-1zM6 3v10H5V3h1z"/>
                                  <path d="M5 2.905a1 1 0 0 1 .9-.995l8-.8a1 1 0 0 1 1.1.995V3L5 4V2.905z"/>
                                </svg>
                              </a>
                            </div>
                            <div class="col-md-4">
                              <a href="{{url('professional/av-library/create-video-file',$course->id)}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-film" viewBox="0 0 16 16">
                                  <path d="M0 1a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm4 0v6h8V1H4zm8 8H4v6h8V9zM1 1v2h2V1H1zm2 3H1v2h2V4zM1 7v2h2V7H1zm2 3H1v2h2v-2zm-2 3v2h2v-2H1zM15 1h-2v2h2V1zm-2 3v2h2V4h-2zm2 3h-2v2h2V7zm-2 3v2h2v-2h-2zm2 3h-2v2h2v-2z"/>
                                </svg>
                              </a>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="row" >
                              <div class="col-md-12">
                                <a  class="mt-4 text-decoration-none w-100 border-0">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-pencil-square text-dark" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                  </svg>
                                  <span class="text-decoration-none  btn  border-3  btn-outline-dark">Edit Formative Assessment</span>
                                </a>

                              </div>
                          </div>
                      </div>
                      @endif
                      @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <a  class="text-decoration-none w-100  btn  btn-secondary border-0 text-white btn-outline-danger">
                        <span>Summative Assessments</span>
                    </a>
                    <div class="row" >
                      @foreach($sectionGuidelines as $guideline)
                      @if($guideline->type == 'summative')
                      <div class="col-md-12">
                          <div class="row p-3" >
                              <div class="col-md-4">
                                <a href="{{url('professional/assessments/type',[$course->id,'summative'])}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">

                                  <img width="50" height="50" src="{{asset('word-pic.jpeg')}}"/>
                                </a>
                              </div>
                            <div class="col-md-4">
                              <a href="{{url('professional/av-library/create-audio-file',$course->id)}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-music-note-beamed" viewBox="0 0 16 16">
                                  <path d="M6 13c0 1.105-1.12 2-2.5 2S1 14.105 1 13c0-1.104 1.12-2 2.5-2s2.5.896 2.5 2zm9-2c0 1.105-1.12 2-2.5 2s-2.5-.895-2.5-2 1.12-2 2.5-2 2.5.895 2.5 2z"/>
                                  <path fill-rule="evenodd" d="M14 11V2h1v9h-1zM6 3v10H5V3h1z"/>
                                  <path d="M5 2.905a1 1 0 0 1 .9-.995l8-.8a1 1 0 0 1 1.1.995V3L5 4V2.905z"/>
                                </svg>
                              </a>
                            </div>
                            <div class="col-md-4">
                              <a href="{{url('professional/assessments/course/'.$course->id.'/type/formative/guideline/'.$guideline->id.'/guideline-edit')}}" class="mt-1 text-decoration-none w-100 border-0" title="Edit Formative Assessment">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-film" viewBox="0 0 16 16">
                                  <path d="M0 1a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm4 0v6h8V1H4zm8 8H4v6h8V9zM1 1v2h2V1H1zm2 3H1v2h2V4zM1 7v2h2V7H1zm2 3H1v2h2v-2zm-2 3v2h2v-2H1zM15 1h-2v2h2V1zm-2 3v2h2V4h-2zm2 3h-2v2h2V7zm-2 3v2h2v-2h-2zm2 3h-2v2h2v-2z"/>
                                </svg>
                              </a>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="row" >
                              <div class="col-md-12">
                                <a  class="mt-4 text-decoration-none w-100 border-0" >
                                  <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-pencil-square text-dark" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                  </svg>
                                  <span class="text-decoration-none  btn  border-3  btn-outline-dark">Edit Summative Assessment</span>
                                </a>

                              </div>
                          </div>
                      </div>
                      @endif
                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
