@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu') 
<section>
    <div class="row">
        <div class="col-md-12">
            
            <div class="row mt-3">
                <div class="col-md-12" >
                    <form method="POST" enctype="multipart/form-data" action="{{ url('professional/profile/weblink') }}">
                        @csrf
                    <div class="row">
                        <div class="col-sm-12">
                                
                            @include('common/flash-message')
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="ckeditor form-control" name="detail">{{@$details->name}}</textarea>
                            </div> 
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2 mt-4">
                            <a href="{{ url('professional/profile') }}" class="text-decoration-none   text-secondary "> 
                                <p class="btn  btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                            </a>
                        </div>  
                        <div class="col-sm-2 mt-4">
                            <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>
                        </div> 
                        <div class="col-sm-2 mt-4">
                            <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>
                        </div> 
                    </div> 
                    </form>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
