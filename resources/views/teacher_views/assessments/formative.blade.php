@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu') 
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12" >
                   <div class="form-control form-control-sm   text-white text-center" >
                       <p class="font-weight-bold text-decoration-none text-dark">Create Your Assessment Toolkit</p>
                   </div>
               </div>
            </div>
            <div class="row mt-3">
                 <div class="col-md-4" >
                    <div class=" form-control  color-bar-gray text-white text-center" >
                        <a href="{{url('professional/assessments-formative',$course->id)}}" class="font-weight-bold text-decoration-none text-white">Formative Assessment</a>
                    </div>
                    <div class=" form-control color-bar-green text-center">
                        <a href="{{url('professional/assessments/formative/guidelines-suggestions')}}" class="font-weight-bolder text-decoration-none text-dark">Guidelines & Suggestions</a>
                    </div>
                </div>
                <div class="col-md-8" >
                    <div class="row">
                        <div class="col-md-4   alert"   >
                            <div class="row mb-2">
                            <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-detail')?'color-bar-green':'color-bar-gray'}}  text-center " ><p class=" m-2">Write</p></div>
                            </div>
                            <div class="row  border-1 card-height " >
                            <div class="col-sm-12 ">
                                <strong class="  text-center">
                                    Write your personal information for your profile page.</strong>
                                
                            </div>
                            <div class="col-sm-12 mt-3">
                            <h4 class="text-center">Word Link <i class="fa fa-pencil "></i></h4>
                            @if (Auth()->user()->mediacheck('profile-detail'))
                            <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                            @endif
                                
                            </div>
                            </div>
                            <div class="col-sm-12 mt-2 text-center">
                                <a href="{{ url('professional/assessments-weblink',$course->id) }}" class=" text-white btn btn-success font-weight-bold">Select</a>
                            
                            </div>
                            
                        </div>
                        <div class="col-md-4  alert">
                            <div class="row mb-2">
                            <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-audio')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Audio Record</p></div>
                            </div>
                            <div class="row  border-1 card-height ">
                            <div class="col-sm-12 " >
                                <strong class="  text-center">Audio record or upload your
                                    audio file for your profile page.</strong>
            
                            </div>
                            <div class="col-sm-12 mt-3">
                                <h4 class="text-center">Audio Link <i class="fa fa-pencil "></i></h4>
                                @if (Auth()->user()->mediacheck('profile-audio'))
                                <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                                @endif
            
                            </div>
                            </div>
                            <div class="col-sm-12 mt-2 text-center">
                                <a href="{{ url('professional/profile/audio') }}" class=" text-white btn btn-success font-weight-bold">Select</a>
                            </div>
                            
                        </div>
                        <div class="col-md-4  alert">
                            <div class="row mb-2">
                            <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-audio')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Video Record</p></div>
                            </div>
                            <div class="row  border-1 card-height ">
                            <div class="col-sm-12 " >
                                <strong class="  text-center">Video record or upload your video file for your profile page.</strong>
            
                            </div>
                            <div class="col-sm-12 mt-3">
                                <h4 class="text-center">Video Link <i class="fa fa-pencil "></i></h4>
                                @if (Auth()->user()->mediacheck('profile-video'))
                                <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                                @endif
                            </div>
                            </div>
                            <div class="col-sm-12 mt-2 text-center">
                                <a href="{{ url('professional/profile/video') }}" class=" text-white btn btn-success font-weight-bold">Select</a>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
