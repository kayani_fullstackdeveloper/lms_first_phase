<div class="col-md-12 mb-2">
    <div id="chnage_data" data-ucourse_id='{{$data['course_id']}}' data-ustd_id='{{$data['std_id']}}' data-usection_id='{{$data['section_id']}}' data-utype_id='{{$data['type']}}'></div>

    {{-- <button class="btn btn-success float-right add_assessments">Add</button> --}}
	<h4>Standard  Five</h4>
	<p >Assess provide feedback and report on student learning.</p>
	<h4>Evidence  1-</h4>
	
</div>
<div class="row" style="">
@foreach ($list as $item)
    
<div class="col-md-4  row{{$item->id}}">
    <div  class="card tab-box "  style="">
        <div class="card-header bg-secondary text-white">
            <div class="row" >
                <div class="col-10"><strong> {{$item->lable}}</strong> {{$item->title}}</div>
                <div class="col-2">
					{{-- <i data-id="{{$item->id}}" class=" mr-2 fa fa-trash-alt  float-right remove-assessment"></i> --}}
					 <i data-id="{{$item->id}}" data-title="{{$item->title}}" data-lable="{{$item->lable}}" data-details="{{$item->details}}"  class=" mr-2 fa fa-edit float-right edit-assessment"></i>
				</div>
            </div>
        </div>
        <div  class="card-body">
            {{$item->details}}
            </div>
    </div>
    
</div>
@endforeach
</div>
<div class="col-sm-12 text-center"><h4>Professional  Knowledge</h4> </div>
@foreach ($list2 as $item)
    
<div class="col-md-6  row{{$item->id}}">
    <div  class="card tab-box "  style="">
        <div class="card-header bg-secondary text-white">
            <div class="row" >
                <div class="col-10"><strong> {{$item->lable}}</strong> :{{$item->title}}</div>
                <div class="col-2">
					{{-- <i data-id="{{$item->id}}" class=" mr-2 fa fa-trash-alt  float-right remove-assessment"></i> --}}
					 <i data-id="{{$item->id}}" data-title="{{$item->title}}" data-lable="{{$item->lable}}" data-details="{{$item->details}}"  class=" mr-2 fa fa-edit float-right edit-assessment"></i>
				</div>
            </div>
        </div>
        <div  class="card-body">
            {{$item->details}}
            </div>
    </div>
    
</div>
@endforeach
<div class="col-sm-12 text-center"><h4>Professional Practice</h4> </div>
@foreach ($list3 as $item)
    
<div class="col-md-4  row{{$item->id}}">
    <div  class="card tab-box "  style="">
        <div class="card-header bg-secondary text-white">
            <div class="row" >
                <div class="col-10"><strong> {{$item->lable}}</strong> {{$item->title}}</div>
                <div class="col-2">
					{{-- <i data-id="{{$item->id}}" class=" mr-2 fa fa-trash-alt  float-right remove-assessment"></i> --}}
					 <i data-id="{{$item->id}}" data-title="{{$item->title}}" data-lable="{{$item->lable}}" data-details="{{$item->details}}"  class=" mr-2 fa fa-edit float-right edit-assessment"></i>
				</div>
            </div>
        </div>
        <div  class="card-body">
            {{$item->details}}
            </div>
    </div>
    
</div>
@endforeach
<div class="col-sm-12 text-center"><h4>Professional Practice</h4> </div>
@foreach ($list4 as $item)
    
<div class="col-md-6  row{{$item->id}}">
    <div  class="card tab-box "  style="">
        <div class="card-header bg-secondary text-white">
            <div class="row" >
                <div class="col-10"><strong> {{$item->lable}}</strong> {{$item->title}}</div>
                <div class="col-2">
					{{-- <i data-id="{{$item->id}}" class=" mr-2 fa fa-trash-alt  float-right remove-assessment"></i> --}}
					 <i data-id="{{$item->id}}" data-title="{{$item->title}}" data-lable="{{$item->lable}}" data-details="{{$item->details}}"  class=" mr-2 fa fa-edit float-right edit-assessment"></i>
				</div>
            </div>
        </div>
        <div  class="card-body">
            {{$item->details}}
            </div>
    </div>
    
</div>
@endforeach

{{-- add assessments  --}}
<div class="modal fade " id="add-modal" tabindex="-1" role="dialog" aria-labelledby="map12" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="addTitle">Add New</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<div class="modal-body" id="addBody">
				<form method="POST" name="myForm" id="myForm"  enctype="multipart/form-data" onsubmit="return form_validation()">
					<input type="hidden" name="type" value="{{$data['type']}}">
					<input type="hidden" name="course_id" value="{{$data['course_id']}}">
					<input type="hidden" name="std_id" value="{{$data['std_id']}}">
					<input type="hidden" name="section_id" value="{{$data['section_id']}}">
		         
		        		   
		        <div class="form-group row">
		        	<div class="col-md-6">
		            		<label class="col-form-label">Lable</label>
		            		<input type="Text" required  name="lable" class=" form-control">
		            	
		            </div> 
		            <div class="col-md-6">
		            		<label class="col-form-label">Title</label>
		            		<input type="text" required name="title" class=" form-control">
		            	
		            </div> 
		        </div>
		        <div class="form-group row">
		             <div class="col-md-12">
		              <label class="col-form-label">Details</label>
          			<textarea id="editor1" required name="details"  class="form-control my-2" rows="7"></textarea>
		            </div>
		          </div>  

		           
		           	 <span class="preview-area"></span>
		         <span id="myFormError"></span>
		 		<div class="form-group row">
		            <div class="col-md-12">
		     
		                 <button type="submit" class="float-right btn btn-primary btn-sm ">Save </button>
		            </div>
		          </div>  
		        </form>
			</div>					
		</div>
	</div>
</div>
{{-- update assessments  --}}
<div class="modal fade " id="update-modal" tabindex="-1" role="dialog" aria-labelledby="map12" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="addTitle">Update</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<div class="modal-body" id="addBody">
				<form method="POST" name="myupdate" id="myupdate"  enctype="multipart/form-data" onsubmit="return form_update()">
					<input type="hidden" name="id" value="" id="id">
		         
		        		   
		        {{-- <div class="form-group row">
		        	<div class="col-md-6">
		            		<label class="col-form-label">Lable</label>
		            		<input type="Text" required  name="lable" id="lable" class=" form-control">
		            	
		            </div> 
		            <div class="col-md-6">
		            		<label class="col-form-label">Title</label>
		            		<input type="text" required name="title" id="title" class=" form-control">
		            	
		            </div> 
		        </div> --}}
		        <div class="form-group row">
		             <div class="col-md-12">
		              <label class="col-form-label">Details</label>
          			<textarea id="details" required name="details"  class="form-control my-2" rows="7"></textarea>
		            </div>
		          </div>  

		           
		           	 <span class="preview-area"></span>
		         <span id="myFormError"></span>
		 		<div class="form-group row">
		            <div class="col-md-12">
		     
		                 <button type="submit" class="float-right btn btn-primary btn-sm ">Update</button>
		            </div>
		          </div>  
		        </form>
			</div>					
		</div>
	</div>
</div>
    <script>
        $('.add_assessments').click(function () { 
            $("#add-modal").modal()
        });
        function form_validation() 
{   
  
    var form = new FormData($('#myForm')[0]);
    $.ajax({
      type: "POST",
      url: '{{url('professional/assessments/save_assessments')}}',
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res)
      {
      	//alert(res);
          getData();
          $("#add-modal").modal('hide')
      }
       
    });
      return false;
}
function form_update() 
{   
  
    var form = new FormData($('#myupdate')[0]);
    $.ajax({
      type: "POST",
      url: '{{url('professional/assessments/update_assessments')}}',
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res)
      {
      	//alert(res);
          getData();
          $("#update-modal").modal('hide')
      }
       
    });
      return false;
}

$(".edit-assessment").click(function(){

    let id=$(this).data('id');
    let lable=$(this).data('lable');
    let title=$(this).data('title');
    let details=$(this).data('details');
    $("#id").val(id)
    $("#lable").val(lable)
    $("#title").val(title)
    $("#details").val(details)
    $("#update-modal").modal()
})

getData=()=>
{
            $(".loader_list div").addClass('spinner-grow');
            let stdData=$("#chnage_data").data()
            var token = $("meta[name='csrf-token']").attr("content");
            var url="{{url('professional/assessments/get_assessments')}}";
            let std_id=stdData['ustd_id']
            let course_id=stdData['ucourse_id']
            let section_id=stdData['usection_id']
            let type=stdData['utype_id']
            
             $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {std_id,course_id,section_id,type},
                        success: function (data){
                            $('.loader_list div').removeClass('spinner-grow');
                           $('.student_list').html(data.list)
                           
                        }
                    }); 
}
$(".remove-assessment").click(function () {

	if(confirm('Are you sure!'))
	{	
		var id=$(this).data('id')
		$.ajax(
                    {
                        url:'{{url("professional/assessments/delete")}}',
                        type: 'POST',
                        data: {'id':$(this).data('id')},
                        success: function (data){
                            $('.loader_list div').removeClass('spinner-grow');
                           $('.row'+id).remove();
                           
                        }
                    });	
	}
	
  })
    </script>
