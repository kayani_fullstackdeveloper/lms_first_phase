@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')

@include('teacher_views.inc.rightMenu')
<section>
    <div class="row">

        <div class=" col-md-12  form-group color-bar-green" >
          <h4 class=" text-white">Promotional & Personal Information</h4>  
        </div>
    </div>
</section>
<section>
    <div class="row">

        <div class=" col-md-12  form-group color-bar-yellow" >
          <h4 class=" ">Your Social Media links</h4>  
        </div>
    </div>
</section>
<form method="POST" enctype="multipart/form-data" action="{{ url('professional/profile/bioupdate') }}">
<section class="">
    <div class=" ">
            @csrf
        <div class="row">
            <div class="col-sm-12">
                    
                @include('common/flash-message')
            </div>
        </div>
        <div class="row">
             {{-- social media --}}
             
             <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Youtube</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="youtube" id="phone" value="{{Auth()->user()->youtube}}" aria-describedby="helpId" placeholder="Please Enter youtube Link">
                    @if ($errors->has('youtube'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('youtube') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Facebook</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="facebook" id="phone" value="{{Auth()->user()->facebook}}" aria-describedby="helpId" placeholder="Please Enter facebook Link">
                    @if ($errors->has('facebook'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('facebook') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Instagram</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="instagram" id="phone" value="{{Auth()->user()->instagram}}" aria-describedby="helpId" placeholder="Please Enter instagram Link">
                    @if ($errors->has('instagram'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('instagram') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Twitter</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="twitter" id="phone" value="{{Auth()->user()->twitter}}" aria-describedby="helpId" placeholder="Please Enter twitter Link">
                    @if ($errors->has('twitter'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('twitter') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">LinkedIn</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="LinkedIn" id="phone" value="{{Auth()->user()->LinkedIn}}" aria-describedby="helpId" placeholder="Please Enter LinkedIn Link">
                    @if ($errors->has('LinkedIn'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('LinkedIn') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Pinterest</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="Pinterest" id="phone" value="{{Auth()->user()->Pinterest}}" aria-describedby="helpId" placeholder="Please Enter Pinterest Link">
                    @if ($errors->has('Pinterest'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Pinterest') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Reddit</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="Reddit" id="phone" value="{{Auth()->user()->Reddit}}" aria-describedby="helpId" placeholder="Please Enter Reddit Link">
                    @if ($errors->has('Reddit'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Reddit') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Snapchat</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="Snapchat" id="phone" value="{{Auth()->user()->Snapchat}}" aria-describedby="helpId" placeholder="Please Enter Snapchat Link">
                    @if ($errors->has('Snapchat'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Snapchat') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Tik Tok</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="Tik_tok" id="phone" value="{{Auth()->user()->Tik_tok}}" aria-describedby="helpId" placeholder="Please Enter Tik tok Link">
                    @if ($errors->has('Tik_tok'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Tik_tok') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Quora</label>
                  </div>
                  <div class="col-sm-9">
                  <input type="link"
                    class="form-control " name="Quora" id="phone" value="{{Auth()->user()->Quora}}" aria-describedby="helpId" placeholder="Please Enter Quora Link">
                    @if ($errors->has('Quora'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Quora') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            
            
            
        </div>


        {{-- personal data  --}}
        <section>
            <div class="row">
        
                <div class=" col-md-12  form-group color-bar-green" >
                  <h4 class="">Personal Information</h4>  
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Name</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text"
                    class="form-control" name="name" id="address" value="{{Auth()->user()->name}}" aria-describedby="helpId" placeholder="Please Enter Name">
                    @if ($errors->has('name'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            
            {{-- <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Country</label>
                  </div>
                  <div class="col-sm-9">
                    <select name="country" id="country" class="form-control ">
                        <option value="">Select Country</option>
                        @foreach ($countries as $country)
                            <option @if (Auth()->user()->country==$country)
                                selected
                            @endif value="{{ $country }}">{{ $country }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('country'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('country') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div> --}}
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Email</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="email"
                    class="form-control" name="email" id="email" value="{{Auth()->user()->email}}" aria-describedby="helpId" placeholder="Please Enter Email Addres">
                    @if ($errors->has('email'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group row">
                <div class="col-sm-3 color-bar-gray">  
                <label class="col-form-label btn text-white">Contact </label>
                </div>
                <div class="col-sm-9">
                  <input type="text"
                  class="form-control" name="address" id="address" value="{{Auth()->user()->phone}}" aria-describedby="helpId" placeholder="Mobile/Cell Number (with international dialling code)">
                  @if ($errors->has('phone'))
                  <small id="passwordHelp" class="text-danger">
                      <strong>{{ $errors->first('phone') }}</strong>
                      </small> 
                @endif
                </div>
              </div>
          </div>
            <div class="col-sm-6">
                <div class="form-group row">
                  <div class="col-sm-3 color-bar-gray">  
                  <label class="col-form-label btn text-white">Website</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="phone"
                    class="form-control" disabled name="Website" id="phone" value="{{'https://'.Auth()->user()->domain_name.'.'.config('app.domain')}}" aria-describedby="helpId" placeholder="Please Enter your Website">
                    @if ($errors->has('Website'))
                    <small id="passwordHelp" class="text-danger">
                        <strong>{{ $errors->first('Website') }}</strong>
                        </small> 
                  @endif
                  </div>
                </div>
            </div>
         
            
            
           
            
            {{-- <div class="col-sm-12 mt-3 mb-5">
                <button type="submit" class="btn btn-primary">Update</button>
                
            </div> --}}
            
        </div> 
    </div>
</section>
<section>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group row">
        <div class="col-sm-3 color-bar-gray">  
        <label class="col-form-label btn text-white">Educational Qualification</label>
        </div>
        <div class="col-sm-9">
          <div class="row">
            <div class="col-md-4">
              <input type="text" class="form-control" name="edu_qualification[]" placeholder="Educational Qualification"/>
              
            </div>
            <div class="col-md-4">
              <select name="edu_year[]" id="edu_qualification" class="form-control " >
                <option value="">Select Year</option>
                @for ($year=date('Y'); $year>=1900; $year--)
                    <option value="{{$year}}">{{ $year }}</option>
                @endfor
            </select>
            </div>
            <div class="col-md-4 text-center">
              <button type="button" class="btn btn-info addPluss"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>
      </div>
  </div>
  </div>
</div>
  @foreach (Auth()->user()->teacher_edu as $item)
  <div >
    <div class="row mt-3 " >
      <div class="col-md-3"></div>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-4">
              <input type="text" class="form-control" name="edu_qualification[]" value="{{$item->degree_name}}" placeholder="Educational Qualification"/>

          </div>
          <div class="col-md-4">
            <select name="edu_year[]" id="edu_qualification" class="form-control " >
              <option value="">Select Year</option>
              @for ($year=date('Y'); $year>=1900; $year--)
                  <option  @if ($item->year==$year))
                    selected
                @endif value="{{$year}}">{{ $year }}</option>
              @endfor
          </select>
          </div>
          <div class="col-md-4 text-center">
            <button type="button" class="btn btn-danger " onclick="removeData(this)"><i class="fa fa-minus" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>
    </div>
    </div>
  @endforeach

  <div id="clone_edu"></div>
  <div class="row mt-4 ">
    <div class="col-sm-12">
      <div class="form-group row">
        <div class="col-sm-3 color-bar-gray">  
        <label class="col-form-label btn text-white">Professional Qualification</label>
        </div>
        <div class="col-sm-9">
          <div class="row">
            <div class="col-md-4">
              <input type="text" class="form-control" name="professional_edu[]" placeholder="Educational Qualification"/>

              
            </div>
            <div class="col-md-4">
              <select name="pro_year[]" id="edu_qualification" class="form-control " >
                <option value="">Select Year</option>
                @for ($year=date('Y'); $year>=1900; $year--)
                    <option value="{{$year}}">{{ $year }}</option>
                @endfor
            </select>
            </div>
            <div class="col-md-4 text-center">
              <button type="button" class="btn btn-info addPluss2"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
      </div>
  </div>
  </div>
  @foreach (Auth()->user()->teacher_pro as $item)
  <div >
    <div class="row mt-3 ">
      <div class="col-md-3"></div>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-4">
            <input type="text" class="form-control" value="{{$item->degree_name}}" name="professional_edu[]" placeholder="professional Qualification"/>

          </div>
          <div class="col-md-4">
            <select name="pro_year[]" id="edu_qualification" class="form-control " >
              <option value="">Select Year</option>
              @for ($year=date('Y'); $year>=1900; $year--)
                  <option @if ($item->year==$year))
                    selected
                @endif value="{{$year}}">{{ $year }}</option>
              @endfor
          </select>
          </div>
          <div class="col-md-4 text-center">
            <button type="button" class="btn btn-danger " onclick="removeData(this)"><i class="fa fa-minus" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>
    </div>
    </div>    
  @endforeach
  <div id="clone_pro"></div>
</section>
<section>
    <div class="row">
            
        <div class="col-sm-2 mt-4 offset-8">
            <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>

        </div> 
        <div class="col-sm-2 mt-4">
            <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>

        </div>
    </div> 
</section>
</form> 
  {{-- edu clone --}}
<div style="display: none">
  <div class="row mt-3 " id="edu">
    <div class="col-md-3"></div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-4">
          <input type="text" class="form-control" name="edu_qualification[]" placeholder="Educational Qualification"/>

        </div>
        <div class="col-md-4">
          <select name="edu_year[]" id="edu_qualification" class="form-control " >
            <option value="">Select Year</option>
            @for ($year=date('Y'); $year>=1900; $year--)
                <option value="{{$year}}">{{ $year }}</option>
            @endfor
        </select>
        </div>
        <div class="col-md-4 text-center">
          <button type="button" class="btn btn-danger " onclick="removeData(this)"><i class="fa fa-minus" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
  </div>
  </div>
    {{-- pro clone --}}
<div style="display: none">
  <div class="row mt-3 " id="pro">
    <div class="col-md-3"></div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-4">
          <input type="text" class="form-control" name="professional_edu[]" placeholder="Professional Qualification"/>

        </div>
        <div class="col-md-4">
          <select name="pro_year[]" id="edu_qualification" class="form-control " >
            <option value="">Select Year</option>
            @for ($year=date('Y'); $year>=1900; $year--)
                <option value="{{$year}}">{{ $year }}</option>
            @endfor
        </select>
        </div>
        <div class="col-md-4 text-center">
          <button type="button" class="btn btn-danger " onclick="removeData(this)"><i class="fa fa-minus" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
  </div>
  </div>
@endsection

@push('custom_js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush
@push('custom_cs')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@push('custom_js')
    <script>
      $(document).ready(function() {
    $('.select2').select2();
});
    </script>
@endpush
@push('custom_js')
    <script>
      $('.addPluss').click(function(e){
        $("#clone_edu").append($("#edu").clone())
      })
      
      function  removeData(elm) { 
        $(elm).parent().parent().parent().parent().remove()
       }
       $('.addPluss2').click(function(e){
        $("#clone_pro").append($("#pro").clone())
      })
      
      function  removeData(elm) { 
        $(elm).parent().parent().parent().parent().remove()
       }
    </script>
@endpush