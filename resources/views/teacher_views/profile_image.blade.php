@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-3 alert " style="background:#FCFF33"  role="alert">
        <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">
        <div class="row ">
       <div class="col-md-6 "><h3 class="">Step 4</h3></div> 
       <div class="col-md-6 text-right"><h3>Profile</h3></div> 
       </div>
        </a>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-sm-12">
                <p class=" text-muted ml-5 tab-text tab-text tab-text tab-text"></p>
                @php
                $count=6;
                @endphp
                  @if ($profile_count==0)
                  <span  class="box alert  text-white bg-danger" ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                @php
                    $count=4;
                @endphp
                  @endif
                @for($i=1;$i<$profile_count;$i++)
                <span class="box alert  text-white " style="background: #72C21C  "></span>
                
                @endfor
                @if ($profile_count)
                <span  class="box alert  text-white " style="background: #72C21C  " ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
               
                @endif
               
                
                @for($i=$profile_count;$i<$count;$i++)
                <span class="box  alert text-white"></span>
                @endfor
            </div>
        </div>
        
    </div>
    <div class="col-sm-2 ">
        @include('teacher_views.inc.RightNav')
    </div>
    
</div>
</section>
<section>
    <div class="row">
        <div class="col-md-5">
            <div id="container">

                <div id="player">
            
                    <h1>Upload  Profile Photograph</h1>
                </div>
                @if (Auth()->user()->image)
            <img id="profile_image" src="{{ asset('storage/thumb/'.Auth::user()->image->name) }}" width="150" height="150">
                
            @endif
            </div>
        </div>
        <div class="col-md-4 text-center ">
            <video  class="jumbotron" id="video" playsinline autoplay>
                
            </video>
            
            <div class="row">
                <div class="col-sm-12">
                    
                        <button id="camra-on" class="btn btn-sm  btn-outline-dark" >Camera on &nbsp;  <span class="fa fa-image fa-2x text-success"></span></button>
                        <button id="btnCapture" disabled class="btn btn-sm  btn-outline-dark" >Take Photograph &nbsp;  <span class="fa fa-image fa-2x text-success"></span></button>

                </div>
            </div>

        </div>
        <div class="col-md-3 text-center image-view" style="display: none">
            <canvas class="jumbotron" id="canvas" width="150" height="150">
            </canvas>
            <button id="btnSave" disabled class="btn btn-sm  btn-outline-dark" >upload image &nbsp;  <span class="fa fa-image fa-2x text-success"></span></button>

        </div>
        
    </div>
    <hr>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('professional/profile/upload_profile_image') }}">
            @csrf
        <div class="row">
            <div class="col-md-12">
                    
                @include('common/flash-message')
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Upload your Profile Photograph</label>
                    <input type="hidden" name="type" value="profile-image">
                    <input type="file" class="form-control" name="file" id="image" placeholder="" aria-describedby="fileHelpId">
                    @if ($errors->has('file'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('file') }}</strong>
                                  </small> 
                                @endif
                  </div> 
            </div>
            
            {{-- <div class="col-sm-4 mt-4">
                <button type="submit" class="btn btn-primary m-1">Upload</button>
            </div> --}}
            <div class="col-md-6 mt-2">
                <a href="{{ url('professional/profile') }}" class="text-decoration-none btn  text-secondary pt-4 "> 
                <p class="btn btn-sm  btn-outline-dark " >Go Back &nbsp;  <span class="fa fa-undo fa-2x text-warning"></span></p>
                </a>
                    
                    <button type="submit" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></button>
                    
                    <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                    <a  style="display: none"  href="{{url('professional/course/create')}}" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                    <a style="display: none" href="{{url('professional')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>    
            
        </div>
            <div class="col-md-12">
                <span class="preview-area"></span>
            </div>
        </div> 
        </form>   
    </div>
</section>


@endsection
@push('custom_js')
<script type="text/javascript">
    var inputLocalFont = document.getElementById("image");
  inputLocalFont.addEventListener("change",previewImages,false);
  function previewImages(){
      var fileList = this.files;
      var anyWindow = window.URL || window.webkitURL;
  $('.preview-area').html('');
          for(var i = 0; i < fileList.length; i++){
            var objectUrl = anyWindow.createObjectURL(fileList[i]);
            $('.preview-area').append('<span class="span2"><img class="img-thumbnail" src="' + objectUrl + '" alt="" style="width:111px;height:100px"></span>');
            window.URL.revokeObjectURL(fileList[i]);
          }
      }
  
  </script>
   <script>
        var video = document.querySelector("#video");  
        const constraints = {  
          audio: false,  
          video: {  
              width: 150, height: 150  
          }  
      };
  // Basic settings for the video to get from Webcam 
  $("#camra-on").click(function () {

       
  

  // This condition will ask permission to user for Webcam access  
  if (navigator.mediaDevices.getUserMedia) {  
      navigator.mediaDevices.getUserMedia(constraints)  
          .then(function (stream) {  
              video.srcObject = stream;  
          })  
          .catch(function (err0r) {  
              console.log("Something went wrong!");  
          });  
          $('#btnCapture').attr('disabled', false);
  }  
}) 

  function stop() {  
    $('.image-view').hide();
        $('#btnSave').attr('disabled',true)
        $('#btnCapture').attr('disabled',true)
      var stream = video.srcObject;  
      var tracks = stream.getTracks();  

      for (var i = 0; i < tracks.length; i++) {  
          var track = tracks[i];  
          track.stop();  
      }  
      video.srcObject = null;  
  }  
  
    // Below code to capture image from Video tag (Webcam streaming)  
    $("#btnCapture").click(function () {  
        $('.image-view').show();
        $('#btnSave').attr('disabled',false)

        var canvas = document.getElementById('canvas');  
        var context = canvas.getContext('2d');  
  
        // Capture the image into canvas from Webcam streaming Video element  
        context.drawImage(video, 0, 0);  
    });  
    function dataURLtoBlob(dataURL) {
    // Decode the dataURL
    var dataURL = canvas.toDataURL();    
    var binary = atob(dataURL.split(',')[1]);
    // Create 8-bit unsigned array
    var array = [];
    for(var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    // Return our Blob object
    return new Blob([new Uint8Array(array)], {type: 'image/png'});
  }
    // Upload image to server - ajax call - with the help of base64 data as a parameter  
    $("#btnSave").click(function () {  
        stop()
        // Below new canvas to generate flip/mirron image from existing canvas  
        var destinationCanvas = document.createElement("canvas");  
        var destCtx = destinationCanvas.getContext('2d');  
        var dataURL= canvas.toDataURL();

    // Get Our File
    var file= dataURLtoBlob(dataURL);
    var fd = new FormData();

    // Append our image
        fd.append("file", file);
    // alert(file)
        destinationCanvas.height = 500;  
        destinationCanvas.width = 500;  
  
        destCtx.translate(video.videoWidth, 0);  
        destCtx.scale(-1, 1);  
        destCtx.drawImage(document.getElementById("canvas"), 0, 0);  
  
        // Get base64 data to send to server for upload  
        var imagebase64data = destinationCanvas.toDataURL("image/png"); 
       
        imagebase64data = imagebase64data.replace('data:image/png;base64,', ''); 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
        $.ajax({  
            type: 'POST',  
            url: "{{ url('professional/profile/webimageupload') }}",  
            data: fd,  
            processData: false,
            contentType: false,  
            success: function (out) { 
                
                notif({
                    msg: "Your photograph saved successfully",
                    type: "success"
                    })
                    setInterval(() => {
                    location.reload()
                        
                    }, 2000);
                     $(".btn-save").hide()
                     $('.btn-upload').show(); 
                     
            }  
        });  
    });  
</script>   
@endpush
@push('custom_js')
    <script>
        $('input:file').change(function() {
           
      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide(); 
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });
    </script>
@endpush