<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <title> @yield('title','Lms')</title>
         <meta name="csrf-token" content="{{ csrf_token() }}" >
         <input type="hidden" id="base_url" value="{{url('')}}">
         
        <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
      
        <!-- Perfect Scrollbar -->
        <link type="text/css" href="{{asset('css/site_assets/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.css')}}" rel="stylesheet">
        {{-- <link type="text/css" href="{{asset('css/site_assets/css/material-icons.rtl.css')}}" rel="stylesheet"> --}}
        <!-- Font Awesome Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.css')}}" rel="stylesheet">
        {{-- <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.rtl.css')}}" rel="stylesheet"> --}}
        <!-- App CSS -->
        <link type="text/css" href="{{asset('css/site_assets/css/app.css')}}" rel="stylesheet">
        {{-- <link type="text/css" href="{{asset('css/site_assets/css/app.rtl.css')}}" rel="stylesheet"> --}}
        <link type="text/css" href="{{asset('css/site_assets/css/custom.css')}}" rel="stylesheet">
        {{-- notification --}}
        <link type="text/css" href="{{asset('notification/notifications.css')}}" rel="stylesheet">
            <!-- Flatpickr -->
    <link type="text/css" href="{{asset('css/site_assets/css/flatpickr.css')}}" rel="stylesheet">
        
        @yield('custom_css') 
         @stack('custom_cs')


        <style>
        .login-color{
        background-color: rgb(245, 182, 131) !important;
        
        }
        .login-dp{
        color: deepskyblue !important;
        }
        </style>
    </head>
    <body class="login" style="background: #ffffff">
        <div class="container ">
            <div class="row">
        
                <div class="col-md-4 ">
                    <div class="avatar avatar-xl">
                        {{-- <img src="{{asset('css/site_assets/images/logo/logo2.jpg')}}" alt="LOGO" class="avatar-img rounded"> --}}
                        <img src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}" alt="LOGO" class=" ">
                        </div>
                </div>
                <div class="col-md-4 text-center mt-4">
                    @if (auth()->user()->gender=='Male')
                        @php
                        $gender='Mrs .'    
                        @endphp
                    @elseif(auth()->user()->gender=='Femail')
                    @php
                        $gender='Mrs .'    
                        @endphp
                        @else
                        @php
                        $gender=''    
                        @endphp
                        @endif
                    <h2 >{{$gender.''}} {{ auth()->user()->name}}</h2>
                    <h4 class="jumbotron" >Administrator Control Panel</h4>
                </div>
                
                <div class="col-md-4 text-right" >
                
                    
                </div>
                
            
                </div>
            @yield('content')
           
        </div>
        <footer class="mt-4"><span>&nbsp;</span></footer>

    </body>
    <script src="{{asset('css/site_assets/vendor/jquery.min.js')}}"></script>

 <!-- Bootstrap -->
 <script src="{{asset('css/site_assets/vendor/popper.min.js')}}"></script>
 <script src="{{asset('css/site_assets/vendor/bootstrap.min.js')}}"></script>

 <!-- Perfect Scrollbar -->
 <script src="{{asset('css/site_assets/vendor/perfect-scrollbar.min.js')}}"></script>

 <!-- MDK -->
 <script src="{{asset('css/site_assets/vendor/dom-factory.js')}}"></script>
 <script src="{{asset('css/site_assets/vendor/material-design-kit.js')}}"></script>

 <!-- App JS -->
 <script src="{{asset('css/site_assets/js/app.js')}}"></script>

 <!-- Highlight.js -->
 <script src="{{asset('css/site_assets/js/hljs.js')}}"></script>

 <!-- App Settings (safe to remove) -->
 <script src="{{asset('css/site_assets/js/app-settings.js')}}"></script>
 <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
 {{-- notification --}}
 <script src="{{ asset('notification/notifications.js') }}"></script>
<!-- Flatpickr -->
<script src="{{ asset('css/site_assets/vendor/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ asset('css/site_assets/js/flatpickr.js')}}"></script>

 @yield('custom_js') 
 @stack('custom_js')
 <script>
     $(".dropdown-menu-sm-full").mouseover(function(){
        // alert();
        $(this).children('button').addClass('show')
        $(this).children('div').first().addClass('show')
    });
    
    $(".dropdown-menu-sm-full").mouseout(function(){
        // alert();
        $(this).children('button').removeClass('show')
        $(this).children('div').first().removeClass('show')
    });
    // for searching 
    $(document).ready(function(){
      $(".searchInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#searchList .list").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
     </script>
        
    </html>
    