@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-3 alert " style="background:#FCFF33"  role="alert">
        <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">
        <div class="row ">
       <div class="col-md-6 "><h3 class="">Step 1</h3></div>
       <div class="col-md-6 text-right"><h3>Profile</h3></div>
       </div>
        </a>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-sm-12">
                <p class=" text-muted ml-5 tab-text tab-text tab-text tab-text"></p>
                @php
                $count=6;
                @endphp
                  @if ($profile_count==0)
                  <span  class="box alert  text-white bg-danger" ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                @php
                    $count=4;
                @endphp
                  @endif
                @for($i=1;$i<$profile_count;$i++)
                <span class="box alert  text-white " style="background: #72C21C  "></span>

                @endfor
                @if ($profile_count)
                <span  class="box alert  text-white " style="background: #72C21C  " ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>

                @endif


                @for($i=$profile_count;$i<$count;$i++)
                <span class="box  alert text-white"></span>
                @endfor
            </div>
        </div>

    </div>
    <div class="col-sm-2 ">
        @include('teacher_views.inc.RightNav')
    </div>

</div>
</section>
<section>
    <div class="row">
        <div class="col-sm-10">
            <div id="container">
                {{-- <h1>Web Link </h1> --}}
                <div>


                </div>

                {{-- <div>
                    <h4>Media Stream Constraints options</h4>
                    <p>Echo cancellation: <input type="checkbox" id="echoCancellation"></p>
                </div> --}}

                <div>
                    <span id="errorMsg"></span>
                </div>


            </div>

        </div>

    </div>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('professional/profile/weblink') }}">
            @csrf
        <div class="row">
            <div class="col-sm-12">

                @include('common/flash-message')
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <textarea class="ckeditor form-control" name="detail">{{@$details->name}}</textarea>
                  </div>
            </div>
            {{-- <div class="col-sm-4 mt-3 mb-5">
                <button type="submit" class="btn btn-primary">Save</button>
            </div> --}}
            <div class="col-sm-1"></div>
            <div class="col-sm-2 mt-4">
                    <a href="{{ url('professional/profile') }}" class="text-decoration-none   text-secondary ">
                        <p class="btn  btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                        </a>

            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>

            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>

            </div>
        </div>
        </form>
    </div>
</section>
@endsection
@push('custom_js')

@endpush
