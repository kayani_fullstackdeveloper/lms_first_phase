@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
{{-- <section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                      </div>
                </div>
            </div>
        </div>




    </div>
</section> --}}
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color" >
                        <a class="text-decoration-none" href="{{url('professional/syllabusoption',$course->id)}}">
                        <div class="card-body">
                            <h5 class="card-title">Syllabus / Course Material
                            </h5>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/self-library',$course->id)}}">
                        <div class="card-body">
                            <h5 class="card-title">Self Study Library</h5>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/course/'.$course->id.'/cluster')}}">

                        <div class="card-body">
                            <h5 class="card-title">Class Clusters</h5>
                        </div>
                        </a>
                    </div>
                </div>
                    <div class="col-md-3">
                        <div class="card tab-box tab-box-color">
                            <a class="text-decoration-none" href="{{url('professional/assessments/section',$course->id)}}">

                            <div class="card-body">
                                <h5 class="card-title">Assessments
                                    Formative &
                                    Summative</h5>
                            </div>
                            </a>
                        </div>
                    </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/live-class-session/'.$course->id.'/room-list')}}">
                        <div class="card-body">
                            <h5 class="card-title">Live Classroom
                                Sessions
                            </h5>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                      <a class="text-decoration-none" href="{{url('professional/lesson-lecture/'.$course->id.'/lesson-lecture-list')}}">

                        <div class="card-body">
                            <h5 class="card-title">Lesson Lectures</h5>
                        </div>
                      </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                      <a class="text-decoration-none" href="{{url('professional/discussion-borad/'.$course->id.'/discussion-borad-list')}}">

                        <div class="card-body">
                            <h5 class="card-title">Discussion Boards
                             </h5>
                        </div>
                      </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/assignment/'.$course->id.'/assignment-list')}}">

                        <div class="card-body">
                            <h5 class="card-title">Assignments</h5>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/test-quiz/'.$course->id.'/test-quiz-list')}}">

                        <div class="card-body">
                            <h5 class="card-title">Tests / Quizzes
                                Exams</h5>
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <div class="card-body">
                            <h5 class="card-title">Results & Reports</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <div class="card-body">
                            <h5 class="card-title">Analytics</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <a class="text-decoration-none" href="{{url('professional/attendance',$course->id)}}">
                        <div class="card-body">
                            <h5 class="card-title">Attendance</h5>
                        </div>
                        </a>
                    </div>
                </div>
                 {{-- <div class="col-md-3">
                    <div class="card tab-box tab-box-color">
                        <div class="card-body">
                            <h5 class="card-title">Others</h5>
                        </div>
                    </div>
                </div> --}}
            </div>

        </div>
    </div>
</section>
@endsection
