@section('title', __(Auth()->user()->name.' Course'))
@extends('teacher_views.profile_layout')
@section('profile_layout')
<div class="row">
    
    @foreach ($course as $item)
    <div class="col-md-6">
        <div class="  form-group  ">
            <div class="d-flex flex-row border-1 ">
                <div class="p-2 b">{{$item->coure_title}}</div>
                
            </div>
            <div class="row  ">
                <div class="col-sm-12 mt-3 ">
                    <div class="btn-group  ml-4">
                        <button type="button" class="btn  btn-default">Outline</button>
                        <button type="button" class="btn btn-primary">$400</button>
                        <button type="button" class="btn btn-success ">Select</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    @endforeach
</div>
@endsection
