@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  syllabus list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
{{-- <section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control color-bar-green" >
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">Syllabus / Course
                                Material
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        <div class="row">
            
            <div class="col-md-3">
                <div class="card tab-box  syllabus-color ">
                    <a class="text-decoration-none" href="{{url('professional/upload_course_material',$course->id)}}">
                    <div class="card-body">
                        <h5 class="card-title">Upload Course Material</h5>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card tab-box  syllabus-color ">
                    <a class="text-decoration-none" href="{{url('professional/create_course_material',$course->id)}}">

                    <div class="card-body">
                        <h5 class="card-title">Create Course Material</h5>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card tab-box  syllabus-color ">
                    <a class="text-decoration-none" href="{{url('professional/weblink_course_material',$course->id)}}">

                    <div class="card-body">
                        <h5 class="card-title">Weblink to Course Material</h5>
                    </div>
                    </a>
                </div>
            </div>
             <div class="col-md-3">
                <div class="card tab-box  syllabus-color ">
                    <div class="card-body">
                        <h5 class="card-title">Other mode of input</h5>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
<section>
    <div class="row mt-3">
        <div class="col-md-10">
            <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                <div class="card-body">
                    <h3 class="card-title text-white">Syllabus / Course
                        List
                    </h3>
                </div>
            </div>
        </div>
        
    
</div>
<div class="row">
    <div class="col-md-10">
        <table class="table border">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Action</th>

              </tr>
            </thead>
            <tbody id="show_data">
                
            </tbody>
          </table>
    </div>
</div>
</section>
@endsection
@push('custom_js')
    <script>
    getCourseMaterial=()=>{
    let html=``
    $.ajax({
        type: "Get",
        url:'{{ url("professional/syllabusoption/$course->id") }}',
        dataType: "json",
        success: function (res) {
            $.each(res, function (index, value) { 
                html+=`<tr  id="del`+value.id+`">
                    <th scope="row">`+(index+1)+`</th>
                    <th scope="row">`+value.name+`</th>
                    <th scope="row">`+value.type+`</th>
                    <td>  <button onclick="removeList(`+value.id+`)" class="btn btn-danger">Remove</button></td>
                    </tr>`
                
            });
            $("#show_data").html(html)
        }

    });
}
getCourseMaterial()
removeList=(id)=>
{
    $.ajax({
        type: "Post",
        url:'{{ url("professional/del_course_material") }}',
        data: {id:id},
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        dataType: "json",
        success: function (response) {
           if(response)
           {
             $("#del"+id+"").hide()
             notif({
                msg: "<b>Record :</b> Remove ",
                type: "error"
                });
           }

        }
    });
}
    </script>
@endpush