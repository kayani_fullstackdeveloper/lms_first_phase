@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
  <section class="mt-4">

    <form method="POST" action="{{url('professional/course/cluster/update',[$course->id,$cluster->id])}}" enctype="multipart/form-data">
      <div class="row">
          @csrf 
          @method('post')
            <div class="col-sm-4 ">
              <input type="hidden" name="cluster" value="{{request()->get('cluster')}}"/>
              <table class="table table-hover table-bordered mt-2">
                <input class="form-control" type="hidden" name="course_id" value="{{$course->id}}">
                <input class="form-control" type="hidden" name="professional_id" value="{{auth()->user()->id}}">
                <tbody>

                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Title</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="text" name="title" placeholder="Title" value="{{$cluster->title}}"></td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Start Date</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="date" name="start_date" placeholder="Start Date" value="{{$cluster->start_date}}"></td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >End Date</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="date" name="end_date" placeholder="End Date" value="{{$cluster->end_date}}"></td>
                  </tr>
                      <tr class="table-primary">
                        <td class="text-center font-weight-bold" >Timing</td>
                        <td class="text-center font-weight-bold" ><input class="form-control" required type="time" name="timing" placeholder="Timing" value="{{$cluster->timing}}"></td>
                      </tr>

                    </tbody>
                  </table>
            </div>

                </div>

                <div class="row">

                  <div class="col-sm-12 float-right mt-4">
                    <button onclick="history.back()" class="btn btn-sm btn-outline-dark" name="save_con" value="10" type="button">Go Back   <span class="fa fa-undo fa-3x text-warning"></span></button>
                    <button class="btn btn-sm btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>
                    <button class="btn btn-sm btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>
                  </div>
                </div>
              </form>

  </section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
