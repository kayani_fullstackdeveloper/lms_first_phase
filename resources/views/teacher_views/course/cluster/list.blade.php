@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section class="mt-4 ">
    <div class="row ">
          <div class="col-sm-3">
            <div  class="mt-1 btn w-100">
                <a href="{{url('professional/course/'.$course->id.'/cluster/create')}}" class="text-decoration-none w-100  btn  btn-success" >Add Cluster</a>
            </div>
        </div> 
    </div>
    <div class="row">
        <?php $i = 0 ?>
        @foreach($clusters as $index => $cluster)
        <?php $i++ ?>
            <div class="col-sm-4 ">
              <h4 class="btn bg-secondary text-white  d-flex" >
                Cluster {{ $i}}
              </h4>
              <table class="table table-hover table-bordered mt-2">
                <tbody>

                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Title</td>
                    <td class="text-center font-weight-bold" >{{$cluster->title}}</td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Start Date</td>
                    <td class="text-center font-weight-bold" >{{$cluster->start_date}}</td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >End Date</td>
                    <td class="text-center font-weight-bold" >{{$cluster->end_date}}</td>
                  </tr>
                      <tr class="table-primary">
                        <td class="text-center font-weight-bold" >Timing</td>
                        <td class="text-center font-weight-bold" >{{$cluster->timing}}</td>
                      </tr>

                    </tbody>
                  </table>
                  <div class="col-sm-4">
                            <div  class="mt-1 btn w-100">
                                <a href="{{url('professional/course/'.$course->id.'/cluster/edit',$cluster->id)}}" class="text-decoration-none w-100  btn btn-sm btn-success" >Edit</a>
                                <a href="{{url('professional/cluster/delete',$cluster->id)}}" class="ml-2 text-decoration-none w-100  btn btn-sm  btn-danger" >Delete</a>
                            </div>
                        </div>
            </div>

            @endforeach
    </div>
  </section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
