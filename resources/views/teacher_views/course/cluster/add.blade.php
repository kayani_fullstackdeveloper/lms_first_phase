@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section class="mt-4 ">
    <div class="row ">
          <div class=" col-sm-12 ">
            <div  class="card mt-1 btn w-100" style="border: 1px solid black;">
                <h3 class="card-title text-center" style="color: black;">Select Number of Clusters</h3>
            </div>
        </div>
    </div> 
    <div class="row d-flex justify-content-sm-center">
      <div class=" col-sm-1 "></div>

        <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=1" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==1) color-bar-green @endif ">
                1</span>
            </a>
           </div>
        <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=2" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==2) color-bar-green @endif">
              <span>  2</span>
                </a>
           </div>
           <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=3" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==3) color-bar-green @endif">
              <span> 3</span>
                </a>
           </div>
           <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=4" class="text-decoration-none w-100  btn  btn-outline-dark  @if(request()->get('cluster')==4) color-bar-green @endif">
              <span> 4</span>
                </a>
           </div>
           <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=5" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==5) color-bar-green @endif">
              <span>5</span>
                </a>
           </div>
           <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=6" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==6) color-bar-green @endif">
              <span>6</span>
                </a>
           </div>
           <div class="col-sm-1">
            <a href="{{request()->url()}}?cluster=7" class="text-decoration-none w-100  btn  btn-outline-dark @if(request()->get('cluster')==7) color-bar-green @endif">
              <span>7</span>
                </a>
           </div>

    </div>
  </section>
  <section class="mt-4">

    <form method="POST" action="{{url('professional/course/')}}/{{$course->id}}/cluster/store" enctype="multipart/form-data">
      <div class="row">
          @csrf
          @method('post')
          @for ($i = 1; $i <= request()->get('cluster'); $i++)
            <div class="col-sm-4 ">
              <input type="hidden" name="cluster" value="{{request()->get('cluster')}}"/>
              <h4 class="btn bg-secondary text-white  d-flex" >
                Cluster {{$i}}
              </h4>
              <table class="table table-hover table-bordered mt-2">
                <input class="form-control" type="hidden" name="course_id" value="{{$course->id}}">
                <input class="form-control" type="hidden" name="professional_id" value="{{auth()->user()->id}}">
                <tbody>

                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Title</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="text" name="title[]" placeholder="Title"></td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >Start Date</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="date" name="start_date[]" placeholder="Start Date"></td>
                  </tr>
                  <tr class="table-primary">
                    <td class="text-center font-weight-bold" >End Date</td>
                    <td class="text-center font-weight-bold" ><input class="form-control" required type="date" name="end_date[]" placeholder="Start Date"></td>
                  </tr>
                      <tr class="table-primary">
                        <td class="text-center font-weight-bold" >Timing</td>
                        <td class="text-center font-weight-bold" ><input class="form-control" required type="time" name="timing[]" placeholder="Start Date"></td>
                      </tr>

                    </tbody>
                  </table>
            </div>
                  @endfor

                </div>
                @if (request()->get('cluster'))

                <div class="row">

                  <div class="col-sm-12 float-right mt-4">
                    <button onclick="history.back()" class="btn btn-sm btn-outline-dark" name="save_con" value="10" type="button">Go Back   <span class="fa fa-undo fa-3x text-warning"></span></button>
                    <button class="btn btn-sm btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>
                    <button class="btn btn-sm btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>
                  </div>
                </div>
                @endif
              </form>

  </section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
