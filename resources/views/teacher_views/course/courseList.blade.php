@extends('teacher_views.app')
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">

        <div class=" col-md-12  form-group color-bar-green" >
         <h4 class=" text-white mt-1">Your Course/s</h4>  
        </div>
    </div>
</section>
<section >
    <div class="row">
      <div class="col-sm-3">
        <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Add Course</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>View Course/s</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist?course=edit') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Edit Course/s</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist?course=delete') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Delete Course/s</span> 
            </a>
       </div>
  
    </div>
  </section>
  {{-- edit list  --}}
  @if (Request()->course=='delete')
      
  <section class="mt-3">
      <div class="row">
          @foreach ($course as $item)
          <div class="col-md-3 text-center  course-<?=$item->id?>">
              <div class=" color-bar-green btn w-100 mb-2 mt-1">
                  <strong class="text-white">{{Str::limit($item->coure_title, 30)}}</strong>
                </div>
              <button class="btn btn-danger " onclick="delcourse({{$item->id}})">Delete</button>
            </div>
            @endforeach
            
        </div>
    </section>
  @elseif (Request()->course=='edit')
      
  <section class="mt-3">
      <div class="row">
          @foreach ($course as $item)
          <div class="col-md-3 ">
              <a href="{{  url('professional/course/'.$item->id.'/edit') }}" class="text-decoration-none">
              <div class=" color-bar-green btn w-100 mb-2">
                  <strong class="text-white">{{$item->coure_title}}</strong>
                  
                </div>
              </a>
            </div>
            @endforeach
            
        </div>
    </section>
    @else
<section class="mt-3">
    <div class="row">
        <div class="col-sm-12">
            <div class="row ">
                
                @foreach ($course as $item)
                <div class="col-sm-4  p-1 text-center">
                    <div class="corners1" style="border: 2px solid @php echo bgcolor();@endphp;">
                        <a href="{{url('professional/courseoption/'.$item->id)}}" style="text-decoration: none !important; color: inherit;">
                        <h4>{{$item->coure_title}}</h4>
                        <div class="text-left mt-4">
                            <strong>Course Description</strong>
                        </div>
                        <div class="text-left mt-4">
                            <p>{!!Str::words($item->course_description,30,'...')!!}</p>
                            
                        </div>
                        </a>
                    </div>
                </div>
                {{-- <div class="col-md-6  course-<?=$item->id?>">
                    <div class="  form-group  ">
                        <div class="d-flex flex-row border-1 ">
                            <div class="p-2 b">{{$item->coure_title}}</div>
                            
                        </div>
                        <div class="row">
                            {{$item->course_description}}
                        </div>
                        <div class="row  ">
                            <div class="col-sm-7 offset-sm-5 ">
                                <div class="btn-group  ml-4">
                                    <a href="{{ url('professional/courseoption',$item->id) }}" type="button" class="btn  btn-success text-white">Develop</a>
                                    <a href="{{ url('professional/coursedetail',$item->id) }}" type="button" class="btn btn-primary text-white">View</a>
                                    <button onclick="delcourse({{$item->id}})" type="button" class="btn btn-danger ">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div> --}}
                @endforeach
                             
            </div>
        </div>
    </div>
    
         
        
</section>
@endif
@endsection
@push('custom_js')
<script>
    delcourse=(id)=>
    {
        // var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    var url="{{url('professional/coursedestroy')}}/"+id;
    if(confirm('Are you sure you want to remove this course !'))
    {
    $.ajax(
    {
        url: url,
        type: 'DELETE',
        data: {
            "id": id,
            "_token": token,
            '_method':'DELETE'
        },
        success: function (data){
            notif({
                msg: "<b>Success:</b> "+data.success+" ",
                type: "success"
                });
                $(".course-"+id).hide()
        }
    });
}
    }
</script>
@endpush
@push('custom_cs')
    <style>
    .corners1 {
  border-radius: 25px;
  
  padding: 20px;
  min-height: 100%
}
        </style>
@endpush
@php
    function bgcolor(){
        $background_colors = array('#FCFF33', '#92D050 ', '#1C91C2', '#0e0f0e');

      return $rand_background = $background_colors[array_rand($background_colors)];
    };
@endphp