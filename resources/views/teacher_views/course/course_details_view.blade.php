@extends('teacher_views.app')
@section('content')
@include('teacher_views.inc.courseNavMenu')

<section> 
    <div class="row">
        <div class="col-10">
              <div class="row mt-2">
                <div class="col-md-3 text-center">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Code & Title</p>
                     </div>
                     <div class=" form-control     color-bar-yellow " >
                        <p class="font-weight-bold">Course Ends:</p>
                     </div>
                     <div class=" form-control     color-bar-orange " >
                        <p class="font-weight-bold">Course Starts:</p>
                     </div>
                </div>
                <div class="col-md-9 ">
                    <div class="row">
                        <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Syllabus / Course Material</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Self Study Library</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Assessments Formative </p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Live Classroom Sessions</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Lesson Lectures</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Discussion Boards</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Assignments</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Tests / Quizzes Exams</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Results & Reports</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Analytics</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Student registration</p>
                         </div>
                         </div>
                         <div class="col-md-4">
                         <div class=" form-control text-center     color-bar-gray " >
                        <p class="font-weight-bold">Other</p>
                         </div>
                         </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<section class="mt-4">
    <div class="row">
        <div class="col-12">
            <span class="text-danger font-weight-bold">NOTE:</span> View without any input. Input data to show content in this section
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-10">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
    </div>
</section>
@endsection
