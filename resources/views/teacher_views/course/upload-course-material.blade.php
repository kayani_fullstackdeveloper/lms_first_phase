@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  syllabus list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!--<section>-->
<!--    <div class="row">-->
<!--        <div class=" col-md-10  form-group " >-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course code</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">ENGL 101</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
                       
<!--                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>-->

                        
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row mt-2">-->
<!--                <div class="col-md-2">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">Course Title</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-9">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold">{{$course->coure_title}}</p>-->
<!--                      </div>-->
<!--                </div>-->
<!--                <div class="col-md-1">-->
<!--                    <div class=" form-control     color-bar-green " >-->
<!--                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>-->
<!--                      </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">Upload Course
                                Material
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        <div class="container mt-5 mb-5">
            <form method="POST" name="myForm" id="myForm"  enctype="multipart/form-data" onsubmit="return form_validation()">
                @csrf
            @method('post')

            <div class="row">
                <div class="col-sm-12">
                        
                    @include('common/flash-message')
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Upload  File</label>
                        <input type="hidden" name="type" value="file">
                        <input type="hidden" name="course_id" value="{{$course->id}}">
                        <input type="file" class="form-control" name="file" id="image" placeholder="" aria-describedby="fileHelpId" required>
                        @if ($errors->has('file'))
                                    <small id="passwordHelp" class="text-danger">
                                        <strong>{{ $errors->first('file') }}</strong>
                                      </small> 
                                    @endif
                      </div> 
                </div>
                
                <div class="col-sm-4 mt-4">
                    <button type="submit" class="btn btn-primary m-1">Upload</button>
                </div>
                <div class="col-sm-12">
                    <span class="preview-area"></span>
                </div>
            </div> 
            </form>   
        </div>
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">List
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <table class="table border">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Files</th>
                    <th scope="col">Action</th>
 
                  </tr>
                </thead>
                <tbody id="show_data">
                    
                {{-- @foreach ($courseMaterialList as $item) --}}
                
                {{-- @endforeach   --}}
                </tbody>
              </table>
        </div>
    </div>
    
</section>
@endsection
@push('custom_js')
  <script>
    function form_validation() 
{   
  
    var form = new FormData($('#myForm')[0]);
    $.ajax({
      type: "POST",
      url:" {{ url('professional/save_course_material') }}",
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res)
      {
      	if(res.success)
          {
            notif({
                msg: "<b>Success:</b> "+res.success+" ",
                type: "success"
                });
                $('#myForm').trigger("reset"); 
                getCourseMaterial()  

          }else
          {
            notif({
                msg: "<b>Fail!:</b> All Field Required",
                type: "error"
                });
          }
      }
       
    });
      return false;
}

getCourseMaterial=()=>{
    let html=``
    $.ajax({
        type: "Get",
        url:'{{ url("professional/upload_course_material/$course->id") }}',
        dataType: "json",
        success: function (res) {
            $.each(res, function (index, value) { 
                html+=`<tr  id="del`+value.id+`">
                    <th scope="row">`+(index+1)+`</th>
                    <td>`+value.data+` </td>
                    <td>  <a class="btn btn-success" href="{{asset('storage/course/`+value.data+`')}}">Download</a>
                        <button onclick="removeList(`+value.id+`)" class="btn btn-danger">Remove</button></td>
                    </tr>`
                
            });
            $("#show_data").html(html)
        }

    });
}
getCourseMaterial()//get data
removeList=(id)=>
{
    $.ajax({
        type: "Post",
        url:'{{ url("professional/del_course_material") }}',
        data: {id:id},
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        dataType: "json",
        success: function (response) {
           if(response)
           {
             $("#del"+id+"").hide()
             notif({
                msg: "<b>Record :</b> Remove ",
                type: "error"
                });
           }

        }
    });
}
    
  </script>
@endpush