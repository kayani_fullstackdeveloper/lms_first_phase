@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">

        <div class=" col-md-12  form-group color-bar-green" >
          <h4 class=" text-white">Your Course/s</h4>  
        </div>
    </div>
</section>
<section >
    <div class="row">
      <div class="col-sm-3">
        <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Add Course</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>View Course/s</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist?course=edit') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Edit Course/s</span> 
            </a>
       </div>
       <div class="col-sm-3">
        <a href="{{  url('professional/courselist?course=delete') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
          <span>Delete Course/s</span> 
            </a>
       </div>
  
    </div>
  </section>
<section  class=" mt-4 ">
<form method="POST" name="myForm" id="myForm"  enctype="multipart/form-data" onsubmit="return form_validation()">
  @csrf
   @method('post')

  <div class="row">
    <div class="col-sm-3 ">
      <div  class=" w-100  btn   border-1"> 
        <strong>Course Code & Title</strong> 
      </div>
     </div>
     <div class="col-sm-5 ">
      <input type="text" class="form-control" required placeholder="Course code & Title here" name="coure_title">
     </div>
     <div class="col-sm-4 color-bar-green btn">
       <strong class="text-white">Tips for writing a course description</strong>
     </div>
  </div>
  
  <div class="row mt-4">
    <div class="col-sm-3 ">
      <div  class=" w-100  btn   border-1"> 
        <strong>Course Description</strong> 
      </div>
      <div  class=" mt-4 w-100  btn   border-1 color-bar-yellow" > 
        <strong>Review Course</strong> 
      </div>
      <div  class=" mt-4 w-100     border-1 color-bar-green" > 
        
        <button type="submit" class="w-100 border-0 btn text-white" ><strong>Save Course</strong></button>

      </div>
     </div>
     <div class="col-sm-9 ">
       <div class="row">
         <div class="col-sm-3">
          <div class="form-group">
            <label for="">Start Date</label>
            <input type="date" required class="form-control @error('date_start')is-invalid @enderror" name="date_start" placeholder="Enter Course Publish Date" id="">
                @error('date_start')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
         </div>
         <div class="col-sm-3">
          <div class="form-group">
            <label for="">End Date</label>
            <input type="date" required class="form-control @error('end_date')is-invalid @enderror" name="end_date" placeholder="Enter Course End Date" id="">
            @error('end_date')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
         </div>
         <div class="col-sm-3">
          <div class="form-group">
            <label for="email">Price</label>
            <input type="number" required class="form-control @error('price')is-invalid @enderror" name="price" placeholder="Enter Course Price" id="email">
            @error('price')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
         </div>
         <div class="col-sm-3">
          <div class="form-group">
            <label for="email">Currency</label>
            <select id="currency" name="currency" class="form-control @error('currency')is-invalid @enderror">
              <option>select currency</option>
              <option value="AFN">Afghan Afghani</option>
              <option value="ALL">Albanian Lek</option>
              <option value="DZD">Algerian Dinar</option>
              <option value="AOA">Angolan Kwanza</option>
              <option value="ARS">Argentine Peso</option>
              <option value="AMD">Armenian Dram</option>
              <option value="AWG">Aruban Florin</option>
              <option value="AUD">Australian Dollar</option>
              <option value="AZN">Azerbaijani Manat</option>
              <option value="BSD">Bahamian Dollar</option>
              <option value="BHD">Bahraini Dinar</option>
              <option value="BDT">Bangladeshi Taka</option>
              <option value="BBD">Barbadian Dollar</option>
              <option value="BYR">Belarusian Ruble</option>
              <option value="BEF">Belgian Franc</option>
              <option value="BZD">Belize Dollar</option>
              <option value="BMD">Bermudan Dollar</option>
              <option value="BTN">Bhutanese Ngultrum</option>
              <option value="BTC">Bitcoin</option>
              <option value="BOB">Bolivian Boliviano</option>
              <option value="BAM">Bosnia-Herzegovina Convertible Mark</option>
              <option value="BWP">Botswanan Pula</option>
              <option value="BRL">Brazilian Real</option>
              <option value="GBP">British Pound Sterling</option>
              <option value="BND">Brunei Dollar</option>
              <option value="BGN">Bulgarian Lev</option>
              <option value="BIF">Burundian Franc</option>
              <option value="KHR">Cambodian Riel</option>
              <option value="CAD">Canadian Dollar</option>
              <option value="CVE">Cape Verdean Escudo</option>
              <option value="KYD">Cayman Islands Dollar</option>
              <option value="XOF">CFA Franc BCEAO</option>
              <option value="XAF">CFA Franc BEAC</option>
              <option value="XPF">CFP Franc</option>
              <option value="CLP">Chilean Peso</option>
              <option value="CNY">Chinese Yuan</option>
              <option value="COP">Colombian Peso</option>
              <option value="KMF">Comorian Franc</option>
              <option value="CDF">Congolese Franc</option>
              <option value="CRC">Costa Rican ColÃ³n</option>
              <option value="HRK">Croatian Kuna</option>
              <option value="CUC">Cuban Convertible Peso</option>
              <option value="CZK">Czech Republic Koruna</option>
              <option value="DKK">Danish Krone</option>
              <option value="DJF">Djiboutian Franc</option>
              <option value="DOP">Dominican Peso</option>
              <option value="XCD">East Caribbean Dollar</option>
              <option value="EGP">Egyptian Pound</option>
              <option value="ERN">Eritrean Nakfa</option>
              <option value="EEK">Estonian Kroon</option>
              <option value="ETB">Ethiopian Birr</option>
              <option value="EUR">Euro</option>
              <option value="FKP">Falkland Islands Pound</option>
              <option value="FJD">Fijian Dollar</option>
              <option value="GMD">Gambian Dalasi</option>
              <option value="GEL">Georgian Lari</option>
              <option value="DEM">German Mark</option>
              <option value="GHS">Ghanaian Cedi</option>
              <option value="GIP">Gibraltar Pound</option>
              <option value="GRD">Greek Drachma</option>
              <option value="GTQ">Guatemalan Quetzal</option>
              <option value="GNF">Guinean Franc</option>
              <option value="GYD">Guyanaese Dollar</option>
              <option value="HTG">Haitian Gourde</option>
              <option value="HNL">Honduran Lempira</option>
              <option value="HKD">Hong Kong Dollar</option>
              <option value="HUF">Hungarian Forint</option>
              <option value="ISK">Icelandic KrÃ³na</option>
              <option value="INR">Indian Rupee</option>
              <option value="IDR">Indonesian Rupiah</option>
              <option value="IRR">Iranian Rial</option>
              <option value="IQD">Iraqi Dinar</option>
              <option value="ILS">Israeli New Sheqel</option>
              <option value="ITL">Italian Lira</option>
              <option value="JMD">Jamaican Dollar</option>
              <option value="JPY">Japanese Yen</option>
              <option value="JOD">Jordanian Dinar</option>
              <option value="KZT">Kazakhstani Tenge</option>
              <option value="KES">Kenyan Shilling</option>
              <option value="KWD">Kuwaiti Dinar</option>
              <option value="KGS">Kyrgystani Som</option>
              <option value="LAK">Laotian Kip</option>
              <option value="LVL">Latvian Lats</option>
              <option value="LBP">Lebanese Pound</option>
              <option value="LSL">Lesotho Loti</option>
              <option value="LRD">Liberian Dollar</option>
              <option value="LYD">Libyan Dinar</option>
              <option value="LTL">Lithuanian Litas</option>
              <option value="MOP">Macanese Pataca</option>
              <option value="MKD">Macedonian Denar</option>
              <option value="MGA">Malagasy Ariary</option>
              <option value="MWK">Malawian Kwacha</option>
              <option value="MYR">Malaysian Ringgit</option>
              <option value="MVR">Maldivian Rufiyaa</option>
              <option value="MRO">Mauritanian Ouguiya</option>
              <option value="MUR">Mauritian Rupee</option>
              <option value="MXN">Mexican Peso</option>
              <option value="MDL">Moldovan Leu</option>
              <option value="MNT">Mongolian Tugrik</option>
              <option value="MAD">Moroccan Dirham</option>
              <option value="MZM">Mozambican Metical</option>
              <option value="MMK">Myanmar Kyat</option>
              <option value="NAD">Namibian Dollar</option>
              <option value="NPR">Nepalese Rupee</option>
              <option value="ANG">Netherlands Antillean Guilder</option>
              <option value="TWD">New Taiwan Dollar</option>
              <option value="NZD">New Zealand Dollar</option>
              <option value="NIO">Nicaraguan CÃ³rdoba</option>
              <option value="NGN">Nigerian Naira</option>
              <option value="KPW">North Korean Won</option>
              <option value="NOK">Norwegian Krone</option>
              <option value="OMR">Omani Rial</option>
              <option value="PKR">Pakistani Rupee</option>
              <option value="PAB">Panamanian Balboa</option>
              <option value="PGK">Papua New Guinean Kina</option>
              <option value="PYG">Paraguayan Guarani</option>
              <option value="PEN">Peruvian Nuevo Sol</option>
              <option value="PHP">Philippine Peso</option>
              <option value="PLN">Polish Zloty</option>
              <option value="QAR">Qatari Rial</option>
              <option value="RON">Romanian Leu</option>
              <option value="RUB">Russian Ruble</option>
              <option value="RWF">Rwandan Franc</option>
              <option value="SVC">Salvadoran ColÃ³n</option>
              <option value="WST">Samoan Tala</option>
              <option value="SAR">Saudi Riyal</option>
              <option value="RSD">Serbian Dinar</option>
              <option value="SCR">Seychellois Rupee</option>
              <option value="SLL">Sierra Leonean Leone</option>
              <option value="SGD">Singapore Dollar</option>
              <option value="SKK">Slovak Koruna</option>
              <option value="SBD">Solomon Islands Dollar</option>
              <option value="SOS">Somali Shilling</option>
              <option value="ZAR">South African Rand</option>
              <option value="KRW">South Korean Won</option>
              <option value="XDR">Special Drawing Rights</option>
              <option value="LKR">Sri Lankan Rupee</option>
              <option value="SHP">St. Helena Pound</option>
              <option value="SDG">Sudanese Pound</option>
              <option value="SRD">Surinamese Dollar</option>
              <option value="SZL">Swazi Lilangeni</option>
              <option value="SEK">Swedish Krona</option>
              <option value="CHF">Swiss Franc</option>
              <option value="SYP">Syrian Pound</option>
              <option value="STD">São Tomé and Príncipe Dobra</option>
              <option value="TJS">Tajikistani Somoni</option>
              <option value="TZS">Tanzanian Shilling</option>
              <option value="THB">Thai Baht</option>
              <option value="TOP">Tongan pa'anga</option>
              <option value="TTD">Trinidad & Tobago Dollar</option>
              <option value="TND">Tunisian Dinar</option>
              <option value="TRY">Turkish Lira</option>
              <option value="TMT">Turkmenistani Manat</option>
              <option value="UGX">Ugandan Shilling</option>
              <option value="UAH">Ukrainian Hryvnia</option>
              <option value="AED">United Arab Emirates Dirham</option>
              <option value="UYU">Uruguayan Peso</option>
              <option value="USD">US Dollar</option>
              <option value="UZS">Uzbekistan Som</option>
              <option value="VUV">Vanuatu Vatu</option>
              <option value="VEF">Venezuelan BolÃ­var</option>
              <option value="VND">Vietnamese Dong</option>
              <option value="YER">Yemeni Rial</option>
              <option value="ZMK">Zambian Kwacha</option>
            </select>
          @error('currency')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
         </div>
       </div>
      <div class="row">
        <div class="col-sm-12">
          <label for="email">Description</label>

        <textarea class="ckeditor form-control" name="detail"></textarea>
        </div>
      </div> 
     </div>
     
  </div>
</form>
</section>
   

@endsection
@push('custom_js')
  <script>
      let count=0;
    $('.add-course').click(function (e) { 
      e.preventDefault();
     let  html=`
     <div class="col-sm-5 course`+count+`">
        <div class="form-group">
          <label for=""></label>
          <input type="text"
            class="form-control" required name="course_name[]" id="" aria-describedby="helpId" placeholder="">
          <small id="helpId" class="form-text text-muted">Help text</small>
        </div>
        <div class="text-center">
          <button type="button" class="btn  btn-success" disabled>Develop</button>
          <button type="button" class="btn btn-primary" disabled>View</button>
          <button type="button" onClick="remove(`+count+`)" class="btn btn-danger ">Delete</button>
        </div>
      </div>
      `;
      count++;
      $(".courseList").append(html)
    });
    remove=(index)=>
    {
        $(".course"+index+"").remove()
        notif({
                msg: "<b>Success:</b> Remove ",
                type: "success"
                });
    }

    function form_validation() 
{   

  for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
    var form = new FormData($('#myForm')[0]);
    $.ajax({
      type: "POST",
      url:" {{ url('professional/course') }}",
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res)
      {
      	if(res.success)
          {
            notif({
                msg: "<b>Success:</b> "+res.success+" ",
                type: "success"
                });
                // window.location.href ="{{url('professional/profile')}}";
                $("#myForm").trigger("reset")

          }else
          {
            notif({
                msg: "<b>Success:</b> All Field Required",
                type: "error"
                });
          }
      }
       
    });
      return false;
}
    
$(".show_course").mouseover(function(){
  let html='';
  $.post("{{ url('professional/getCourseList') }}",
    {
        '_token': $('meta[name=csrf-token]').attr('content'),
        
    },function (res) {
      let count=1;
      html+='<h3 class="text-center text-white">Your Saved Course/s</h3>'
      $.each(res, function (indexInArray, value) { 
        html+=`<p class="text-left">`+ count++ +`. `+value.coure_title+`</p>`
         console.log(value)
      });
      $('.d-details').html(html)
      $('.slide-details').css('background-color','#72C21C');
        $('.slide-details').css('color','white');

      })
    

        // alert($(this).data('tab'));
        // let obj = data.find(o => o.tab == $(this).data('tab'));
        // $(".d-details").text(obj.details)
        // $('.slide-details').css('background-color',$(this).data('bgcolor'));
        // $('.slide-details').css('color',$(this).data('color'));

        $('.show_tab').addClass('show')
    });
$(".show_course").mouseout(function(){
    // alert();
    $('.show_tab').removeClass('show')
});

  </script>
@endpush
@push('custom_js')
<script>
     $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endpush
