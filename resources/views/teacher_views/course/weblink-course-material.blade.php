@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  syllabus list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">Weblink to Course Material
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
           <div class="row">
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/fb.png')}}">
                </a>
            </div> 
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img  width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/instagram.png')}}">
                </a>
            </div> 
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/linkedin.png')}}">
                </a>
            </div> 
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/twitter.png')}}">
                </a>
            </div> 
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/google.png')}}">
                </a>
            </div> 
            <div class="col-md-2 ">
                <a class="text-decoration-none" href="{{url('professional/create_weblink_course_material',$course->id)}}">

                <img width="90" height="90" class="btn-blank" src="{{asset('css/site_assets/images/social/youtube.jpg')}}">
                </a>
            </div>    
           </div>
        
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">List
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <table class="table border">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Url</th>
                    <th scope="col">Action</th>
 
                  </tr>
                </thead>
                <tbody id="show_data">
                    
                {{-- @foreach ($courseMaterialList as $item) --}}
                
                {{-- @endforeach   --}}
                </tbody>
              </table>
        </div>
    </div>
    
</section>

@endsection
@push('custom_js')
    <script>
        getCourseMaterial=()=>{
    let html=``
    $.ajax({
        type: "Get",
        url:'{{ url("professional/weblink_course_material/$course->id") }}',
        dataType: "json",
        success: function (res) {
            $.each(res, function (index, value) { 
                html+=`<tr  id="del`+value.id+`">
                    <th scope="row">`+(index+1)+`</th>
                    <th scope="row">`+value.name+`</th>
                    <td> <a target="blank" href="`+value.data+`">Link</a> </td>
                    <td>  <button onclick="removeList(`+value.id+`)" class="btn btn-danger">Remove</button></td>
                    </tr>`
                
            });
            $("#show_data").html(html)
        }

    });
}
getCourseMaterial()//get data
removeList=(id)=>
{
    $.ajax({
        type: "Post",
        url:'{{ url("professional/del_course_material") }}',
        data: {id:id},
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        dataType: "json",
        success: function (response) {
           if(response)
           {
             $("#del"+id+"").hide()
             notif({
                msg: "<b>Record :</b> Remove ",
                type: "error"
                });
           }

        }
    });
}


    </script>
@endpush