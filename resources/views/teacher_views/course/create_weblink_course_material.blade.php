@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  syllabus list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                       
                        <p class="font-weight-bold"><a href="{{url('professional/course/create')}}"> NCI</a></p>

                        
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
                <div class="col-md-1">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold"><a href="{{url('professional/courseoption',$course->id)}}"> CDS</a></p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box  syllabus-color  text-center " style="background-color: gray;" >
                        <div class="card-body">
                            <h3 class="card-title text-white">Weblink to Course Material
                            </h3>
                        </div>
                    </div>
                </div>
                
            
        </div>
        <div class="container mt-5 mb-5">
            <form method="POST" name="myForm" id="myForm"  enctype="multipart/form-data" onsubmit="return form_validation()">
                @csrf
            @method('post')

            <div class="row">
                <div class="col-sm-12">
                        
                    @include('common/flash-message')
                </div>
                  <input type="hidden" name="type" value="media">
                        <input type="hidden" name="course_id" value="{{$course->id}}">
                <div class="col-sm-4">
                    <div class="form-group">
                      <label for="">Media Name</label>
                      <input type="text"
                        class="form-control" name="media_name" required id="" aria-describedby="helpId" placeholder="">
                      
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                      <label for="">Media Url</label>
                      <input type="url"
                        class="form-control" required name="data" required id="" aria-describedby="helpId" placeholder="">
                      
                    </div>
                </div>
                
                <div class="col-sm-4 mt-4">
                    <button type="submit" class="btn btn-primary m-1">Uplaod</button>
                </div>
                <div class="col-sm-12">
                    <span class="preview-area"></span>
                </div>
            </div> 
            </form>   
        </div>
        </div>
    </div>
</section>
@endsection
@push('custom_js')
  <script>
       
    function form_validation() 
{   
    
    var form = new FormData($('#myForm')[0]);
    $.ajax({
      type: "POST",
      url:" {{ url('professional/save_weblinlk_course_material') }}",
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res)
      {
      	if(res.success)
          {
            notif({
                msg: "<b>Success:</b> "+res.success+" ",
                type: "success"
                });
                
                $('#myForm').trigger("reset");

          }else
          {
            notif({
                msg: "<b>Fail!:</b> All Field Required",
                type: "error"
                });
          }
      }
       
    });
      return false;
}
    
  </script>
@endpush