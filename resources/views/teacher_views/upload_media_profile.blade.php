@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-4 alert " style="background:#FCFF33"  role="alert">
        <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">
        
        <div class="row ">
        
       <div class="col-md-12 "><h3>Profile</h3></div> 
       
       </div>
        </a>
    </div>
    <div class="col-sm-6"></div>
    <div class="col-sm-2">
        @include('teacher_views.inc.RightNav')
    </div>

   
    
        
   
   
    
</div>
</section>
<section class="mt-3">
    <div class="row">
        <div class="col-sm-2">
            <a href="{{ url('professional') }}" class="text-decoration-none w-100  btn  border-3  btn-outline-dark"> 
                Go Back &nbsp;  <span class="fa fa-undo "></span>
                </a>
           </div>
        <div class="col-sm-2">
            <a href="{{ url('professional/profile?modify=edit_oncreate') }}" class="text-decoration-none w-100  btn btn- btn-danger border-0 text-white btn-outline-dark"> 
              <span>  Update  Profession</span> 
                </a>
           </div>
           <div class="col-sm-2">
            <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn btn- btn-success border-0 text-white btn-outline-dark"> 
              <span> Course/s</span> 
                </a>
           </div>
           <div class="col-sm-2">
            <a href="{{  url('/') }}" class="text-decoration-none w-100  btn btn- btn-primary border-0 text-white btn-outline-dark"> 
              <span> Finances</span> 
                </a>
           </div>
           <div class="col-sm-2">
            <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn bg-dark border-0 text-white btn-outline-dark"> 
              <span>Dashboard</span> 
                </a>
           </div>
           <div class="col-sm-2">
            <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
              <span>(ACP)</span> 
                </a>
           </div>
    </div>
</section>
<section class="mt-4" >
    <div class="row  ">
        <div class="col-sm-12">
                    
            @include('common/flash-message')
        </div>
    <div class="col-sm-12   " style="border: solid gray 1px;">
        <div class="row ">
            <div class="col-sm-12 text-center mt-2 font-weight-bold">
                <p class="text-secondary">Create Your Profile  </p>
            </div>
        </div>
        
         
            
    </div>
    
    
</div>
<div class="row " >
    
            <div class="col-md-3   alert"   >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-detail')?'color-bar-green':'color-bar-gray'}}  text-center " ><p class=" m-2">Write</p></div>
                </div>
                <div class="row  border-1 card-height " >
                <div class="col-sm-12 ">
                    <strong class="  text-center">
                        Write your personal information for your profile page.</strong>
                    
                </div>
                <div class="col-sm-12 mt-3">
                <h4 class="text-center">Word Link <i class="fa fa-pencil "></i></h4>
                @if (Auth()->user()->mediacheck('profile-detail'))
                <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                @endif
                    
                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/profile/weblink') }}" class=" text-white btn btn-success font-weight-bold">Select</a>
                   
                </div>
                
            </div>
            <div class="col-md-3  alert">
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-audio')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Audio Record</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12 " >
                    <strong class="  text-center">Audio record or upload your
                        audio file for your profile page.</strong>

                </div>
                <div class="col-sm-12 mt-3">
                    <h4 class="text-center">Audio Link <i class="fa fa-pencil "></i></h4>
                    @if (Auth()->user()->mediacheck('profile-audio'))
                    <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                    @endif

                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/profile/audio') }}" class=" text-white btn btn-success font-weight-bold">Select</a>
                </div>
                
            </div>
            <div class="col-md-3  alert" >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-video')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Video Record</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12  ">
                    <strong class="  text-center">
                        Video record
                        or upload your
                        video file for
                        your profile
                        page.
                    </strong>

                </div>
                <div class="col-sm-12 mt-5">
                    <h4 class="text-center">Video Link <i class="fa fa-pencil "></i></h4>
                    @if (Auth()->user()->mediacheck('profile-video'))
                    <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                    @endif

                 </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/profile/video') }}" class=" text-white btn btn-success font-weight-bold">Select</a>

                </div>
                
            </div>
            
            <div class="col-md-3  alert" >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-image')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Photograph</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12  ">
                    <strong class=" text-center ">
                        Take your photograph or
                        upload photograph for
                        your profile page.
                    </strong>

                </div>
                <div class="col-sm-12 mt-3">
                    <h4 class="text-center">Photo Link <i class="fa fa-pencil "></i></h4>
                    @if (Auth()->user()->mediacheck('profile-image'))
                    <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                    @endif
                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/profile/image') }}" class=" text-white btn btn-success font-weight-bold">Select</a>

                </div>
                
            </div>
            <div class="col-md-3  alert" >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-course')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Your Course/s List</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12  ">
                    <strong class="  text-center ">
                        list your course/s for
                        your profile page.
                    </strong>

                </div>
                <div class="col-sm-12 mt-3">
                    <h4 class="text-center mt-4">Course/s Link <i class="fa fa-pencil "></i></h4>
                    @if (Auth()->user()->mediacheck('profile-course'))
                    <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                    @endif

                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/course/create') }}" class=" text-white btn btn-success font-weight-bold">Select</a>

                </div>
                
            </div>
            <div class="col-sm-3  alert" >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold {{Auth()->user()->mediacheck('profile-bio')?'color-bar-green':'color-bar-gray'}} text-center"><p class=" m-2">Promotional & Personal Data</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12  ">
                    <strong class="  text-center">
                    Fill personal data for your profile page.
                    </strong>

                </div>
                <div class="col-sm-12 mt-3">
                    <h4 class="text-center">Data Link <i class="fa fa-pencil "></i></h4>
                    @if (Auth()->user()->mediacheck('profile-bio'))
                <p class="text-center text-success"> <i class="fa fa-check-circle fa-3x  " aria-hidden="true"></i></p>
                @endif

                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="{{ url('professional/profile/bio') }}" class=" text-white btn btn-success font-weight-bold">Select</a>

                </div>
                
            </div>
            
            <div class="col-md-2 mt-5">
                <svg class="mt-5 ml-5 bd-placeholder-img rounded-circle color-bar-gray" style="font-size: 140%; " width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Completely round image: 75x75"><title>Completely round image</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="35%" y="50%" fill="#dee2e6" dy=".3em">OR</text></svg> 
              
            </div>
            <div class="col-md-3  alert" >
                <div class="row mb-2">
                <div class="col-sm-12 border-4 col font-weight-bold color-bar-gray text-center"><p class=" m-2">Upload Your Profile</p></div>
                </div>
                <div class="row  border-1 card-height ">
                <div class="col-sm-12  ">
                    <strong class="  text-center">
                        We recommend that you use the links to create your
                        profile. However, if you do not wish to create your
                        profile, you can upload your document here, which will
                        become your profile that your audiences will view.
                    </strong>
                    {{-- <h4 class="text-center">Course  <i class="fa fa-pencil "></i></h4> --}}

                </div>
                </div>
                <div class="col-sm-12 mt-2 text-center">
                    <a href="#" class=" text-white btn btn-success font-weight-bold">Select</a>

                </div>
                
            </div>
            

</div>
{{-- <div class="open-ot-alert">Click to View</div>
<div class="more-ot-alert  ">
  <span class="close-ot-alert">
    <i class="fa fa-close"></i>
  </span>
  <p>There is more OT in the upcoming weeks. 
    Scan your calendar to see it.</p>
</div> --}}
</section>

@endsection

@push('custom_js')
<script>
   $('[data-toggle="popover"]').popover('show');  
   $(document).on("click", ".popover .closedata" , function(){
        $(this).parents(".popover").popover('hide');
    });
</script>
@endpush
@push('custom_cs')
<style>
    .card-height
    {
        height: 80%;
    }
    .alert{margin-top: 3%}
    </style>
    
@endpush