@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-3 alert " style="background:#FCFF33"  role="alert">
        <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">
        <div class="row ">
       <div class="col-md-6 "><h3 class="">Step 3</h3></div> 
       <div class="col-md-6 text-right"><h3>Profile</h3></div> 
       </div>
        </a>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-sm-12">
                <p class=" text-muted ml-5 tab-text tab-text tab-text tab-text"></p>

                @php
                $count=6;
                @endphp
                  @if ($profile_count==0)
                  <span  class="box alert  text-white bg-danger" ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
                @php
                    $count=4;
                @endphp
                  @endif
                @for($i=1;$i<$profile_count;$i++)
                <span class="box alert  text-white " style="background: #72C21C  "></span>
                
                @endfor
                @if ($profile_count)
                <span  class="box alert  text-white " style="background: #72C21C  " ><p class="tab-arrow-text">NOW</p><i class="fa fa-arrow-down fa-1x  tab-arrow" ></i></span>
               
                @endif
               
                
                @for($i=$profile_count;$i<$count;$i++)
                <span class="box  alert text-white"></span>
                @endfor
            </div>
        </div>
        
    </div>
    <div class="col-sm-2 ">
        @include('teacher_views.inc.RightNav')
    </div>
    
</div>
</section>
<section>
    <div class="row">
        <div class="col-sm-12">
            <div id="container">
                <h1>Video Recording </h1>

                <div class="row">
                  <div class="col-sm-6 text-center">
                    @if (Auth()->user()->video)
                    <video  id="tem_video" controls>
                       <source src="{{ asset('storage/video/'.Auth()->user()->video->name) }}" type="">
                      @else
                    <video  id="tem_video" autoplay>

                      <source src="{{asset('professional_assets/video/stunning_video_mockups.webm')}}" type="video/webm" >
                      @endif
                    </video>
                    <video id="gum" style="display: none" playsinline autoplay muted></video>
                    <div class="text-center">
                    <button id="start" class="btn btn-info">Start Camera</button>
                    <button id="record" class="btn btn-info" disabled>Start Recording</button>
                    
                    </div>
                  </div>
                  <div class="col-sm-6 text-center">
                      <img  class="border-2 " id="demo_image"  src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}">
                       <video id="recorded" style="display: none" playsinline ></video>
                      <div class="text-center mt-2">
                        <button id="play" class="btn btn-info" disabled>Playback</button>
                    {{-- <button id="download" class="btn btn-info" disabled>Download</button> --}}
                    <button id="download" class="btn btn-info" disabled style="display: none">save</button>
                    <button id="upload" class="btn btn-info" disabled>Upload Video</button>
                      </div>
                  </div>
                </div>
            
                
            
                
            
                <div>
                    
                    
                </div>
            
                {{-- <div>
                    <h4>Media Stream Constraints options</h4>
                    <p>Echo cancellation: <input type="checkbox" id="echoCancellation"></p>
                </div> --}}
            
                <div>
                    <span id="errorMsg"></span>
                </div>
            
            
            </div>
            
        </div>

    </div>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('professional/profile/profilevideo') }}">
            @csrf
        <div class="row">
            <div class="col-sm-12">
                    
                @include('common/flash-message')
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Upload  Video</label>
                    <input type="hidden" name="type" value="profile-video">
                    <input type="file" class="form-control-file video-file" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                    @if ($errors->has('file'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('file') }}</strong>
                                  </small> 
                                @endif
                  </div> 
            </div>
            {{-- <div class="col-sm-4 mt-3">
                <button type="submit" class="btn btn-primary">Upload</button>
            </div> --}}
            <div class="col-sm-6 mt-2">
                <a href="{{ url('professional/profile') }}" class="text-decoration-none btn  text-secondary pt-4 "> 
                <p class="btn btn-sm  btn-outline-dark " >Go Back &nbsp;  <span class="fa fa-undo fa-2x text-warning"></span></p>
                </a>
                    
                    <button type="submit" name="save_con" value="10" class="btn btn-sm   btn-outline-dark btn-save" disabled >Save & Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></button>
                    <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                    <a  style="display: none"  href="{{url('professional/profile/image')}}" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                    <a style="display: none" href="{{url('professional')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>  
            
        </div>
            
        </div> 
        </form>   
    </div>
</section>
@endsection
@section('custom_js')
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/main.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
@endsection
@section('custom_css')
<style>
    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */







video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}



    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
 

video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}
/* 
video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
} */
</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {
           
      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide(); 
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });
    </script>
@endpush