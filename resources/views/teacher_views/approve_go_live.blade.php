@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
@include('teacher_views.inc.rightMenu')
<style>

</style>
<div class="row">
    <div class="col-md-12">
        <div class="row">

                <div class="col-md-3 col-xl-3" style="border: none;">
                            <div class="container">
                                 <div class="row">
                                    <div class="card border-1 bg-light rounded-lg">
                                        <div class="card-body">
                                            @foreach ($data as $val)
                                            @if($val['type'] == 'profile-image')
                                            <img src="{!! asset('storage/images/'. $val->name ) !!}" style="height: auto;" class="card-img-top " alt="...">
                                            @endif
                                            @endforeach
                                            <h5 class="card-title text-center">{{$teacher->name}}</h5>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card border-1 bg-light" >
                                        <span class="text-left ml-2">Video</span>

                                        <div class="card-body">

                                            @foreach ($data as $val)
                                            @if($val['type'] == 'profile-video')
                                            <video class="embed-responsive embed-responsive-4by3" src="{!! asset('storage/video/'. $val->name ) !!}"  controls >
                                            </video>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                        <span class="text-left ml-2">Audio</span>
                                            @foreach ($data as $val)
                                            @if($val['type'] == 'profile-audio')
                                            <audio class="embed-responsive-item" controls="" preload="none">
                                                <source src="{!! asset('storage/audio/'. $val->name ) !!}" type="audio/mp3">
                                            </audio>
                                            @endif
                                            @endforeach
                                </div>
                                <div class="card mt-3 border-1 bg-light" >
                                    <h6 class="ml-2">My Social Media links
                                    </h6>

                                    <div class="container">
                                        <div class="row no-gutters mb-2">
                                          <div class="col">
                                            <a href="{{$teacher->youtube}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25"
                                                    height="25" fill="currentColor" class="bi bi-youtube"
                                                    viewBox="0 0 16 16">

                                                    <path d="M8.051 1.999h.089c.822.003 4.987.033
                                                    6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172
                                                    .883.22 1.402l.01.104.022.26.008.104c.065.914.073
                                                    1.77.074 1.957v.075c-.001.194-.01 1.108-.082
                                                    2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235
                                                    1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312
                                                    -5.569.334-6.18.335h-.142c-.309 0-1.587-.006
                                                    -2.927-.052l-.17-.006-.087-.004-.171-.007-.171
                                                    -.007c-1.11-.049 -2.167-.128-2.654-.26a2.007
                                                    2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986
                                                    -.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0
                                                    1 0 7.68v-.123c.002-.215.01-.958.064-1.778l
                                                    .007-.103.003-.052.008-.104.022-.26.01-.104c
                                                    .048-.519.119-1.023.22-1.402a2.007 2.007 0 0
                                                    1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17
                                                    -.007.172-.006.086-.003.171-.007A99.788 99.788
                                                    0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157
                                                    -2.408L6.4 5.209z" />
                                                </svg>
                                            </a>
                                          </div>
                                          <div class="col">
                                            <a href="{{$teacher->facebook}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                                                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                                  </svg>
                                            </a>
                                          </div>
                                          <div class="col">
                                            <a href="{{$teacher->twitter}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25"
                                                    height="25" fill="currentColor" class="bi bi-twitter"
                                                    viewBox="0 0 16 16">

                                                    <path d="M5.026 15c6.038 0 9.341-5.003
                                                    9.341-9.334 0-.14 0-.282-.006-.422A6.685
                                                    6.685 0 0 0 16 3.542a6.658 6.658 0 0
                                                    1-1.889.518 3.301 3.301 0 0 0 1.447-1.817
                                                    6.533 6.533 0 0 1-2.087.793A3.286 3.286 0
                                                    0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429
                                                    3.289 3.289 0 0 0 1.018 4.382A3.323 3.323
                                                    0 0 1 .64 6.575v.045a3.288 3.288 0 0 0
                                                    2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23
                                                    3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067
                                                    2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32
                                                    0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
                                                </svg>
                                            </a>
                                          </div>
                                          <div class="col">
                                            <a href="{{$teacher->LinkedIn}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                                    <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                                                  </svg>
                                            </a>
                                          </div>

                                        </div>
                                      </div>
                                </div>

                            </div>

                </div>

                <div class="col-md-6 col-xl-6">
                        <div class="container">
                            <div class="row">
                                <div class="card bg-light" >
                                    <div class="card-body">
                                        <h5 class="card-title font-weight-bold">Professional Profile</h5><hr style="height:2px;border:none;color:#333;background-color:#333;">
                                        @foreach ($data as $val)
                                        @if($val['type'] == 'profile-detail')
                                        <p>
                                            {!! $val->name !!}
                                        </p>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="card bg-light" >
                                    <div class="card-body">
                                        <h5 class="card-title font-weight-bold border-0">Core Skill</h5><hr style="height:2px;color:#333;background-color:#333;" >
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <ul class="">
                                                        <li>Reviewing, Reporting, and Research.</li>
                                                        <li>Team Management.</li>
                                                        <li>Decision Making.</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <ul class="">
                                                        <li>Reviewing, Reporting, and Research.</li>
                                                        <li>Team Management.</li>
                                                        <li>Decision Making.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="card mt-2 bg-light">
                        <h5 class="card-title text-center font-weight-bold">Courses / Subjects I Teach</h5>
                        <div class="container">
                            <div class="row">
                            @foreach($courses as $course)

                                <div class="col-md-4 col-xl-4">
                                        <div class="card" style="background-color: #F0F8FF;">
                                                <small class="align-items-center font-weight-bold" >{{$course->coure_title}}</small>
                                        </div>
                                        <div class="row">

                                            <div class="col">
                                                <p ><a class="text-white " href="#" style="font-size: 10px;padding: 4px;background-color:#D3D3D3;"> line</a></p>
                                            </div>
                                            <div class="col">
                                                <p ><a class="bg-primary text-white " href="#" style="font-size: 10px;padding: 4px;"> $200</a></p>
                                            </div>
                                            <div class="col">
                                                <p><a class="bg-success text-white" href="#" style="font-size: 10px;padding: 4px;"> select</a></p>
                                            </div>
                                        </div>
                                </div>
                            @endforeach

                            </div>

                            <div class="row mt-3">
                                <div class="container">
                                    <div class="row mb-2">
                                        <div class="col">
                                            <div class="card">
                                                <span class="border rounded p-3 text-center bg-warning">Testimonials</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card" style="background-color: #F0F8FF;">
                                                <span class="border text-center">Dennis Merchant</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card" style="background-color: #F0F8FF;">
                                                <span class="border text-center">Saima Abdullah</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card" style="background-color: #F0F8FF;">
                                                <span class="border text-center">Qaiser kayani</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card" style="background-color: #F0F8FF;">
                                                <span class="border text-center">Susan Parker</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card mt-2" style="background-color: #FFF0F5;">
                        <h5 class="card-title text-center">Notice Board</h5>

                        <div class="container">
                            <div class="row" >
                                @foreach($courses as $course)
                                <div class="col-md-4">
                                    <h5 class="card-title text-center">{{substr($course->coure_title, 0,  17)}} </h5>

                                    <p>
                                        {!! substr($course->course_description, 0,  40) !!}
                                    </p>
                                </div>
                                @endforeach
                            </div>
                            {{-- <div class="row justify-content-md-center" >
                                <div class="col-md-4">
                                    <h5 class="card-title text-center">ENGL 201 </h5>

                                    <p>
                                        Winter Term Timetable<br>
                                        Starts 10 December 2021<br>
                                        Ends 15 February 2022  <br>
                                    </p>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>


                <div class="col-md-3 col-xl-3">

                        <div class="card bg-light">
                            <h5 class="card-title text-center">Qualification</h5>
                            <div class="card-body">
                            <h5 class="card-title text-primary font-weight-bold">Education</h5>
                                @foreach($profesional_edus as $profesional_edu)
                                @if($profesional_edu->edu_type =="edu_qual")
                                    <p class="font-weight-normal">{{$profesional_edu->degree_name}}</p>
                                 @endif
                                @endforeach
                                    <h5 class="card-title text-primary font-weight-bold">
                                        Professional
                                </h5>
                                @foreach($profesional_edus as $profesional_edu)
                                @if($profesional_edu->edu_type =="pro_edu")
                                    <p class="font-weight-normal">{{$profesional_edu->degree_name}}</p>
                                 @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="card  mt-3" style="background-color: #F0F8FF;">
                        @include('common/flash-message')

                            <form class="mt-3" method="POST" action="{{url('professional/profile/contact-form')}}" style="padding: 20px;">
                                @csrf
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Name</label>
                                  <input type="text" name="name" class="form-control form-control-sm @error('name')is-invalid @enderror" placeholder="Please Enter your Name !">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Email</label>
                                  <input type="email" name="email" class="form-control form-control-sm @error('email')is-invalid @enderror" placeholder="Please Enter your Email">
                                    @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Message</label>
                                    <textarea class="form-control @error('message')is-invalid @enderror" name="message" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    @error('message')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                              </form>
                        </div>

                        <div class="card bg-light mt-3">
                            <h6 class="text-center mt-1">Please rate my profile</h6>

                            <div class="container">
                                <div class="row mb-4">
                                  <div class="col">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-star" viewBox="0 0 16 16">
                                        <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                                      </svg>
                                  </div>
                                  <div class="col">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-star" viewBox="0 0 16 16">
                                        <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                                      </svg>
                                  </div>
                                  <div class="col">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-star" viewBox="0 0 16 16">
                                        <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                                      </svg>
                                  </div>
                                  <div class="col">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-star" viewBox="0 0 16 16">
                                        <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                                      </svg>
                                  </div>


                                </div>
                              </div>
                        </div>
                        <div class="mt-3 border-0 font-weight-bold">
                            <h6 class="text-center text-danger">
                                Terms & Conditions
                            </h6>

                        </div>

                </div>
            </div>

        </div>


    </div>
</div>
</section>
@endsection
