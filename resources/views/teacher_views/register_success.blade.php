<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <title>{{ config('app.name', 'Laravel') }}</title>
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
      
        <!-- Perfect Scrollbar -->
        <link type="text/css" href="{{asset('css/site_assets/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.rtl.css')}}" rel="stylesheet">
        <!-- Font Awesome Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.rtl.css')}}" rel="stylesheet">
        <!-- App CSS -->
        <link type="text/css" href="{{asset('css/site_assets/css/app.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/app.rtl.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/custom.css')}}" rel="stylesheet">
        


        <style>
        .login-color{
        background-color: rgb(245, 182, 131) !important;
        
        }
        .login-dp{
        color: deepskyblue !important;
        }
        </style>
    </head>
    <body class="login" style="background: #ffffff">
        
        <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}" alt="LOGO" class="" style="margin:0 auto;">

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <div class=" text-center">
                            <h2 class="">Thank You! </h2>
                        </div>
                        <div class=" text-center">
                            <h4 class="">Your Have Successfully Registered</h4>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        
                        <div class="text-center">
                            <h2 class="mt-1 text-success" >Congratulations!</h2>
                        </div>
                        {{-- <div class="text-center">
                            <h6 class="mt-1 " >Your Membership / A/C Number is:</h6>
                        </div>
                        <div class="text-center">
                            <h6 class=" " >XXXX 22344556676</h6>
                        </div>
                        <div class="text-center">
                            <h6 class=" " >Your account Manager is Ms Lucy Fulbright</h6>
                        </div> --}}
                        <div class="text-center">
                            <h2 class="bold " style="font-family:Forte " >You are a viizard now!</h2>
                            <h6 class=" " >Please Login your details and access your</h6>
                            <h3>Admin Control Panel (ACP)</h3>
                        </div>
                       
                    </div>
                </div>
                <div class="">
                    <div class="justify-content-center d-flex ">
                        <a  href="{{ route('teacher.login') }}" class="btn btn-primary text-white">Login</a>
                    </div>
                </div>
                
            </div>
        </body>
        
    </html>
    
   