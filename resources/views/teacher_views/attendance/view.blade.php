@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
table, tr, td, th {
  border: 1px solid !important;
}
</style>
<section>
    <div class="row">
        <div class=" col-md-12  form-group " >
            @include('teacher_views.attendance.common')

            <div class="row mt-2">
                <div class="col-md-2">
                    <div class=" form-control text-center" style="background-color:#FADBD8;" >
                        <p class="font-weight-bold">Cluster </p>
                      </div>
                </div>
                <div class="col-md-12">
                  <table class="mt-1 table">

                    <tr>
                      <th scope="col">REF</th>
                      <th scope="col">Date</th>
                      <th scope="col">Time</th>
                      <th scope="col">Duration </th>
                      <th scope="col">Regd. Students </th>
                      <th scope="col">Attended </th>
                      <th scope="col">Absent </th>
                    </tr>
                    @forelse($attendances as $attendance)
                    <tr>
                      <td scope="row">{{$attendance->reference_number}}</td>
                      <td>{{$attendance->date}}</td>
                      <td>{{$attendance->time}}</td>
                      <td>{{$attendance->duration}} </td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    @empty
                        <p>Empty</p>
                    @endforelse
                  </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
