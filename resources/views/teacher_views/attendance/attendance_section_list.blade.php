@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<!-- <section>
    <div class="row">
        <div class=" col-md-10  form-group " >
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course code</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">ENGL 101</p>
                      </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-3">
                    <div class=" form-control     color-bar-green " >
                        <p class="font-weight-bold">Course Title</p>
                      </div>
                </div>
                <div class="col-md-9">
                    <div class=" form-control     color-bar-green courde_id" data-course="{{$course->id}}" >
                        <p class="font-weight-bold">{{$course->coure_title}}</p>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row ">

                <div class="col-md-12 ">

                    <div  class="card tab-box syllabus-color"  style=" border-color: green;border-style: dashed">
                        <div  class="card-body">
                            <h3 class="card-title  text-center">Attendance</h3>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 ">


                <div class="card border-top-3 border-top-success">

                    <div class="card-body text-center">
                        <h4 class="card-title">Cluster 1</h4>
                        @foreach ($clusters as $cluster)

                        <div class="">
                        <p>

                                {{str_replace(' ','_',$course->coure_title).'_00'.$cluster->id}}

                        </p>
                        <p>
                            <strong> {{date('M-d-Y',strtotime($cluster->start_date))}} to  {{date('M-d-Y',strtotime($cluster->end_date))}}</strong>

                        </p>
                        </div>
                        @endforeach

                        <div class="page-separator">
                            <div class="page-separator__text">Attendance</div>
                        </div>

                        <div class="text-center row">
                            <div  class=" text-center col-6">
                                <a href="{{url('professional/attendance/type',[$course->id,$cluster->id,'live_class_session'])}}" class="btn btn-white  btn-block flex-column">
                                    View  Attendance
                                     </a>
                            </div>
                            <div  class=" text-center col-6">
                               <a href="{{url('professional/attendance/form',[$course->id,$cluster->id,'live_class_session'])}}" class="btn btn-white  btn-block flex-column">
                              Mark  Attendance
                               </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


             </div>


        </div>

    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
