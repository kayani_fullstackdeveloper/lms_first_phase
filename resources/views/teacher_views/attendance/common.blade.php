<div class="row mt-2">
    <div class="col-md-4">
        <div class=" form-control color-bar-green text-white text-center" >
            <a href="{{url('professional/attendance/'.$course->id)}}" class="text-decoration-none text-white">Attendance</a>

          </div>
    </div>
    <div class="col-md-2">
        <div class=" form-control text-center" style="background-color: #eff1f2;">
            <a href="{{url('professional/attendance/type',[$course->id,$section->id,'live_class_session'])}}" class="text-decoration-none text-dark">Live Class Session</a>
          </div>
    </div>
    <div class="col-md-2">
        <div class=" form-control text-center" style="background-color: #eff1f2;">
            <a href="{{url('professional/attendance/type',[$course->id,$section->id,'discussion_board'])}}" class="text-decoration-none text-dark">Discussion Board</a>
          </div>
    </div>
    <div class="col-md-2">
        <div class=" form-control text-center" style="background-color: #eff1f2;">
            <a href="{{url('professional/attendance/type',[$course->id,$section->id,'Lesson_lectures'])}}" class="text-decoration-none text-dark">Lesson Lectures</a>
          </div>
    </div>
</div>
