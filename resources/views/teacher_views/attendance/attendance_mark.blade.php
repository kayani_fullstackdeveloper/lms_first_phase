@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')

<section>
    <div class="row">
        <div class="col-md-10">
            <div class="row ">


                <div class="col-md-12 ">

                    <div  class="card tab-box syllabus-color"  style=" border-color: green;border-style: dashed">

                        <div  class="card-body">
                            <div class="row">
                                <div class="col-sm-3">

                            <h4 class="card-title">Attendance Mark</h4>
                                </div>
                                <div class="col-sm-8">
                            <h3 class="card-title  text-center">{{str_replace(' ','_',$course->coure_title).'_00'.$section->id}} </h3>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 ">


                    <div class="card ">

                        <div class="card-body ">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h4 class="card-title">Date: {{Carbon\Carbon::now()->format('d-M-Y');}}</h4>
                                </div>
                                <div class="col-sm-4">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox"  value="" id="invalidCheck01" >
                                        <label class="custom-control-label" for="invalidCheck01">
                                            All Present
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">

                                </div>
                                <div class="col-sm-2">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                <div class="row">
                    <div class="col-md-3">
                        <div class=" form-control color-bar-green text-white text-center" >
                            <a href="{{url('professional/attendance/'.$course->id)}}" class="text-decoration-none text-white">Attendance</a>
                
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class=" form-control text-center" style="background-color: #eff1f2;">
                            <a href="{{url('professional/attendance/form',[$course->id,$section->id,'live_class_session'])}}" class="text-decoration-none text-dark">Live Class Session</a>
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class=" form-control text-center" style="background-color: #eff1f2;">
                            <a href="{{url('professional/attendance/form',[$course->id,$section->id,'discussion_board'])}}" class="text-decoration-none text-dark">Discussion Board</a>
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class=" form-control text-center" style="background-color: #eff1f2;">
                            <a href="{{url('professional/attendance/form',[$course->id,$section->id,'lesson_lectures'])}}" class="text-decoration-none text-dark">Lesson Lectures</a>
                          </div>
                    </div>
                </div>

                </div>

<div class="col-md-12 ">
                <form action="{{url('professional/attendance/form')}}" method="POST">
                  @csrf
                  <input type="hidden" name="course_id" value="{{$course->id}}">
                  <input type="hidden" name="section_id" value="{{$section->id}}">
                  @if(Request()->type =='live_class_session')
                        <input type="hidden" name="type" value="live_class_session">
                        @elseif(Request()->type =='discussion_board')
                        <input type="hidden" name="type" value="discussion_board">
                        @elseif(Request()->type =='lesson_lectures')
                        <input type="hidden" name="type" value="lesson_lectures">
                        @endif
                  <div class="card ">

                      <div class="card-body ">
                        <div class="row mt-2">
                          <div class="col-sm">
                              <div class=" form-control     color-bar-yellow  text-dark text-center " >
                                <span class="font-weight-bold text-decoration-none"> Reference</a>
                              </div>
                                <input type="text" name="reference_number" value="{{$attendance->reference_number ?? ''}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="REF">
                                @error('reference_number')
                                  <label class="error mt-2 text-danger">{{ $message }}</label></br>
                                @enderror
                          </div>
                          <div class="col-sm">
                              <div class=" form-control     color-bar-yellow  text-dark text-center " >
                                <span class="font-weight-bold text-decoration-none"> Date</a>
                              </div>
                              <input type="date" name="date" value="{{$attendance->date ?? ''}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Date">
                              @error('date')
                                <label class="error mt-2 text-danger">{{ $message }}</label></br>
                              @enderror
                          </div>
                          <div class="col-sm">
                              <div class=" form-control     color-bar-yellow  text-dark text-center " >
                                <span class="font-weight-bold text-decoration-none"> Time</a>
                              </div>
                              <input type="time" name="time" value="{{$attendance->time ?? ''}}" class="mt-3 form-control color-bar-white text-dark"  placeholder="Grading">
                              @error('time')
                                <label class="error mt-2 text-danger">{{ $message }}</label></br>
                              @enderror
                          </div>
                          <div class="col-sm">
                              <div class=" form-control     color-bar-yellow  text-dark text-center " >
                                <span class="font-weight-bold text-decoration-none"> Duration</a>
                              </div>
                              <select class="mt-3 form-control color-bar-white text-dark " name="duration" >
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                                <option value="60">60</option>
                              </select>
                              @error('duration')
                                <label class="error mt-2 text-danger">{{ $message }}</label></br>
                              @enderror

                          </div>
                        </div>
                        <div class="row mt-2">
                          <div class="col-sm">

                            <button class="btn btn-primary btn-outline-dark"  name=""  type="submit">Save</button>

                          </div>
                        </div>
                      </div>
                  </div>
                    <div class="card ">

                        <div class="card-body ">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h4 class="card-title">List</h4>
                                </div>
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-2">
                                    Present
                                </div>
                                <div class="col-sm-2">
                                    absent
                                </div>
                            </div>
                        </div>
                    </div>
                @foreach ($students as $student)

                <div class="col-md-12 ">


                <div class="card border-top-3 border-top-success">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="hidden" name="userid[]" value="{{$student->student->id}}">
                                <input name="{{$student->student->id}}" value="{{ $student->student->name}}" type="text" class="form-control" readonly>

                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col">
                                <div class="custom-control custom-radio">
                                  <input type="radio" name="attendance{{$student->student->id}}"  value="p" class="form-control w-25" >
                                    <label for="p{{$student->id}}" >P</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-radio">
                                  <input type="radio" name="attendance{{$student->student->id}}" value="a" class="form-control w-25">

                                    <label for="a{{$student->id}}" >A</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-radio">
                                  <input type="radio" name="attendance{{$student->student->id}}" value="l" class="form-control w-25">

                                    <label  for="l{{$student->id}}">L</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
              </form>
             </div>
           </div>

        </div>
    </div>
</section>
@endsection
@push('custom_js')
    <script>

        $('#invalidCheck01').change(function (e) {
            e.preventDefault();
            if(this.checked)
            {
                $('.all_present').attr('checked','checked');
                $('.all_absent').removeAttr('checked');

            }else
            {
                location.reload()
            }

        });
    </script>
@endpush
