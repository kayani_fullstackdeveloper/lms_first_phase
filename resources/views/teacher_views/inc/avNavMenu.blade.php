@include('teacher_views.inc.RightNav')
<br>

<div class="container mb-4">
    <div class="row">
        <div class="col-md-2 ">
            <div class="avatar avatar-xl">
                <img src="{{asset('css/site_assets/images/logo/av-library.png')}}" alt="LOGO" class="border img-fuild">
            </div>
        </div>
        <div class="col-md-10  ">
            <div class="container ">
                <div class="row ">
                    <div class="col-sm-2">
                        <a href="javascript:history.back()" class="text-decoration-none w-100  btn btn-sm  border-3  btn-outline-dark"> 
                            <span> Go Back &nbsp;  <span class="fa fa-undo "></span>
                            </a>
                    </div>
                    
                    <div class="col-sm-2 ">
                        <a href="{{ url('professional/profile?modify=edit_oncreate') }}" class="text-decoration-none w-100  btn btn-sm btn-danger border-0 text-white "> 
                        <span> Update</span> 
                        </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn btn-sm btn-success border-0 text-white btn-outline-dark"> 
                        <span> Course/s</span> 
                            </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="{{  url('/') }}" class="text-decoration-none w-100  btn btn-sm btn-primary border-0 text-white btn-outline-dark"> 
                        <span> Finances</span> 
                            </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn btn-sm bg-dark border-0 text-white btn-outline-dark"> 
                        <span>Dashboard</span> 
                            </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn btn-sm bg-secondary border-0 text-white btn-outline-secondary"> 
                        <span>(ACP)</span> 
                            </a>
                    </div>
                    
                </div>
                <div class="row mt-2">
                    <div class="col-sm-3 ">
                        <div class="dropdown">
                            
                            <button role="button" type="button" 
                            class="btn btn-sm w-100 text-dark font-weight-bold 
                            dropdown color-bar-yellow" 
                            data-toggle="dropdown"
                            
                            > 
                                View AV Files
                            </button>
                            <div class="dropdown-menu p-0 m-0"  aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item mb-1 text-dark " href="{{url('professional/av-library/view-all')}}" style="border: 1px solid black !important;">View All Files</a>
                              <a class="dropdown-item mb-1 text-dark" href="{{url('professional/av-library/view-audio-file')}}" style="border: 1px solid black !important;">View Audio Files</a>
                              <a class="dropdown-item mb-1 text-dark" href="{{url('professional/av-library/view-video-file')}}" style="border: 1px solid black !important;">View Video Files</a>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-3 ">
                        <div class="dropdown">
                            
                            <button role="button" type="button" class="btn btn-sm w-100 text-dark font-weight-bold dropdown color-bar-orange" data-toggle="dropdown"> 
                                Create New AV File
                            </button>
                            
                            <div class="dropdown-menu p-0 m-0"  aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item mb-1 text-dark " href="{{url('professional/av-library/create-audio-file')}}" style="border: 1px solid black !important;">Create Audio</a>
                                <a class="dropdown-item mb-1 text-dark" href="{{url('professional/av-library/create-video-file')}}" style="border: 1px solid black !important;">Create Video</a>
                              </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row mt-2">
                    @if(Request::path() == 'professional/av-library/view-all')
                    <div class="col-sm-3 ">
                        <a href="#" class="text-decoration-none w-100  btn btn-sm color-bar-gray border-0 text-white font-weight-bold"> 
                        <span> All AV Files</span> 
                        </a>
                    </div>
                    @elseif(Request::path() == 'professional/av-library/view-audio-file')
                    <div class="col-sm-3 ">
                        <a href="#" class="text-decoration-none w-100  btn btn-sm color-bar-gray border-0 text-white font-weight-bold"> 
                        <span> Audio Files</span> 
                        </a>
                    </div>
                    @elseif(Request::path() == 'professional/av-library/view-video-file')
                    <div class="col-sm-3 ">
                        <a href="#" class="text-decoration-none w-100  btn btn-sm color-bar-gray border-0 text-white font-weight-bold"> 
                        <span> Video Files</span> 
                        </a>
                    </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
   
</div>