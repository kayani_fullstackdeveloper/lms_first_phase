<section class="mt-5" >
  <div class="row ">
  <div class="col-md-4 alert " style="background:#FCFF33"  role="alert">
      <a href="{{ url('professional/profile/approve-go-live') }}" class="text-decoration-none">
      
      <div class="row ">
      
     <div class="col-md-12 "><h3>Profile</h3></div> 
     
     </div>
      </a>
  </div>
  <div class="col-sm-3">
    
    </div>
  <div class="col-sm-3">
    
  <a href="{{ url('professional/profile?modify=true') }}" class="btn btn-warning btn-sm  w-100 font-weight-bold display-decoration-none text-white">
    <i class="fa fa-edit p-2"></i>
Edit / Go Back
</a>
  </div>
  <div class="col-sm-2">
      @include('teacher_views.inc.RightNav')
  </div>
 
  
</div>
</section>
<section class="mb-3">
  <div class="row">
      <div class="col-sm-2">
          <a href="javascript:history.back()" class="text-decoration-none w-100  btn  border-3  btn-outline-dark"> 
              Go Back &nbsp;  <span class="fa fa-undo "></span>
              </a>
         </div>
      <div class="col-sm-2">
          <a href="{{ url('professional/profile?modify=edit_oncreate') }}" class="text-decoration-none w-100  btn btn- btn-danger border-0 text-white btn-outline-dark"> 
            <span>  Update  Profession</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn btn- btn-success border-0 text-white btn-outline-dark"> 
            <span> Course/s</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('/') }}" class="text-decoration-none w-100  btn btn- btn-primary border-0 text-white btn-outline-dark"> 
            <span> Finances</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn bg-dark border-0 text-white btn-outline-dark"> 
            <span>Dashboard</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
            <span>(ACP)</span> 
              </a>
         </div>
  </div>
</section>