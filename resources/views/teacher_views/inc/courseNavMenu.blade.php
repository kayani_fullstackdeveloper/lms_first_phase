<section class="mt-5" >
  <div class="row ">
  <div class="col-md-4 alert " style="background: #72C21C"  role="alert">
      <a href="{{ url('professional/courselist') }}" class="text-decoration-none">
        <div class="row ">
        
          <div class="col-md-12 ">
            <h3 class="text-white mb-3">Course/s</h3>
            @if(Request::path() != 'professional/courselist' && Request::path() != 'professional/course/create')
            <p class="text-white">{{$course->coure_title}}</p>
           @endif
          </div> 
      </div>
      </a>
  </div>
  <div class="col-sm-3">
     
    </div>
  <div class="col-sm-3">
    
  {{-- <a href="{{ url('professional/profile?modify=true') }}" class="btn btn-warning btn-sm  w-100 font-weight-bold display-decoration-none text-white">
    <i class="fa fa-edit p-2"></i>
Edit / Go Back --}}
</a>
  </div>
  <div class="col-sm-2">
      @include('teacher_views.inc.RightNav')
  </div>
 
  
</div>
</section>
<section class="mb-3">
  <div class="row">
      <div class="col-sm-2">
          <a href="javascript:history.back()" class="text-decoration-none w-100  btn  border-3  btn-outline-dark"> 
              Go Back &nbsp;  <span class="fa fa-undo "></span>
              </a>
         </div>
      <div class="col-sm-2">
          <a href="{{ url('professional/profile?modify=edit_oncreate') }}" class="text-decoration-none w-100  btn btn- btn-danger border-0 text-white btn-outline-dark"> 
            <span>  Update  Profession</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/course/create') }}" class="text-decoration-none w-100  btn btn- btn-success border-0 text-white btn-outline-dark"> 
            <span> Course/s</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('/') }}" class="text-decoration-none w-100  btn btn- btn-primary border-0 text-white btn-outline-dark"> 
            <span> Finances</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn bg-dark border-0 text-white btn-outline-dark"> 
            <span>Dashboard</span> 
              </a>
         </div>
         <div class="col-sm-2">
          <a href="{{  url('professional/') }}" class="text-decoration-none w-100  btn  bg-secondary border-0 text-white btn-outline-secondary"> 
            <span>(ACP)</span> 
              </a>
         </div>
        
  </div>
  @if(Request::path() != 'professional/courselist')
  <div class="row float-right mb-4">
    <div class="col-md-12 col-sm-12 ">
      <a href="#" class="text-decoration-none w-100  btn  color-bar-yellow border-0 text-dark btn-outline-secondary"> 
          <span>
              <span class="font-weight-bold">View CDP</span><br>
          <small class="mb-4">Course Delivery Planning</small> 
  
          </span> 
      </a>
  </div>
 </div>
 @endif
</section>
@if(Request::path() != 'professional/courselist' && Request::path() != 'professional/course/create')
<section>
  <div class="row">
      <div class="col-sm-2">
        <a  class="text-decoration-none w-100  btn  color-bar-green border-0 text-white btn-outline-dark"> 
          <span> Course code</span> 
        </a>
      </div>
      <div class="col-sm-8">
        <a  class="text-decoration-none w-100  btn btn- color-bar-green border-0 text-white btn-outline-dark"> 
          <span>  ENGL 101</span> 
        </a>
      </div>
      <div class="col-sm-2">
        <a href="{{url('professional/course/create')}}" title="New Course Input" class="text-decoration-none w-100  btn btn- color-bar-green border-0 text-white btn-outline-dark"> 
          <span> NCI</span> 
        </a>
      </div>   
  </div>
  <div class="row mt-1">
    <div class="col-sm-2">
      <a  class="text-decoration-none w-100  btn btn- color-bar-green border-0 text-white btn-outline-dark"> 
        <span> Course Title</span> 
      </a>
    </div>
    <div class="col-sm-8">
      <a  class="text-decoration-none w-100  btn btn- color-bar-green border-0 text-white btn-outline-dark"> 
        <span>  {{$course->coure_title}}</span> 
      </a>
    </div>
    <div class="col-sm-2">
      <a href="{{url('professional/courseoption/'.$course->id)}}" title="Course Delivery Framework" class="text-decoration-none w-100  btn btn- color-bar-green border-0 text-white btn-outline-dark"> 
        <span> CDF</span> 
      </a>
    </div>   
</div>
</section>
@endif
