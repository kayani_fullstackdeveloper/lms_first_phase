<ul class="nav navbar-nav flex-nowrap form-group float-right">




    <!-- Notifications dropdown -->
    <li class="nav-item dropdown dropdown-notifications dropdown-menu-sm-full">
        <button class="nav-link btn-flush dropdown-toggle" type="button" data-toggle="dropdown" data-dropdown-disable-document-scroll="" data-caret="false" aria-expanded="false">
            <!-- <i class="material-icons">setting</i> -->
            <span class="float-right">Settings <i class="fa fa-cog"></i></span>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <div data-perfect-scrollbar="" class="position-relative ps">
              
                
                <div class="form-control side-menu "><a href=""><strong>Faqs</strong></a></div>
                <div class="form-control side-menu"><a href=""><strong>Support</strong></a></div>
                <div class="form-control side-menu"><a href=""><strong>Vizzard.com</strong></a></div>
                <div class="form-control side-menu"><a href=""><strong>Contact us</strong></a></div>
                <div class="form-control side-menu text-white bg-danger"><a
                    href="{{url('professional/logout/teacher')}}"><strong>Logout</strong></a></div>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
        </div>
    </li>
    <!-- // END Notifications dropdown -->
   
</ul>