@section('title', __(Auth()->user()->name.' panel'))
@extends('teacher_views.app')
@section('content')
@include('teacher_views.inc.rightMenu')
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card tab-box   " style="background-color:#F8F8F8;" >
                        <div class="card-body">
                           
                           <div class="row">
                               <div class="col-sm-3">
                                   <img src="{{ asset('storage/thumb/'.Auth::user()->image->name) }}">
                               </div>
                               <div class="col-sm-9">
                                   <div class="row mt-3">
                                       <div class="col-sm-6">
                                           <h3 class="text-uppercase">{{Auth()->user()->name}}</h3>
                                       </div>
                                       <div class="col-sm-6  ">
                                        <p class=" ">{{number_format(($media/6)*100)}}% Profile Complete</p><br>
                                           

                                       </div>
                                       <div class="col-sm-6">
                                        <span> <i class=" fa fa-map-marker mr-2" aria-hidden="true"></i>{{Auth()->user()->address}}</span>
                                       </div>
                                       <div class="col-sm-6">
                                           @for ($i =0; $i <=$media-1; $i++)
                                              @if ($i<$media)
                                                  
                                              <span class="box alert  text-white bg-success"></span>
                                              @else
                                                <span class="box alert  text-white"></span>
                                              @endif 
                                           @endfor
                                           {{-- <span class="box alert  text-white bg-success"></span>
                                           <span class="box alert  text-white bg-success"></span>
                                           <span class="box alert  text-white bg-success"></span>
                                           <span class="box alert  text-white"></span>
                                           <span class="box alert  text-white"></span> --}}
                                       </div>
                                   </div>
                                   <hr>
                                   <div class="row">
                                       <div class="col-sm-2 text-center"> 
                                            <h3 class="text-primary font-weight-bold">{{$student}}</h3>
                                            <p class="font-weight-bold mb-4">Students</p>
                                       </div>
                                       <div class="col-sm-2 text-center"> 
                                            <h3 class="text-primary font-weight-bold">{{$course->count()}}</h3>
                                            <p class="font-weight-bold mb-4">Courses</p>
                                       </div>
                                       <div class="col-sm-2 text-center"> 
                                            <h3 class="text-primary font-weight-bold">{{$other}}</h3>
                                            <p class="font-weight-bold mb-4">others</p>
                                       </div>
                                       <div class="col-sm-6 text-center mt-3 "> 
                                        <a href="{{ url('professional/profile/approve-go-live') }}" class="btn btn-primary font-weight-bold display-decoration-none text-white">
                                            <i class="fa fa-check p-2"></i>

                                               Approve/ Go Live 
                                        </a>
                                        <a href="{{ url('professional/profile?modify=true') }}" class="btn btn-warning font-weight-bold display-decoration-none text-white">
                                            <i class="fa fa-edit p-2"></i>
                                           Edit / Go Back
                                        </a>
                                   </div>
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                
            
        </div>
        <div class="row">
            
            <div class="col-md-4">
                <div class="card tab-box box-hover  {{  request()->is('professional/profile') ? 'box-active' : '' }}">
                    <a href="{{ url('professional/profile') }}" class="text-decoration-none">
                   <div class="card-body">
                        <h5 class="card-title">Profile</h5>
                    </div>
                    </a>
                </div>
                <div class="card tab-box box-hover ">
                    <div class="card-body">
                        <h5 class="card-title">Students</h5>
                    </div>
                </div>
                <div class="card tab-box  box-hover {{  request()->routeIs('professional/course.*') ? 'box-active' : '' }}">
                    <a href="{{ url('professional/course') }}" class="text-decoration-none">
                    <div class="card-body">
                        <h5 class="card-title">Courses</h5>
                    </div>
                    </a>
                </div>
                <div class="card tab-box box-hover">
                    <div class="card-body">
                        <h5 class="card-title">Social Media</h5>
                    </div>
                </div>
                <div class="card tab-box box-hover">
                    <div class="card-body">
                        <h5 class="card-title">Online session</h5>
                    </div>
                </div>
                <div class="card tab-box box-hover">
                    <div class="card-body">
                        <h5 class="card-title">Other</h5>
                    </div>
                </div>
                
            </div>
            <div class="col-md-8">
                @yield('profile_layout')
                
            </div>
            
        </div>
        </div>
    </div>
</section>
@endsection
