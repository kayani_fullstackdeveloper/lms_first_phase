@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.' panel'))
@section('content')

@include('teacher_views.inc.avNavMenu')
<section>
    <div class="row">
        <div class="col-sm-12">

            @include('common/flash-message')
        </div>
        <div class="col-sm-12">
            <div id="container" class="container">

                <div id="player">

                    <h1 class="mt-3">Audio Recording </h1>
                    <div class="row">
                        <div class="col-sm-4">



                            @if (Auth()->user()->avLibraryAudio)
                            <audio   controls >
                        <source src="{{ asset('storage/av-library/audio/'.Auth()->user()->avLibraryAudio->file) }}" type="audio/mpeg">

                                @else
                            <audio   controls autoplay >

                                @endif
                            </audio>

                        </div>
                        <div class="col-sm-8 mt-2">
                            <form method="POST" enctype="multipart/form-data" id="audioForm">
                              <input type="hidden" class="form-control course_id" id="course_id" name="course_id" value="{{$course->id}}">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Title</label>
                                            <input type="text" class="form-control title  @error('last_name')is-invalid @enderror"
                                            name="title" >
                                            @error('title')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category</label>

                                            <select name="category" class="form-control category">
                                              <option value="potcast" >potcast</option>
                                              <option value="lesson_lecture" >lesson lecture</option>
                                              <option value="assesment" >Assesment</option>
                                                </select>
                                            @error('category')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Reference Number</label>
                                            <input type="text" class="form-control reference_number  @error('last_name')is-invalid @enderror"
                                            name="reference_number">
                                            @error('reference_number')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </form>
                            <button class="btn btn-success" id="btn-start-recording">Start Recording</button>
                            <button class="btn btn-success" id="btn-stop-recording" disabled>Stop Recording</button>
                            <button class="btn btn-success" style="display: none" id="btn-download" disabled>Save</button>
                            <a href="{{url('professional/av-library/create-audio-file')}}" class="btn btn-success">Clear</a>
                            <button class="btn btn-success" id="btn-upload" disabled>Upload Recording</button>

                        </div>

                    </div>


                    <hr>

                </div>
            </div>


        </div>

    </div>
    <div class="container mt-5">
        <form id="audioForm" method="POST" enctype="multipart/form-data" action="{{ url('professional/av-library/audio') }}">
            @csrf
        <div class="row">
          <input type="hidden" class="form-control course_id" id="course_id" name="course_id" value="{{$course->id}}">

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Upload  Audio</label>
                    <input type="hidden" name="type" value="av-library-audio">
                    <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                    @if ($errors->has('file'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('file') }}</strong>
                                  </small>
                                @endif
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <input type="hidden" class="form-control " id="hidetitle" name="title" >

                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="hidden" class="form-control " id="category" name="category" >


                    <select hidden   name="category" id="hidecategory" class="form-control category">
                        <option  value="podcast">podcast</option>
                        <option  value="lesson_lecture">lesson lecture</option>
                        <option value="assesment">Assesment</option>
                        </select>
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <input type="hidden" class="form-control " id="reference_number" name="reference_number" >

                  </div>
            </div>

            <div class="col-sm-4">
                <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                    <a  style="display: none"  href="#" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                    <a style="display: none" href="{{url('professional/av-library/create-audio-file')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>

            </div>

            {{-- <div class="col-sm-4 mt-4">
                <button type="submit" class="btn btn-primary m-1">Upload</button>
            </div> --}}

        </div>
        </form>
    </div>
</section>
@endsection
@section('custom_js')
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/avlibrary-audio.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
<script>
//    var recorder;
//     var blob = recorder.getBlob();
//     console.log(blob)
</script>
@endsection
@section('custom_css')
<style>
    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */







video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}



    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */


video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}
/*
video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
} */
</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {

      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide();
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });

    $(document).on('change', '.title', function() {
    var title_id =  $(this).val();
    $('#hidetitle').val(title_id)
});

$(document).on('change', '.category', function() {
    var category_id =  $(this).val();
    $('#hidecategory').val(category_id)
});

$(document).on('change', '.reference_number', function() {
    var reference_number =  $(this).val();
    $('#reference_number').val(reference_number)
});

$(document).on('change', '.course_id', function() {
    var course_id =  $(this).val();
    $('#course_id').val(course_id)
});
    </script>

@endpush
