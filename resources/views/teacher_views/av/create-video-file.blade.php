@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.' panel'))
@section('content')

@include('teacher_views.inc.avNavMenu')
<section>
    <div class="row">
      <div class="col-sm-12">

        @include('common/flash-message')
    </div>
        <div class="col-sm-12">
            <div id="container">
                <h1 class="mt-3">Video Recording </h1>

                <div class="row">
                  <div class="col-sm-4 text-center">
                    @if (Auth()->user()->avLibraryvideo)
                    <video  id="tem_video" controls>
                       <source src="{{ asset('storage/av-library/video/'.Auth()->user()->avLibraryvideo->file) }}" type="">
                      @else
                    <video  id="tem_video" autoplay>

                      <source src="{{asset('professional_assets/video/stunning_video_mockups.webm')}}" type="video/webm" >
                      @endif
                    </video>
                    <video id="gum" style="display: none" playsinline autoplay muted></video>
                    <div class="text-center">
                    <button id="start" class="btn btn-info">Start Camera</button>
                    <button id="record" class="btn btn-info" disabled>Start Recording</button>

                    </div>
                  </div>
                  <div class="col-sm-4 text-center">

                    <div class="text-center">
                      <form method="POST" enctype="multipart/form-data" id="videoForm">
                        <input type="hidden" class="form-control course_id" id="course_id" name="course_id" value="{{$course->id}}">

                        <div class="row align-text-bottom">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text"  class="form-control title @error('title')is-invalid @enderror" name="title" >
                                    @error('title')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-control category">
                                        <option value="podcast">podcast</option>
                                        <option value="lesson_lecture">lesson lecture</option>
                                        <option value="assesment">Assesment</option>
                                        </select>
                                    @error('category')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label for="">Reference Number</label>
                                  <input type="text" class="form-control reference_number  @error('last_name')is-invalid @enderror" name="reference_number" >
                                  @error('reference_number')
                                  <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                              </div>
                          </div>

                        </div>
                    </form>
                    </div>
                  </div>
                  <div class="col-sm-4 text-center">
                      <img  class="border-2 " id="demo_image"  src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}">
                       <video id="recorded" style="display: none" playsinline ></video>
                      <div class="text-center mt-2">
                        <button id="play" class="btn btn-info" disabled>Playback</button>
                    {{-- <button id="download" class="btn btn-info" disabled>Download</button> --}}
                    <button id="download" class="btn btn-info" disabled style="display: none">save</button>
                    <button id="upload" class="btn btn-info" disabled>Upload Video</button>
                      </div>
                  </div>
                </div>


                <hr>


                <div>


                </div>

                {{-- <div>
                    <h4>Media Stream Constraints options</h4>
                    <p>Echo cancellation: <input type="checkbox" id="echoCancellation"></p>
                </div> --}}

                <div>
                    <span id="errorMsg"></span>
                </div>


            </div>

        </div>

    </div>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('professional/av-library/video') }}">
            @csrf
        <div class="row">
          <input type="hidden" class="form-control course_id" id="course_id" name="course_id" value="{{$course->id}}">

            <div class="col-sm-4">
              <div class="form-group">
                  <label for="">Upload  Video</label>
                  <input type="hidden" name="type" value="profile-video">
                  <input type="file" class="form-control-file video-file" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                  @if ($errors->has('file'))
                              <small id="passwordHelp" class="text-danger">
                                  <strong>{{ $errors->first('file') }}</strong>
                                </small>
                              @endif
                </div>
          </div>
            <div class="col-sm-4">
              <div class="form-group">

                  <input type="hidden" class="form-control " id="hidetitle" name="title" >

                </div>
          </div>
          <div class="col-sm-4">
              <div class="form-group">
                  <input type="hidden" class="form-control " id="category" name="category" >


                  <select hidden   name="category" id="hidecategory" class="form-control category">
                      <option  value="podcast">podcast</option>
                      <option  value="lesson_lecture">lesson lecture</option>
                      </select>
                </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">

                <input type="hidden" class="form-control " id="reference_number" name="reference_number" >

              </div>
        </div>

            {{-- <div class="col-sm-4 mt-3">
                <button type="submit" class="btn btn-primary">Upload</button>
            </div> --}}
            <div class="col-sm-6 mt-2">

                    {{-- <button type="submit" name="save_con" value="10" class="btn btn-sm   btn-outline-dark btn-save" disabled >Save & Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></button> --}}
                    <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                    {{-- <a  style="display: none"  href="{{url('professional/profile/image')}}" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                    <a style="display: none" href="{{url('professional')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>   --}}

        </div>

        </div>
        </form>
    </div>
</section>
@endsection
@section('custom_js')
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/avlibrary-video.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
@endsection
@section('custom_css')
<style>
    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */







video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}



    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */


video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}
/*
video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
} */
</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {

      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide();
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });

    $(document).on('change', '.title', function() {
    var title_id =  $(this).val();
    $('#hidetitle').val(title_id)
});

$(document).on('change', '.category', function() {
    var category_id =  $(this).val();
    $('#hidecategory').val(category_id)
});

$(document).on('change', '.reference_number', function() {
    var reference_number =  $(this).val();
    $('#reference_number').val(reference_number)
});

$(document).on('change', '.course_id', function() {
    var course_id =  $(this).val();
    $('#course_id').val(course_id)
});
    </script>
@endpush
