@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.' panel'))
@section('content')

@include('teacher_views.inc.avNavMenu')
<br><br><br>
<div class="table-responsive-lg">
    <table class="table">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Reference</th>
        <th scope="col">Title</th>
        <th scope="col">Type</th>
        <th scope="col">Category</th>
        <th scope="col">Published</th>
        <th scope="col">Updated</th>
        <th scope="col">Audio File</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($avAudios as $avAudio)
    <tr>
    <td>{{$avAudio->reference_number}}</td>
    <td>{{$avAudio->title}}</td>
    <td>{{$avAudio->type}}</td>
    <td>{{$avAudio->category}}</td>
    <td>{{date('d-m-Y', strtotime($avAudio->created_at))}}</td>
    <td>{{date('d-m-Y', strtotime($avAudio->updated_at))}}</td>

    <td>
      <audio width="100" height="100" controls>
        <source src="{{ asset('storage/av-library/audio/'.$avAudio->file) }}" type="audio/ogg">
      </audio>
    </td>
    <td>  
        <a href="{{url('professional/av-library/audio/'. $avAudio->id .'/edit')}}" class="delete font-weight-bold text-decoration-none   btn btn-sm  color-bar-green border-0 text-dark color-bar-green" title="Edit" data-toggle="tooltip">Update</a>
        <a href="{{url('professional/av-library/delete-audio',$avAudio->id)}}" class="edit font-weight-bold text-decoration-none  btn btn-sm  color-bar-red border-0 text-dark" title="delete" data-toggle="tooltip">Delete</a>
        
    </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
<div class="row ">
  <div class="col-sm-12 d-flex justify-content-center">
      {{$avAudios->links('pagination::bootstrap-4')}}
  </div>
</div> 
@endsection 

@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')

<script language="javascript">
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush

