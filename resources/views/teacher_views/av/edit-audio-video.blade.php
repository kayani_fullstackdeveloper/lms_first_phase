@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.' panel'))
@section('content')

@include('teacher_views.inc.avNavMenu')
<section>
    <div class="row">
        <div class="col-sm-12">

            @include('common/flash-message')
        </div>
    </div>

    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('professional/av-library/update-audio-video', $data->id) }}"  enctype="multipart/form-data">
            @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" class="form-control title  @error('last_name')is-invalid @enderror" value="{{$data->title}}" name="title" >
                    @error('title')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" class="form-control category">
                        <option value="potcast" {{ $data->category == 'potcast' ? 'selected' : '' }}>potcast</option>
                        <option value="lesson_lecture" {{ $data->category == 'lesson_lecture' ? 'selected' : '' }}>lesson lecture</option>
                        <option value="assesment" {{ $data->category == 'assesment' ? 'selected' : '' }}>Assesment</option>
                        </select>
                    @error('category')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Reference Number</label>
                    <input type="text" class="form-control  @error('last_name')is-invalid @enderror" value="{{$data->reference_number}}" name="reference_number" >
                    @error('reference_number')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-sm-6">
              @if($data->type === 'avlibrary-video')
                <div class="form-group">
                    <label for="">Upload  Video</label>
                    <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                        @if ($errors->has('file'))
                            <small id="passwordHelp" class="text-danger">
                                <strong>{{ $errors->first('file') }}</strong>
                            </small>
                        @endif

                      <video class="mt-3" width="320" height="240" controls>
                        <source src="{{asset('storage/av-library/video/').'/'.$data->file}}" type="video/mp4">
                      </video>
                  </div>
                  @elseif($data->type === 'avlibrary-audio')
                  <div class="form-group">
                    <label for="">Upload  Audio</label>
                    <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                        @if ($errors->has('file'))
                            <small id="passwordHelp" class="text-danger">
                                <strong>{{ $errors->first('file') }}</strong>
                            </small>
                        @endif

                        <audio width="100" height="100" controls>
                          <source src="{{ asset('storage/av-library/audio/'.$data->file) }}" type="audio/ogg">
                        </audio>
                  </div>
                  @endif
            </div>

            <div class="col-sm-4">
                <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                    <a  style="display: none"  href="#" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                    <a style="display: none" href="{{url('professional/av-library/create-audio-file')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>

            </div>

            {{-- <div class="col-sm-4 mt-4">
                <button type="submit" class="btn btn-primary m-1">Upload</button>
            </div> --}}

        </div>
        </form>
    </div>
</section>
@endsection
@section('custom_js')
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/avlibrary-audio.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
<script>
//    var recorder;
//     var blob = recorder.getBlob();
//     console.log(blob)
</script>
@endsection
@section('custom_css')
<style>
    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */







video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}



    /*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */


video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}
/*
video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
} */
</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {

      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide();
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });

    $(document).on('change', '.title', function() {
    var title_id =  $(this).val();
    $('#hidetitle').val(title_id)
});

$(document).on('change', '.category', function() {
    var category_id =  $(this).val();
    $('#hidecategory').val(category_id)
});
    </script>

@endpush
