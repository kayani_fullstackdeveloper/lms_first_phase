@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.' panel'))
@section('content')

@include('teacher_views.inc.avNavMenu')
<br><br><br>
<div class="table-responsive-lg">
    <table class="table">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Reference</th>
        <th scope="col">Title</th>
        <th scope="col">Type</th>
        <th scope="col">Category</th>
        <th scope="col">Published</th>
        <th scope="col">Updated</th>
        <th scope="col">All Files</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @foreach($avLibraries as $avLibrary)
    <tr>
      
      <td>{{$avLibrary->reference_number}}</td>
      <td>{{$avLibrary->title}}</td>
      <td>{{$avLibrary->type}}</td>
      <td>{{$avLibrary->category}}</td>
      <td>{{date('d-m-Y', strtotime($avLibrary->created_at))}}</td>
      <td>{{date('d-m-Y', strtotime($avLibrary->updated_at))}}</td>
        <td>
          @if($avLibrary->type == 'avlibrary-audio')
          <audio width="100" height="100" controls>
            <source src="{{ asset('storage/av-library/audio/'.$avLibrary->file) }}" type="audio/ogg">
          </audio>
          @elseif($avLibrary->type == 'avlibrary-video')
          <video width="300" height="100" controls>
            <source src="{{ asset('storage/av-library/video/'.$avLibrary->file) }}" type="video/mp4">
          </video>
          @endif
        </td>
        <td>
        <a href="{{url('professional/av-library/edit-audio-video',$avLibrary->id)}}" class="font-weight-bold text-decoration-none w-50  btn btn-sm  color-bar-green border-0 text-dark"> 
            <span>Update</span> 
        </a>
        <a href="{{url('professional/av-library/delete-audio-video',$avLibrary->id)}}" class="font-weight-bold text-decoration-none w-50  btn btn-sm  color-bar-red border-0 text-dark"> 
            <span>Delete</span> 
        </a>
    </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
<div class="row ">
  <div class="col-sm-12 d-flex justify-content-center">
      {{$avLibraries->links('pagination::bootstrap-4')}}
  </div>
</div> 
@endsection 

@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
@push('custom_js')

<script language="javascript">
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush
