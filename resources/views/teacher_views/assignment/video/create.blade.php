@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
  input ,select{
    border:2px solid !important;
  }
</style>
<section>
  <div class="container">

          <div class="row mt-2">
            <div class="col-sm-3 col-md-3 text-center">
              @if (Auth()->user()->assignment)
              <video style="width:100%"  id="tem_video" controls>
                 <source src="{{ asset('storage/assignment/video/'.Auth()->user()->assignment->file) }}" type="">
                @else
              <video style="width:100%"  id="tem_video" autoplay>

                <source src="{{asset('professional_assets/video/stunning_video_mockups.webm')}}" type="video/webm" >
                @endif
              </video>
              <video id="gum" style="display: none;width:100%" playsinline autoplay muted></video>
              <div class="text-center">
              <button id="start" class="btn btn-info">Start Camera</button>
              <button id="record" class="btn btn-info" disabled>Start Recording</button>

              </div>
            </div>
            <div class="col-md-6">
          <form method="POST" enctype="multipart/form-data" id="videoForm">
              <input type="hidden"  name="course_id" value="{{$course->id}}"/>

            <div class="row mt-2">
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Reference</a>
                  </div>
                    <input type="text" name="reference_number" id="reference_number" class="reference_number mt-3 form-control color-bar-white text-dark"  placeholder="DS - 001">
                    @error('reference_number')
                      <label class="error mt-2 text-danger">{{ $message }}</label></br>
                    @enderror
              </div>
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Title</a>
                  </div>
                  <input type="text" name="title" id="title" class="title mt-3 form-control color-bar-white text-dark"  placeholder="Title">
                  @error('title')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Grading</a>
                  </div>
                  <input type="text" name="grading" id="grading" class="grading mt-3 form-control color-bar-white text-dark"  placeholder="Grading">
                  @error('grading')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Link</a>
                  </div>
                  <input type="text" name="link" id="link" class="link mt-3 form-control color-bar-white text-dark"  placeholder="Link">
                  @error('link')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
                </div>
              </div>
            </form>
            </div>
            <div class="col-sm-3 text-center">
                <img  class="border-2 img-fluid" id="demo_image"  src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}">
                 <video id="recorded" style="display: none" playsinline ></video>
                <div class="text-center mt-2">
                  <button id="play" class="btn btn-info" disabled>Playback</button>
              {{-- <button id="download" class="btn btn-info" disabled>Download</button> --}}
              <button id="download" class="btn btn-info" disabled style="display: none">save</button>
              <button id="upload" class="btn btn-info" disabled>Upload Video</button>
                </div>
            </div>
            </div>
          </div>
          <div class="container mt-5">
              <form method="POST" enctype="multipart/form-data" action="{{url('professional/assignment/'.$course->id.'/create-video-store')}}">
                  @csrf
              <div class="row">
                <input type="hidden" class="form-control course_id" id="course_id" name="course_id" value="{{$course->id}}">
                <input type="hidden" class="form-control reference_number" id="hidereference_number" name="reference_number" >
                <input type="hidden" class="form-control title" id="hidetitle" name="title" >
                <input type="hidden" class="form-control grading" id="hidegrading" name="grading" >
                <input type="hidden" class="form-control link" id="hidelink" name="link" >

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label for="">Upload  Audio</label>
                          <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                          @if ($errors->has('file'))
                                      <small id="passwordHelp" class="text-danger">
                                          <strong>{{ $errors->first('file') }}</strong>
                                        </small>
                                      @endif
                        </div>
                  </div>
                  <div class="col-sm-4">
                      <button type="submit" class="btn btn-sm  btn-outline-dark btn-save" disabled >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                          <a  style="display: none"  href="#" name="save_con" value="10" class="btn btn-sm  btn-outline-dark btn-upload"  > Continue  &nbsp;  <span class="fa fa-redo fa-2x text-success"></span></a>
                          <a style="display: none" href="{{url('professional/av-library/create-audio-file')}}" class="btn btn-sm  btn-outline-dark btn-upload"  >Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></a>

                  </div>



              </div>
              </form>
          </div>
</section>
@endsection
@section('custom_js')
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('media/assignment-video.js') }}" async></script>
<script src="{{ asset('media/ga.js') }}" ></script>
@endsection
@section('custom_css')
<style>

video {
  background: #222;
  margin: 0 0 20px 0;
  --width: 100%;
  width: var(--width);
  height: calc(var(--width) * 0.75);
}


video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}

</style>
@endsection
@push('custom_js')
    <script>
        $('input:file').change(function() {

      if($(this).val()) {
        $('.btn-save').removeAttr("disabled");
        $(".btn-save").show()
         $('.btn-upload').hide();
      } else {
        $('.btn-save').attr('disabled', 'disabled');
      }
    });

    $(document).on('change', '.reference_number', function() {
    var reference_number =  $(this).val();
    $('#hidereference_number').val(reference_number)
});

  $(document).on('change', '.title', function() {
  var title_id =  $(this).val();
  $('#hidetitle').val(title_id)
});
    $(document).on('change', '.grading', function() {
    var grading =  $(this).val();
    $('#hidegrading').val(grading)
});

$(document).on('change', '.link', function() {
    var link =  $(this).val();
    $('#hidelink').val(link)
});

$(document).on('change', '.course_id', function() {
    var course_id =  $(this).val();
    $('#course_id').val(course_id)
});
    </script>

@endpush
