@extends('teacher_views.app')
@section('title', __(Auth()->user()->name.'  course list'))
@section('content')
@include('teacher_views.inc.courseNavMenu')
<style>
  input ,select{
    border:2px solid !important;
  }
</style>
<section>
  <div class="container">

          <div class="row mt-2">
            <div class="col-md-12">
              <form method="POST" enctype="multipart/form-data" action="{{url('professional/assignment/'.$course->id.'/video-update',$assignment)}}">
                  @csrf
              <input type="hidden"  name="course_id" value="{{$course->id}}"/>
            <div class="row mt-2">
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Reference</a>
                  </div>
                    <input type="text" name="reference_number" value="{{$assignment->reference_number}}" id="reference_number" class="reference_number mt-3 form-control color-bar-white text-dark"  placeholder="DS - 001">
                    @error('reference_number')
                      <label class="error mt-2 text-danger">{{ $message }}</label></br>
                    @enderror
              </div>
              <div class="col-sm-4">
                <div class=" form-control     color-bar-yellow  text-dark text-center " >
                   <span class="font-weight-bold text-decoration-none"> Type</a>
                </div>
                 <input type="text" name="type" id="type" value="{{$assignment->type}}" class="type mt-3 form-control color-bar-white text-dark"  placeholder="Test">
                 @error('type')
                   <label class="error mt-2 text-danger">{{ $message }}</label></br>
                 @enderror
              </div>
              <div class="col-sm-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Title</a>
                  </div>
                  <input type="text" name="title" id="title" value="{{$assignment->title}}" class="title mt-3 form-control color-bar-white text-dark"  placeholder="Title">
                  @error('title')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Grading</a>
                  </div>
                  <input type="text" name="grading" value="{{$assignment->grading}}" id="grading" class="grading mt-3 form-control color-bar-white text-dark"  placeholder="Grading">
                  @error('grading')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Duration</a>
                  </div>
                  <select class="mt-3 form-control color-bar-white text-dark duration" name="duration" id="duration">
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                    <option value="60">60</option>
                  </select>
                  @error('duration')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror

              </div>
              <div class="col-sm-4 mt-4">
                  <div class=" form-control     color-bar-yellow  text-dark text-center " >
                    <span class="font-weight-bold text-decoration-none"> Link</a>
                  </div>
                  <input type="text" name="link" id="link" value="{{$assignment->link}}" class="link mt-3 form-control color-bar-white text-dark"  placeholder="Link">
                  @error('link')
                    <label class="error mt-2 text-danger">{{ $message }}</label></br>
                  @enderror
                </div>
                <div class="col-sm-4 mt-4">
                    <div class=" form-control     color-bar-yellow  text-dark text-center " >
                      <span class="font-weight-bold text-decoration-none"> File</a>
                    </div>
                    <input type="file" class="form-control" name="file" id="" placeholder="" aria-describedby="fileHelpId">
                    @error('file')
                      <label class="error mt-2 text-danger">{{ $message }}</label></br>
                    @enderror
                    <video class="mt-3" width="320" height="240" controls>
                      <source src="{{asset('storage/assignment/video/').'/'.$assignment->file}}" type="video/mp4">
                    </video>
                  </div>
                  <div class="col-sm-4 mt-3">
                    <div class="mt-5" >

                    </div>
                      <button type="submit" class="btn btn-sm  btn-outline-dark " >Save & Close &nbsp;  <span class="fa fa-share-square fa-2x text-primary"></span></button>
                  </div>
              </div>
            </form>
            </div>
            </div>
          </div>

</section>
@endsection
