@extends('admin_views.app')
@section('master_view')
@php
    $heading='Podcast';
@endphp
<div class="page-content">

    <div class="page-content">
        
      
        <div class="card">
            <div class="card-body">
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">{{$heading}}</div>   
                    </div>
                  <div class="ms-auto"><a href="{{ route('podcast.create') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class="bx bxs-plus-square"></i>Add New </a></div>
                </div>
                <div class="table-responsive">
                    <table class=" data-table table table-light" style="width:100%">
                        <thead class="table-light">
                            <tr>
                                <th>id#</th>
                                <th> title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

    
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>
        
    @endpush  
@endif
