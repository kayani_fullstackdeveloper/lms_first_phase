@extends('admin_views.app')
@section('master_view')
@php
    $heading='Add Podcast ';
@endphp
<div class="page-content">

    <div class="page-content">
        <!--breadcrumb-->
        
        <!--end breadcrumb-->
      
        <div class="card">
            <div class="card-body">
                @if(count($errors) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
@endif
                
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">{{$heading}}</div>
                    </div>
                    <div class="ms-auto"><a href="{{ route('podcast.index') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class=" bx bx-left-arrow"></i>Back </a></div>
                  </div>
                <div class="p-4 border rounded">
                    <form  method="post" action="{{ route('podcast.update',$data->id) }}" enctype="multipart/form-data" class="row g-3 needs-validation" novalidate="">
                        @csrf
                        @method('put')
                        <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="validationCustom01" class="form-label"> Title</label>
                            <input type="text" class="form-control @if($errors->has('title')) is-invalid @else in-valid  @endif" id="validationCustom01" name="title" value="{{$data->title }}" >
                            @if ($errors->has('title'))
                            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                               <label class="form-label" for="my-select">Category</label>
                               <select id="" class="form-select mb-3" name="cat_id">
                                   @foreach ($podcats as $item)
                                       <option @if ($data->cat_id==$item->id) selected @endif value="{{$item->id}}" >{{$item->title}}</option>
                                   @endforeach
                               </select>
                               @if ($errors->has('cat_id'))
                               <div class="invalid-feedback">Please select Category</div>
                               @else
                               <div class="valid-feedback">Looks good!</div>
                               @endif
                           </div>
                           
                        </div>
                        <div class="col-md-4">
                            <label for="validationCustom01" class="form-label">Media Type</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="link" @if ($data->type=='link') checked @endif>
                                <label class="form-check-label" for="inlineRadio2">Link</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="audio" @if ($data->type=='audio') checked @endif>
                                <label class="form-check-label" for="inlineRadio2">Audio</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="video" @if ($data->type=='video') checked @endif>
                                <label class="form-check-label" for="inlineRadio2">Video</label>
                            </div>
                            @if ($errors->has('type'))
                            <div class="text-danger">Media Required</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                            
                        </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 media link " style=" @if ($data->type=='link') checked @else display: none @endif">
                                <div class="form-group">
                                    <label for="my-input">link</label>
                                    <input id="my-input" class="form-control m-list" type="url" name="link" value="{{$data->audio_video_link}}" required>
                                </div>    
                            </div>
                            <div class="col-sm-4 media audio " style="@if ($data->type=='audio') checked @else display: none @endif">
                                <div class="form-group">
                                    <label for="my-input">Audio</label>
                                    <input id="my-input m-list" class="form-control" type="file" name="audio" required>
                                </div>    
                            </div>
                            <div class="col-sm-4 media video " style="@if ($data->type=='video') checked @else display: none @endif">
                                <div class="form-group">
                                    <label for="my-input">Video</label>
                                    <input id="my-input" class="form-control m-list" type="file" name="video" required>
                                </div>    
                            </div> 
                            <div class="col-sm-4  " >
                                <div class="form-group">
                                    <label for="my-input">Thumbnail IMage</label>
                                    <input id="my-input " class="form-control" type="file" name="image">
                                </div>    
                            </div> 
                            <div class="col-sm-4">
                                <img class="img-thumbnail" src="{{ asset('storage/podcast/thambnail/'.$data->image) }}" alt="">
                            </div>   
                        </div> 
                        
                        
                        
                       
                        
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    
   
@endsection
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        $('.m-list').val('');
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush

