@extends('admin_views.app')
@section('master_view')
@php
    $heading='Add Podcast Category';
@endphp
<div class="page-content">

    <div class="page-content">
        <!--breadcrumb-->
        
        <!--end breadcrumb-->
      
        <div class="card">
            <div class="card-body">
                
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">{{$heading}}</div>
                    </div>
                    <div class="ms-auto"><a href="{{ route('podcastcategory.index') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class=" bx bx-left-arrow"></i>Back </a></div>
                  </div>
                <div class="p-4 border rounded">
                    <form  method="post" action="{{ route('podcastcategory.store') }}" class="row g-3 needs-validation" novalidate="">
                        @csrf
                        <div class="col-md-6">
                            <label for="validationCustom01" class="form-label"> Title</label>
                            <input type="text" class="form-control @if($errors->has('title')) is-invalid @else in-valid  @endif" id="validationCustom01" name="title" value="{{ old('title') }}" >
                            @if ($errors->has('title'))
                            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        
                        
                       
                        
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    
   
@endsection

