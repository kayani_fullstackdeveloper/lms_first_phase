<div class="d-flex order-actions float-right">
    <a href="{{ route('library.edit',$id) }}" class="btn btn-info"><i class='bx bxs-edit'></i></a>
    <a class="btn btn-danger btn-delete ms-3" data-remote="{{ route('library.destroy',$id) }}"><i class='bx bxs-trash'></i></a>
</div>