@extends('admin_views.app')
@section('master_view')
@php
    $heading='Edit library ';
@endphp
<div class="page-content">

    <div class="page-content">
        <!--breadcrumb-->
        
        <!--end breadcrumb-->
      
        <div class="card">
            <div class="card-body">
                @if(count($errors) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
@endif
                
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">{{$heading}}</div>
                    </div>
                    <div class="ms-auto"><a href="{{ route('library.index') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class=" bx bx-left-arrow"></i>Back </a></div>
                  </div>
                <div class="p-4 border rounded">
                    <form  method="post" action="{{ route('library.update',$data->id) }}" enctype="multipart/form-data" class="row g-3 needs-validation" novalidate="">
                        @csrf
                        @method('put')
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="validationCustom01" class="form-label"> Title</label>
                                <input type="text" class="form-control @if($errors->has('title')) is-invalid @else in-valid  @endif" id="validationCustom01" name="title" value="{{ $data->title }}" >
                                @if ($errors->has('title'))
                                <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                @else
                                <div class="valid-feedback">Looks good!</div>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="" class="form-lable mb-2">Display</label>
                                    <select id="my-select" class="form-control" name="display">
                                        <option {{($data->display=='1')?'selected':''}} value="1">Yes</option>
                                        <option {{($data->display=='0')?'selected':''}} value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="my-input" class="mt-2">Display Order</label>
                                    <input id="my-input" min="1" class="form-control" type="number" name="displayorder" value="{{$data->displayorder}}">
                                    @if ($errors->has('displayorder'))
                                <div class="invalid-feedback">{{ $errors->first('displayorder') }}</div>
                                @else
                                <div class="valid-feedback">Looks good!</div>
                                @endif
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <label for="validationCustom01" class="form-label">Media Type</label><br>
                                <div class="form-check form-check-inline">
                                    <input {{ $data->type == 'link' ? 'checked' : ''}} class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="link">
                                    <label class="form-check-label" for="inlineRadio2">Link</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input {{ $data->type == 'file' ? 'checked' : ''}} class="form-check-input link_change" type="radio" name="type" id="inlineRadio2" value="file">
                                    <label class="form-check-label" for="inlineRadio2">File</label>
                                </div>
                                
                                @if ($errors->has('type'))
                                <div class="text-danger">Media Required</div>
                                @else
                                <div class="valid-feedback">Looks good!</div>
                                @endif
                                
                            </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-4 media link " style="{{ $data->type == 'link' ? '' : 'display: none'}} ">
                                    <div class="form-group">
                                        <label for="my-input">link</label>
                                        <input id="my-input" class="form-control" type="url" name="link"  value="{{$data->file_link}}">
                                    </div>    
                                </div>
                                <div class="col-sm-4 media file " style="{{ $data->type == 'file' ? '' : 'display: none'}}">
                                    <div class="form-group">
                                        <label for="my-input">File</label>
                                        <input id="my-input" class="form-control" type="file" name="file" required>
                                    </div>    
                                </div>
                                
                                
                            
                             
                            <div class="col-sm-4  " >
                                <div class="form-group">
                                    <label for="my-input">Thumbnail IMage</label>
                                    <input id="my-input " class="form-control" type="file" name="image">
                                </div>    
                            </div> 
                            <div class="col-sm-4">
                                <img class="img-thumbnail" src="{{ asset('storage/library/thambnail/'.$data->image) }}" alt="">
                            </div>   
                        </div> 
                        
                        
                        
                       
                        
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    
   
@endsection
@push('custom_js')
 <script>  
    
    $('.link_change').change(function (e) { 
        $('.m-list').val('');
        e.preventDefault();
        $(".media").hide()
        $('.'+this.value+'').show();
        
    });
                                                
 </script>    
@endpush

