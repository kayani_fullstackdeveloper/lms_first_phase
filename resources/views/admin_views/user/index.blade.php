@extends('admin_views.app')
@section('master_view')
<div class="page-content">

    <div class="page-content">
      
      
        <div class="card">
            <div class="card-body">
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">Admin user</div>   
                    </div>
                  <div class="ms-auto"><a href="{{ route('users.create') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class="bx bxs-plus-square"></i>Add New </a></div>
                </div>
                <div class="table-responsive">
                    <table class=" data-table table table-striped table-bordered" style="width:100%">
                        <thead class="table-light">
                            <tr>
                                <th>id#</th>
                                <th> Name</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

    {{-- delete form  --}}
    <form style="display: none;" id="delete_form" method="POST" 
           action="" >
    <input type="hidden" name="id" id="delete_key">
    @method('DELETE')
    @csrf  
  </form>
   
@endsection
@push('custom_js')
  <script>
    $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.datatable') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
   
//    del row 

  $(document).on("click", ".btn-delete", function() { 
        var $ele = $(this).parent().parent().parent();
        var url= $(this).data('remote');
        
        // var dltUrl = url+"/"+id;
        if (confirm("Are you sure to delete?"))
	{ 
		$.ajax({
			url: url,
			type: "DELETE",
			cache: false,
			data:{
				_token:'{{ csrf_token() }}'
			},
			success: function(dataResult){
                success_noti('Record Delete')
				$ele.fadeOut().remove();
			}
		});
    }
	});
    
  });  

    </script>  
@endpush

