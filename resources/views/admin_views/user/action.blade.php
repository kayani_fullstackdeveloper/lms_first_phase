<div class="d-flex order-actions">
    <a href="{{ route('users.edit',$id) }}" class=""><i class='bx bxs-edit'></i></a>
    <a class="btn-delete ms-3" data-remote="{{ route('users.destroy',$id) }}"><i class='bx bxs-trash'></i></a>
</div>