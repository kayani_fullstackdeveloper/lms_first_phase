@extends('admin_views.app')
@section('master_view')
<div class="page-content">

    <div class="page-content">
        <!--breadcrumb-->
        
        <!--end breadcrumb-->
      
        <div class="card">
            <div class="card-body">
                
                <div class="d-lg-flex align-items-center mb-4 gap-3">
                    <div class="position-relative">
                        <div class="breadcrumb-title pe-3">Update user</div>
                    </div>
                    <div class="ms-auto"><a href="{{ route('users.index') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class=" bx bx-left-arrow"></i>Back </a></div>
                  </div>
                <div class="p-4 border rounded">
                    <form  method="post" action="{{ route('users.update',$user->id) }}" class="row g-3 needs-validation" novalidate="">
                        @csrf
                        @method('put')
                        <div class="col-md-6">
                            <label for="validationCustom01" class="form-label"> Name</label>
                            <input type="text" class="form-control @if($errors->has('name')) is-invalid @else in-valid  @endif" id="validationCustom01" name="name" value="{{$user->name}}" required="">
                            @if ($errors->has('name'))
                            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="validationCustom02" class="form-label">Email</label>
                            <input type="email" class="form-control @if($errors->has('email')) is-invalid @else in-valid  @endif" id="validationCustom02" name="email" value="{{ $user->email }}" required="">
                            @if ($errors->has('email'))
                            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        
                        <div class="col-md-6">
                            <label for="validationCustom03" class="form-label">New Password</label>
                            <input type="password" class="form-control @if($errors->has('password')) is-invalid @else in-valid  @endif" name="password" id="validationCustom03" required="">
                            @if ($errors->has('password'))
                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="validationCustom03" class="form-label">Confirm Password</label>
                            <input type="password" class="form-control" @if($errors->has('password_confirmation')) is-invalid @else in-valid  @endif name="password_confirmation" id="validationCustom03" required="">
                            @if ($errors->has('password_confirmation'))
                            <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                            @else
                            <div class="valid-feedback">Looks good!</div>
                            @endif
                        </div>
                        
                       
                        
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

   
@endsection

