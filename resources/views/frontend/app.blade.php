<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <title>{{ config('app.name', 'Laravel') }}</title>
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
      
        <!-- Perfect Scrollbar -->
        <link type="text/css" href="{{asset('css/site_assets/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.rtl.css')}}" rel="stylesheet">
        <!-- Font Awesome Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.rtl.css')}}" rel="stylesheet">
        <!-- App CSS -->
        <link type="text/css" href="{{asset('css/site_assets/css/app.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/app.rtl.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/custom.css')}}" rel="stylesheet">
        


        <style>
        .login-color{
        background-color: rgb(245, 182, 131) !important;
        
        }
        .login-dp{
        color: deepskyblue !important;
        }
        </style>
    </head>
    <body class="login" style="background: #ffffff">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
        
                <a class="navbar-brand mr-5" href="#">
                <div class="avatar avatar-xl">
                    <img src="{{asset('css/site_assets/images/logo/logo2.jpg')}}" alt="LOGO" class="avatar-img rounded">
                  </div></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <div class="navbar-nav">
                      <ul  class="navbar-nav mr-auto">
                          
                   <li class="nav-item"> <a class="item nav-link active" href="#">Product <span class="sr-only">(current)</span></a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Soluction</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Features</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Support</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Pricing</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Blog</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Company</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Get Start</a></li>
                    <li class="nav-item"><a class="item nav-link" href="#">Conatc us</a></li>
                </ul>
                <form class="form-inline my-2 my-lg-0 ">
                    <!-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"> -->
                    <button class="btn  mr-sm-2" type="submit"><span><li class="fa fa-search"></li></span></button>
                  </form>
                  </div>
                </div>
              </nav>
        </div>
        <div class="container">
            
            @yield('content')    
        
        </div>
        </body>
        
    </html>
    