<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <title>{{ config('app.name', 'Laravel') }}</title>
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->

        <!-- Perfect Scrollbar -->
        <link type="text/css" href="{{asset('css/site_assets/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.rtl.css')}}" rel="stylesheet">
        <!-- Font Awesome Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.rtl.css')}}" rel="stylesheet">
        <!-- App CSS -->
        <link type="text/css" href="{{asset('css/site_assets/css/app.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/app.rtl.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/custom.css')}}" rel="stylesheet">



        <style>
        .login-color{
        background-color: rgb(245, 182, 131) !important;

        }
        .login-dp{
        color: #0070C0 !important;
        }
        </style>
    </head>
    <body class="login" style="background: #ffffff">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">

                <a class="navbar-brand mr-5" href="">
                    <div class="">
                        <img src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}" alt="LOGO" class=" ">
                      </div>
                </a>
              </nav>
        </div>
    <div class="container">

        <div class="row">



        <div class="col-md-12">
                <div class="card-header text-center">
                        <h4 class="card-title">Account Registration</h4>
                    </div>
        </div>
            </div>


        <form   method="POST" action="{{route('student.register')}} ">
            @csrf
    <div class="row" style="margin: 20px 0">

                 <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                    <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#FFFF00"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                </div>

                                <div class="col-md-5 form-group">
                                <label class="form-label text-capitalize" for="UsernameUsername">Full Name</label>
                                <div class="input-group input-group-merge">
                                    <input id="Username" type="text" required="" name="name" value="{{ old('name') }}" class="form-control form-control-prepended {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <span class="far fa-key"></span>
                                        </div>
                                    </div>

                                </div>


                                @if ($errors->has('name'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('name') }}</strong>
                                  </small>
                                @endif
                            </div>

        <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                    <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#4472C4"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                </div>

                                <div class="col-md-5 form-group">
                                <label class="form-label text-capitalize" for="UsernameUsername">Email</label>

                                <div class="input-group input-group-merge">
                                    <input id="Username" type="text" required="" name="email" value="{{ old('email') }}" class="form-control form-control-prepended {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="">


                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <span class="far fa-key"></span>
                                        </div>

                                    </div>
                                </div>
                                @if ($errors->has('email'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                  </small>
                                @endif
                                    <label class="form-text">Your Email address for all communication purposes</label>
                            </div>



    </div>
            <hr/>
            <div class="row" style="margin: 20px 0">

                 <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                    <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#92D050"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                </div>

                                <div class="col-md-5 form-group">
                                <label class="form-label text-capitalize" for="UsernameUsername">User Name</label>
                                <div class="input-group input-group-merge">
                                    <input id="user_name" type="text" required="" name="user_name" value="{{ old('user_name') }}" class="form-control form-control-prepended {{ $errors->has('user_name') ? ' is-invalid' : '' }}" placeholder="">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <span class="far fa-key"></span>
                                        </div>
                                    </div>
                                </div>
                                @if ($errors->has('user_name'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('user_name') }}</strong>
                                  </small>
                                @endif
                                    <label class="form-text">Please create a username  for login purposes</label>
                            </div>

        <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                    <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="black"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                </div>

                                <div class="col-md-5 form-group">
                                <label class="form-label text-capitalize " for="UsernameUsername">Password</label>
                                <div class="input-group input-group-merge">
                                    <input id="upassword" type="password"  required="" name="password" value="{{ old('password') }}" class="form-control form-control-prepended {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <span class="far fa-key"></span>
                                        </div>
                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                  </small>
                                @endif
                                    <label class="form-text"> Please create a password to access your account when
                                        you login</label>
                                    <label>
                                        <input type="checkbox" id="show-password">
                                  <label for="show-password">Show Password
                                    </label>
                            </div>



    </div>
            <hr/>
            <div class="row" style="margin: 20px 0">

                 <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                    <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#BFBFBF"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                </div>

                                <div class="col-md-5 form-group">
                                <label class="form-label text-capitalize" for="UsernameUsername">Your Domain Name</label>
                                <div class="input-group input-group-merge">
                                    <input id="domain" readonly type="text" required="" name="domain_name" value="{{ old('domain_name')}}" class="form-control form-control-prepended {{ $errors->has('domain_name') ? ' is-invalid' : '' }}" placeholder="">
                                    <div class=" input-group-append">


                                </div>
                                </div>
                                @if ($errors->has('domain_name'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('domain_name')}}</strong>
                                  </small>
                                @endif
                                    <label class="form-text bottom_domain">Your domain name is your website URL  {{ old('domain_name')}}</label>
                            </div>



                                <div class="col-md-3 form-group text-center">


                                    <div class="custom-control custom-checkbox" style="margin-bottom: 10px;">
                                                <input id="customCheck01" name="tearn_condition" type="checkbox" class="custom-control-input">
                                                <label for="customCheck01" class="custom-control-label" style="margin-top:15px;">I accept the <span style="color:#0070C0"><a href="{{url('/')}}">Terms of Services</a> </span></label>
                                                @if ($errors->has('tearn_condition'))
                                                <small id="passwordHelp" class="text-danger">
                                                    <strong>require team of service</strong>
                                                  </small>
                                                @endif
                                            </div>
                                    <button type="submit" class="btn login-color btn-rounded btn-lg">Create <br>Account</button>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ route('teacher.login') }}" class="text-decoration-none">
                               <p style="font-size:20px; margin-top: 20px;color:#383b3d">Already have an <br>account?
                                   <span style="color: #0070C0;">Login</span>
                                </p>
                                </a>
                            </div>

                    </div>


        </form>
    </div>
    </body>
    </html>
    <script src="{{asset('css/site_assets/vendor/jquery.min.js')}}"></script>
    {{-- @push('custom_js') --}}
    <script>

        $("#user_name").keyup(function (e) {
            e.preventDefault();
            let domain=$('#user_name').val().replace(/\s+/g, '-').toLowerCase()
            if(domain){
            domain=domain+'.viizard.com'
            $('#domain').val(domain)
            $(".bottom_domain").text('Your domain name is your website URL '+domain)
            }

        });
        $("#show-password").change(function(){
            $(this).prop("checked") ?  $("#upassword").prop("type", "text") : $("#upassword").prop("type", "password");
        });
    </script>

    {{-- @endpush --}}
