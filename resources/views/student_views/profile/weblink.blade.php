@section('title', __(Auth()->user()->name.' panel'))
@extends('student_views.layout.app')
@section('content')
<section class="mt-5" >
    <div class="row ">
    <div class="col-md-3 alert " style="background:#FCFF33"  role="alert">
        <a href="{{ url('student/profile/approve-go-live') }}" class="text-decoration-none">
        <div class="row ">
       <div class="col-md-6 "><h3 class="">Step 1</h3></div>
       <div class="col-md-6 text-right"><h3>Profile</h3></div>
       </div>
        </a>
    </div>

</div>
</section>
<section>
    <div class="row">
        <div class="col-sm-10">
            <div id="container">
                <div>
                    <span id="errorMsg"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data" action="{{ url('student/profile/weblink-store') }}">
            @csrf
        <div class="row">
            <div class="col-sm-12">

                @include('common/flash-message')
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <textarea class="ckeditor form-control" name="detail">{{@$details->name}}</textarea>
                  </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-2 mt-4">
                    <a href="{{ url('student/profile') }}" class="text-decoration-none   text-secondary ">
                        <p class="btn  btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                        </a>

            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>

            </div>
            <div class="col-sm-2 mt-4">
                <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>

            </div>
        </div>
        </form>
    </div>
</section>
@endsection
@push('custom_js')

@endpush
