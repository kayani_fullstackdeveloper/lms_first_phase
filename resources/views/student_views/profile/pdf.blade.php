@section('title', __(Auth()->user()->name.' panel'))
@extends('student_views.layout.app')
@section('content')
<section class="mt-5" >
    <div class="container mb-4">
        <div class="row">
          <div class="col-md-4 alert " style="background:#F9E79F"  role="alert">
              <a href="#" class="text-decoration-none">
                <h3 class="">Personal Development Planner</h3>
              </a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2">
              <a href="javascript:history.back()" style="border:1px solid black;"  class="text-decoration-none w-100 text-dark btn btn-sm  btn-outline-warning">
                  <span> Go Back
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style="background:#82E0AA;"  class="text-decoration-none w-100 text-dark btn btn-sm">
                  <span> toolkit
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style=""  class="text-decoration-none w-100 text-white btn btn-sm btn-danger btn-outline-warning">
                  <span> Dashboard
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style="border-radius: :3px;"  class="text-decoration-none w-100 text-white btn btn-sm btn-primary">
                  <span> Profile
              </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2 mt-2" style="background:#EAF2F8;border-radius: 5px;" >
              <a href="{{url('student/profile/pdf/type','motivation')}}" class="text-decoration-none">
                <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Motivation </br> what </br> motivates my </br> Plan</h5>
              </a>
          </div>
          <div class="col-md-2 mt-2 ml-2" style="background:#EAF2F8;border-radius: 5px;" >
              <a href="{{url('student/profile/pdf/type','goals-objectives')}}" class="text-decoration-none">
                <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Goals & </br> Objectives </br> What do I want </br> to achieve</h5>
              </a>
          </div>
          <div class="col-md-2 mt-2 ml-2" style="background:#6495ED;border-radius: 5px;" >
              <a href="{{url('student/profile/pdf/type','develop')}}" class="text-decoration-none">
                <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Develop </br> achieve my </br> What do I want </br> goals</h5>
              </a>
          </div>
          <div class="col-md-2 mt-2 ml-2" style="background:#FFBF00;border-radius: 5px;" >
              <a href="{{url('student/profile/pdf/type','timeline')}}" class="text-decoration-none">
                <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Timeline </br> How long will </br> it take</h5>
              </a>
          </div>
          <div class="col-md-2 mt-2 ml-2" style="background:#DFFF00;border-radius: 5px;" >
              <a href="{{url('student/profile/pdf/type','milestones')}}" class="text-decoration-none">
                <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Milestones </br> How will </br> I map my </br>direction</h5>
              </a>
          </div>
        </div>
        <div class="container">
          <div class="row offset-md-1">
            <div class="col-md-2 mt-2" style="background:#D5F5E3;border-radius: 5px;" >
                <a href="{{url('student/profile/pdf/type','strengths')}}" class="text-decoration-none">
                  <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Strengths </br> what are my </br> strengths / My </br> Opportunities</h5>
                </a>
            </div>
            <div class="col-md-2 mt-2 ml-2" style="background:#EAFAF1;border-radius: 5px;" >
                <a href="{{url('student/profile/pdf/type','weaknesses')}}" class="text-decoration-none">
                  <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Weaknesses </br> What are my </br> limitations / My </br> weaknesses</h5>
                </a>
            </div>
            <div class="col-md-2 mt-2 ml-2" style="background:#D5F5E3;border-radius: 5px;" >
                <a href="{{url('student/profile/pdf/type','opportunities')}}" class="text-decoration-none">
                  <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Opportunities </br> what are </br> my </br> Opportunities</h5>
                </a>
            </div>
            <div class="col-md-2 mt-2 ml-2" style="background:#EAFAF1;border-radius: 5px;" >
                <a href="{{url('student/profile/pdf/type','threats')}}" class="text-decoration-none">
                  <h5 class="text-center mt-3" style="font-family: 'Chilanka';">Threats </br> what threats </br> do I </br> face</h5>
                </a>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@push('custom_cs')
 {{-- notification --}}
 <link rel="stylesheet" href="{{asset('admin_assets/assets/plugins/notifications/css/lobibox.min.css')}}" />
@endpush
@push('custom_js')
 {{-- notification --}}
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/lobibox.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notifications.min.js')}}"></script>
 <script src="{{asset('admin_assets/assets/plugins/notifications/js/notification-custom-script.js')}}"></script>
@endpush
@if ($message = Session::get('success'))
    @push('custom_js')
    <script>
        success_noti('<?=$message?>')
    </script>

    @endpush
@endif
