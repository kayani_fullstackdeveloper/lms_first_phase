@section('title', __(Auth()->user()->name.' panel'))
@extends('student_views.layout.app')
@section('content')
<section class="mt-5" >
    <div class="container mb-4">
        <div class="row">
          <div class="col-md-4 alert " style="background:#F9E79F"  role="alert">
              <a href="{{ url('student/profile/approve-go-live') }}" class="text-decoration-none">
                <h3 class="">Personal Development Planner</h3>
              </a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2">
              <a href="javascript:history.back()" style="border:1px solid black;"  class="text-decoration-none w-100 text-dark btn btn-sm  btn-outline-warning">
                  <span> Go Back
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style="background:#82E0AA;"  class="text-decoration-none w-100 text-dark btn btn-sm">
                  <span> toolkit
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style=""  class="text-decoration-none w-100 text-white btn btn-sm btn-danger btn-outline-warning">
                  <span> Dashboard
              </a>
          </div>
          <div class="col-sm-2">
              <a href="" style="border-radius: :3px;"  class="text-decoration-none w-100 text-white btn btn-sm btn-primary">
                  <span> Profile
              </a>
          </div>
        </div>
        <div class="container mt-5">
            <form method="POST" enctype="multipart/form-data" action="{{ url('student/profile/pdf/type/store') }}">
                @csrf

            <div class="row">
                <div class="col-sm-12">

                    @include('common/flash-message')
                    <input type="hidden" name="type" value="{{$type}}">
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <textarea class="ckeditor form-control" name="details"></textarea>
                      </div>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2 mt-4">
                        <a href="{{ url('student/profile') }}" class="text-decoration-none   text-secondary ">
                            <p class="btn  btn-outline-dark" >Go Back &nbsp;  <span class="fa fa-undo fa-3x text-warning"></span></p>
                            </a>

                </div>
                <div class="col-sm-2 mt-4">
                    <button class="btn  btn-outline-dark" name="save_con" value="10" type="submit">Save And Continue   <span class="fa fa-redo fa-3x text-success"></span></button>

                </div>
                <div class="col-sm-2 mt-4">
                    <button class="btn  btn-outline-dark"  name=""  type="submit">Save And Close  <span class="fa fa-share-square fa-3x text-primary"></span></button>

                </div>
            </div>
            </form>
        </div>
    </div>
</section>
@endsection
