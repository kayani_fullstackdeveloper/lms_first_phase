<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <title>{{ config('app.name', 'Laravel') }}</title>
         <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->

        <!-- Perfect Scrollbar -->
        <link type="text/css" href="{{asset('css/site_assets/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/material-icons.rtl.css')}}" rel="stylesheet">
        <!-- Font Awesome Icons -->
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/fontawesome.rtl.css')}}" rel="stylesheet">
        <!-- App CSS -->
        <link type="text/css" href="{{asset('css/site_assets/css/app.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/app.rtl.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('css/site_assets/css/custom.css')}}" rel="stylesheet">



        <style>
        .login-color{
        background-color: rgb(245, 182, 131) !important;

        }
        .login-dp{
        color: deepskyblue !important;
        }
        </style>
    </head>
    <body class="login" style="background: #ffffff">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
                <a class="navbar-brand mr-5" href="">
                    <div class="">
                        <img src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}" alt="LOGO" class=" ">
                      </div>
                </a>
              </nav>
        </div>
        <div class="container">

            <div class="row form-group">
        <div class="col-md-12">
                    <div class=" text-center">
                            <h4 class="card-title">Login</h4>
                        </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6 offset-1">
                    <p style="font-size:24px; color: deepskyblue;">Teach, train, coach, <br>mentor and more online…</p>

                </div>
                <div class="col-sm-5 float-right">
                    <p class="float-right">Need help setting up<br>
                        your account.<span class="text-primary"><a href="{{url('/')}}">FAQs</a></span> </p>

                </div>
            </div>


            <form  method="POST" action="{{route('student.login') }}">
                @csrf

        <div class="row" style="margin: 20px 0">
                <div class="col-sm-12">

                    @include('common/flash-message')
                </div>

                     <div class="col-md-1 mt-3 " style="border-right: solid 1px #efefef">
                            <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#FFFF00"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                    </div>

                                <div class="col-md-5 form-group">
                                    <label class="form-label text-capitalize" for="UsernameUsername">Username</label>
                                    <div class="input-group input-group-merge">
                                        <input id="Username" type="text" name="user_name"  value="{{old('user_name')}}" class="form-control form-control-prepended" placeholder="">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <span class="far fa-key"></span>
                                            </div>
                                        </div>
                                    </div>
                                        @if ($errors->has('user_name'))
                                    <small id="passwordHelp" class="text-danger">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </small>
                                    @endif
                                </div>

            <div class="col-md-1 mt-3" style="border-right: solid 1px #efefef">
                                        <svg class="bd-placeholder-img rounded " width="60" height="60" role="img" ><rect width="100%" height="100%" fill="#4472C4"></rect><text x="%" y="50%" fill="#dee2e6" dy=".3em"></text></svg>
                                    </div>

                                    <div class="col-md-5 form-group">
                                    <label class="form-label text-capitalize" for="UsernameUsername">Password</label>
                                    <div class="input-group input-group-merge">
                                        <input id="Username" type="password"  name="password" value="{{old('password')}}"  class="form-control form-control-prepended" placeholder="">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <span class="far fa-key"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                <small id="passwordHelp" class="text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                  </small>
                                @endif
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5 mt-3 form-group">
                                    <!--<label class="control-label">Captcha</label>-->
                                  {!! NoCaptcha::renderJs() !!}
                                    {!! app('captcha')->display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                    <small id="passwordHelp" class="text-danger">
                                        <strong>Captcha Required</strong>
                                    </small>
                                    </span>
                                    @endif
                                </div>

        </div>



        </div>

                <hr/>
                <div class="row" style="margin: 20px 0">
                                    <div class="col-md-1"></div>
                                    <div class="col-sm-5 col-xs-6 form-group">
                                        <div class="form-group">
                                        <button type="submit" style="color: #0070C0" class="btn login-color login-dp btn-rounded btn-lg">Log in</button>
                                    </div>
                                        <div class="custom-control custom-checkbox">
                                                    <input id="customCheck01" type="checkbox" class="custom-control-input">
                                                    <label for="customCheck01" class="custom-control-label" style="margin-top:15px;">Stay signed in </label>
                                         </div>
                                </div>



                                    <div class="col-sm-4 col-xs-12 text-right">
                                   <p style="font-size:20px; margin-top: 20px;">Don't have an account?<br>
                                    <a href="{{ route('teacher.register') }}" class="text-decoration-none " style="color:#383b3d">
                                       <span style="color:#0070C0">Sign Up</span> for a free account.
                                    </a>
                                    </p>
                                    <p>
                                        <a href="{{ route('teacher.reset-password') }}" class="text-decoration-none">
                                            <span style="color: rgb(14, 15, 15);">Forgot your</span> Password
                                         </a>?
                                    </p>
                                </div>




        </div>
            </form>
        </div>
        </body>

    </html>
