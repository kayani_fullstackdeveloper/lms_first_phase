<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Viizard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->
	<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('comming_soon/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('comming_soon/css/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('comming_soon/css/bootstrap.css')}}">
	<!-- Theme style  -->
	<link rel="stylesheet" href="{{asset('comming_soon/css/style.css')}}">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

	<!-- Modernizr JS -->
	<script src="{{asset('comming_soon/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="{{asset('comming_soon/js/respond.min.js')}}"></script>
	<![endif]-->

	</head>
	<body >
		
	<div class="fh5co-loader"></div>
	
	<div id="page">

	<div id="fh5co-container" class="" >
		<div class="countdown-wrap "style="height: auto" >
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="display-t ">
						<div class="display-tc animate-box">
							<img src="{{asset('css/site_assets/images/logo/logo-teacher.jpg')}}" alt="LOGO" class=" ">
							<nav class="fh5co-nav" role="navigation">
								<div id="fh5co-logo">
								</div>
							</nav>
							<div class="" style="background-color:gray;font-family:Arial">
							<h1 style="font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Learning Management System</h1>
							</div>
							<!--<h2>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h2>-->
							<div class="simply-countdown simply-countdown-one"></div>
							<div class="row">
								<div class="" style="background-color:gray">
									<h1 style="font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Become a Viizard</h1>
									</div>
								<div class="col-md-12 desc">
									
									
									<a href="{{ route('teacher.login') }}" class="btn btn-primary">Log in</a>
										<a href="{{ route('teacher.register') }}" class="btn btn-primary">Register</a>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-cover " style="background-image:url({{asset('comming_soon/images/img_bg_1.jpg')}}); height:100%">
			<div class="row text-white " style="margin-top: 5%" >
				<div class="col-md-12 text-center">
					<div class="display-t" >
						<h1  style="color:white;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Cloud based</h1>
						<h1  style="color:white;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Learning Solution</h1>
						<img style="position: ;margin-boottom:40%" src="{{asset('comming_soon/images/Cloud-Based-Learning-Management-System.jpeg')}}" alt="LOGO" class=" ">	
						<h1  style="color:white;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Empower your learners </h1>
						<h1  style="color:white;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif" >Do it online</h1>
						
						<a style="color: white" href="{{ route('teacher.login') }}" class=""><i class="fas fa-door-open fa-4x "></i> <strong> Start a Free Trial</strong></a>&nbsp;
						<a style="color: white" href="{{ route('teacher.login') }}" class=""><i class="fas fa-door-open fa-4x"></i> <strong>Request a Demo</strong></a>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="{{asset('comming_soon/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('comming_soon/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('comming_soon/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('comming_soon/js/jquery.waypoints.min.js')}}"></script>

	<!-- Count Down -->
	<script src="{{asset('comming_soon/js/simplyCountdown.js')}}"></script>
	<!-- Main -->
	<script src="{{asset('comming_soon/js/main.js')}}"></script>

	<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>

	</body>
</html>

