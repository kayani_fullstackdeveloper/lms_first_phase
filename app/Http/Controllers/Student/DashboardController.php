<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use Auth,Image,DB;
use App\Models\Student\StudentMedia;
use App\Models\Student\StudentPdf;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;


class DashboardController extends Controller
{
  public function index()
  {
     return view('student_views.dashboard.index');
  }

  public function profile(Request $request)
  {
    return view('student_views.profile.index');
  }

  public function webLink()
  {
    $details=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-detail'])->first();
    $media=StudentMedia::where('std_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
    $data=array(
        'profile_count'=>$media,
        'details'=>$details
    );
    return view('student_views.profile.weblink',$data);
  }

  public function webLinkstore(Request $request)
  {

      $request->validate( [
          'detail' => 'required',
          ]);
      $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-detail'])->first();
      if ($media==null) {
          $media=new StudentMedia();
      }
      $media->type='profile-detail';
      $media->name=$request->detail;
      $media->guard_name='student';
      $media->std_id=Auth::user()->id;
      $media->save();
      if($request->save_con)
      {

          return redirect('student/profile/audio')->with('success','Your word document saved successfully');

      }else
      {
          return redirect('student')->with('success','Your word document saved successfully');

      }


  }

  public function audio()
  {
      return view('student_views.profile.audio');
  }

  public function audioStore(Request $request)
  {   $request->validate( [
      'file' => 'required|mimes:mpga,wav,mp3',
      ]);
      if ($request->hasFile('file')) {
               $temp_name  = $request->file('file')->store('audio','public');
               $request['image'] = str_replace('audio/', '', $temp_name);
      }
        $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-audio'])->first();

          if($media)
          {
              Storage::disk('public')->delete('audio/'.$media->name);
          }else
          {
              $media=new StudentMedia();
          }
          $media->type=$request->type;
          $media->name=$request->image;
          $media->guard_name='student';
          $media->std_id=Auth::user()->id;
          $media->save();
          if($request->save_con)
          {

              return redirect('student/profile/video')->with('success','Your audio file saved successfully');

          }else
          {
              return redirect('student')->with('success','Your audio file saved successfully');

          }
  }

  public function recordAudio(Request $request)
  {
      if ($request->hasFile('audio-blob')) {
          $temp_name  = $request->file('audio-blob')->store('audio','public');
          $request['image'] = str_replace('audio/', '', $temp_name);

       }
      $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-audio'])->first();

          if($media)
          {
              Storage::disk('public')->delete('audio/'.$media->name);
          }else
          {
              $media=new StudentMedia();
          }
          $media->type='profile-audio';
          $media->name=$request->image;
          $media->guard_name='student';
          $media->std_id=Auth::user()->id;
          $media->save();
          return  response()->json(200);
  }

  public function video()
  {
      return view('student_views.profile.video');
  }

  public function videoStore(Request $request)
  {   $request->validate( [
      'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
      ]);
      if ($request->hasFile('file')) {
               $temp_name  = $request->file('file')->store('video','public');
               $request['image'] = str_replace('video/', '', $temp_name);

      }
        $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-video'])->first();
          if($media)
          {
              Storage::disk('public')->delete('video/'.$media->name);
          }else
          {
              $media=new StudentMedia();

          }
          $media->type=$request->type;
          $media->name=$request->image;
          $media->guard_name='student';
          $media->std_id=Auth::user()->id;
          $media->save();
          if($request->save_con)
          {

              return redirect('student/profile/image')->with('success','Your video file saved successfully');

          }else
          {
              return redirect('student')->with('success','Your video file saved successfully');

          }
  }

  public function recordVideo(Request $request)
  {
    if ($request->hasFile('video-file')) {
        $temp_name  = $request->file('video-file')->store('video','public');
        $request['image'] = str_replace('video/', '', $temp_name);
    }
   $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-video'])->first();
     if($media)
     {
         Storage::disk('public')->delete('video/'.$media->name);
     }else
     {
         $media=new StudentMedia();

     }
     $media->type='profile-video';
     $media->name=$request->image;
     $media->guard_name='student';
     $media->std_id=Auth::user()->id;
     $media->save();
     return  response()->json(200);
  }

  public function image()
  {
    $media=StudentMedia::where('std_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
    $data=array(
        'profile_count'=>$media
    );
    return view('student_views.profile.image',$data);
  }
  public function uploadImage(Request $request)
  {
      $request->validate( [
      'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);
      if ($request->hasFile('file')) {
               $temp_name  = $request->file('file')->store('images','public');
               $request['image'] = str_replace('images/', '', $temp_name);
               Image::make(public_path('storage/images/'.$request['image']))->resize(250,250)->save(config('constants.store_thumb_path').$request['image']);

      }
        $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-image'])->first();
          if($media)
          {
              Storage::disk('public')->delete(config('constants.store_thumb_path').$media->name);
              Storage::disk('public')->delete(config('constants.images').$media->name);
          }else
          {
              $media=new StudentMedia();
          }
          $media->type=$request->type;
          $media->name=$request->image;
          $media->guard_name='student';
          $media->std_id=Auth::user()->id;
          $media->save();
          if($request->save_con)
          {

              return redirect('student/course/create')->with('success','Your photograph saved successfully');

          }else
          {
              return redirect('student')->with('success','Your photograph saved successfully');

          }
  }

  public function webimageupload(Request $request)
  {
      if ($request->hasFile('file')) {

          $temp_name  = $request->file('file')->store('images','public');
          $request['image'] = str_replace('images/', '', $temp_name);
          Image::make(public_path('storage/images/'.$request['image']))->resize(250,250)->save(config('constants.store_thumb_path').$request['image']);

       }

        $media=StudentMedia::where(['std_id'=>Auth::user()->id,'guard_name'=>'student','type'=>'profile-image'])->first();

          if($media)
          {
              Storage::disk('public')->delete(config('constants.store_thumb_path').$media->name);
              Storage::disk('public')->delete(config('constants.images').$media->name);
          }else
          {
              $media=new StudentMedia();
          }
          $media->type='profile-image';
          $media->name=$request->image;
          $media->guard_name='student';
          $media->std_id=Auth::user()->id;
          $media->save();
          return response()->json($media);
  }

  public function pdf()
  {
      return view('student_views.profile.pdf');
  }
  public function pdfType(Request $request)
  {
      $details=StudentPdf::where(['std_id'=>Auth::user()->id,'type'=>$request->type])->first();
      $data=array(
          'details'=>$details,
          'type'=>$request->type
      );
      return view('student_views.profile.pdf-type',$data);

  }

  public function pdfTypestore(Request $request)
  {
      $request->validate( [
          'details' => 'required',
          ]);
      $pdf=StudentPdf::where(['std_id'=>Auth::user()->id,'type'=>$request->type])->first();
      if ($pdf==null) {
          $pdf=new StudentPdf();
      }
      $pdf->type=$request->type;
      $pdf->details=$request->details;
      $pdf->std_id=Auth::user()->id;
      $pdf->save();
      if($request->save_con)
      {

          return redirect('student/profile/pdf')->with('success','Your saved successfully');

      }else
      {
          return redirect('student')->with('success','Your word document saved successfully');

      }


  }

  public function logout()
  {
      Auth::guard('student')->logout();
      return redirect()->intended('login/student');
  }

}
