<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\admin\Admin;
use App\Models\Teacher\Teacher;
use App\Models\Student\Student;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rules\Password;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:student');
        $this->middleware('guest:teacher');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAdminRegisterForm()
    {
        return view('auth.register', ['url' => 'admin']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showStudentRegisterForm()
    {
        return view('student_views.register', ['url' => 'blogger']);
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createAdmin(Request $request)
    {
        $this->validator($request->all())->validate();
        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return redirect()->intended('login/admin');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createStudent(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:students',
            'domain_name' => 'required|string|max:255|unique:students',
            'user_name' => 'required|string|max:255|unique:students',
            'password' => ['required',Password::min(8)
            ->letters()
            ->mixedCase()
            ->numbers()
            ->symbols()
            ->uncompromised(3)],
            'tearn_condition'=>'required'
        ]);
       $user= Student::create([
            'name' => $request->name,
            'email' => $request->email,
            'user_name' => $request->user_name,
            'domain_name' =>strtolower(str_replace(' ', '-', $request->user_name)),
            'password' => Hash::make($request->password),
        ]);
        return redirect()->intended('reg-success/student');
    }

    // teacher registration
    public function showTeacherRegisterForm()
    {
        return view('teacher_views.register', ['url' => 'teacher']);
    }
    public function createteacher(Request $request)
    {
        // $this->validator($request->all())->validate();
        // dd($request->user_name);

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:teachers',
            'domain_name' => 'required|string|max:255|unique:teachers',
            'user_name' => 'required|string|max:255|unique:teachers',
            'password' => ['required',Password::min(8)
            ->letters()
            ->mixedCase()
            ->numbers()
            ->symbols()
            ->uncompromised(3)],
            'tearn_condition'=>'required'
        ]);
       $user= Teacher::create([
            'name' => $request->name,
            'email' => $request->email,
            'user_name' => $request->user_name,
            'domain_name' =>strtolower(str_replace(' ', '-', $request->user_name)),
            'password' => Hash::make($request->password),
        ]);
        // Auth::guard('teacher')->login($user);
        // dd(Auth::guard('teacher')->user());
        // $domain=Auth::guard('teacher')->user()->domain_name.'.'.config('app.domain');
        // return redirect()->intended('http://'.$domain.'/professional');
        return redirect()->intended('reg-success/teacher');


        // return redirect()->intended('login/teacher');
    }
    // teacher register success
    public function reg_success(Request $request)
    {
        return view('teacher_views.register_success', ['url' => 'blogger']);

    }

    public function studentRegistersuccess(Request $request)
    {
        return view('student_views.register_success', ['url' => 'blogger']);

    }
}
