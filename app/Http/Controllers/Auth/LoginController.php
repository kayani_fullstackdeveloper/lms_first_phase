<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\admin\Admin;
use App\Models\Teacher\Teacher;
use App\Models\student\Student;
use Session;


class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
            $this->middleware('guest')->except('logout');
            $this->middleware('guest:admin')->except('logout');
            $this->middleware('guest:student')->except('logout');
            $this->middleware('guest:teacher')->except('logout');
    }

     public function showAdminLoginForm()
    {
        return view('auth.login', ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        // dd();
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    //student forms

     public function showStudentLoginForm()
    {
        return view('student_views.login', ['url' => 'blogger']);
    }


    public function StudentLogin(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
            'user_name' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

            if (Auth::guard('student')->attempt(['user_name'=>$request->user_name,'password' => $request->password], $request->get('remember'))) {

            $domain=Auth::guard('student')->user()->domain_name.'.'.config('app.domain');
            $student=Student::where(['domain_name'=>$request->domain_name,'user_name'=>$request->user_name,'email' => $request->email])->first();
            return redirect()->intended('https://'.$domain.'/student');
        }
        return back()->with('error', 'Invalid credentials!');
    }
    //teacher login form
    public function showTeacherLoginForm()
    {
        return view('teacher_views.login', ['url' => 'blogger']);
    }

    public function TeacherLogin(Request $request)
    {
        // dd();
        $this->validate($request, [
            // 'email'   => 'required|email',
            'password' => 'required|min:6',
            // 'domain_name' => 'required',
            'user_name' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        // if (Auth::guard('teacher')->attempt(['domain_name'=>$request->domain_name,'user_name'=>$request->user_name,'email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            if (Auth::guard('teacher')->attempt(['user_name'=>$request->user_name,'password' => $request->password], $request->get('remember'))) {

            $domain=Auth::guard('teacher')->user()->domain_name.'.'.config('app.domain');
            $teacher=Teacher::where(['domain_name'=>$request->domain_name,'user_name'=>$request->user_name,'email' => $request->email])->first();
            // dd(Auth::guard('teacher')->user());
            // dd($domain);

            // Auth::login(Auth::guard('teacher')->user());
            return redirect()->intended('https://'.$domain.'/professional');
            // return Redirect::away('http://'.$domain);


        }
        return back()->with('error', 'Invalid credentials!');
    }



}
