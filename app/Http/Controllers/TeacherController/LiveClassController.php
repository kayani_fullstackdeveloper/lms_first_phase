<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\Professional_Live_Class;
use Illuminate\Support\Facades\Validator;


class LiveClassController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...
        $data['course']=Course::findorFail($request->id);

        return view('teacher_views.liveClass.liveClassSession',$data);

    }
    public function roomCreate(Request $request)
    {
        # code...
        $data['course']=Course::findorFail($request->id);
        // dd($data);
        return view('teacher_views.liveClass.roomCreate',$data);

    }
    public function getRoomList(Request $request)
    {
        # code...
        $data['course']=Course::findorFail($request->id);
        $data['room']=Professional_Live_Class::where('p_id',auth()->user()->id)->get();
        // dd($data);
        return view('teacher_views.liveClass.roomList',$data);

    }
    public function roomSave(Request $request)
    {
        # code...
        $room=new Professional_Live_Class();
        $room->course_id=$request->course_id;
        $room->p_id=Auth()->user()->id;
        $room->session_number=$request->session_number;
        $room->title=$request->title;
        $room->date=$request->date;
        $room->professionalUrl=$request->professionalUrl;
        $room->visiterUrl='dasdasd';
        $room->start_time=$request->start_time;
        $room->duration=$request->duration;
        $room->type=$request->type;
        $room->password=$request->password;
        $room->save();
        return redirect('professional/live-class-session/'.$request->course_id.'/room-list')->with('success','Successfully Added');
        
        // $obj->title=$request->title;
        // $obj->type=$request->type;
        // $obj->professionalUrl=$request->professionalUrl;
        // $obj->visiterUrl=$request->visiterUrl;
        // $obj->professionalUrl='dads';
        // $obj->visiterUrl='dasdasd';
        // $obj->duration=$request->duration;
        // $obj->date=$request->date;
        // $obj->p_id=Auth()->user()->id;


    }
    public function edit(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['room']=Professional_Live_Class::findorFail($request->room);
        return view('teacher_views.liveClass.edit',$data);

    }
    public function update(Request $request)
    {
      $validator = Validator::make($request->all(), [
              'title' => 'required',
              'date' => 'required',
              'professionalUrl' => 'required|url',
              'start_time' => 'required',
              'duration' => 'required',
              'password' => 'required',
          ]
      );
      if ($validator->fails()) {
          $errors = $validator->errors();

          return redirect()->back()->withErrors($errors)->withInput();
      }
        $room=Professional_Live_Class::where('id',$request->room)->first();
        $room->course_id=$request->course_id;
        $room->p_id=Auth()->user()->id;
        $room->session_number=$request->session_number;
        $room->title=$request->title;
        $room->date=$request->date;
        $room->professionalUrl=$request->professionalUrl;
        $room->visiterUrl='dasdasd';
        $room->start_time=$request->start_time;
        $room->duration=$request->duration;
        $room->type=$request->type;
        $room->password=$request->password;
        $room->save();
        return redirect('professional/live-class-session/'.$request->course_id.'/room-list')->with('success','Successfully updated');

    }
    public function destory(Request $request)
    {
      $room = Professional_Live_Class::where('id',$request->room)->first();
      $room->delete();
      return redirect('professional/live-class-session/'.$request->id.'/room-list')->with('success','Successfully Deleted');
    }
}
