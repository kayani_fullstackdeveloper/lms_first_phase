<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon; 
use Mail,DB;
use Illuminate\Support\Str; 

class ForgotPasswordController extends Controller
{
    //
    public function getEmail()
    {
  
       return view('teacher_views.forgetpassword.email');
    }
  
   public function postEmail(Request $request)
    {
      $request->validate([
          'email' => 'required|email|exists:teachers',
      ]);
  
      $token = Str::random(64);
  
        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
        );
  
        Mail::send('teacher_views.forgetpassword.verify', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password Notification');
        });
  
        return back()->with('status', 'We have e-mailed your password reset link!');
    }
}
