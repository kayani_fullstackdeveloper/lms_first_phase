<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\ProfessionalLessonLecture;
use Illuminate\Support\Facades\Validator;


class LessonLectureController extends Controller
{
  public function index(Request $request)
  {
    $data['course']=Course::findorFail($request->id);
    $data['lessons']=ProfessionalLessonLecture::get();
    return view('teacher_views.lessonLecture.index',$data);
  }
  public function create(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.lessonLecture.create',$data);

  }
  public function store(Request $request)
  {
      $validator = Validator::make($request->all(), [
              'session_number' => 'required|string|unique:professional_lesson_lectures',
              'title' => 'required',
              'date' => 'required',
              'link' => 'required|url',
              'start_time' => 'required',
              'duration' => 'required',
              'password' => 'required',
          ]
      );
      if ($validator->fails()) {
          $errors = $validator->errors();

          return redirect()->back()->withErrors($errors)->withInput();
      }
      $lesson=new ProfessionalLessonLecture();
      $lesson->course_id=$request->course_id;
      $lesson->p_id=Auth()->user()->id;
      $lesson->session_number=$request->session_number;
      $lesson->title=$request->title;
      $lesson->date=$request->date;
      $lesson->link=$request->link;
      $lesson->start_time=$request->start_time;
      $lesson->duration=$request->duration;
      $lesson->type=$request->type;
      $lesson->password=$request->password;
      $lesson->save();
      return redirect('professional/lesson-lecture/'.$request->course_id.'/lesson-lecture-list')->with('success','Successfully added');

  }
  public function edit(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      $data['lesson']=ProfessionalLessonLecture::findorFail($request->lesson);
      return view('teacher_views.lessonLecture.edit',$data);

  }
  public function update(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'session_number' => 'required',
            'title' => 'required',
            'date' => 'required',
            'link' => 'required|url',
            'start_time' => 'required',
            'duration' => 'required',
            'password' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $lesson=ProfessionalLessonLecture::where('id',$request->lesson)->first();
      $lesson->course_id=$request->course_id;
      $lesson->p_id=Auth()->user()->id;
      $lesson->session_number=$request->session_number;
      $lesson->title=$request->title;
      $lesson->date=$request->date;
      $lesson->link=$request->link;
      $lesson->start_time=$request->start_time;
      $lesson->duration=$request->duration;
      $lesson->type=$request->type;
      $lesson->password=$request->password;
      $lesson->save();
      return redirect('professional/lesson-lecture/'.$request->course_id.'/lesson-lecture-list')->with('success','Successfully updated');

  }
  public function destory(Request $request)
  {
    $lesson = ProfessionalLessonLecture::where('id',$request->lesson)->first();
    $lesson->delete();
    return redirect('professional/lesson-lecture/'.$request->id.'/lesson-lecture-list')->with('success','Successfully Deleted');
  }
}
