<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\Teacher\Course;
use App\Models\CommonModels\Podcast;
use App\Models\CommonModels\CoursePodcast;


class PodcastController extends Controller
{
    public function index(Request $request)
    {   
        if($request->ajax() && $request->page)
        {
            $data['podcast']=Podcast::where('user_guard','admin')->paginate(20);
            return view('teacher_views.library.podcast.ajax-paginate',$data);
        }
        else if($request->ajax() && $request->filter)
        {   
            
            $data['podcast']=Podcast::where('user_guard','admin')->Where ('title','LIKE','%'.$request->filter."%")->paginate(20 );
            
            return view('teacher_views.library.podcast.ajax-paginate',$data);

        }
        else if(!$request->ajax())
        {
        $data['course']=Course::findorFail($request->id);
        $data['podcast']=Podcast::where('user_guard','admin')->paginate(20);
        $data['user_podcast']=Podcast::where('user_id',auth()->user()->id)->where('user_guard','teacher')->paginate(30);
        return view('teacher_views.library.podcast.index',$data);
        }else if($request->ajax())
        {
            $data['podcast']=Podcast::where('user_guard','admin')->paginate(20);
            return view('teacher_views.library.podcast.ajax-paginate',$data); 
        }
    }
    public function addcoursepodcast(Request $request)
    {       
        $id=Auth::user()->id;
        $course_id=$request->course_id;
        $podcasts=$request->list;
        $list=array();
        $count=0;
        foreach ($podcasts as $key => $value) {
            $data=new CoursePodcast;
             $check=CoursePodcast::where(['course_id'=>$course_id,'podcast_id'=>$value,'professional_id'=>$id])->first();
            //  dd($check->id);
             if($check!=null)
             {  
                $getpodcast=Podcast::find($check->podcast_id);
                $list[]=$getpodcast->title;
             }else
             {
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->podcast_id=$value;
            $data->save();
            $count++;
             }
            
        }
        return Response::json(['list'=>$list,'save'=>$count,'total'=>count($podcasts)]);
    }
}
