<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use App\Models\Teacher\Course;
use App\Models\CommonModels\Library;
use App\Models\CommonModels\CourseLibrary;

class SelfstudyController extends Controller
{
    
    public function index(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.selfstudy.index',$data);
    }
    public function online_digital(Request $request)
    {   
       
        // dd($data['library']->toArray());
        if($request->ajax() && $request->page)
        {   
            $data['library']=Library::where('display',1)->orderBy('displayorder','asc')->paginate(20);
            return view('teacher_views.library.selfstudy.online_digital_datatable',$data);

        }
        else if($request->ajax() && $request->filter)
        {   
            
            $data['library']=Library::where('display',1)->Where ('title','LIKE','%'.$request->filter."%")->paginate(20 );
            
            return view('teacher_views.library.selfstudy.online_digital_datatable',$data);
        }
        else if(!$request->ajax())
        {   
            $data['library']=Library::simplePaginate(20);
            $data['course']=Course::findorFail($request->id);
            return view('teacher_views.library.selfstudy.online_digital')->with($data);
        }
    }

// save self study course library
    public function addcourselibrary(Request $request)
    {
        $course_id=$request->course_id;
        $library_id=$request->library_id;
        $id=auth()->user()->id;
        // dd($id);
        $check=CourseLibrary::where(['course_id'=>$course_id,'library_id'=>$library_id,'professional_id'=>auth()->user()->id])->get();
        if($check->count())
        {
            echo(1);
        }else
        {
            // CourseLibrary::create([ 'professional_id'=>$id,'course_id'=>$course_id,'library_id'=>$library_id ]);
            $data=new CourseLibrary;
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->library_id=$library_id;
            $data->save();
            echo(0); 
        }
    }
    
    public function create(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.selfstudy.add',$data);   
    }
    public function store(Request $request)
    {   
        
        $request->validate( [
            'title' => 'required|string|max:255',
            // 'display'=>'required',
            // 'displayorder'=>'required',
            // 'image' => 'required',
            // 'type' => 'required',
 
        ]);
        if ($request->hasFile('file')) {
            $temp_name  = $request->file('file')->store('medialibrary','public');
            $request['file_link'] = str_replace('medialibrary/', '', $temp_name);
           }
           if($request->link){
               $request['file_link']=$request->link;
           }
           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('medialibrary','public');
             $request->file('image')->store('medialibrary/thambnail','public');
            $request['thambnail'] = str_replace('medialibrary/', '', $temp_name);
            Image::make(public_path('storage/medialibrary/'.$request['thambnail']))->resize(150,150)->save('storage/medialibrary/thambnail/'.$request['thambnail']);
           }
        Library::create([
            'title' => $request->title,
            'display' => $request->display,
            'displayorder' => $request->displayorder,
            'file_link' => $request->file_link,
            'image' => $request->thambnail,
            'type' => $request->type,
        ]);
        
        return redirect("professional/self-library/online_digital/$request->id")->with('success','Successfully Saved ');

    }
    
    public function edit(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['data']=Library::findorFail($request->edit);
        return view('teacher_views.library.selfstudy.edit',$data);
    }

    public function update(Request $request)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
            // 'display'=>'required',
            // 'displayorder'=>'required',
            
            // 'type' => 'required',
 
        ]);

        $data=Library::findorFail($request->edit);
        $request['file_link']=$data->audio_video_link;
        $request['thambnail']=$data->image;
        if($request->type=='file'){
        if ($request->hasFile('file')) {
            
            $temp_name  = $request->file('file')->store('library','public');
            $request['file_link'] = str_replace('library/', '', $temp_name);
            Storage::disk('public')->delete('storage/library/'.$data->file_link);
           }
        }
        if($request->type=='link'){
           
           if($request->link){
               $request['file_link']=$request->link;
           }
        }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('library','public');
             $request->file('image')->store('library/thambnail','public');
            $request['thambnail'] = str_replace('library/', '', $temp_name);
            Image::make(public_path('storage/library/'.$request['thambnail']))->resize(150,150)->save('storage/library/thambnail/'.$request['thambnail']);
            Storage::disk('public')->delete('storage/library/thambnail/'.$data->image);
            Storage::disk('public')->delete('storage/library/'.$data->image);
           }
           $data->title=$request->title;
           $data->type=$request->type;
           $data->display=$request->display;
           $data->displayorder=$request->displayorder;
           $data->image=$request->thambnail;
           $data->file_link = $request->file_link;
           $data->save();
        return redirect("professional/self-library/online_digital/$request->id")->with('success','Successfully Updated');

    }

    public function deleteMultiple(Request $request)
	{
		$id = $request->id;
		foreach ($id as $library) 
		{

			$libraries = Library::where('id','=' ,$library)->first();
            $libraries->delete();

		}
		return back()->with('success','Library Deleted Successfully');
	}
    public function destory(Request $request)
    {
        $book = Library::find($request->id);
        $book->delete();
        return back()->with('success','Library Deleted Successfully');
    }
}
