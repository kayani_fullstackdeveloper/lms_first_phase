<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\Teacher\Course;
use App\Models\CommonModels\Booklibrary as obj;
use App\Models\CommonModels\Coursesbooks;
class Booklibrary extends Controller
{
    public function index(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        $data['books']=obj::where('user_id',auth()->user()->id)->paginate(20);
        return view('teacher_views.library.booklibrary.index',$data);
    }
    public function create(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.booklibrary.add',$data);   
    }
    public function save(Request $request)
    {   
        $request->validate( [
            'title' => 'required|string|max:255',
            'writer_name' => 'required',
            'file' => 'required',
            'isbn' => 'required',
 
        ]);
        $data=new obj;
        $data->title=$request->title;
        $data->isbn=$request->isbn;
        $data->writer_name=$request->writer_name;
        if ($request->hasFile('file')) {
            
            $temp_name  = $request->file('file')->store('booklibrary','public');
            $request['link'] = str_replace('booklibrary/', '', $temp_name);
            $data->link=$request->link;
           
           }
        
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/study-book/$request->id")->with('success','Successfully Saved');

    }
    public function edit(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::findorFail($request->edit);
        return view('teacher_views.library.booklibrary.edit',$data);
    }
    public function update(Request $request)
    {    
        // dd($request->all());
        $request->validate( [
            'title' => 'required|string|max:255',
            'writer_name' => 'required',
            
            'isbn' => 'required',
 
        ]);
        $data= obj::findorFail($request->edit);
        $data->title=$request->title;
        $data->isbn=$request->isbn;
        $data->writer_name=$request->writer_name;
        if ($request->hasFile('file')) {
            
            $temp_name  = $request->file('file')->store('booklibrary','public');
            $request['link'] = str_replace('booklibrary/', '', $temp_name);
            $data->link=$request->link;
           
           }
        
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/study-book/$request->id")->with('success','Successfully Updated');
        
    }
    public function addcoursebook(Request $request)           
    {
        $course_id=$request->course_id;
        $book_id=$request->book_id;
        $id=auth()->user()->id;
        // dd($id);
        $check=coursesbooks::where(['course_id'=>$course_id,'book_id'=>$book_id,'professional_id'=>auth()->user()->id])->get();
        if($check->count())
        {
            echo(1);
        }else
        {
            // CourseLibrary::create([ 'professional_id'=>$id,'course_id'=>$course_id,'library_id'=>$library_id ]);
            $data=new coursesbooks;
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->book_id=$book_id;
            $data->save();
            echo(0); 
        } 
    }
    public function deleteMultiple(Request $request)
	{
		$id = $request->id;
		foreach ($id as $book) 
		{

			$books = obj::where('id','=' ,$book)->first();
            $books->delete();

		}
		return back()->with('success','Books Deleted Successfully');
	}
    public function destroy(Request $request)
    {
        $book = obj::find($request->id);
        $book->delete();
        return back()->with('success','Book Deleted Successfully');
    }
}
