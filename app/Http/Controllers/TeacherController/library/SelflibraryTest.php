<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\Teacher\Course;
use App\Models\CommonModels\SelfLibraryPractise as obj;
use App\Models\CommonModels\CoursePractise ;
class SelflibraryTest extends Controller
{
    //
    public function list(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::where('user_id',auth()->user()->id)->paginate(20);
        return view('teacher_views.library.selflibrarypractise.list',$data);
    }
    public function index(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::where('user_id',auth()->user()->id)->where('type',$request->type)->paginate(20);
        return view('teacher_views.library.selflibrarypractise.index',$data);
    }
    public function create(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.selflibrarypractise.add',$data);   
    }
    public function save(Request $request)
    {   
        $request->validate( [
            'title' => 'required|string|max:255',
            'type' => 'required',
            'media' => 'required',
 
        ]);
        $data=new obj;
        $data->title=$request->title;
        if ($request->hasFile('media')) {
            
            $temp_name  = $request->file('media')->store('selflibrarypractise','public');
            $request['file'] = str_replace('selflibrarypractise/', '', $temp_name);
          
           
           }
        $data->file=$request->file;
        $data->type=$request->type;
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/task/$request->id/$request->type")->with('success','Successfully Saved');

    }
    public function edit(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::findorFail($request->edit);
        return view('teacher_views.library.selflibrarypractise.edit',$data);
    }
    public function update(Request $request)
    {    
        // dd($request->all());
        $request->validate( [
            'title' => 'required|string|max:255',
 
        ]);
        $data= obj::findorFail($request->edit);
        $data->title=$request->title;
        if ($request->hasFile('media')) {
            
            $temp_name  = $request->file('media')->store('selflibrarypractise','public');
            $request['file'] = str_replace('selflibrarypractise/', '', $temp_name);
            $data->file=$request->file;
           
           }
        
        // $data->link=$request->link;
        $data->type=$data->type;
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/task/$request->id/$data->type")->with('success','SuccessfullyUpdated');

        
    }
    public function addcoursepractise(Request $request)           
    {   
        
        $course_id=$request->course_id;
        $practise_id=$request->practise_id;
        $id=auth()->user()->id;
        // dd($id);
        $check=CoursePractise::where(['course_id'=>$course_id,'practise_id'=>$practise_id,'professional_id'=>auth()->user()->id])->get();
        if($check->count())
        {
            echo(1);
        }else
        {
            // CourseLibrary::create([ 'professional_id'=>$id,'course_id'=>$course_id,'library_id'=>$library_id ]);
            $data=new CoursePractise;
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->practise_id=$practise_id;
            $data->save();
            echo(0); 
        } 
    }

    public function deleteMultiple(Request $request)
	{
		$id = $request->id;
		foreach ($id as $test) 
		{

			$tests = obj::where('id','=' ,$test)->first();
            $tests->delete();

		}
		return back()->with('success','Deleted Successfully');
	}

    public function destroy(Request $request)
    {
        //$task = obj::find($request->id);
        $task = obj::findorFail($request->edit);
        $task->delete();
        return back()->with('success','Deleted Successfully');
    }
}
