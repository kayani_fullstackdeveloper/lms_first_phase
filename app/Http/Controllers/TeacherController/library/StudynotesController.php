<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\Teacher\Course;
use App\Models\CommonModels\Studynotes as obj;
use App\Models\CommonModels\Coursestudynotes;

class StudynotesController extends Controller
{
    
    public function index(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        $data['notes']=obj::where('user_id',auth()->user()->id)->paginate(20);
        return view('teacher_views.library.studynotes.index',$data);
    }
    public function create(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.studynotes.add',$data);   
    }
    public function save(Request $request)
    {   
        $request->validate( [
            'title' => 'required|string|max:255',
            'type' => 'required',
            'description' => 'required',
 
        ]);
        $data=new obj;
        $data->title=$request->title;
        $data->description=$request->description;
        if ($request->hasFile('media')) {
            
            $temp_name  = $request->file('media')->store('studynotes','public');
            $request['link'] = str_replace('studynotes/', '', $temp_name);
          
           
           }
        $data->link=$request->link;
        $data->type=$request->type;
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/study-notes/$request->id")->with('success','Successfully Saved');

    }
    public function edit(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::findorFail($request->edit);
        return view('teacher_views.library.studynotes.edit',$data);
    }
    public function update(Request $request)
    {    
        // dd($request->all());
        $request->validate( [
            'title' => 'required|string|max:255',
            'type' => 'required',
            'description' => 'required',
 
        ]);
        $data= obj::findorFail($request->edit);
        $data->title=$request->title;
        $data->description=$request->description;
        if ($request->hasFile('media')) {
            
            $temp_name  = $request->file('media')->store('studynotes','public');
            $request['link'] = str_replace('studynotes/', '', $temp_name);
          
           
           }elseif($request->link)
           {
            $data->link=$request->link;
           }
        
        // $data->link=$request->link;
        $data->type=$request->type;
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/study-notes/$request->id")->with('success','Successfully Updated');
        
    }
    public function addcoursenotes(Request $request)           
    {
        $course_id=$request->course_id;
        $studynotes_id=$request->studynotes_id;
        $id=auth()->user()->id;
        // dd($id);
        $check=Coursestudynotes::where(['course_id'=>$course_id,'studynotes_id'=>$studynotes_id,'professional_id'=>auth()->user()->id])->get();
        if($check->count())
        {
            echo(1);
        }else
        {
            // CourseLibrary::create([ 'professional_id'=>$id,'course_id'=>$course_id,'library_id'=>$library_id ]);
            $data=new Coursestudynotes;
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->studynotes_id=$studynotes_id;
            $data->save();
            echo(0); 
        } 
    }
    
  
    public function deleteMultiple(Request $request)
	{
		$id = $request->id;
		foreach ($id as $note) 
		{

			$notes = obj::where('id','=' ,$note)->first();
            $notes->delete();

		}
		return back()->with('success','Deleted Successfully');
	}
    public function destroy(Request $request)
    {
        $note = obj::find($request->id);
        $note->delete();
        return back()->with('success','Deleted Successfully');
    }
   
}
