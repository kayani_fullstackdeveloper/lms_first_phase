<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\Teacher\Course;
use App\Models\CommonModels\SocialMedia as obj;
use App\Models\CommonModels\CourseSocial;

class SocialMediaController extends Controller
{
    public function index(Request $request)
    {       
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.social_media.index',$data);
    }
    public function list(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['social']=obj::where(['user_id'=>auth()->user()->id,'user_guard'=>'teacher'])->where('media_type',$request->social)->get();
        return view('teacher_views.library.social_media.list',$data);
    }
    public function add(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.social_media.add',$data);
    }
    public function save(Request $request)
    {    
        // dd($request->all());
        $request->validate( [
            'title' => 'required|string|max:255',
            'link' => 'required',
            'description' => 'required',
 
        ]);
        $data=new obj;
        $data->title=$request->title;
        $data->description=$request->description;
        $data->link=$request->link;
        $data->media_type=$request->social;
        $data->user_id=auth()->user()->id;
        $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/social-media/list/$request->id/$request->social")->with('success','Successfully Saved');

        
    }
    public function edit(Request $request)
    {    
        // dd($request->social);
        $data['course']=Course::findorFail($request->id);
        $data['data']=obj::findorFail($request->edit);
        return view('teacher_views.library.social_media.edit',$data);
    }
    public function update(Request $request)
    {    
        // dd($request->all());
        $request->validate( [
            'title' => 'required|string|max:255',
            'link' => 'required',
            'description' => 'required',
 
        ]);
        $data= obj::findorFail($request->edit);
        $data->title=$request->title;
        $data->description=$request->description;
        $data->link=$request->link;
        $data->media_type=$request->social;
        // $data->user_id=auth()->user()->id;
        // $data->user_guard='teacher';
        $data->save();
        return redirect("professional/self-library/social-media/list/$request->id/$request->social")->with('success','Successfully Updated');

        
    }
    public function addcoursesocial(Request $request)           
    {
        $course_id=$request->course_id;
        $social_id=$request->social_id;
        $id=auth()->user()->id;
        // dd($id);
        $check=CourseSocial::where(['course_id'=>$course_id,'social_id'=>$social_id,'professional_id'=>auth()->user()->id])->get();
        if($check->count())
        {
            echo(1);
        }else
        {
            // CourseLibrary::create([ 'professional_id'=>$id,'course_id'=>$course_id,'library_id'=>$library_id ]);
            $data=new CourseSocial;
            $data->professional_id=$id;
            $data->course_id=$course_id;
            $data->social_id=$social_id;
            $data->save();
            echo(0); 
        } 
    }
}
