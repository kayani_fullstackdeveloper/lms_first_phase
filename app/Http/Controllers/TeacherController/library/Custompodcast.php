<?php

namespace App\Http\Controllers\TeacherController\library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use Response;
use App\Models\CommonModels\PodcastCategories as podcat;
use App\Models\CommonModels\Podcast as obj;
use App\Models\CommonModels\CoursePodcast;
use App\Models\Teacher\Course;


class Custompodcast extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $data['data']=obj::where(['user_id'=>auth()->user()->id,'user_guard'=>'teacher'])->get();
            $data['course']=Course::findorFail($request->id);
        return view('teacher_views.library.custompodcast.index',$data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $data=array(
            'podcats'=>podcat::all(),
            'course'=>Course::findorFail($request->id)
        );
        return view('teacher_views.library.custompodcast.create',$data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
            'cat_id' => 'required',
            'image' => 'required',
            'type' => 'required',
 
        ]);
        
        if ($request->hasFile('audio')) {
            
            $temp_name  = $request->file('audio')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
          
           
           }
           if ($request->hasFile('video')) {
            $temp_name  = $request->file('video')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
          
           
           }
           if($request->link){
               $request['audio_video_link']=$request->link;
           }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('podcast','public');
             $request->file('image')->store('podcast/thambnail','public');
            $request['thambnail'] = str_replace('podcast/', '', $temp_name);
            Image::make(public_path('storage/podcast/'.$request['thambnail']))->resize(150,150)->save('storage/podcast/thambnail/'.$request['thambnail']);
           }
         obj::create([
            'title' => $request->title,
            'cat_id' => $request->cat_id,
            'audio_video_link' => $request->audio_video_link,
            'image' => $request->thambnail,
            'type' => $request->type,
            'user_id' => auth()->guard('teacher')->user()->id,
            'user_guard' =>'teacher',
        ]);
        return redirect("professional/custompodcast/$request->id")->with('success','successfully Saved ');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   
        $data['data']=obj::findorFail($request->edit);
        $data['podcats']=podcat::all();
        return view('teacher_views.library.custompodcast.edit',$data);

        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $id=$request->edit;
        $request->validate( [
            'title' => 'required|string|max:255',
            'cat_id' => 'required',
            'type' => 'required',
 
        ]);

        $data=obj::findorFail($id);
        $request['audio_video_link']=$data->audio_video_link;
        $request['thambnail']=$data->image;
        if ($request->hasFile('audio')) {
            
            $temp_name  = $request->file('audio')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
            Storage::disk('public')->delete('storage/podcast/'.$data->audio_video_link);
           }
           if ($request->hasFile('video')) {
            $temp_name  = $request->file('video')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
            Storage::disk('public')->delete('storage/podcast/'.$data->audio_video_link);
           }
           if($request->link){
               $request['audio_video_link']=$request->link;
           }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('podcast','public');
             $request->file('image')->store('podcast/thambnail','public');
            $request['thambnail'] = str_replace('podcast/', '', $temp_name);
            Image::make(public_path('storage/podcast/'.$request['thambnail']))->resize(250,250)->save('storage/podcast/thambnail/'.$request['thambnail']);
            Storage::disk('public')->delete('storage/podcast/thambnail/'.$data->image);
            Storage::disk('public')->delete('storage/podcast/'.$data->image);
           }
           $data->title=$request->title;
           $data->type=$request->type;
           $data->image=$request->thambnail;
           $data->cat_id=$request->cat_id;
           $data->audio_video_link = $request->audio_video_link;
           $data->save();
        return redirect("professional/custompodcast/$request->id")->with('success','successfully Update ');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        // dd($id);
        $remove=obj::findorFail($request->edit);
        Storage::disk('public')->delete('storage/podcast/thambnail/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->audio_video_link);
        $remove->delete();
    }
    public function remove(Request $request)
    {   
        // dd($id);
        $remove=obj::findorFail($request->edit);
        Storage::disk('public')->delete('storage/podcast/thambnail/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->audio_video_link);
        $remove->delete();
        return redirect("professional/custompodcast/$request->id")->with('success','successfully Remove ');

    }
    
}
