<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\Assignment;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Storage;
class AssignmentController extends Controller
{
  public function index(Request $request)
  {
    $data['course']=Course::findorFail($request->id);
    $data['assignments']=Assignment::where('p_id',auth()->user()->id)->get();
    return view('teacher_views.assignment.index',$data);
  }
  public function create(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.assignment.create',$data);

  }
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required|string|unique:assignments',
            'title' => 'required',
            'grading' => 'required',
            'link' => 'required|url',
            'description' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $assignment=new Assignment();
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="Pending";
      $assignment->date_close="Pending";
      $assignment->formate="word";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;
      $assignment->description=$request->description;
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Successfully added');

  }
  public function edit(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      $data['assignment']=Assignment::findorFail($request->assignment);
      if ($data['assignment']->formate == 'word') {
        return view('teacher_views.assignment.edit',$data);

      }elseif($data['assignment']->formate == 'audio') {
        return view('teacher_views.assignment.audio.edit',$data);
      }
      else {
        return view('teacher_views.assignment.video.edit',$data);
      }

  }
  public function update(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required',
            'title' => 'required',
            'grading' => 'required',
            'link' => 'required|url',
            'description' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $assignment=Assignment::where('id',$request->assignment)->first();
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="Pending";
      $assignment->date_close="Pending";
      $assignment->formate="word";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;
      $assignment->description=$request->description;
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Successfully updated');

  }
  public function destory(Request $request)
  {
    $assignment = Assignment::where('id',$request->assignment)->first();
    $assignment->delete();
    return redirect('professional/assignment/'.$request->id.'/assignment-list')->with('success','Successfully Deleted');
  }

  public function createAudio(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.assignment.audio.create',$data);

  }
  public function audioFile(Request $request)
  {
      $request->validate( [
          'reference_number' => 'required',
          'title' => 'required',
      ]);
      if ($request->hasFile('audio-blob')) {
          $temp_name  = $request->file('audio-blob')->store('assignment/audio','public');
          $request['file'] = str_replace('assignment/audio/', '', $temp_name);
       }
          $media=Assignment::where(['p_id'=>Auth::user()->id,'formate'=>'audio'])->first();
          if($media)
          {
              Storage::disk('public')->delete('assignment/'.$media->title);
          }else
          {
              $media=new Assignment();
          }
          $media->reference_number=$request->reference_number;
          $media->course_id=$request->course_id;
          $media->p_id=Auth::user()->id;
          $media->title=$request->title;
          $media->date_open="pending";
          $media->date_close="pending";
          $media->file=$request->file;
          $media->formate='audio';
          $media->grading=$request->grading;
          $media->link=$request->link;
          $media->save();
          return  response()->json(200);
  }
  public function uploadAudio(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required|string|unique:test_quizzes',
            'title' => 'required',
            'grading' => 'required',
            'link' => 'required|url',
            'file' => 'required|mimes:mpga,wav,mp3',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }

      $assignment=new Assignment();
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="pending";
      $assignment->date_close="pending";
      $assignment->formate="audio";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;
      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('assignment/audio','public');
          $request->file = str_replace('assignment/audio/', '', $temp_name);
          $assignment->file=$request->file;

         }
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Audio File Added Successfully');

  }
  public function updateAudio(Request $request)
  {
      $request->validate( [
        'reference_number' => 'required',
        'title' => 'required',
        'grading' => 'required',
        'link' => 'required|url',
        // 'file' => 'required|mimes:mpga,wav,mp3',
      ]);
      $assignment= Assignment::findorFail($request->assignment);
      // dd($assignment);
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="pending";
      $assignment->date_close="pending";
      $assignment->formate="audio";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;

      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('assignment/audio','public');
          $request->file = str_replace('assignment/audio/', '', $temp_name);
          $assignment->file=$request->file;

         }
         // dd($assignment);
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Audio File updated Successfully');

  }
  public function createVideo(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.assignment.video.create',$data);

  }
  public function uploadVideo(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required|string|unique:test_quizzes',
            'title' => 'required',
            'grading' => 'required',
            'link' => 'required|url',
            'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }

      $assignment=new Assignment();
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="pending";
      $assignment->date_close="pending";
      $assignment->formate="video";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;
      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('assignment/video','public');
          $request->file = str_replace('assignment/video/', '', $temp_name);
          $assignment->file=$request->file;

         }
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Video uploaded Successfully');


  }
  public function videoFile(Request $request)
  {
      $request->validate( [
          'title' => 'required',
          'reference_number' => 'required',
      ]);

       if ($request->hasFile('video-file')) {
           $temp_name  = $request->file('video-file')->store('assignment/video','public');
           $request['file'] = str_replace('assignment/video/', '', $temp_name);
        }
          $media=Assignment::where(['p_id'=>Auth::user()->id,'formate'=>'video'])->first();

          if($media)
          {
              Storage::disk('public')->delete('assignment/'.$media->title);
          }else
          {
              $media=new Assignment();
          }

          $media->reference_number=$request->reference_number;
          $media->course_id=$request->course_id;
          $media->p_id=Auth::user()->id;
          $media->title=$request->title;
          $media->date_open="pending";
          $media->date_close="pending";
          $media->file=$request->file;
          $media->formate='video';
          $media->grading=$request->grading;
          $media->link=$request->link;

          // dd($media);
          $media->save();
          return  response()->json(200);
  }
  public function updateVideo(Request $request)
  {
      $request->validate( [
        'reference_number' => 'required',
        'title' => 'required',
        'grading' => 'required',
        'link' => 'required|url',
        // 'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
      ]);
      $assignment= Assignment::findorFail($request->assignment);

      // dd($assignment);
      $assignment->course_id=$request->course_id;
      $assignment->p_id=Auth()->user()->id;
      $assignment->reference_number=$request->reference_number;
      $assignment->title=$request->title;
      $assignment->date_open="pending";
      $assignment->date_close="pending";
      $assignment->formate="video";
      $assignment->grading=$request->grading;
      $assignment->link=$request->link;

      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('assignment/video','public');
          $request->file = str_replace('assignment/video/', '', $temp_name);
          $assignment->file=$request->file;

         }
         // dd($assignment);
      $assignment->save();
      return redirect('professional/assignment/'.$request->course_id.'/assignment-list')->with('success','Video File updated Successfully');

  }
}
