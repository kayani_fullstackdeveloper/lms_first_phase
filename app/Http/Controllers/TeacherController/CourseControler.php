<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\CoureseSyllabusMaterial;
use Illuminate\Support\Facades\Storage;
use Auth,Image;
use App\Models\Media;
use Validator;

class CourseControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=array(
            'media'=>$media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count(),
            'student'=>0,
            'other'=>0,
            'course'=>Course::where(['f_id'=>Auth()->user()->id,'guard'=>'teacher'])->get()
        );
        return view('teacher_views.course.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media
        );
        return view('teacher_views.course.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'coure_title' => 'required',
            // 'detail' => 'required',
            'date_start' => 'required',
            'end_date' => 'required',
            'price' => 'required',
        ]);
        if($validator->passes())
        {   
            $data=[];
            // dd($request->course_name);
            // foreach ($request->course_name as $key => $value) {
                $Course=new Course();
                $Course->coure_title=$request->coure_title;
                $Course->course_description=$request->detail;
                $Course->date_start=$request->date_start;
                $Course->end_date=$request->end_date;
                $Course->price=$request->price;
                $Course->currency=$request->currency;
                $Course->guard='teacher';
                $Course->f_id=Auth()->user()->id;
                $data[]=$Course->attributesToArray();
                
            // }
            Course::insert($data);
            $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-course'])->first();
            if ($media==null) {
                $media=new Media();
            }
            $media->type='profile-course';
            $media->name='course';
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            return response()->json(['success'=>'Added new Course.']);
        }else
        {
            return response()->json(['error'=>$validator->errors()]);
        }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id,$edit)
    {
        $data['data']=Course::findorfail($edit);
        return view('teacher_views.course.course_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $course=Course::findorfail($request->id);
        $course->coure_title=$request->coure_title;
        $course->course_description=$request->detail;
        $course->date_start=$request->date_start;
        $course->end_date=$request->end_date;
        $course->price=$request->price;
        $course->currency=$request->currency;
        $course->save();
        return response()->json(['success'=>'Updated Course Description.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Course::find($request->id)->delete($request->id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
        
    }

    public function courseList(Request $request)
    {   
        $data['course']=Course::where(['f_id'=>Auth()->user()->id,'guard'=>'teacher'])->get();
        return view('teacher_views.course.courseList',$data);
    }
    public function coursedetail($id)
    {   
        return view('teacher_views.course.course_details_view');
    }
    public function courseoption(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        
        return view('teacher_views.course.course_option_list',$data);
    }
    public function Syllabusoption(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        
        return view('teacher_views.course.syllabusoption',$data);
    }
    public function upload_course_material(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        if($request->ajax())
        {
            $courseMaterialList= CoureseSyllabusMaterial::where(['course_id'=>$request->id,'type'=>'file'])->get();
            return response()->json($courseMaterialList);
        }
        return view('teacher_views.course.upload-course-material',$data);
    }
    public function create_course_material(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        if($request->ajax())
        {
            $courseMaterialList= CoureseSyllabusMaterial::where(['course_id'=>$request->id,'type'=>'text'])->get();
            return response()->json($courseMaterialList);
        }
        return view('teacher_views.course.create-course-material',$data);
    }
    public function weblink_course_material(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        if($request->ajax())
        {
            $courseMaterialList= CoureseSyllabusMaterial::where(['course_id'=>$request->id,'type'=>'media'])->get();
            return response()->json($courseMaterialList);
        }
        return view('teacher_views.course.weblink-course-material',$data);
    }
    public function create_weblink_course_material(Request $request)
    {   
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.course.create_weblink_course_material',$data);
    }
    // save course data 
    public function save_course_material(Request $request)
    {   
        $file_name='text';
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('course','public');
                 $request['data'] = str_replace('course/', '', $temp_name);
                //  dd($request->all());
                $file_name=$request->file('file');
                
        }
        
         $data=new CoureseSyllabusMaterial();
         $data->name=$file_name;
         $data->type=$request->type;
         $data->data=$request->data;
         $data->course_id=$request->course_id;
         
         if($data->save())
         {
         return response()->json(['success'=>'Added successfully.']);
        }else
        {
            return response()->json(['error'=>$validator->errors()]);
        }
    }
    public function save_weblinlk_course_material(Request $request)
    {   
         $data=new CoureseSyllabusMaterial();
         $data->name=$request->media_name;
         $data->type=$request->type;
         $data->data=$request->data;
         $data->course_id=$request->course_id;
         
         if($data->save())
         {
         return response()->json(['success'=>'Added successfully.']);
        }else
        {
            return response()->json(['error'=>$validator->errors()]);
        }
    }
    public function getCourseList(Request $request)
    {   
        $data=Course::where(['f_id'=>Auth()->user()->id,'guard'=>'teacher'])->get();
         return response()->json($data);
    }
     public function del_course_material(Request $request)
    {
        return CoureseSyllabusMaterial::find($request->id)->delete();
    }
}
