<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\CoureseSyllabusMaterial;
use Storage;
use Auth,Image;
use App\Models\Media;
use App\Models\AvLibraryMeidaFile;
use Validator;


class AvLibarayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=array(
            'media'=>$media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count(),
            'student'=>0,
            'other'=>0,
            'course'=>Course::where(['f_id'=>Auth()->user()->id,'guard'=>'teacher'])->get()
        );
        return view('teacher_views.av.index',$data);
    }
    public function viewAllFile(Request $request)
    {
        $avLibraries = AvLibraryMeidaFile::where('f_id',Auth::user()->id)->paginate(10);
        return view('teacher_views.av.all-file',compact('avLibraries'));
    }
    public function viewAudioFile(Request $request)
    {
        $avAudios = AvLibraryMeidaFile::where('f_id',Auth::user()->id)->where('type','avlibrary-audio')->paginate(10);
        return view('teacher_views.av.audio-file',compact('avAudios'));
    }
    public function viewVideoFile(Request $request)
    {
        $avVideos = AvLibraryMeidaFile::where('f_id',Auth::user()->id)->where('type','avlibrary-video')->paginate(10);
        return view('teacher_views.av.video-file',compact('avVideos'));
    }
    public function createAudioFile(Request $request,$course_id = null)
    {
        //$data['course']=Course::findorFail($request->course_id);
        $data['course']=Course::where('id', $request->course_id ?? true)->first();
        $data['audio'] = AvLibraryMeidaFile::where('course_id',$request->course_id)->first();

        return view('teacher_views.av.create-audio-file',$data);
    }

    public function audioFile(Request $request)
    {
        $request->validate( [
        'file' => 'required|mimes:mpga,wav,mp3',
        ]);
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('av-library/audio','public');
                 $request['image'] = str_replace('av-library/audio/', '', $temp_name);
        }
        $media=AvLibraryMeidaFile::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'avlibrary-audio'])->first();

        if($media)
        {
            Storage::disk('public')->delete('avlibrary-audio/'.$media->title);
        }else
        {
            $media=new AvLibraryMeidaFile();
        }
        $media->title=$request->title;
        $media->category=$request->category;
        $media->course_id=$request->course_id;
        $media->reference_number=$request->reference_number;
        $media->type='avlibrary-audio';
        $media->file=$request->image;
        $media->guard_name='teacher';
        $media->f_id=Auth::user()->id;
        $media->save();
        if($request->save_con)
        {
            return redirect('professional/av-library/view-audio-file')->with('success','Your Audio File Saved Successfully');
        }else
        {
            return redirect('professional/av-library/view-audio-file')->with('success','Your Audio File Saved Successfully');
        }
    }

    public function uploadAudio(Request $request)
    {
        $request->validate( [
            'title' => 'required',
            'category' => 'required',
        ]);
        if ($request->hasFile('audio-blob')) {
            $temp_name  = $request->file('audio-blob')->store('av-library/audio','public');
            $request['image'] = str_replace('av-library/audio/', '', $temp_name);
            // dd($request->all());

         }

        //  dd($request->image);


            $media=AvLibraryMeidaFile::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'avlibrary-audio'])->first();

            if($media)
            {
                Storage::disk('public')->delete('avlibrary-audio/'.$media->title);
            }else
            {
                $media=new AvLibraryMeidaFile();
            }



            $media->title=$request->title;
            $media->category=$request->category;
            $media->course_id=$request->course_id;
            $media->reference_number=$request->reference_number;
            $media->file=$request->image;
            $media->type='avlibrary-audio';
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            return  response()->json(200);
    }

    public function editAudio(Request $request)
    {
        $data=AvLibraryMeidaFile::findorFail($request->id);
        return view('teacher_views.av.edit-audio',compact('data'));
    }

    public function updateAudio(Request $request)
    {
        $request->validate( [
        'title' => 'required',
        'category' => 'required',
        'file' => 'required|mimes:mpga,wav,mp3',
        ]);
        $data= AvLibraryMeidaFile::findorFail($request->id);

        $data->title = $request->title;
        $data->category = $request->category;
        $data->reference_number=$request->reference_number;
        $data->type='avlibrary-audio';

        if ($request->hasFile('file')) {
            $temp_name  = $request->file('file')->store('av-library/audio','public');
            $request->file = str_replace('av-library/audio/', '', $temp_name);
            $data->file=$request->file;

           }

        $data->guard_name='teacher';
        $data->f_id=Auth::user()->id;

        $data->save();
        return redirect("professional/av-library/view-audio-file")->with('success','Successfully Updated ');
    }

    public function deleteAudio(Request $request, $id)
    {
        $audio = AvLibraryMeidaFile::find($request->id);
        $audio->delete();
        return redirect('professional/av-library/view-audio-file')->with('success','Successfully Deleted');

    }

    public function createVideoFile(Request $request,$course_id=null)
    {
      $data['course']=Course::findorFail($request->course_id);
      $data['audio'] = AvLibraryMeidaFile::where('course_id',$request->course_id)->first();
      return view('teacher_views.av.create-video-file',$data);
    }

    public function videoFile(Request $request)
    {
        $request->validate( [
            'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
        ]);
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('av-library/video','public');
                 $request['image'] = str_replace('av-library/video/', '', $temp_name);
                //  dd($request->all());

        }
        $media=AvLibraryMeidaFile::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'avlibrary-audio'])->first();

        if($media)
        {
            Storage::disk('public')->delete('avlibrary-audio/'.$media->title);
        }else
        {
            $media=new AvLibraryMeidaFile();
        }

            $media->title=$request->title;
            $media->category=$request->category;
            $media->course_id=$request->course_id;
            $media->reference_number=$request->reference_number;
            $media->type='avlibrary-video';
            $media->file=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();

            if($request->save_con)
            {

                return redirect('professional/av-library/view-video-file')->with('success','Your Video File Saved Successfully');

            }else
            {
                return redirect('professional/av-library/view-video-file')->with('success','Your Video File Saved Successfully');

            }
    }

    public function uploadVideo(Request $request)
    {
        $request->validate( [
            'title' => 'required',
            'category' => 'required',
        ]);
        if ($request->hasFile('video-file')) {
            $temp_name  = $request->file('video-file')->store('av-library/video','public');
            $request['image'] = str_replace('av-library/video/', '', $temp_name);


                }
     $media=AvLibraryMeidaFile::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'avlibrary-video'])->first();
       if($media)
       {
           Storage::disk('public')->delete('av-library/video/'.$media->title);
       }else
       {
           $media=new AvLibraryMeidaFile();

       }

       $media->title=$request->title;
       $media->category=$request->category;
       $media->course_id=$request->course_id;
       $media->reference_number=$request->reference_number;
        $media->file=$request->image;
        $media->type='avlibrary-video';
        $media->guard_name='teacher';
        $media->f_id=Auth::user()->id;
        $media->save();
        return  response()->json(200);
    }

    public function editVideo(Request $request)
    {
        $data=AvLibraryMeidaFile::findorFail($request->id);
        return view('teacher_views.av.edit-video',compact('data'));
    }

    public function updateVideo(Request $request)
    {
        $request->validate( [
        'title' => 'required',
        'category' => 'required',
        'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
        ]);
        $data= AvLibraryMeidaFile::findorFail($request->id);

        $data->title = $request->title;
        $data->category = $request->category;
        $data->reference_number=$request->reference_number;
        $data->type='avlibrary-video';

        if ($request->hasFile('file')) {
            $temp_name  = $request->file('file')->store('av-library/video','public');
            $request->file = str_replace('av-library/video/', '', $temp_name);
            $data->file=$request->file;

           }

        $data->guard_name='teacher';
        $data->f_id=Auth::user()->id;

        $data->save();
        return redirect("professional/av-library/view-video-file")->with('success','Successfully Updated ');
    }

    public function deleteVideo(Request $request, $id)
    {
        $video = AvLibraryMeidaFile::find($request->id);
        $video->delete();
        return redirect('professional/av-library/view-video-file')->with('success','Successfully Deleted');

    }
    public function deleteAudioVideo(Request $request, $id)
    {
        $video = AvLibraryMeidaFile::find($request->id);
        $video->delete();
        return redirect('professional/av-library/view-all')->with('success','Successfully Deleted');

    }
    public function EditAudioVideo(Request $request)
    {
        $data=AvLibraryMeidaFile::findorFail($request->id);
        return view('teacher_views.av.edit-audio-video',compact('data'));
    }
    public function updateAudioVideo(Request $request)
    {
        if($request->type =='avlibrary-video'){
            $request->validate( [
                'title' => 'required',
                'category' => 'required',
                'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
                ]);
        }
        else {
            $request->validate( [
                'title' => 'required',
                'category' => 'required',
                'file' => 'required|mimes:mpga,wav,mp3',
                ]);
        }

                $data= AvLibraryMeidaFile::findorFail($request->id);
                $data->title = $request->title;
                $data->category = $request->category;
                $data->reference_number=$request->reference_number;
                if($data->type =='avlibrary-video')
                {
                $data->type='avlibrary-video';
                    if ($request->hasFile('file'))
                    {
                        $temp_name  = $request->file('file')->store('av-library/video','public');
                        $request->file = str_replace('av-library/video/', '', $temp_name);
                        $data->file=$request->file;
                    }
                }
                else
                {
                    $data->type='avlibrary-audio';
                    if ($request->hasFile('file'))
                    {
                        $temp_name  = $request->file('file')->store('av-library/audio','public');
                        $request->file = str_replace('av-library/audio/', '', $temp_name);
                        $data->file=$request->file;
                    }
                }
                $data->guard_name='teacher';
                $data->f_id=Auth::user()->id;
                $data->save();

        return redirect("professional/av-library/view-all")->with('success','Successfully Updated ');
    }
}
