<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\DiscussionBoard;
use Illuminate\Support\Facades\Validator;

class DiscussionBoardController extends Controller
{
  public function index(Request $request)
  {
    $data['course']=Course::findorFail($request->id);
    $data['discussions']=DiscussionBoard::get();
    return view('teacher_views.discussionBoard.index',$data);
  }
  public function create(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.discussionBoard.create',$data);

  }
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'session_number' => 'required|string|unique:discussion_boards',
            'title' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'duration' => 'required',
            'password' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $discussion=new DiscussionBoard();
      $discussion->course_id=$request->course_id;
      $discussion->p_id=Auth()->user()->id;
      $discussion->session_number=$request->session_number;
      $discussion->title=$request->title;
      $discussion->date=$request->date;
      $discussion->start_time=$request->start_time;
      $discussion->duration=$request->duration;
      $discussion->password=$request->password;
      $discussion->save();
      return redirect('professional/discussion-borad/'.$request->course_id.'/discussion-borad-list')->with('success','Successfully added');

  }
  public function edit(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      $data['discussion']=DiscussionBoard::findorFail($request->discussion);
      return view('teacher_views.discussionBoard.edit',$data);

  }
  public function update(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'session_number' => 'required',
            'title' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'duration' => 'required',
            'password' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $lesson=DiscussionBoard::where('id',$request->discussion)->first();
      $lesson->course_id=$request->course_id;
      $lesson->p_id=Auth()->user()->id;
      $lesson->session_number=$request->session_number;
      $lesson->title=$request->title;
      $lesson->date=$request->date;
      $lesson->start_time=$request->start_time;
      $lesson->duration=$request->duration;
      $lesson->password=$request->password;
      $lesson->save();
      return redirect('professional/discussion-borad/'.$request->course_id.'/discussion-borad-list')->with('success','Successfully updated');

  }
  public function destory(Request $request)
  {
    $discussion = DiscussionBoard::where('id',$request->discussion)->first();
    $discussion->delete();
    return redirect('professional/discussion-borad/'.$request->id.'/discussion-borad-list')->with('success','Successfully Deleted');
  }
}
