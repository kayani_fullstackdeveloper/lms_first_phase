<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use Auth,Image,DB;
use App\Models\Media;
use Monarobase\CountryList\CountryListFacade;
use App\Models\Teacher\Course;
use App\Models\Teacher\Teacher;
use App\Models\Teacher\Profesional_Edu;
use App\Models\ContactForm;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\Educational_qualification as edu_qua;
use App\Models\Professional_qualification as pro_edu;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('TeacherDomain');
    }
    public function index()
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media
        );
       return view('teacher_views.dashbord',$data);
    }
    public function dashboard(Request $request)
    {

        return view('teacher_views.dashbord');
    }
    public function profile(Request $request)
    {

        $user=Auth()->user();
        if ($user->hasRole(['Teacher','Trainer','Coach','Consultant'])==false||$request->get('modify')=='edit_oncreate') {

            return view('teacher_views.profile_oncrete');

        }

        else
        {
            $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();

            if($media<6||$request->get('modify')=='true'){

                return view('teacher_views.upload_media_profile');
            }
            else{
                $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->get();
                $teacher = Teacher::where('id',Auth()->user()->id)->first();
                // $courses = Course::where('f_id',Auth()->user()->id)->limit(6)->where('date_start', '>=', date('Y-m-d'))->get();
                $courses = Course::where('f_id',Auth()->user()->id)->get();
                $profesional_edus = Profesional_Edu::where('professional_id',Auth()->user()->id)->limit(3)->get();


                return view('teacher_views.approve_go_live',[
                    'data'=>$media,
                    'teacher'=> $teacher,
                    'courses'=> $courses,
                    'profesional_edus'=> $profesional_edus,
                ]);
            }
            // else
            // {
            // $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();

            //     $data=array(
            //         'media'=>$media,
            //         'student'=>0,
            //         'other'=>0,
            //         'course'=>Course::where(['f_id'=>Auth()->user()->id,'guard'=>'teacher'])->get()
            //     );
            //     return view('teacher_views.profile_complete',$data);

            // }
        }
    }
    public function approveGolive(Request $request)
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->get();
        $teacher = Teacher::where('id',Auth()->user()->id)->first();
        // $courses = Course::where('f_id',Auth()->user()->id)->limit(6)->where('date_start', '>=', date('Y-m-d'))->get();
        $courses = Course::where('f_id',Auth()->user()->id)->get();
        $profesional_edus = Profesional_Edu::where('professional_id',Auth()->user()->id)->limit(3)->get();


        return view('teacher_views.approve_go_live',[
            'data'=>$media,
            'teacher'=> $teacher,
            'courses'=> $courses,
            'profesional_edus'=> $profesional_edus,
        ]);
    }
    public function contactForm(Request $request)
    {
        $request->validate( [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
            ]);
        $contact = new ContactForm();
        $contact->name = $request->name;
        $contact->professional_id = Auth()->user()->id;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();
        return back()->with('success', 'mesage send successfully');

    }
    public function add_profile(Request $request)
    {
        $msg='';
        $user=Auth()->user();
        if ($request->post('teacher')) {
            if($request->post('teacher_type'))
            {
                // dd($request->post('teacher_type'));
                $user->removeRole('Teacher');
                DB::table('model_has_permissions')->where('model_id','=',$user->id)->delete();
                // dd();
                foreach ($request->post('teacher_type') as $key => $value) {

                    $user->assignRole('Teacher');
                    $user->hasRole('Teacher');
                    $user->givePermissionTo($value);
                }
            return redirect('professional/profile/approve-go-live')->with('success','');

            }else
            {

               return back()->withErrors(['teacher_type' => 'Teacher type must be select!.'])->withInput($request->input());

            }
        }
        else
        {
            // return back()->with('info','Please Fill Form !')->withInput($request->input());
            // return redirect('professional/profile')->with('success','Successfully Register');
            return back()->withErrors(['teacher_type' => 'Teacher type must be select!.'])->withInput($request->input());


        }
    }
    public function audio_view()
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media
        );
        return view('teacher_views.audio_view',$data);
    }
    public function video_view()
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media
        );
        return view('teacher_views.video_view',$data);
    }
    public function profile_image()
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media
        );
        return view('teacher_views.profile_image',$data);
    }
    public function web_link()
    {
        $details=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-detail'])->first();
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media,
            'details'=>$details
        );
        return view('teacher_views.weblink_view',$data);
    }
    public function profile_complete()
    {

        return view('teacher_views.profile_complete');
    }
    public function bio_view()
    {
        $media=Media::where('f_id',Auth()->user()->id)->whereIn('type',['profile-video','profile-audio','profile-image','profile-detail','profile-course','profile-bio'])->count();
        $data=array(
            'profile_count'=>$media,
            'edu'          =>edu_qua::all(),
            'pro_edu'      =>pro_edu::all()
        );
        $data['countries']=CountryListFacade::getList('en');

        return view('teacher_views.bio_view',$data);
    }
    // media link
    public function profilevideo(Request $request)
    {   $request->validate( [
        'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
        ]);
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('video','public');
                 $request['image'] = str_replace('video/', '', $temp_name);
                //  dd($request->all());

        }
          $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-video'])->first();
            // $media=new Media();
            if($media)
            {
                Storage::disk('public')->delete('video/'.$media->name);
            }else
            {
                $media=new Media();

            }
            $media->type=$request->type;
            $media->name=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            if($request->save_con)
            {

                return redirect('professional/profile/image')->with('success','Your video file saved successfully');

            }else
            {
                return redirect('professional')->with('success','Your video file saved successfully');

            }
    }
    public function profileaudio(Request $request)
    {   $request->validate( [
        'file' => 'required|mimes:mpga,wav,mp3',
        ]);
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('audio','public');
                 $request['image'] = str_replace('audio/', '', $temp_name);
                //  dd($request->all());

        }
          $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-audio'])->first();

            if($media)
            {
                Storage::disk('public')->delete('audio/'.$media->name);
            }else
            {
                $media=new Media();
            }
            $media->type=$request->type;
            $media->name=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            if($request->save_con)
            {

                return redirect('professional/profile/video')->with('success','Your audio file saved successfully');

            }else
            {
                return redirect('professional')->with('success','Your audio file saved successfully');

            }
    }
    // profile image
    public function upload_profile_image(Request $request)
    {
        $request->validate( [
        'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($request->hasFile('file')) {
                 $temp_name  = $request->file('file')->store('images','public');
                 $request['image'] = str_replace('images/', '', $temp_name);
                 Image::make(public_path('storage/images/'.$request['image']))->resize(250,250)->save(config('constants.store_thumb_path').$request['image']);

        }
          $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-image'])->first();

            if($media)
            {
                Storage::disk('public')->delete(config('constants.store_thumb_path').$media->name);
                Storage::disk('public')->delete(config('constants.images').$media->name);
            }else
            {
                $media=new Media();
            }
            $media->type=$request->type;
            $media->name=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            if($request->save_con)
            {

                return redirect('professional/course/create')->with('success','Your photograph saved successfully');

            }else
            {
                return redirect('professional')->with('success','Your photograph saved successfully');

            }
    }
    public function upload_weblink(Request $request)
    {

        $request->validate( [
            'detail' => 'required',
            ]);
        $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-detail'])->first();
        if ($media==null) {
            $media=new Media();
        }
        $media->type='profile-detail';
        $media->name=$request->detail;
        $media->guard_name='teacher';
        $media->f_id=Auth::user()->id;
        $media->save();
        if($request->save_con)
        {

            return redirect('professional/profile/audio')->with('success','Your word document saved successfully');

        }else
        {
            return redirect('professional')->with('success','Your word document saved successfully');

        }


    }
    public function bioupdate(Request $request)
    {
        // dd($request->all());
        $user=Auth::user();
        $request->validate( [
            // 'name'   =>'required',
            // 'address'   =>'required',
            // 'country'   =>'required',
            // 'phone'   =>'required',
            // 'email' => 'required|string|email|max:255|unique:teachers ,email,'.$user->id.',id',
            // 'youtube'   =>'required',
            // 'twitter'   =>'required',
            // 'facebook'   =>'required',
            // 'instagram'   =>'required',
            ]);
        // $user->gender=$request->gender;
        $user->name=$request->name;
        // $user->address=$request->address;
        $user->Website=$request->Website;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->phone=$request->phone;
        $user->youtube=$request->youtube;
        $user->twitter=$request->twitter;
        $user->facebook=$request->facebook;
        $user->instagram=$request->instagram;
        $user->LinkedIn=$request->LinkedIn;
        $user->Pinterest=$request->Pinterest;
        $user->Reddit=$request->Reddit;
        $user->Snapchat=$request->Snapchat;
        $user->Tik_tok=$request->Tik_tok;
        $user->Quora=$request->Quora;
        // $user->professional_edu=implode(',',$request->professional_edu);
        // $user->edu_qualification=implode('',$request->edu_qualification);

        $user->save();
        // dd($request->all());
        $delete_edu=Profesional_Edu::where('professional_id',$user->id)->delete();
        if($request->edu_qualification){
            $count=0;
        foreach ($request->edu_qualification as $key => $value) {
                if($value!=null){
                $edu=new Profesional_Edu();
                $edu->professional_id=$user->id;
                $edu->edu_id=1;
                 $edu->degree_name=$value;
                $edu->edu_type='edu_qual';
                if($request->edu_year[$count])
                {
                    $edu->year=$request->edu_year[$count];

                    $edu->save();
                }
            }
            $count++;


        }
        }
        if($request->professional_edu){
                $count=0;
        foreach ($request->professional_edu as $key => $value) {
            if($value!=null && isset($request->pro_year[$count])){
                $edu=new Profesional_Edu();
            $edu->professional_id=$user->id;
            $edu->edu_id=1;
            $edu->degree_name=$value;
            if($request->pro_year[$count])
            {
                $edu->edu_type='pro_edu';
                $edu->year=$request->pro_year[$count];

                $edu->save();

            }
        }
        $count++;

         }
        }
        // media check for profile
        $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-bio'])->first();
        if ($media==null) {
            $media=new Media();
        }
        $media->type='profile-bio';
        $media->name='none';
        $media->guard_name='teacher';
        $media->f_id=Auth::user()->id;
        $media->save();

        if($request->save_con)
            {
                return redirect('professional/profile')->with('success','Personal data uploaded successfully');

            }else
            {
                return redirect('professional/profile')->with('success','Personal data uploaded successfully');
            }

    }
    // teacher logout
    public function logout()
    {
        Auth::guard('teacher')->logout();
        // return view('teacher_views.login', ['url' => 'blogger']);
        return redirect()->intended('login/teacher');

    }
    public function uploadAudio(Request $request)
    {
        # code...
        if ($request->hasFile('audio-blob')) {
            $temp_name  = $request->file('audio-blob')->store('audio','public');
            $request['image'] = str_replace('audio/', '', $temp_name);
            // dd($request->all());

         }
        //  dd($request->image);
        $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-audio'])->first();

            if($media)
            {
                Storage::disk('public')->delete('audio/'.$media->name);
            }else
            {
                $media=new Media();
            }
            $media->type='profile-audio';
            $media->name=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            return  response()->json(200);
    }
    public function uploadVideo(Request $request)
    {
        # code...
        if ($request->hasFile('video-file')) {
            $temp_name  = $request->file('video-file')->store('video','public');
            $request['image'] = str_replace('video/', '', $temp_name);


                }
     $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-video'])->first();
       // $media=new Media();
    //    dd($request->all());
       if($media)
       {
           Storage::disk('public')->delete('video/'.$media->name);
       }else
       {
           $media=new Media();

       }
       $media->type='profile-video';
       $media->name=$request->image;
       $media->guard_name='teacher';
       $media->f_id=Auth::user()->id;
       $media->save();
       return  response()->json(200);
    }
    public function webimageupload(Request $request)
    {
    //    dd($request->file);
        // $data = Input::all();
        if ($request->hasFile('file')) {

            $temp_name  = $request->file('file')->store('images','public');
            $request['image'] = str_replace('images/', '', $temp_name);
            Image::make(public_path('storage/images/'.$request['image']))->resize(250,250)->save(config('constants.store_thumb_path').$request['image']);

         }

          $media=Media::where(['f_id'=>Auth::user()->id,'guard_name'=>'teacher','type'=>'profile-image'])->first();

            if($media)
            {
                Storage::disk('public')->delete(config('constants.store_thumb_path').$media->name);
                Storage::disk('public')->delete(config('constants.images').$media->name);
            }else
            {
                $media=new Media();
            }
            $media->type='profile-image';
            $media->name=$request->image;
            $media->guard_name='teacher';
            $media->f_id=Auth::user()->id;
            $media->save();
            return response()->json($media);
    }
    public function updateFile(Request $request)
    {
        $file = $request->file;
        $request->validate([
            'file' => 'required|mimes:pdf,doc,docx|max:2048'
            ]);
            $fileName = $file->getClientOriginalName();

        $pdfParser = new Parser();
        $pdf = $pdfParser->parseFile($file->path());
        $content = $pdf->getText();
        dd($content);


    }


}
