<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\TestQuiz;
use Illuminate\Support\Facades\Validator;
use Auth;

class TestQuizController extends Controller
{
  public function index(Request $request)
  {
    $data['course']=Course::findorFail($request->id);
    $data['testquizes']=TestQuiz::where('p_id',auth()->user()->id)->get();
    return view('teacher_views.testQuiz.index',$data);
  }
  public function create(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.testQuiz.create',$data);

  }
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required|string|unique:test_quizzes',
            'type' => 'required',
            'title' => 'required',
            'grading' => 'required',
            'duration' => 'required',
            'link' => 'required|url',
            'description' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $testQuiz=new TestQuiz();
      $testQuiz->course_id=$request->course_id;
      $testQuiz->p_id=Auth()->user()->id;
      $testQuiz->reference_number=$request->reference_number;
      $testQuiz->type=$request->type;
      $testQuiz->title=$request->title;
      $testQuiz->date_open=$request->date_open;
      $testQuiz->date_close=$request->date_close;
      $testQuiz->formate="word";
      $testQuiz->grading=$request->grading;
      $testQuiz->duration=$request->duration;
      $testQuiz->link=$request->link;
      $testQuiz->description=$request->description;
      $testQuiz->save();
      return redirect('professional/test-quiz/'.$request->course_id.'/test-quiz-list')->with('success','Successfully added');

  }
  public function edit(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      $data['testQuiz']=TestQuiz::findorFail($request->testquiz);
      if ($data['testQuiz']->formate == 'word') {
        return view('teacher_views.testQuiz.edit',$data);

      }elseif($data['testQuiz']->formate == 'audio') {
        return view('teacher_views.testQuiz.audio.edit',$data);
      }
      else {
        return view('teacher_views.testQuiz.video.edit',$data);
      }

  }
  public function update(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required',
            'type' => 'required',
            'title' => 'required',
            'grading' => 'required',
            'link' => 'required|url',
            'description' => 'required',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }
      $testQuiz=TestQuiz::where('id',$request->testquiz)->first();
      $testQuiz->course_id=$request->course_id;
      $testQuiz->p_id=Auth()->user()->id;
      $testQuiz->reference_number=$request->reference_number;
      $testQuiz->type=$request->type;
      $testQuiz->title=$request->title;
      $testQuiz->date_open="pending";
      $testQuiz->date_close="pending";
      $testQuiz->formate="word";
      $testQuiz->grading=$request->grading;
      $testQuiz->duration=$request->duration;
      $testQuiz->link=$request->link;
      $testQuiz->description=$request->description;
      $testQuiz->save();
      return redirect('professional/test-quiz/'.$request->course_id.'/test-quiz-list')->with('success','Successfully updated');

  }
  public function destory(Request $request)
  {
    $testQuiz = TestQuiz::where('id',$request->testquiz)->first();
    $testQuiz->delete();
    return redirect('professional/test-quiz/'.$request->id.'/test-quiz-list')->with('success','Successfully Deleted');
  }

  public function createAudio(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.testQuiz.audio.create',$data);

  }

  public function audioFile(Request $request)
  {
      $request->validate( [
          'title' => 'required',
          'type' => 'required',
      ]);
      if ($request->hasFile('audio-blob')) {
          $temp_name  = $request->file('audio-blob')->store('testQuiz/audio','public');
          $request['file'] = str_replace('testQuiz/audio/', '', $temp_name);
          // dd($request->all());

       }
          $media=TestQuiz::where(['p_id'=>Auth::user()->id,'type'=>'audio'])->first();

          if($media)
          {
              Storage::disk('public')->delete('testQuiz/'.$media->title);
          }else
          {
              $media=new TestQuiz();
          }

          $media->reference_number=$request->reference_number;
          $media->course_id=$request->course_id;
          $media->p_id=Auth::user()->id;
          $media->title=$request->title;
          $media->date_open="pending";
          $media->date_close="pending";
          $media->type=$request->type;
          $media->file=$request->file;
          $media->formate='audio';
          $media->grading=$request->grading;
          $media->duration=$request->duration;
          $media->link=$request->link;

          // dd($media);
          $media->save();
          return  response()->json(200);
          // return redirect('professional/test-quiz/'.$request->id.'/test-quiz-list')->with('success','Audio Successfully Added');

  }
  public function uploadAudio(Request $request)
  {
    $validator = Validator::make($request->all(), [
            'reference_number' => 'required|string|unique:test_quizzes',
            'type' => 'required',
            'title' => 'required',
            'grading' => 'required',
            'duration' => 'required',
            'link' => 'required|url',
            'file' => 'required|mimes:mpga,wav,mp3',
        ]
    );
    if ($validator->fails()) {
        $errors = $validator->errors();

        return redirect()->back()->withErrors($errors)->withInput();
    }

      $testQuiz=new TestQuiz();
      $testQuiz->course_id=$request->course_id;
      $testQuiz->p_id=Auth()->user()->id;
      $testQuiz->reference_number=$request->reference_number;
      $testQuiz->type=$request->type;
      $testQuiz->title=$request->title;
      $testQuiz->date_open="pending";
      $testQuiz->date_close="pending";
      $testQuiz->formate="audio";
      $testQuiz->grading=$request->grading;
      $testQuiz->duration=$request->duration;
      $testQuiz->link=$request->link;
      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('testQuiz/audio','public');
          $request->file = str_replace('testQuiz/audio/', '', $temp_name);
          $testQuiz->file=$request->file;

         }
      $testQuiz->save();
      return redirect('professional/test-quiz/'.$request->course_id.'/test-quiz-list')->with('success','Audio File Added Successfully');

  }
  public function updateAudio(Request $request)
  {
      $request->validate( [
        'reference_number' => 'required',
        'type' => 'required',
        'title' => 'required',
        'grading' => 'required',
        'duration' => 'required',
        'link' => 'required|url',
        'file' => 'required|mimes:mpga,wav,mp3',
      ]);
      $testQuiz= TestQuiz::findorFail($request->testquiz);
      // dd($testQuiz);
      $testQuiz->course_id=$request->course_id;
      $testQuiz->p_id=Auth()->user()->id;
      $testQuiz->reference_number=$request->reference_number;
      $testQuiz->type=$request->type;
      $testQuiz->title=$request->title;
      $testQuiz->date_open="pending";
      $testQuiz->date_close="pending";
      $testQuiz->formate="audio";
      $testQuiz->grading=$request->grading;
      $testQuiz->duration=$request->duration;
      $testQuiz->link=$request->link;

      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('testQuiz/audio','public');
          $request->file = str_replace('testQuiz/audio/', '', $temp_name);
          $testQuiz->file=$request->file;

         }
         // dd($testQuiz);
      $testQuiz->save();
      return redirect('professional/test-quiz/'.$request->course_id.'/test-quiz-list')->with('success','Audio File updated Successfully');

  }
  public function createVideo(Request $request)
  {
      $data['course']=Course::findorFail($request->id);
      return view('teacher_views.testQuiz.video.create',$data);

  }
  public function videoFile(Request $request)
  {
      $request->validate( [
          'title' => 'required',
          'type' => 'required',
      ]);

       if ($request->hasFile('video-file')) {
           $temp_name  = $request->file('video-file')->store('testQuiz/video','public');
           $request['file'] = str_replace('testQuiz/video/', '', $temp_name);
        }
          $media=TestQuiz::where(['p_id'=>Auth::user()->id,'formate'=>'video'])->first();

          if($media)
          {
              Storage::disk('public')->delete('testQuiz/'.$media->title);
          }else
          {
              $media=new TestQuiz();
          }

          $media->reference_number=$request->reference_number;
          $media->course_id=$request->course_id;
          $media->p_id=Auth::user()->id;
          $media->title=$request->title;
          $media->date_open="pending";
          $media->date_close="pending";
          $media->type=$request->type;
          $media->file=$request->file;
          $media->formate='video';
          $media->grading=$request->grading;
          $media->duration=$request->duration;
          $media->link=$request->link;

          // dd($media);
          $media->save();
          return  response()->json(200);
  }
  public function updateVideo(Request $request)
  {
      $request->validate( [
        'reference_number' => 'required',
        'type' => 'required',
        'title' => 'required',
        'grading' => 'required',
        'duration' => 'required',
        'link' => 'required|url',
        // 'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
      ]);
      $testQuiz= TestQuiz::findorFail($request->testquiz);

      // dd($testQuiz);
      $testQuiz->course_id=$request->course_id;
      $testQuiz->p_id=Auth()->user()->id;
      $testQuiz->reference_number=$request->reference_number;
      $testQuiz->type=$request->type;
      $testQuiz->title=$request->title;
      $testQuiz->date_open="pending";
      $testQuiz->date_close="pending";
      $testQuiz->formate="video";
      $testQuiz->grading=$request->grading;
      $testQuiz->duration=$request->duration;
      $testQuiz->link=$request->link;

      if ($request->hasFile('file')) {
          $temp_name  = $request->file('file')->store('testQuiz/video','public');
          $request->file = str_replace('testQuiz/video/', '', $temp_name);
          $testQuiz->file=$request->file;

         }
         // dd($testQuiz);
      $testQuiz->save();
      return redirect('professional/test-quiz/'.$request->course_id.'/test-quiz-list')->with('success','Video File updated Successfully');

  }
}
