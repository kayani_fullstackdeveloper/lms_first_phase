<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\Teacher\Attendance;
use App\Models\Teacher\Attendance_mark;
use App\Models\CommonModels\StudentCourseSectionModel;
use App\Models\CommonModels\CoursePublishModel;
use Illuminate\Support\Facades\Validator;
use Auth;
use Session;
class AttendanceController extends Controller
{
    public function section(Request $request)
    {


        $data['course']=Course::findorFail($request->id);
        $data['clusters']=CoursePublishModel::where('status','=','active')->where('course_id',$request->id)->get();
        return view('teacher_views.attendance.attendance_section_list',$data);
    }
    public function attendance_mark(Request $request)
    {
        $data['course']=Course::findorFail($request->course_id);
        $data['section']=CoursePublishModel::findorFail($request->section_id);

        $data['attendance'] = Attendance::with(['attendanceMark:id,attendance'])->where('course_id', $request->course_id)->where('professional_id', Auth::user()->id)->where('section_id',$request->section_id)->first();
        $data['students']=StudentCourseSectionModel::with('course','student','professional','section')->where(['course_id'=>$request->course_id,'section_id'=>$request->section_id])->get();
        return view('teacher_views.attendance.attendance_mark',$data);
    }
    public function type(Request $request)
    {

        $data['course']=Course::findorFail($request->id);
        $data['section']=CoursePublishModel::findorFail($request->section_id);
        $data['list']=CoursePublishModel::where('status','=','active')->where('course_id',$request->id)->get();
        $data['attendances'] = Attendance::with(['attendanceMark:id,attendance'])->where('course_id', $request->id)->where('professional_id', Auth::user()->id)->where('section_id',$request->section_id)->where('type',$request->type)->get();
        return view('teacher_views.attendance.type-list',$data);
    }
    public function attendanceView(Request $request)
    {

        $data['course']=Course::findorFail($request->course_id);
        $data['section']=CoursePublishModel::findorFail($request->section_id);
        // $data['attendance'] = Attendance::where('course_id', $request->course_id)->where('professional_id', Auth::user()->id)->where('section_id',$request->section_id)->first();

        $data['attendances'] = Attendance::with(['attendanceMark:id,attendance'])->where('course_id', $request->id)->where('professional_id', Auth::user()->id)->where('section_id',$request->section_id)->where('type','live_class_session')->get();
        // dd($data['attendance']);
        $data['list']=CoursePublishModel::where('status','=','active')->where('course_id',$request->id)->get();
        return view('teacher_views.attendance.view',$data);
    }
    public function attendanceForm(Request $request)
    {
        // dd($request->toArray());
      $validator = Validator::make($request->all(), [
              'reference_number' => 'required',
              'date' => 'required',
              'time' => 'required',
              'duration' => 'required',
          ]
      );
      if ($validator->fails()) {
          $errors = $validator->errors();

          return redirect()->back()->withErrors($errors)->withInput();
      }
      // $attendances = Attendance::where()
      $checkAlready = Attendance::where('course_id', $request->course_id)->where('professional_id', Auth::user()->id)->where('section_id',$request->section_id)->where('type',$request->type)->first();


        if (!$checkAlready) {
            $attendance       = new Attendance();
            Session::flash('success', 'You successfully add the Attendance');

        } else {
            $attendance = Attendance::where('id', $checkAlready->id)->first();
            Session::flash('success', 'You successfully updated the Attendance');
        }
            $attendance->professional_id    = Auth::user()->id;
            $attendance->course_id  = $request->course_id;
            $attendance->section_id      = $request->section_id;
            $attendance->reference_number = $request->reference_number;
            $attendance->date = $request->date;
            $attendance->time = $request->time;
            $attendance->duration = $request->duration;
            $attendance->type = $request->type;
            $attendance->save();

            $students=StudentCourseSectionModel::select('std_id')
            ->where(['course_id'=>$request->course_id,'section_id'=>$request->section_id])
            ->get();

             $allinterests=[];
            foreach($request->userid as $key=>$studentid){

                $att= new Attendance_mark();
                $att->attendance_id=$attendance->id;
                $att->std_id=$studentid;
                $att->attendance=$_POST['attendance'.$studentid];
                $allinterests[] = $att->attributesToArray();

            }
            Attendance_mark::insert($allinterests);


        return redirect('professional/attendance/'.$request->course_id);
    }
    public function form(Request $request)
      {

        $data['course']=Course::findorFail($request->id);

        $data['section']=CoursePublishModel::findorFail($request->section_id);
        $data['students']=StudentCourseSectionModel::with('course','student','professional','section')->where(['course_id'=>$request->id,'section_id'=>$request->section_id])->get();
        return view('teacher_views.attendance.attendance_mark',$data);
    }
}
