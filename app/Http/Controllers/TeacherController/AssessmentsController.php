<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth,Image,DB;
use App\Models\Teacher\Course;
use App\Models\Teacher\CourseSectionGuideline;
use App\Models\CommonModels\StudentCourseSectionModel;
use App\Models\CommonModels\AssessmentsModel;
use App\Models\CommonModels\CoursePublishModel;
use Carbon\Carbon;
class AssessmentsController extends Controller
{
    //
    public function index(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['section']=CoursePublishModel::where('status','=','active')->findorFail($request->section);
        $data['clusters'] = CoursePublishModel::where('professional_id',Auth::user()->id)->get();
        $data['sectionGuidelines'] = CourseSectionGuideline::where('course_id',$request->id)->get();
        return view('teacher_views.assessments.list',$data);
    }
    public function toolkit(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['section']=CoursePublishModel::where('status','=','active')->findorFail($request->section);
        $data['clusters'] = CoursePublishModel::get();
        return view('teacher_views.assessments.toolkit',$data);
    }
    public function section(Request $request)
    {

        $data['course']=Course::findorFail($request->id);
        $data['list']=CoursePublishModel::where('status','=','active')->where('course_id',$request->id)->get();
        $data['clusters'] = CoursePublishModel::where('professional_id',Auth::user()->id)->get();
        $data['sectionGuidelines'] = CourseSectionGuideline::where('course_id',$request->id)->get();

        // $data['books']=obj::where('user_id',auth()->user()->id)->paginate(20);
        return view('teacher_views.assessments.section_list',$data);
    }

    public function registerStudentList(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['list']=StudentCourseSectionModel::with('course','student','professional','section')->where(['course_id'=>$request->id,'section_id'=>$request->section])->get();
        $data['section']=CoursePublishModel::where('status','=','active')->findorFail($request->section);
        return view('teacher_views.assessments.register_student_list',$data);
    }
    public function type(Request $request)
    {
        $pro_id=auth()->user()->id;
        $data['course']=Course::findorFail($request->id);
        $data['list']=StudentCourseSectionModel::with('course','student','professional','section')->where(['course_id'=>$request->id,'section_id'=>$request->section])->get();
        //$data['section']=CoursePublishModel::where('status','=','active')->findorFail($request->section);
        // dd($data['list']->toArray());
        $data['guideline']=CourseSectionGuideline::where('course_id',$request->id)->where('type',$request->type)->first();
        return view('teacher_views.assessments.index',$data);

    }
    public function get_assessments(Request $request)
    {
        # code...
        $course_id=$request->course_id;
        $std_id=$request->std_id;
        $section_id=$request->section_id;
        $list['data']=['course_id'=>$course_id,'std_id'=>$std_id,'section_id'=>$section_id,'type'=>$request->type];
        $query=AssessmentsModel::query();
       $query-> where(['course_id'=>$course_id,'std_id'=>$std_id,'section_id'=>$section_id,'type'=>$request->type]);
        if($request->type=='summative')
        {
            $query->whereMonth('created_at', Carbon::now()->month);

        }else if($request->type=='formative')
        {

            $query-> where('created_at', '>', Carbon::now()->startOfWeek());
           $query->where('created_at', '<', Carbon::now()->endOfWeek());
        }
        $list['list']=$query->take(3)->get();
        $list['list2']=$query->skip(3)->take(2)->get();
        $list['list3']=$query->skip(5)->take(3)->get();
        $list['list4']=$query->skip(8)->take(2)->get();
        if($list['list']->count()==0)
        {
            $this->generate_accessment($request);
            $query=AssessmentsModel::query();
             $query->where(['course_id'=>$course_id,'std_id'=>$std_id,'section_id'=>$section_id,'type'=>$request->type]);
             $list['list']=$query->take(3)->get();
            $list['list2']=$query->skip(3)->take(2)->get();
            $list['list3']=$query->skip(5)->take(3)->get();
            $list['list4']=$query->skip(8)->take(2)->get();
        }
        // dd($list['list']->count());
        $list=view('teacher_views.assessments.std_assessments_list',$list)->render();
         return response()->json(['list'=>$list]);
    }
    public function save_assessments(Request $request)
    {
        // dd($request->toArray());
        $data=new AssessmentsModel();
        $data->std_id=$request->std_id;
        $data->course_id=$request->course_id;
        $data->section_id=$request->section_id;
        $data->lable=$request->lable;
        $data->title=$request->title;
        $data->details=$request->details;
        $data->type=$request->type;
        $data->save();

    }
    public function update_assessments(Request $request)
    {
        // dd($request->toArray());
        $data=AssessmentsModel::findorFail($request->id);

        // $data->lable=$request->lable;
        // $data->title=$request->title;
        $data->details=$request->details;
        $data->save();

    }
    public function delete(Request $request)
    {
        // dd($request->toArray());
        $data=AssessmentsModel::findorFail($request->id);

        $data->delete();
        echo(1);

    }
    public function generate_accessment(Request $request)
    {
        $object[] = (object) array();
        $object=[
            [
                'lable'=>'Situation ',
                'title'=>'Briefly outline the circumstances surronding the example of a situation that had a positive outcome or one in which professional growth occerred.',
            ],
            [
                'lable'=>'Action',
                'title'=>'Describe the skills/knowledge/competencies required to address the situation-online the steps you took to complete the task?',
            ],
            [
                'lable'=>'Outcome',
                'title'=>'Describe the result of your action- summaries the results of the action and/or the professional Growth that occurred',
            ],
            [
                'lable'=>'Standard 1',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 2',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 3',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 4',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 5',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 6',
                'title'=>'',
            ]
            ,
            [
                'lable'=>'Standard 7',
                'title'=>'',
            ]
        ];
        foreach ($object as $key => $value) {
            $data=new AssessmentsModel();
            $data->std_id=$request->std_id;
            $data->course_id=$request->course_id;
            $data->section_id=$request->section_id;
            $data->lable=$value['lable'];
            $data->title=$value['title'];
            $data->details='';
            $data->type=$request->type;
            $data->save();
        }
        // dd($request->toArray());


    }
    public function Formative(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.assessments.formative',$data);
    }
    public function CourseSectionGuideline(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['guidelines']=CourseSectionGuideline::where('course_id',$request->id)->where('type',$request->type)->get();
      //  $data['section']=CoursePublishModel::where('status','=','active')->findorFail($request->section);

        return view('teacher_views.assessments.guidelines',$data);
    }
    public function GuidelineCreate(Request $request)
    {
        $request->validate( [
            'guideline' => 'required',
            ]);
        $data=CourseSectionGuideline::where('course_id',$request->course_id)->where('type',$request->type)->first();

        if ($data==null) {
            $data=new CourseSectionGuideline();
        }
        $data->course_id=$request->course_id;

        $data->guideline=$request->guideline;
        $data->type=$request->type;
        $data->save();
        return redirect('professional/assessments/section/'.$request->course_id)->with('success','Successfully Updated');

    }
    public function GuidelineEdit(Request $request)
    {
      $data['course']=Course::findorFail($request->id);
      $data['guideline'] = CourseSectionGuideline::findorFail($request->guideline);
      return view('teacher_views.assessments.criteria.edit',$data);
    }
    public function CourseSectionGuidelineUpdate(Request $request)
    {
        $data=CourseSectionGuideline::findorFail($request->edit);
        $data->course_id=$request->course_id;
        $data->section_id=$request->section_id;
        $data->guideline=$request->guideline;
        $data->type=$request->type;
        $data->save();
        return redirect('professional/assessments/section/'.$request->course_id)->with('success','Successfully Updated');

    }
    public function destroy(Request $request)
    {
        $guideline = CourseSectionGuideline::find($request->id);
        $guideline->delete();
        return back()->with('success','Guideline Deleted Successfully');
    }
    public function weblink(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        return view('teacher_views.assessments.weblink',$data);
    }
}
