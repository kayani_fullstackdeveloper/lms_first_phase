<?php

namespace App\Http\Controllers\TeacherController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher\Course;
use App\Models\CommonModels\CoursePublishModel;


class CourseClusterController extends Controller
{
    //
    public function cluster(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['clusters'] = CoursePublishModel::where('professional_id',auth()->user()->id)->get();
        return view('teacher_views.course.cluster.list',$data);
    }
    public function clusterCreate(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        // $data['books']=obj::where('user_id',auth()->user()->id)->paginate(20);
        return view('teacher_views.course.cluster.add',$data);
    }
    public function clusterStore(Request $request)
    {
        $re=$request->toArray();
        // dd($re);
        for ($i=0; $i <$request->cluster ; $i++) {
            # code...
            $data=new CoursePublishModel();
            $data->course_id=$request->course_id;
            $data->professional_id=$request->professional_id;
            $data->title=$re['title'][$i];
            $data->start_date=$re['start_date'][$i];
            $data->end_date=$re['end_date'][$i];
            $data->timing=$re['timing'][$i];
            $data->save();
        }
        return redirect('professional/course/'.$request->course_id.'/cluster')->with('success','Successfully Added');
    }

    public function clusterEdit(Request $request)
    {
        $data['course']=Course::findorFail($request->id);
        $data['cluster']=CoursePublishModel::findorFail($request->edit);
        return view('teacher_views.course.cluster.edit',$data);

    }
    public function clusterUpdate(Request $request)
    {
        $data= CoursePublishModel::where('id',$request->edit)->first();
        $data->course_id=$request->course_id;
        $data->professional_id=$request->professional_id;
        $data->title=$request->title;
        $data->start_date=$request->start_date;
        $data->end_date=$request->end_date;
        $data->timing=$request->timing;
        $data->save();
        return redirect('professional/course/'.$request->course_id.'/cluster')->with('success','Successfully Updated');

    }

    public function clusterDelete(Request $request,$delete)
    {
        $cuslter = CoursePublishModel::where('id',$request->id)->first();
        $cuslter->delete();
        return back()->with('success','Successfully Deleted');
    }
}
