<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables,Helper,Auth;
use App\Models\CommonModels\Podcast as obj;
use App\Models\CommonModels\PodcastCategories as podcat;
use Illuminate\Support\Facades\Storage;
use Image;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin_views.professional.podcast.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data=array(
            'podcats'=>podcat::all()
        );
       return view('Admin_views.professional.podcast.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $request->validate( [
            'title' => 'required|string|max:255',
            'cat_id' => 'required',
            'image' => 'required',
            'type' => 'required',
 
        ]);
        
        if ($request->hasFile('audio')) {
            
            $temp_name  = $request->file('audio')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
          
           
           }
           if ($request->hasFile('video')) {
            $temp_name  = $request->file('video')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
          
           
           }
           if($request->link){
               $request['audio_video_link']=$request->link;
           }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('podcast','public');
             $request->file('image')->store('podcast/thambnail','public');
            $request['thambnail'] = str_replace('podcast/', '', $temp_name);
            Image::make(public_path('storage/podcast/'.$request['thambnail']))->resize(150,150)->save('storage/podcast/thambnail/'.$request['thambnail']);
           }
         obj::create([
            'title' => $request->title,
            'cat_id' => $request->cat_id,
            'audio_video_link' => $request->audio_video_link,
            'image' => $request->thambnail,
            'type' => $request->type,
            'user_id' => auth()->guard('admin')->user()->id,
            'user_guard' =>'admin',
        ]);
        return redirect()->route('podcast.index')->with('success','successfully Added ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data']=obj::findorFail($id);
        $data['podcats']=podcat::all();
            
        
        return view('Admin_views.professional.podcast.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
            'cat_id' => 'required',
            'type' => 'required',
 
        ]);

        $data=obj::findorFail($id);
        $request['audio_video_link']=$data->audio_video_link;
        $request['thambnail']=$data->image;
        if ($request->hasFile('audio')) {
            
            $temp_name  = $request->file('audio')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
            Storage::disk('public')->delete('storage/podcast/'.$data->audio_video_link);
           }
           if ($request->hasFile('video')) {
            $temp_name  = $request->file('video')->store('podcast','public');
            $request['audio_video_link'] = str_replace('podcast/', '', $temp_name);
            Storage::disk('public')->delete('storage/podcast/'.$data->audio_video_link);
           }
           if($request->link){
               $request['audio_video_link']=$request->link;
           }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('podcast','public');
             $request->file('image')->store('podcast/thambnail','public');
            $request['thambnail'] = str_replace('podcast/', '', $temp_name);
            Image::make(public_path('storage/podcast/'.$request['thambnail']))->resize(250,250)->save('storage/podcast/thambnail/'.$request['thambnail']);
            Storage::disk('public')->delete('storage/podcast/thambnail/'.$data->image);
            Storage::disk('public')->delete('storage/podcast/'.$data->image);
           }
           $data->title=$request->title;
           $data->type=$request->type;
           $data->image=$request->thambnail;
           $data->cat_id=$request->cat_id;
           $data->audio_video_link = $request->audio_video_link;
           $data->save();
        return redirect()->route('podcast.index')->with('success','successfully Update ');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove=obj::findorFail($id);
        Storage::disk('public')->delete('storage/podcast/thambnail/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->image);
        Storage::disk('public')->delete('storage/podcast/'.$remove->audio_video_link);
        $remove->delete();
    }
    public function datatable(Request $request)
    {  
        $items = obj::select('*');
        // dd($items);
       return datatables($items)
        ->addColumn('action',function($item){

             return view('Admin_views.professional.podcast.action',$item);

        })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action'])
        ->toJson();
    }
}
