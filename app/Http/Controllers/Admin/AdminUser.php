<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables,Helper;
use Hash;

use App\Models\Admin\Admin;

class AdminUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        return view('admin_views.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_views.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request->toArray());
        $request->validate( [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user= Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return redirect('admin/users')->with('success','successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data['user']=Admin::findorFail($id);
        
        return view('admin_views.user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $user=Admin::findorFail($id);
        $request->validate( [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:admins,email,'.$user->id.',id',
            
        ]);
        $user->name=$request->name;
        $user->email=$request->email;
        if($request->password)
        {
            $request-validate([

            'password' => 'required|string|min:6|confirmed',
            ]);
            $user->password=Hash::make($request->password);

        }
        $user->save();
        return redirect('admin/users')->with('success','successfully Added');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove=Admin::findorFail($id);
        $remove->delete();
    }
    public function datatable(Request $request)
    {  
        $items = Admin::select('*');
        
       return datatables($items)
        ->addColumn('action',function($item){

             return view('admin_views.user.action',$item);

        })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action'])
        ->toJson();
    }
}
