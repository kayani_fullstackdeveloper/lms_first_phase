<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables,Helper;
use App\Models\CommonModels\PodcastCategories as obj;

class PostCategoiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=array(
            // 'data'=>obj::all(),
        );
       
        return view('Admin_views.professional.podcastcategories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin_views.professional.podcastcategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
 
        ]);
         obj::create([
            'title' => $request->title,
        ]);
        return redirect('admin/podcastcategory')->with('success','successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['list']=obj::findorFail($id);
        
        return view('Admin_views.professional.podcastcategories.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
 
        ]);
        $data=obj::findorFail($id);
        $data->title=$request->title;
        $data->save();
        return redirect('admin/podcastcategory')->with('success','successfully Update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove=obj::findorFail($id);
        $remove->delete();
    }

    public function datatable(Request $request)
    {  
        
        $items = obj::select('*');
        // dd($items);
       return datatables($items)
        ->addColumn('action',function($item){

             return view('Admin_views.professional.podcastcategories.action',$item);

        })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action'])
        ->toJson();
    }
}
