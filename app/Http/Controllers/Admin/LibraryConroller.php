<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables,Helper;
use App\Models\CommonModels\Library as obj;
use Illuminate\Support\Facades\Storage;
use Image;

class LibraryConroller extends Controller
{
    public function index()
    {
        return view('Admin_views.professional.library.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
       return view('Admin_views.professional.library.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //    dd($request->all());
        
        $request->validate( [
            'title' => 'required|string|max:255',
            'display'=>'required',
            'displayorder'=>'required',
            'image' => 'required',
            'type' => 'required',
 
        ]);
        
        if ($request->hasFile('file')) {
            
            $temp_name  = $request->file('file')->store('library','public');
            $request['file_link'] = str_replace('library/', '', $temp_name);
          
           
           }
           
           if($request->link){
               $request['file_link']=$request->link;
           }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('library','public');
             $request->file('image')->store('library/thambnail','public');
            $request['thambnail'] = str_replace('library/', '', $temp_name);
            Image::make(public_path('storage/library/'.$request['thambnail']))->resize(150,150)->save('storage/library/thambnail/'.$request['thambnail']);
           }
        //    dd($request->all());
         obj::create([
            'title' => $request->title,
            'display' => $request->display,
            'displayorder' => $request->displayorder,
            'file_link' => $request->file_link,
            'image' => $request->thambnail,
            'type' => $request->type,
        ]);
        return redirect()->route('library.index')->with('success','successfully Added ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data']=obj::findorFail($id);
        // dd();
        
        return view('Admin_views.professional.library.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate( [
            'title' => 'required|string|max:255',
            'display'=>'required',
            'displayorder'=>'required',
            
            'type' => 'required',
 
        ]);

        $data=obj::findorFail($id);
        $request['file_link']=$data->audio_video_link;
        $request['thambnail']=$data->image;
        if($request->type=='file'){
        if ($request->hasFile('file')) {
            
            $temp_name  = $request->file('file')->store('library','public');
            $request['file_link'] = str_replace('library/', '', $temp_name);
            Storage::disk('public')->delete('storage/library/'.$data->file_link);
           }
        }
        if($request->type=='link'){
           
           if($request->link){
               $request['file_link']=$request->link;
           }
        }
        

           if ($request->hasFile('image')) {
            $temp_name  = $request->file('image')->store('library','public');
             $request->file('image')->store('library/thambnail','public');
            $request['thambnail'] = str_replace('library/', '', $temp_name);
            Image::make(public_path('storage/library/'.$request['thambnail']))->resize(150,150)->save('storage/library/thambnail/'.$request['thambnail']);
            Storage::disk('public')->delete('storage/library/thambnail/'.$data->image);
            Storage::disk('public')->delete('storage/library/'.$data->image);
           }
           $data->title=$request->title;
           $data->type=$request->type;
           $data->display=$request->display;
           $data->displayorder=$request->displayorder;
           $data->image=$request->thambnail;
           $data->file_link = $request->file_link;
           $data->save();
        return redirect()->route('library.index')->with('success','successfully Update ');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove=obj::findorFail($id);
        Storage::disk('public')->delete('storage/library/thambnail/'.$remove->image);
        Storage::disk('public')->delete('storage/library/'.$remove->image);
        Storage::disk('public')->delete('storage/library/'.$remove->audio_video_link);
        $remove->delete();
    }
    public function datatable(Request $request)
    {  
        $items = obj::select('*');
        // dd($items);
       return datatables($items)
        ->addColumn('action',function($item){

             return view('Admin_views.professional.library.action',$item);

        })
        ->addColumn('image',function($item){

            $url=asset("storage/library/thambnail/$item->image"); 
       return "<img src='".$url."' />";

       })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action','image'])
        ->toJson();
    }
}
