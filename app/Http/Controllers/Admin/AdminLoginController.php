<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AdminLoginController extends Controller
{
    public function login()
    {
        # code...
        return view('admin_views.login');
    }
    public function logout()
    {   
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');

    }
}
