<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables,Helper;
use App\Models\Teacher\Teacher;
use App\Models\Teacher\Course;


class AdminProfessional extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view('admin_views.professional',compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $professional)
    {
        $professional->delete();
        // $brand->where('id', 3)->restore();
        return back()->with('success', 'Professional has been deleted Successfully');

    }
    public function datatable(Request $request)
    {
        $items = Teacher::select('*');
        
       return datatables($items)
        ->addColumn('action',function($item){

            // $action = '<a href="javascript:updateRecord('.$item->id.')"  class="btn btn-xs my-1 btn-primary" >'.__('Edit').'</a> ';
            $action = '<a href="javascript:delete_record('.$item->id.')"  class="btn btn-xs my-1 btn-danger" >'.__('Delete').'</a> ';
            return $action;

        })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action'])
        ->toJson();
    }
    public function getCoursedatatable(Request $request)
    {
        $items = Course::select('*');
        
       return datatables($items)
        ->addColumn('action',function($item){

            $action = '<a href="javascript:delete_record('.$item->id.')"  class="btn btn-xs my-1 btn-danger" >'.__('Delete').'</a> ';
            return $action;

        })
        ->editColumn('created_at',function($item){ return $item->created_at; })
        
        ->rawColumns(['action'])
        ->toJson();
    }
}
