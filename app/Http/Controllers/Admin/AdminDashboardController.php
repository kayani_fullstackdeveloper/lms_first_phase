<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Teacher\Course;
use App\Models\Teacher\Teacher;
use App\Models\Student\Student;
class AdminDashboardController extends Controller
{
    //
    public function index()
    {
        $teachers = Teacher::all()->count();
        $courses = Course::all()->count();
        $students = Student::all()->count();
        // dd(Auth::guard('admin')->user());
        return view('admin_views.dashboard',compact('teachers','courses','students'));
    }
    public function getCourses()
    {
        $courses = Course::all();
        return view('admin_views.course',compact('courses'));
    }
    
    
}
