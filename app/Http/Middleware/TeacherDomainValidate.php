<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherDomainValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if (Auth::guard('teacher')->check()) {
            $domain=Auth::guard('teacher')->user()->domain_name;
            if($request->subdomain==$domain)
            {
                return $next($request);
            }else
            {

               abort(403, 'Unauthorized action.');
            }
        }
        
    }
}
