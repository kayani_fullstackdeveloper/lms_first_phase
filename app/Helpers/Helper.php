<?php 
namespace App\Helpers;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use Auth;
use Image;


class Helper
{
    function formatDate($date)
    {
	return date('Y-m-d',strtotime($date));
    }
    public static function  getWorldMenu()
    {
        return Db::table('continents')->get();
    }
    public static function getTopMenu()
    {
        $menu = DB::table('menu')->where('position','like','%top%')->orderBy('display_order')->get();
        $html = ""; 
        foreach ($menu as $key => $value) {
            $html.= "<li><a href='pages/{{ $value->slug }}'>".$value->name."</a></li>";
        }
        echo $html;
    }
    public static function getMenuLinks($tag)
    {
        $menu = DB::table('menu')->where('position','like','%'.$tag.'%')->orderBy('display_order')->get();
        $html = ""; 
        foreach ($menu as $key => $value) {
           
            $html.= "<li><a href='".url($value->slug)."'>".$value->name."</a></li>";
        }
        echo $html;
    }
    
    public static function getMenuRows($parent = 0){
        return DB::table('menu')->where([
            ['display','Yes'],
            ['parent',$parent],
            ['position','like','%navigation%']
        ])->orderBy('display_order','asc')->get();
    }
 

    public static function showAlert($string = 'No Text Given',$type = 'primary')
    {
        return '<div class="alert alert-'.$type.' alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$string.'</div>';
    }
    public static function getOptions($config = [])
    {
        extract($config);
        if(!array_key_exists('table', $config)){
            echo "<option value='' disabled selected>Table missing</option>";
            return;
        }
        if(!array_key_exists('cond', $config)) $cond = [];
        if(!array_key_exists('value', $config)) $value = 'id';
        if(!array_key_exists('key', $config)) $key = $value;

        if(!array_key_exists('select', $config)) $select = '';
        if(!array_key_exists('selectOption', $config)) $selectOption = FALSE;

        if($cond)
            $data = Db::table($table)->where($cond)->get();
        else
            $data = Db::table($table)->get();
  
        if ($data)
        {
            if($selectOption) echo "<option value='' disabled selected>Select option</option>";
            foreach ($data as $row2)
            {
                if($row2->$key == $select) $selected= "selected";
                else $selected ="";
                echo "<option value='".$row2->$key."' ".$selected.">".$row2->$value."</option>";
            }
        }
        else
            echo "<option value='' disabled selected>No Options</option>";

    }
    //older not checked

    public static function shout(string $string)
    {
        return strtoupper($string);
    }
    public static function processImage($originalImage,$path = 'public/images',$thumb_check = TRUE)
    {
    	$image_name  = time(). '_' . uniqid().$originalImage->getClientOriginalExtension();
        $rawImage = Image::make($originalImage);
        $rawImage->save(storage_path($path.'/'.$image_name));

        if($thumb_check){
            // $rawImage->resize(150,150);
            $rawImage->resize(150, null, function ($constraint) {
			    $constraint->aspectRatio();
			});
            $rawImage->save(storage_path($path.'/thumb/'.$image_name)); 
        }
        return  $image_name;
    }
    public static function unlinkImage($path,$image)
    {
    	if($image == "") return true;
    	$full_path = $path.'/'.$image;
    	$thumb_path = $path.'/'.'thumb'.'/'.$image;
    	if (File::exists($full_path)) unlink($full_path);
    	if (File::exists($thumb_path)) unlink($thumb_path);     
    }
  
    public static function getMenuOptions($select = '')
    {
    	$data = Db::table('menus')->where('parent_id',0)->select('menu_id','name')->get();
    	if($data)
    	foreach ($data as $row) {
    		if($row->menu_id == $select) $selected= "selected";
				else $selected ="";
			echo "<option value='".$row->menu_id."' ".$selected.">".$row->name."</option>";
    		$data2 = Db::table('menus')->where('parent_id',$row->menu_id)->select('menu_id','name')->get();
    		foreach ($data2 as $row2) {
    			if($row2->menu_id == $select) $selected= "selected";
				else $selected ="";
				echo "<option value='".$row2->menu_id."' ".$selected.">  &nbsp;&nbsp;--&nbsp;".$row2->name."</option>";
    		}
    	}
    }
    public static function getField($table,$cond,$field)
    {
    	return $data = Db::table($table)->where($cond)->select($field)->pluck($field)->first();
    }
    public static function countRows($tableName,$cond = array())
    {
    	if ($cond)
    		return Db::table($tableName)->where($cond)->count();
    	else
    		return Db::table($tableName)->count();
    }
    public static function cleanSlug($string)
    {
    	$string = strtolower($string);
    	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    ///professional helper list
   

    public static function getEnumOptions( $options, $selected = ''){

        $html = '<option value="">Please select</option>';
        if($options):
            foreach ($options as $option):
                $isSelected = ( $selected == $option) ? 'selected' : '';
                $html .= '<option value="'.$option.'" '.$isSelected.'>'.$option.'</option>';
            endforeach;
        else:
            $html .= "<option value=''>No option available</option>";
        endif;

        echo $html;

    }

}
