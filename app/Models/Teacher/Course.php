<?php

namespace App\Models\Teacher;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    public function teachers() {
        return $this->hasMany(\App\Models\Teacher::class, 'f_id');
    }
}
