<?php

namespace App\Models\Teacher;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseSectionGuideline extends Model
{
    use HasFactory;
    protected $table="course_section_guidelines";
    protected $fillable = [
        'professional_id',
        'course_id',
        'section_id',
        'guideline'
    ];
}
