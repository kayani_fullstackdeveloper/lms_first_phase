<?php

namespace App\Models\Teacher;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance_mark extends Model
{
    use HasFactory;
    protected $fillable = [
        'attendance_id',
        'std_id',
        'attendance'
    ];
}
