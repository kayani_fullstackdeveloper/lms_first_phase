<?php

namespace App\Models\Teacher;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    protected $fillable = [
        'professional_id',
        'course_id',
        'section_id',
        'reference_number',
        'date',
        'time',
        'duration'
    ];
    public function attendanceMark()
    {
        return $this->hasMany('App\Models\Teacher\Attendance_mark','attendance_id');
    }
}
