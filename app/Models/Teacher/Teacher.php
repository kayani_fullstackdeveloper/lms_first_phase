<?php

namespace App\Models\Teacher;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;


class Teacher extends Authenticatable
{
  
    use Notifiable;
    use HasRoles;

        protected $guard = 'teacher';

        protected $fillable = [
            'name', 'email', 'password','user_name','domain_name','profile_live','gender','address','country','phone',
            'youtube','facebook','instagram','twitter','LinkedIn','Pinterest','Reddit','Snapchat','Tik_tok','Quora','Website'
        ];

        protected $hidden = [
            'password', 'remember_token',
        ];
        // profile function 
     function image()
     {
     return $this->hasOne('App\Models\Media','f_id','id')
     ->where('type','profile-image');
     
     }
     function video()
     {
     return $this->hasOne('App\Models\Media','f_id','id')
     ->where('type','profile-video');
     
     }
     function weblink()
     {
     return $this->hasOne('App\Models\Media','f_id','id')
     ->where('type','profile-detail');
     
     }
     function audio()
     {
     return $this->hasOne('App\Models\Media','f_id','id')
     ->where('type','profile-audio');
     
     }
     function mediacheck($type)
     {
     return $this->hasOne('App\Models\Media','f_id','id')
     ->where('type',$type)->count();
     
     }
     public function edu_qualification()
     {
         return $this->hasOne('App\Models\Educational_qualification');
     }
     public function pro_edu()
     {
         return $this->hasOne('App\Models\Professional_qualification');
     }
     //teacher edu list
     public function teacher_edu()
     {
         return $this->hasMany('App\Models\Teacher\Profesional_Edu','professional_id','id')->where('edu_type','edu_qual');
     }
     public function teacher_pro()
     {
         return $this->hasMany('App\Models\Teacher\Profesional_Edu','professional_id','id')->where('edu_type','pro_edu');
     }
     
}
