<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booklibrary extends Model
{
    use HasFactory;
    protected $fillable = [
        'title','writer_name','link','isbn','user_id','user_guard'];
}
