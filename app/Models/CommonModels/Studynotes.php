<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Studynotes extends Model
{
    use HasFactory;
    protected $fillable = [
        'title','description','link','','type','user_id','user_guard'];

}
