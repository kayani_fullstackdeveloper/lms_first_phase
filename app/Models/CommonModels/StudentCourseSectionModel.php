<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentCourseSectionModel extends Model
{
    use HasFactory;



    function course()
     {
        return $this->belongsTo('App\Models\Teacher\Course');
     
     }
     function student()
     {
        return $this->belongsTo('App\Models\Student\Student','std_id','id');
     
     }
     function professional()
     {
        return $this->belongsTo('App\Models\Teacher\Teacher');
     
     }
     function section()
     {
        return $this->belongsTo('App\Models\CommonModels\CoursePublishModel');
     
     }
}
