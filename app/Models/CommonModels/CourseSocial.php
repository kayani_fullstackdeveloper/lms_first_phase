<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseSocial extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id','social_id','professional_id',
    ];
    
}
