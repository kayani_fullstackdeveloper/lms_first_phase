<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLibrary extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id','library_id','professional_id',
    ];
}
