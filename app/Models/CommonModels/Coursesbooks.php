<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coursesbooks extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id','book_id','professional_id',
    ];
}
