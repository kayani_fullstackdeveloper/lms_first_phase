<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoursePodcast extends Model
{
    use HasFactory;
    protected $fillable = [
        'podcast_id','library_id','professional_id',
    ];
}
