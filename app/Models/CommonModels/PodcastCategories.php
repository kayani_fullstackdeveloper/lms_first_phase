<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PodcastCategories extends Model
{
    use HasFactory;
    protected $fillable = [
        'title'
    ];
}
