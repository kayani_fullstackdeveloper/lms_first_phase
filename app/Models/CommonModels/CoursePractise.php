<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoursePractise extends Model
{
    use HasFactory;
    protected $fillable = [
        'practise_id','course_id','professional_id',
    ];

}
