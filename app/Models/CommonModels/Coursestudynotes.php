<?php

namespace App\Models\CommonModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coursestudynotes extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id','studynotes_id','professional_id',
    ];
}
