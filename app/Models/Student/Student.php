<?php

namespace App\Models\Student;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Student extends Authenticatable
{
    use HasFactory;
    use Notifiable;

        protected $guard = 'student';

        protected $fillable = [
            'name', 'email', 'password','user_name','domain_name'
        ];

        protected $hidden = [
            'password', 'remember_token',
        ];
}
