<?php

namespace App\Models\Student;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentMedia extends Model
{
    use HasFactory;
    protected $table="student_media";
    protected $fillable = [
        'name', 'type', 'guard_name','std_id'
    ];
}
