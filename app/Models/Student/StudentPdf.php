<?php

namespace App\Models\Student;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentPdf extends Model
{
    use HasFactory;
    protected $table="student_pdfs";
    protected $fillable = [
        'std_id', 'type', 'details'
    ];
}
