<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvLibraryMeidaFile extends Model
{
    use HasFactory;
    protected $table="av_library_meida_files";
    protected $fillable = [
        'title',
        'category',
        'course_id',
        'reference_number',
        'file',
        'type',
        'guard_name',
        'f_id'
    ];
}
