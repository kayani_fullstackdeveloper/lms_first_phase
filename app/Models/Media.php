<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;
    protected $table="mediafile";
    protected $fillable = [
        'name', 'type', 'guard_name','f_id'
    ];
}
