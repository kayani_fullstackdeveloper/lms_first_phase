<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Auth;
use Illuminate\Support\Arr;
use Closure;
use Session;
class Handler extends ExceptionHandler
{   
   

    protected function unauthenticated($request, AuthenticationException $exception)
    {   
        // dd($request);
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        $guard = Arr::get($exception->guards(), 0);
        switch ($guard) {
            case 'admin':
                $login = '/login/admin';
                break;
            case 'teacher':
                $login = 'teacher.login';
                break;
            case 'student':
                 $login = 'login.student';
                break;

            default:
                $login = 'login';
                break;
        }
        Session::forget('url.intented'); 
        return redirect()->route($login);


       
    }
}