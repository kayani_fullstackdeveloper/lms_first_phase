<?php

return [
	
	 /*
    |--------------------------------------------------------------------------
    | App Constants
    |--------------------------------------------------------------------------
    |List of all constants for the app
    */
    'image' => public_path('storage/images').'/',
    'thumb' => public_path('storage/thumb/'),
    'store_thumb_path' => public_path('storage/thumb/')
];
